# useful sed scripts

* change many parameter settings at once

        sed -i.org \
            -e '/readauxin/s/0/1/' \
            -e '/readauxinpattern/s/my_little/startpoint_for/' \
            -e '/version2/s/1/2\nequalstrengthPERIVASC   0/' \
            -e '/deg_only_ext_lrc/s/0/1\n\nnull_aux1\t\t0\nnull_pin_intensity\t0\nnull_pin_localization\t0\nnull_decay\t\t0/' \
            -e '/ppinmin/s/0.2/0.1/' \
            -e '/bioauxin/s/5/05/' \
            -e '/bioauxinonecell/s/0.0005/0.01/' \
            -e '/fixedmin/s/5/15/' \
            -e '/fixedmax/s/20/5/' \
        root_unifdecay.par

* some more changes

        sed -i.org \
            -e '/readauxin/s/0/1/' \
            -e '/readauxinpattern/s/my_little/startpoint_for/' \
        root_unifdecay.par

* many at once, using tcsh

        foreach i (root*par)
            sed -i.org \
            -e '/readauxin/s/0/1/' \
            -e '/readauxinpattern/s/my_little/startpoint_for/' \
            -e '/version2/s/1/2\nequalstrengthPERIVASC   0/' \
            -e '/deg_only_ext_lrc/s/0/1\n\nnull_aux1\t\t0\nnull_pin_intensity\t0\nnull_pin_localization\t0\nnull_decay\t\t0/' \
            -e '/ppinmin/s/0.2/0.1/' \
            -e '/bioauxin/s/5/05/' \
            -e '/bioauxinonecell/s/0.0005/0.01/' \
            -e '/fixedmin/s/5/15/' \
            -e '/fixedmax/s/20/5/' \
            -e '/dauxin/s/0.000005/0.0000005/' \
            $i
            end

* check each one for correctness and consistency

        foreach i (root*par)
            nedit $i&
            xxdiff $i Ready/root_localdeg.par 
            mv $i Ready
            end


* create a second version of all parameter files, with higher decay

        foreach i (root*par)
        	cp $i $i:r\_hdec.par
            sed -i.org \
            -e '/dauxin/s/0.0000005/0.000001/' \
            $i:r\_hdec.par
            end

* all hdec jobfiles should read the hdec parameter files

        foreach i (job*hdec)
            sed -i.org \
            -e '/par/s/$/\_hdec/' \
            $i
            end

* prevent jobs to spread over multiple hosts

        foreach i (job*)
            sed -i.org \
            -e '/ID/s/ID/ID\n\#BSUB -R \"span\[hosts\=1\]\"     \# dont spread over multiple machines/' \
            -e "/ID/s/dont/don\'t/" \
            $i
            end

* add hdec at the end in create_all_the_images.sh

        sed -i.org -e '/hdec/s/$/_hdec/' create_all_the_images.sh

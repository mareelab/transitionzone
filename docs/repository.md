# Repository

This file contains information regarding the creation and and interaction with the repository itself.

## generation of this repository

### installing cookiecutter

To generate the skeleton of the repository requires cookiecutter. Since it is not yet installed on our machines, we first generated a virtual environment in which we installed cookiecutter. Those steps are only required to be done once ever:

    virtualenv VirtualEnvs/cookiecutter
    source ~/VirtualEnvs/cookiecutter/bin/activate.csh
    rehash
    pip install cookiecutter

### creating first skeleton  

Next we used cookiecutter to create the skeleton:

    source ~/VirtualEnvs/cookiecutter/bin/activate.csh
    rehash
    cookiecutter gh:JIC-CSB/data-analysis-project

when prompted, give the name 
    
    TransitionZone
    
### creating Bitbucket repository

Next, we created a bitbucket repository, and made the first commit, using a script I wrote; a copy can be found in scripts/ :

     bitbucket-create

### others joining repository

To make others work on the repository as well: 

* make sure the owner of the repository logs in to Bitbucket and sends out an invitation. 
* Accept the invitation. 
* Make a copy of the repository:

        git clone git@bitbucket.org:stan_maree/TransitionZone.git
        
### subsequent commits

After that, subsequent commits any team member are made as follows:

* make sure you are working on the latest version:

        git pull
    
* make changes, improvements, and add files
* use the following to know which files have changed, and which new files are not yet part of the git repository:
    
        git status

* add the files that are not yet part of git to commit (staging):

        git add scripts/bitbucket-create

* inversely, remove files that are accidentally  staged (HEAD is current state):

        git reset HEAD README.md~

* now, commit:

        git commit -m "essence of changes in a few words"
        git push origin master

### additional useful git commands
 
* see all submitted verisons at once

        git log --pretty=oneline

* compare the code of two versions

        git diff f18bb162..57b2b9b6 -- root_allTZ_newPINs.c

* add tags to git versions

        git tag -a Current

* print current tag used for version

        git describe

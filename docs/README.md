# Documentation

This directory is intended to contain documentation related to the project, e.g. experimental methodologies, details of analysis scripts etc.

* Information regarding the creation of and interaction with the repository itself can be found [here](./repository.md).
* Information regarding sed scripts to change parameter settings can be found [here](./sedscripts.md).
* An overview of all steps involved from creation of the layout all the way to generation of the figures in the paper, can be found [here](./simulation_steps.md).
# Branching

* see all branches:

        git branch --list

* create new branch:

        git branch BRANCH_NAME

* switch to branch:

        git checkout BRANCH_NAME 

* those two commands can be shortcutted as:

        git checkout -b BRANCH_NAME 

* delete branch:

        git branch -d BRANCH_NAME 

* switching between master and BRANCH_NAME:

        git checkout master
        git checkout BRANCH_NAME

* push the new branch; add -u to allow for tracking remote branch without always having to state what should be pulled, and to mark the start-point branch as "upstream" from the new branch:

        git push -u origin BRANCH_NAME

 this will print
"branch BRANCH_NAME set up to track remote branch BRANCH_NAME from origin."

* the above is equivalent to:

        git push origin BRANCH_NAME
        git branch --set-upstream BRANCH_NAME origin/BRANCH_NAME

* add to config file that the default push is for the currently checked-out branch only, this ends up in ~/.gitconfig

        git config --global push.default simple

 the generated lines in ~/.gitconfig are:

 ````
[push]
	default = simple
````

* tag the for_referees branch:

  * switch to right branch:

            git checkout for_referees

  * tag the latest commit:

            git tag -a v2.0 -m "Version of initial submission"

  * remove an old tag we introduced earlier as a test

            git tag -d Current

  * push it, with the tag

            git push origin --tags

* download the for_referees branch:

  * log in to bitbucket, go to Downloads/Tags
  * download a tarball from there

* realizing we still needed to make some changes, we removed the tag:

  * remove it locally

            git tag -d v2.0

  * also remove it in the repository

            git push origin :refs/tags/v2.0

* good information can be found here:
https://git-scm.com/book/en/v2/Git-Basics-Tagging

# More subtleties, often required when branching:

* regarding pull:

 instead of using pull:

        git pull origin master

 one can do it in a more controlled fashion:

        git fetch --dry-run origin
        git fetch origin
        git diff master origin/master
        git merge origin/master

* regarding rebasing after changes (be careful here, know what you are doing):

        git rebase master for_referees
        git push -f origin for_referees
        git rebase --continue

* Dealing with a push conflict (be careful here, know what you are doing):

    * Alice creates topic branch A, and works on it
    * Bob creates unrelated topic branch B, and works on it
    * Alice does git checkout master && git pull. Master is already up to date.
    * Bob does git checkout master && git pull. Master is already up to date.
    * Alice does git merge topic-branch-A
    * Bob does git merge topic-branch-B
    * Bob does git push origin master before Alice
    * Alice does git push origin master, which is rejected because it's not a fast-forward merge.
    * Alice looks at origin/master's log, and sees that the commit is unrelated to hers.
    * Alice does git pull --rebase origin master
    * Alice's merge commit is unwound, Bob's commit is pulled, and Alice's commit is applied after Bob's commit.
    * Alice does git push origin master, and everyone is happy they don't have to read a useless merge commit when they look at the logs in the future.

* getting other branches after fresh copy:

        git branch -a
        git checkout -b for_referees origin/for_referees
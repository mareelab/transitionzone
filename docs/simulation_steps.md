# This file contains the general steps to get final steady state images and concentration plots

##STEP 1

Compile c codes in ../src 

##STEP 2 – generate layouts and set transporter orientation

    cd ../output
    mkdir -p my_Fig_dir
    cd my_Fig_dir
    ../../build/my_layout_code ../../data/parameterfiles/my_layout.par my_layout

click on the image

    ../../build/my_corners_code -E0 -s3 -g3 -o -T1 -C1 -H1 my_layout.png

press Q

##STEP 3 – run simulations

    ../../build/my_root_code ../../data/parameterfiles/my_root.par my_simulation

##STEP 4 – create images

    cd ..

###step 4a – create raw images

    mkdir -p ../output/Images (only once)
    ../build/showflux_03 ../data/parameterfiles/showflux_03.par ../output/my_Fig_dir/My_simulation ../../Images/my_fig

###step 4b – create final images (rescaled colours, cropping, transparent background)

    mkdir -p ../output/Images/Final/ (only once)

    ../build/pngcolor8 255255255 255255255 ../output/Images/my_fig_auxin.png             ../output/my_Fig_dir/My_simulation/00000.png ../output/Images/Final/my_fig.png

    convert -gravity South -crop 0x4751+0+0 ../output/Images/Final/my_fig.png ../output/Images/Final/my_fig.png

    convert -transparent white ../output/Images/Final/my_fig.png -matte ../output/Images/Final/my_fig.png

###STEP 5 – create concentration plots and save them as .svg

    mkdir -p ../output/Xmgrace (only once)

    ../build/rowcolgraph_01 5 1 2 3 4 8 0 ../output/my_Fig_dir/My_simulation/final_conc.dat

    xmgrace graphs.agr -par ../data/parameterfiles/my_fig.par -hardcopy -saveall graphs.agr -hdevice SVG
    mv graphs.agr ../output/Xmgrace/my_fig.agr
    mv graphs.svg ../output/Images/Final/my_fig.svg

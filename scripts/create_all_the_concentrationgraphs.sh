#!/bin/sh
mkdir -p ../output/Xmgrace
mkdir -p ../output/Images/Final

#Figure 2d
../build/rowcolgraph_01 5 1 2 3 4 8 0 ../output/Fig2c/Root_localdeg/final_conc.dat
sleep 1
xmgrace graphs.agr -par ../data/parameterfiles/fig2d.par -hardcopy -saveall graphs.agr -hdevice SVG
sleep 1
mv graphs.agr ../output/Xmgrace/fig2d.agr
mv graphs.svg ../output/Images/Final/fig2d.svg

#Figure S4c
../build/rowcolgraph_01 0 3 17 18 19 ../output/Fig2c/Root_localdeg/final_conc.dat
sleep 1
xmgrace graphs.agr -par ../data/parameterfiles/figs4c.par -hardcopy -saveall graphs.agr -hdevice SVG
sleep 1
mv graphs.agr ../output/Xmgrace/figs4c.agr
mv graphs.svg ../output/Images/Final/figs4c.svg

# Scripts

This directory is intended to contain all scripts which generate the output and do not require compilation

## creating of repository

* *bitbucket-create* : to create the repository itself, for detailed documentation, look [here](../docs/repository.md).

## compiling of all code

* *compile_all_c_code.sh* : compile all code.

## creating of all the layouts

* *create_all_the_layouts.sh* : create all the layouts; this requires interaction, namely repeatedly clicking (so save the layout) and then pressing 'Q' (so save the polarity information)

* take the end-point of any earlier simulation (final_auxin.dat), call it *startpoint_for.auxin*, and put it in output, in order to speed up calculation time to steady state; not part of repository because of space issues. Same for null model: *startpoint_for_null.auxin*

## running all simulations of the cluster

*  *jobfilefig*\* : to run the simulations on the cluster
    * if you want to change the location of the repository in all jobfile scripts:
    
            foreach i (jobfile*)
            sed -i.org -e '/marees/s/marees\/Micol\/CodesAndPars\/Figures/marees\/Papers\/TransitionZone\/output/' $i
            end

    * To add additional line to jobfile:

            foreach i (jobfile*)
            sed -i.org \
            -e '/ID/s/ID/ID\n\#BSUB -R \"span\[hosts\=1\]\"     \# don'\''t spread over multiple machines/' \
            $i
            end

 

* *submit_simulations_on_cluster* : to submit them all; note that this script has to be run on the cluster, and that on access node to the cluster the right settings are required, most importantly the LD_LIBRARY_PATH has to be correct. 

## producing all final images

* *create_all_the_images.sh* : script to create all the images from the simulations, which are put in the output/Images directory
* *create_all_the_images2.sh* : post-processing, such as cutting out part of figure shown in paper
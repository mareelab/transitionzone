#!/bin/sh
set -x
mkdir -p ../output/Images/Final/

#figure 1
convert -gravity North -crop 0x0+0+2817 ../output/Fig2c/Root_localdeg/00000.png ../output/Images/Final/fig1_layout_root.png
convert -transparent white ../output/Images/Final/fig1_layout_root.png -matte ../output/Images/Final/fig1_layout_root.png

#figure 2a
../build/pngcolor8 255255255 255255255 ../output/Images/fig2a_auxin.png ../output/Fig2a/Root_unifdecay/00000.png ../output/Images/Final/fig2a_unifdecay.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/fig2a_unifdecay.png ../output/Images/Final/fig2a_unifdecay.png
convert -transparent white ../output/Images/Final/fig2a_unifdecay.png -matte ../output/Images/Final/fig2a_unifdecay.png

#figure 2b
../build/pngcolor8 255255255 255255255 ../output/Images/fig2b_auxin.png ../output/Fig2b/Root_unifdeg/00000.png ../output/Images/Final/fig2b_unifdeg.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/fig2b_unifdeg.png ../output/Images/Final/fig2b_unifdeg.png
convert -transparent white ../output/Images/Final/fig2b_unifdeg.png -matte ../output/Images/Final/fig2b_unifdeg.png

#figure 2c
../build/pngcolor8 255255255 255255255 ../output/Images/fig2c_auxin.png ../output/Fig2c/Root_localdeg/00000.png ../output/Images/Final/fig2c_localdeg.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/fig2c_localdeg.png ../output/Images/Final/fig2c_localdeg.png
convert -transparent white ../output/Images/Final/fig2c_localdeg.png -matte ../output/Images/Final/fig2c_localdeg.png

#figure 2e
convert -gravity North -crop 0x399+0+5011 ../output/Images/Final/fig2a_unifdecay.png ../output/Images/Final/fig2e_inset_unifdecay.png

#figure 2f
convert -gravity North -crop 0x399+0+5011 ../output/Images/Final/fig2b_unifdeg.png ../output/Images/Final/fig2f_inset_unifdeg.png

#figure 2g
convert -gravity North -crop 0x399+0+5011 ../output/Images/Final/fig2c_localdeg.png ../output/Images/Final/fig2g_inset_localdeg.png

#figure 2 scalebar
../build/playcolor_07 50 750 6 ../output/Images/Final/fig2_colorbar_015_500.png
../build/playcolor_07 50 125 7 ../output/Images/Final/fig2_colorbar_500_max.png
../build/playcolor_07 50 125 20 ../output/Images/Final/fig2_colorbar_min_015.png

#figure 4b
cp -f ../output/Images/Final/fig2c_localdeg.png ../output/Images/Final/fig4b_localdeg.png

#figure 4d
../build/pngcolor8 255255255 255255255 ../output/Images/fig4d_42_auxin.png ../output/Fig4d/Root_gh3.17mut42/00000.png ../output/Images/Final/fig4d_mutant_42cells.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/fig4d_mutant_42cells.png ../output/Images/Final/fig4d_mutant_42cells.png
convert -transparent white ../output/Images/Final/fig4d_mutant_42cells.png -matte ../output/Images/Final/fig4d_mutant_42cells.png

#figure 4e
cp -f ../output/Images/Final/fig2a_unifdecay.png ../output/Images/Final/fig4e_mutant.png

#figure 4g
../build/pngcolor8 255255255 255255255 ../output/Images/fig4g_20_auxin.png ../output/Fig4g/Root_GH3.17_OE20/00000.png ../output/Images/Final/fig4g_OE_20cells.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/fig4g_OE_20cells.png ../output/Images/Final/fig4g_OE_20cells.png
convert -transparent white ../output/Images/Final/fig4g_OE_20cells.png -matte ../output/Images/Final/fig4g_OE_20cells.png

#figure 4h
cp -f ../output/Images/Final/fig2b_unifdeg.png ../output/Images/Final/fig4h_OE.png

#figure 7a
cp -f ../output/Images/Final/fig2c_localdeg.png ../output/Images/Final/fig7a_localdeg.png

#figure 7b
../build/pngcolor8 255255255 255255255 ../output/Images/fig7b_auxin.png ../output/Fig7b/Root_lessCK_30cells/00000.png ../output/Images/Final/fig7b_lessCK.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/fig7b_lessCK.png ../output/Images/Final/fig7b_lessCK.png
convert -transparent white ../output/Images/Final/fig7b_lessCK.png -matte ../output/Images/Final/fig7b_lessCK.png

#figure 7c
../build/pngcolor8 255255255 255255255 ../output/Images/fig7c_auxin.png ../output/Fig7c/Root_lessCK_45cells/00000.png ../output/Images/Final/fig7c_lessCK_45cells.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/fig7c_lessCK_45cells.png ../output/Images/Final/fig7c_lessCK_45cells.png
convert -transparent white ../output/Images/Final/fig7c_lessCK_45cells.png -matte ../output/Images/Final/fig7c_lessCK_45cells.png

#figure 7d
../build/pngcolor8 255255255 255255255 ../output/Images/fig7d_auxin.png ../output/Fig7d/Root_moreCK_30cells/00000.png ../output/Images/Final/fig7d_moreCK.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/fig7d_moreCK.png ../output/Images/Final/fig7d_moreCK.png
convert -transparent white ../output/Images/Final/fig7d_moreCK.png -matte ../output/Images/Final/fig7d_moreCK.png

#figure 7e
../build/pngcolor8 255255255 255255255 ../output/Images/fig7e_18_auxin.png ../output/Fig7e/Root_moreCK_18cells/00000.png ../output/Images/Final/fig7e_moreCK_18cells.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/fig7e_moreCK_18cells.png ../output/Images/Final/fig7e_moreCK_18cells.png
convert -transparent white ../output/Images/Final/fig7e_moreCK_18cells.png -matte ../output/Images/Final/fig7e_moreCK_18cells.png

#figure S1c
../build/thickenborder_02 ../output/Fig2c/Root_localdeg/00000.png ../output/Fig2c/layout_final_30_corners.png 5 ../output/Images/figs1c_polarity.png ../output/Fig2c/layout_final_30_regions.png 5906
convert -gravity NorthWest -crop 460x0+0+6880 ../output/Images/figs1c_polarity.png ../output/Images/Final/figs1c_polarity_detail.png

#figure S2c
../build/pngcolor8 255255255 255255255 ../output/Images/figs2c_auxin.png ../output/SFig2c/Root_unifdecay_full_pin_intensity_localization/00000.png ../output/Images/Final/figs2c_unifdecay_full_pin_intensity_localization.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs2c_unifdecay_full_pin_intensity_localization.png ../output/Images/Final/figs2c_unifdecay_full_pin_intensity_localization.png
convert -transparent white ../output/Images/Final/figs2c_unifdecay_full_pin_intensity_localization.png -matte ../output/Images/Final/figs2c_unifdecay_full_pin_intensity_localization.png

#figure S2d
../build/pngcolor8 255255255 255255255 ../output/Images/figs2d_auxin.png ../output/SFig2d/Root_unifdecay_null_pin_intensity_localization_aux1_cortex/00000.png ../output/Images/Final/figs2d_unifdecay_null_pin_intensity_localization_aux1_cortex.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs2d_unifdecay_null_pin_intensity_localization_aux1_cortex.png ../output/Images/Final/figs2d_unifdecay_null_pin_intensity_localization_aux1_cortex.png
convert -transparent white ../output/Images/Final/figs2d_unifdecay_null_pin_intensity_localization_aux1_cortex.png -matte ../output/Images/Final/figs2d_unifdecay_null_pin_intensity_localization_aux1_cortex.png

#figure S4b
../build/pngcolor8 255255255 254254254 ../output/Fig2c/Root_localdeg/00001.png ../output/Fig2c/Root_localdeg/00000.png ../output/Images/Final/figs4b_location_additional_breakdown.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs4b_location_additional_breakdown.png ../output/Images/Final/figs4b_location_additional_breakdown.png
convert -transparent "rgb(254,254,254)" ../output/Images/Final/figs4b_location_additional_breakdown.png -matte ../output/Images/Final/figs4b_location_additional_breakdown.png

#figure S5a
convert -gravity North -crop 0x0+0+2793 ../output/SFig5Null/Root_localdeg_null/00000.png ../output/Images/Final/figs5a_layout_null_root.png
convert -transparent white ../output/Images/Final/figs5a_layout_null_root.png -matte ../output/Images/Final/figs5a_layout_null_root.png

#figure S5b
convert -gravity North -crop 0x0+0+2817 ../output/SFig5Null/Root_localdeg_null_size/00000.png ../output/Images/Final/figs5b_layout_null_size_root.png
convert -transparent white ../output/Images/Final/figs5b_layout_null_size_root.png -matte ../output/Images/Final/figs5b_layout_null_size_root.png

#figure S5c
convert -gravity North -crop 0x0+0+2793 ../output/SFig5Null/Root_localdeg_null_lrc/00000.png ../output/Images/Final/figs5c_layout_null_lrc_root.png
convert -transparent white ../output/Images/Final/figs5c_layout_null_lrc_root.png -matte ../output/Images/Final/figs5c_layout_null_lrc_root.png

#Figure S5d-k
../build/pngcolor8 255255255 255255255 ../output/Images/figs5null_null_auxin.png ../output/SFig5Null/Root_localdeg_null/00000.png ../output/Images/Final/figs5d_null_null_auxin.png
convert -gravity North -crop 0x0+0+2793 ../output/Images/Final/figs5d_null_null_auxin.png ../output/Images/Final/figs5d_null_null_auxin.png
convert -transparent white ../output/Images/Final/figs5d_null_null_auxin.png -matte ../output/Images/Final/figs5d_null_null_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5null_size_auxin.png ../output/SFig5Null/Root_localdeg_null_size/00000.png ../output/Images/Final/figs5e_null_size_auxin.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs5e_null_size_auxin.png ../output/Images/Final/figs5e_null_size_auxin.png
convert -transparent white ../output/Images/Final/figs5e_null_size_auxin.png -matte ../output/Images/Final/figs5e_null_size_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5null_lrc_auxin.png ../output/SFig5Null/Root_localdeg_null_lrc/00000.png ../output/Images/Final/figs5f_null_lrc_auxin.png
convert -gravity North -crop 0x0+0+2793 ../output/Images/Final/figs5f_null_lrc_auxin.png ../output/Images/Final/figs5f_null_lrc_auxin.png
convert -transparent white ../output/Images/Final/figs5f_null_lrc_auxin.png -matte ../output/Images/Final/figs5f_null_lrc_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5null_cortex_auxin.png ../output/SFig5Null/Root_localdeg_null_cortex/00000.png ../output/Images/Final/figs5g_null_cortex_auxin.png
convert -gravity North -crop 0x0+0+2793 ../output/Images/Final/figs5g_null_cortex_auxin.png ../output/Images/Final/figs5g_null_cortex_auxin.png
convert -transparent white ../output/Images/Final/figs5g_null_cortex_auxin.png -matte ../output/Images/Final/figs5g_null_cortex_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5null_pin_localization_auxin.png ../output/SFig5Null/Root_localdeg_null_pin_localization/00000.png ../output/Images/Final/figs5h_null_pin_localization_auxin.png
convert -gravity North -crop 0x0+0+2793 ../output/Images/Final/figs5h_null_pin_localization_auxin.png ../output/Images/Final/figs5h_null_pin_localization_auxin.png
convert -transparent white ../output/Images/Final/figs5h_null_pin_localization_auxin.png -matte ../output/Images/Final/figs5h_null_pin_localization_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5null_pin_intensity_auxin.png ../output/SFig5Null/Root_localdeg_null_pin_intensity/00000.png ../output/Images/Final/figs5i_null_pin_intensity_auxin.png
convert -gravity North -crop 0x0+0+2793 ../output/Images/Final/figs5i_null_pin_intensity_auxin.png ../output/Images/Final/figs5i_null_pin_intensity_auxin.png
convert -transparent white ../output/Images/Final/figs5i_null_pin_intensity_auxin.png -matte ../output/Images/Final/figs5i_null_pin_intensity_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5null_aux1_auxin.png ../output/SFig5Null/Root_localdeg_null_aux1/00000.png ../output/Images/Final/figs5j_null_aux1_auxin.png
convert -gravity North -crop 0x0+0+2793 ../output/Images/Final/figs5j_null_aux1_auxin.png ../output/Images/Final/figs5j_null_aux1_auxin.png
convert -transparent white ../output/Images/Final/figs5j_null_aux1_auxin.png -matte ../output/Images/Final/figs5j_null_aux1_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5null_decay_auxin.png ../output/SFig5Null/Root_localdeg_null_decay/00000.png ../output/Images/Final/figs5k_null_decay_auxin.png
convert -gravity North -crop 0x0+0+2793 ../output/Images/Final/figs5k_null_decay_auxin.png ../output/Images/Final/figs5k_null_decay_auxin.png
convert -transparent white ../output/Images/Final/figs5k_null_decay_auxin.png -matte ../output/Images/Final/figs5k_null_decay_auxin.png

#figure S5l
cp -f ../output/Images/Final/fig1_layout_root.png ../output/Images/Final/figs5l_layout_full_root.png

#figure S5m
convert -gravity North -crop 0x0+0+2793 ../output/SFig5Full/Root_localdeg_full_size/00000.png ../output/Images/Final/figs5m_layout_full_size_root.png
convert -transparent white ../output/Images/Final/figs5m_layout_full_size_root.png -matte ../output/Images/Final/figs5m_layout_full_size_root.png

#figure S5n
convert -gravity North -crop 0x0+0+2817 ../output/SFig5Full/Root_localdeg_full_lrc/00000.png ../output/Images/Final/figs5n_layout_full_lrc_root.png
convert -transparent white ../output/Images/Final/figs5n_layout_full_lrc_root.png -matte ../output/Images/Final/figs5n_layout_full_lrc_root.png

#figure S5o
cp -f ../output/Images/Final/fig2c_localdeg.png ../output/Images/Final/figs5o_full_auxin.png

#figure S5p-v
../build/pngcolor8 255255255 255255255 ../output/Images/figs5full_size_auxin.png ../output/SFig5Full/Root_localdeg_full_size/00000.png ../output/Images/Final/figs5p_full_size_auxin.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs5p_full_size_auxin.png ../output/Images/Final/figs5p_full_size_auxin.png
convert -transparent white ../output/Images/Final/figs5p_full_size_auxin.png -matte ../output/Images/Final/figs5p_full_size_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5full_lrc_auxin.png ../output/SFig5Full/Root_localdeg_full_lrc/00000.png ../output/Images/Final/figs5q_full_lrc_auxin.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs5q_full_lrc_auxin.png ../output/Images/Final/figs5q_full_lrc_auxin.png
convert -transparent white ../output/Images/Final/figs5q_full_lrc_auxin.png -matte ../output/Images/Final/figs5q_full_lrc_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5full_cortex_auxin.png ../output/SFig5Full/Root_localdeg_full_cortex/00000.png ../output/Images/Final/figs5r_full_cortex_auxin.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs5r_full_cortex_auxin.png ../output/Images/Final/figs5r_full_cortex_auxin.png
convert -transparent white ../output/Images/Final/figs5r_full_cortex_auxin.png -matte ../output/Images/Final/figs5r_full_cortex_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5full_pin_localization_auxin.png ../output/SFig5Full/Root_localdeg_full_pin_localization/00000.png ../output/Images/Final/figs5s_full_pin_localization_auxin.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs5s_full_pin_localization_auxin.png ../output/Images/Final/figs5s_full_pin_localization_auxin.png
convert -transparent white ../output/Images/Final/figs5s_full_pin_localization_auxin.png -matte ../output/Images/Final/figs5s_full_pin_localization_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5full_pin_intensity_auxin.png ../output/SFig5Full/Root_localdeg_full_pin_intensity/00000.png ../output/Images/Final/figs5t_full_pin_intensity_auxin.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs5t_full_pin_intensity_auxin.png ../output/Images/Final/figs5t_full_pin_intensity_auxin.png
convert -transparent white ../output/Images/Final/figs5t_full_pin_intensity_auxin.png -matte ../output/Images/Final/figs5t_full_pin_intensity_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5full_aux1_auxin.png ../output/SFig5Full/Root_localdeg_full_aux1/00000.png ../output/Images/Final/figs5u_full_aux1_auxin.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs5u_full_aux1_auxin.png ../output/Images/Final/figs5u_full_aux1_auxin.png
convert -transparent white ../output/Images/Final/figs5u_full_aux1_auxin.png -matte ../output/Images/Final/figs5u_full_aux1_auxin.png

../build/pngcolor8 255255255 255255255 ../output/Images/figs5full_decay_auxin.png ../output/SFig5Full/Root_localdeg_full_decay/00000.png ../output/Images/Final/figs5v_full_decay_auxin.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs5v_full_decay_auxin.png ../output/Images/Final/figs5v_full_decay_auxin.png
convert -transparent white ../output/Images/Final/figs5v_full_decay_auxin.png -matte ../output/Images/Final/figs5v_full_decay_auxin.png

#figure S7a
../build/pngcolor8 255255255 255255255 ../output/Images/figs7a_auxin.png ../output/SFig7a/Root_shy2mutant_30cells/00000.png ../output/Images/Final/figs7a_shy2mutant.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs7a_shy2mutant.png ../output/Images/Final/figs7a_shy2mutant.png
convert -transparent white ../output/Images/Final/figs7a_shy2mutant.png -matte ../output/Images/Final/figs7a_shy2mutant.png

#figure S7b
../build/pngcolor8 255255255 255255255 ../output/Images/figs7b_36_auxin.png ../output/SFig7b/Root_shy2mutant_36cells/00000.png ../output/Images/Final/figs7b_shy2mutant_36cells.png
convert -gravity North -crop 0x0+0+2817 ../output/Images/Final/figs7b_shy2mutant_36cells.png ../output/Images/Final/figs7b_shy2mutant_36cells.png
convert -transparent white ../output/Images/Final/figs7b_shy2mutant_36cells.png -matte ../output/Images/Final/figs7b_shy2mutant_36cells.png

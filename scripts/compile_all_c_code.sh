cd ../src
cc -fopenmp -O3 -Wall -o corners_31_TZ_EDZ corners_31_TZ_EDZ.c -lexcal-2 -lpng -lgrace_np -lz -lm -lglut -lGLU -lGL -pthread
cc -fopenmp -O3 -Wall -o layoutofrootplusTZ_last layoutofrootplusTZ_last.c -lexcal-2 -lpng -lgrace_np -lz -lm -lglut -lGLU -lGL -pthread
cc -fopenmp -O3 -Wall -o layoutofrootplusTZ_mostLRC layoutofrootplusTZ_mostLRC.c -lexcal-2 -lpng -lgrace_np -lz -lm -lglut -lGLU -lGL -pthread
cc -fopenmp -O3 -Wall -o layoutofrootplusTZ_mostLRCsize layoutofrootplusTZ_mostLRCsize.c -lexcal-2 -lpng -lgrace_np -lz -lm -lglut -lGLU -lGL -pthread
cc -fopenmp -O3 -Wall -o root_allTZ_newPINs root_allTZ_newPINs.c -lexcal-2 -lpng -lgrace_np -lz -lm -lglut -lGLU -lGL -pthread
cc -fopenmp -O3 -Wall -o showflux_03 showflux_03.c -lexcal-2 -lpng -lgrace_np -lz -lm -lglut -lGLU -lGL -pthread
cc -fopenmp -O3 -Wall -o pngcolor8 pngcolor8.c -lexcal-2 -lpng -lgrace_np -lz -lm -lglut -lGLU -lGL -pthread
cc -fopenmp -O3 -Wall -o playcolor_07 playcolor_07.c -lexcal-2 -lpng -lgrace_np -lz -lm -lglut -lGLU -lGL -pthread
cc -fopenmp -O3 -Wall -o rowcolgraph_01 rowcolgraph_01.c -lexcal-2 -lpng -lgrace_np -lz -lm -lglut -lGLU -lGL -pthread
cc -fopenmp -O3 -Wall -o thickenborder_02 thickenborder_02.c -lexcal-2 -lpng -lgrace_np -lz -lm -lglut -lGLU -lGL -pthread
mv -f corners_31_TZ_EDZ layoutofrootplusTZ_last layoutofrootplusTZ_mostLRC layoutofrootplusTZ_mostLRCsize root_allTZ_newPINs showflux_03 pngcolor8 playcolor_07 rowcolgraph_01 thickenborder_02 ../build

#!/bin/sh
cd ../output
mkdir -p Fig2a
cd Fig2a
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_WT.par layout_final_30
#(click with mouse in window)
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_final_30_state.png
#(press 'Q' in window)
mkdir -p ../Fig2b
cd ../Fig2b
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_WT.par layout_final_30
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_final_30_state.png
mkdir -p ../Fig2c
cd ../Fig2c
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_WT.par layout_final_30
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_final_30_state.png
mkdir -p ../Fig4d
cd ../Fig4d
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_gh3.1742mut.par mutant_42cells
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 mutant_42cells_state.png
mkdir -p ../Fig4g
cd ../Fig4g
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_GH3.17_OE20.par OE_20cells
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 OE_20cells_state.png
mkdir -p ../Fig7b
cd ../Fig7b
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_WT.par layout_final_30
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_final_30_state.png
mkdir -p ../Fig7c
cd ../Fig7c
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_lessCK.par mutants_45cells
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 mutants_45cells_state.png
mkdir -p ../Fig7d
cd ../Fig7d
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_WT.par layout_final_30
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_final_30_state.png
mkdir -p ../Fig7e
cd ../Fig7e
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_moreCK18.par OE_18cells
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 OE_18cells_state.png
mkdir -p ../SFig2c
cd ../SFig2c
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_WT.par layout_final_30
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_final_30_state.png
mkdir -p ../SFig2d
cd ../SFig2d
../../build/layoutofrootplusTZ_mostLRC ../../data/parameterfiles/longitudinallayout_new_null_model.par layout_null
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_null_state.png
mkdir -p ../SFig5Null
cd ../SFig5Null
../../build/layoutofrootplusTZ_mostLRC ../../data/parameterfiles/longitudinallayout_new_null_model.par layout_null
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_null_state.png
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_new_null_model.par layout_null_lrc
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_null_lrc_state.png
../../build/layoutofrootplusTZ_mostLRCsize ../../data/parameterfiles/longitudinallayout_WT.par layout_null_size
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_null_size_state.png
mkdir -p ../SFig5Full
cd ../SFig5Full
../../build/layoutofrootplusTZ_mostLRCsize ../../data/parameterfiles/longitudinallayout_WT.par layout_null_size
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_null_size_state.png
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_new_null_model.par layout_null_lrc
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_null_lrc_state.png
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_WT.par layout_final_30
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_final_30_state.png
mkdir -p ../SFig7a
cd ../SFig7a
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_WT.par layout_final_30
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 layout_final_30_state.png
mkdir -p ../SFig7b
cd ../SFig7b
../../build/layoutofrootplusTZ_last ../../data/parameterfiles/longitudinallayout_gh3.17mut36.par mutant_36cells
../../build/corners_31_TZ_EDZ -E0 -s3 -g3 -o -T1 -C1 -H1 mutant_36cells_state.png
cd ../../scripts

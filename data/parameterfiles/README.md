# Parameter files

This directory is intended to contain all parameter files used in simulations

* *longitudinallayout_*\**.par* : parameter files used for the layout of the root
* *root_*\**.par* : parameter files used for the actual simulations
* *showflux_03.par* : parameter file to generate log concentration and flux picture
* *fig*\**.par* : parameter files to generate the xmgrace plots 
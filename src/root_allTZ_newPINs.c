#include <excal2.h>
#include <limits.h>

#define MEDIUM          0
#define CELLWALL        1

#define VASCULAR        2
#define PERICYCLE       3
#define ENDODERMIS      4
#define CORTEX          5
#define EPIDERMIS       6
//lateral root cap
#define LRC             7
#define COLUMELLAROOT   8
//initials
#define CORTEXENDO_I    9
#define LRCEPI_I        10
#define COL_I           11
#define QCROOT          12
//differentiation zone
//specialvascular should be last
#define SPECIALVASCULAR 13
#define VASCULAR_D      14
#define PERICYCLE_D     15
#define ENDODERMIS_D    16
#define CORTEX_D        17
#define EPIDERMIS_D     18
//transition zone - is it ok to set TZ tissues after DZ ones?
#define VASCULAR_TZ     19
#define PERICYCLE_TZ    20
#define ENDODERMIS_TZ   21
#define CORTEX_TZ       22
#define EPIDERMIS_TZ    23
#define LRC_TZ          24
//elongation zone
#define VASCULAR_EZ     25
#define PERICYCLE_EZ    26
#define ENDODERMIS_EZ   27
#define CORTEX_EZ       28
#define EPIDERMIS_EZ    29
#define DEADCELLS       30
#define FAKECELLWALL    31
#define FAKECELLS       32
#define TOTALROOTTYPES  33 //it shouldnt go over 40...

static char *roottypes[TOTALROOTTYPES] = {
  "MEDIUM",
  "CELLWALL",
  "VASCULAR",
  "PERICYCLE",
  "ENDODERMIS",
  "CORTEX",
  "EPIDERMIS",
  "LRC",
  "COLUMELLAROOT",
  "CORTEXENDO_I",
  "LRCEPI_I",
  "COL_I",
  "QCROOT",
  "SPECIALVASCULAR",
  "VASCULAR_D",
  "PERICYCLE_D",
  "ENDODERMIS_D",
  "CORTEX_D",
  "EPIDERMIS_D",
  "VASCULAR_TZ",
  "PERICYCLE_TZ",
  "ENDODERMIS_TZ",
  "CORTEX_TZ",
  "EPIDERMIS_TZ",
  "LRC_TZ",
  "VASCULAR_EZ",
  "PERICYCLE_EZ",
  "ENDODERMIS_EZ",
  "CORTEX_EZ",
  "EPIDERMIS_EZ",
  "DEADCELLS",
  "FAKECELLWALL",
  "FAKECELLS"
};

#define VASCULARROOT    2
#define VASCULARSHOOT   3
#define ENDODERMISROOT  4
#define ENDODERMISSHOOT 5
#define CORTEXROOT      6
#define CORTEXSHOOT     7
#define EPIDERMISROOT   8
#define EPIDERMISSHOOT  9
#define QCEMBRYO       10
#define PRIMORDIUM     11
#define COLUMELLAEMBRYO 12
#define CENTRALCELL    13
#define TOTALEMBRYOTYPES 14

#define UPPER          1
#define INNER          2
#define LOWER          3
#define OUTER          4
#define UO             5
#define UI             6
#define LI             7
#define LO             8

static char *orientation[5] = {
  "(null)",
  "UPPER",
  "INNER",
  "LOWER",
  "OUTER"
};

/*#define AUX            0  //default
  #define AUXA           1  //specialvascular
  #define AUXB           2  //dynamic changing
  #define PIN            3  //default 
  #define PINA           4  //default low
  #define PINB           5  //specialvascular
  #define PINC           6  //dynamic changing basal
  #define PIND           7  //dynamics changing all sides 
  #define PINE	       8  //downward PIN which is lower	       
  #define PINF	       9  //upward PIN which is lower
  #define PING	      10  //PINA which is lower
  #define TOTALPINS     11
*/

#define AUX             0  
#define AUX1            1
#define AUXB            2
#define AUX1half        3

#define PIN             1

#define PIN1max         4  //this is 2
#define PIN1half        5
#define PIN1third       6
#define PIN1quarter     7
#define PIN1fifth       8
#define PIN1sixth       9
#define PIN1min         10
#define PIN3max         11
#define PIN3half        12
#define PIN3third       13
#define PIN3quarter     14
#define PIN3fifth       15
#define PIN3sixth       16
#define PIN3min         17
#define PIN7max         18
#define PIN7half        19
#define PIN7third       20
#define PIN7quarter     21
#define PIN7fifth       22
#define PIN7sixth       23
#define PIN7min         24
#define PIN2max         25
#define PIN2half        26
#define PIN2third       27
#define PIN2quarter     28
#define PIN2fifth       29
#define PIN2sixth       30
#define PIN2min         31 //this is 1
#define PINdyn_pol      32  //dynamic changing basal
#define PINDYN137       33  //dynamics changing all sides
#define PINDYN2         34  //dynamics changing all sides
#define TOTALPINS       35

#define LOGAUXIN       0
#define SCALEDAUXIN    1
#define ABSAUXIN       2
#define LOGFLUX        3

#define SPECIALCELLS   100
#define MAXCELLS      3000

#define DZ               0
#define EZ               1
#define TZ               2
#define MZ               3  
#define TOTALZONES       4

#define DEBUG            0

typedef struct extras {
  FLOAT correction[9];
  FLOAT auxinbefore;
  FLOAT auxinafter;
  FLOAT auxincorrection;
  FLOAT auxinconcentration;
  FLOAT meanauxbauxinconcentration;
  FLOAT meanpinauxinconcentration;
  FLOAT dynamicpermeability[TOTALPINS];
} Extras;

#define EXTRAS ((Extras*)(cells.extras))

typedef struct external {
  int* cellnumber;
  int numberofcells;
} External;

static int tijd;
static FLOAT realtime;
static Cell cells,oldcells; 

//planes
static TYPE **state,**corner,**gh;
static TYPE ***pinlocalization;

static TYPE **pin[TOTALPINS];
static FLOAT permeability[TOTALPINS];
//static int pinoraux[TOTALPINS]={AUX,AUX,AUX,PIN,PIN,PIN,PIN,PIN,PIN,PIN,PIN}; // why do I set all equal PINs?
//static int pinoraux[TOTALPINS]={AUX,AUX,AUX,PINmax,PINhalf,PINthird,PINquarter,PINfifth,PINsixth,PINmin}; //now I have less PINs
static int pinoraux[TOTALPINS]={AUX,AUX,AUX,AUX,
                                PIN,PIN,PIN,PIN,PIN,
                                PIN,PIN,PIN,PIN,PIN,
                                PIN,PIN,PIN,PIN,PIN,
                                PIN,PIN,PIN,PIN,PIN,
                                PIN,PIN,PIN,PIN,PIN,
                                PIN,PIN,PIN,PIN,PIN,
                                PIN};

static double pinstrength[TOTALROOTTYPES][9];
static double auxstrength[TOTALROOTTYPES][9];

static TYPE **white,**black;
static TYPE **north,**south,**west,**east;
static FLOAT **auxin,**logauxin,**logauxin2;
static FLOAT **h,**s,**v;
static FLOAT **fluxx,**fluxy,**fluxnorm;
static FLOAT **diffusionmatrix[9]; //to be sure, in case of renumbering in future
static FLOAT **influx,**breakdown;
static FLOAT **displayauxin[4];
static TYPE **forwardstate,**backwardstate,**forwardcells;
static FLOAT **forwardauxin;

#define LENGTHAUXINRECORD 5000
static FLOAT totalauxinrecord[LENGTHAUXINRECORD];

//general
static char dirname[STRING];
static char winfirst[STRING]="The Root";
static char winsecond[STRING]="Logarithmic";

//flux
static FLOAT *fluxdown;
static FLOAT *fluxup;
static FLOAT *netflux;
static FLOAT *netfluxcelly;
static FLOAT *netfluxcellx;
static FLOAT *boundaryflux;

//output
static int equilibriuminterval,auxindatainterval,equilibriumstart,equilibriumstop;
static int savegraphics,showinterval,saveinterval,includeequilibrium;
static int savegrace,graceinterval,savegraceinterval;
static int flux,saveflux,gracefinterval,savegracefinterval;
static int *cellrowtofirstpixel=NULL;
static int *cellrowtocenterpixel=NULL;
static int *cellrowtolastpixel=NULL;
static int *cellcoltofirstpixel=NULL;
static int *cellcoltocenterpixel=NULL;
static int *cellcoltolastpixel=NULL;
static int totalcellrownumber;
static int totalcellcolnumber;
static int oldnrow,oldncol;
static int resetdiffusionmatrix=0;

//parameters
static FLOAT Dauxin,dauxin,dauxinTZ,bioauxin,bioauxinonecell,liquid,boundaryconcentration;
static FLOAT leakage;
static int biosyntonecell;
static int whattoshow=0;
static int alsoshowing=0;
//static int upper;
static int left,top;
static int lower,right;
static int scanroot=1;
static int totaltypes;
static FLOAT halfmaxpindyn;
static double powpindyn;
static FLOAT fixedmin,fixedmax;
static FLOAT upperboundary,lowerboundary;
static int upmutant,downmutant,lateralmutant;
static int PINunifstrength,PINunifpolandstrength,PINMZTZ,version2,equalstrengthPERIVASC;
static FLOAT reducedincellwall;
static int run=1;
static int timetoexit=0;
static int noreallyhighvalues;
static FLOAT forcedmax;

//static Png png1,png2,png3,png4,png5,png6,png7,windowpng;
static Png png1,png2,png3;

static char readpattern[STRING],readauxinpattern[STRING];
static int readauxin;
static FLOAT initialdose;
static int bendingroot=0,bendingauxin=0,pindilution=0;
static FLOAT fixedfinalauxinvalue=0.;
static int aux_only_cor_epi_lrc,aux_only_vasc_peri,no_aux_at_all,no_pin2_flip,deg_only_ext_lrc;
static int null_aux1,null_pin_intensity,null_pin_localization,null_decay;

extern int mouse_i,mouse_j;
extern int nrow,ncol,scale,graphics;
extern int boundary,specialcelltype;
extern FLOAT spacestep,timestep;
extern int sigma, sigmaneigh;
extern int celltype,celltypeneigh;
extern int iposition,jposition,ineigh,jneigh,randomneigh;
extern int iextraposition,jextraposition,iextraneigh,jextraneigh,celltypeextra,celltypeneighextra;
extern int sigmaextra,sigmaneighextra,dh;
extern int (*newindex)(int,int);
extern double temperature,lambda,dissipation;
extern double viewpoint;
extern int editor;
extern int blendfunction;
extern int nlay;
extern int grace;
extern int gradlog;
extern void (*mouse)(int,int,int);
extern void (*keyboard)(int,int,int);

// this is to change the spread
/*
  if(pincregulation && c>=SPECIALCELLS)

  EXTRAS[c].dynamicpermeability[PINC]=pow(EXTRAS[c].meanpinauxinconcentration,powpin)/(pow(halfmaxpin,powpin)+pow(EXTRAS[c].meanpinauxinconcentration,powpin));
  else
  EXTRAS[c].dynamicpermeability[PINC]=1.;
 
  if(pindregulation && c>=SPECIALCELLS)

  EXTRAS[c].dynamicpermeability[PIND]=pow(halfmaxpin,powpin)/(pow(halfmaxpin,powpin)+pow(EXTRAS[c].meanpinauxinconcentration,powpin));
  else
  EXTRAS[c].dynamicpermeability[PIND]=0.;
*/

void AllocateFlux()
{
  if(((fluxdown=(FLOAT *)calloc((size_t)(nrow+2),sizeof(FLOAT)))==NULL) ||
     ((fluxup=(FLOAT *)calloc((size_t)(nrow+2),sizeof(FLOAT)))==NULL) ||
     ((netflux=(FLOAT *)calloc((size_t)(nrow+2),sizeof(FLOAT)))==NULL)||
     ((netfluxcelly=(FLOAT *)calloc((size_t)(MAXCELLS),sizeof(FLOAT)))==NULL)||
     ((netfluxcellx=(FLOAT *)calloc((size_t)(MAXCELLS),sizeof(FLOAT)))==NULL)||
     ((boundaryflux=(FLOAT *)calloc((size_t)(ncol+2),sizeof(FLOAT)))==NULL)) {
    fprintf(stderr,"error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
}

void UpdateFlux()
{
  int buur;
  static int oldtime=-1;
  int k;

  if(oldtime==tijd)
    return;

  for(k=0;k<=nrow+1;k++) {
    fluxdown[k]=0;
    fluxup[k]=0;
    netflux[k]=0;
  }
  oldtime=tijd;
  
  FFill(fluxy,0.);
  FFill(fluxx,0.);
  CPLANE(state,
         //buur=(bendingroot?-1:(j<=ncol/2?-1:1));
	 //p = diffusionmatrix/spacestep; J=p*auxin*spacestep=diffusionmatrix*auxin
         buur=(j<=ncol/2?-1:1);
         fluxy[i][j]=diffusionmatrix[NORTH][i-1][j]*auxin[i-1][j]-diffusionmatrix[SOUTH][i][j]*auxin[i][j];
         netflux[i]+=fluxy[i][j];
         if(fluxy[i][j]<0)
           fluxdown[i]-=fluxy[i][j];
         else
           fluxup[i]+=fluxy[i][j];
         if(buur==-1)
           fluxx[i][j]=diffusionmatrix[EAST][i][j-1]*auxin[i][j-1]-diffusionmatrix[WEST][i][j]*auxin[i][j];
         else
           fluxx[i][j]=diffusionmatrix[EAST][i][j]*auxin[i][j]-diffusionmatrix[WEST][i][j+1]*auxin[i][j+1];
         );
}

void UpdateDiffusionMatrix()
{
  int k;

  Boundaries(state);
  PLANE(
        NEIGHBOURS(
		   if(neigh==WEST || neigh==EAST || neigh==NORTH || neigh==SOUTH)
		     {
		       diffusionmatrix[neigh][i][j]=0.;

		       if(state[i][j]) 
			 {
			   if(state[i][j]==state[i+y][j+x]) 
			     {
			       if(state[i][j]==CELLWALL)
				 diffusionmatrix[neigh][i][j]=reducedincellwall*Dauxin; // if my neighbour is the cellwall, Dauxin has a lower value
			       else
				 diffusionmatrix[neigh][i][j]=Dauxin;// meaning that the two neighbouring pixels belong to the same cell (not medium, not cellwall) 
			     }

			   else if(state[i+y][j+x]) //?if (state[i][j]!=state[i+y][j+x])?
                        
			     {
			       for(k=0;k<TOTALPINS;k++)
				 {
				   //   if((pinoraux[k]==PINmax ||  pinoraux[k]==PINhalf  || pinoraux[k]==PINthird || pinoraux[k]==PINquarter || pinoraux[k]==PINfifth || pinoraux[k]==PINsixth || pinoraux[k]==PINmin) && pin[k][i][j]) 
				   if(pinoraux[k]==PIN && pin[k][i][j]) 
				     {
				       if(bendingroot && pindilution)
					 diffusionmatrix[neigh][i][j]+=permeability[k]*EXTRAS[state[i][j]].dynamicpermeability[k]*spacestep*EXTRAS[state[i][j]].correction[corner[i][j]];
				       else
					 diffusionmatrix[neigh][i][j]+=permeability[k]*EXTRAS[state[i][j]].dynamicpermeability[k]*spacestep;
				     }
				   else if(state[i+y][j+x]>=SPECIALCELLS && pinoraux[k]==AUX && pin[k][i+y][j+x])
			 
				     diffusionmatrix[neigh][i][j]+=permeability[k]*EXTRAS[state[i+y][j+x]].dynamicpermeability[k]*spacestep;
				 }

			       if(state[i][j]>CELLWALL /*&& !(diffusionmatrix[neigh][i][j]>EPSILON)*/)
				 diffusionmatrix[neigh][i][j]+=leakage*spacestep;
			     }//close here else if(state[i+y][j+x])?

			 }//close here if(state[i][j])?
		     } 
		   );
        );
}

void BoundaryFlux()
{
  //int j;
  /*  static int step=0;
      FILE *fp=NULL;
    
      snprintf(a_string,STRING,"%s/%.5d_boundaryflux.dat",dirname,step++);
      if((fp=fopen(a_string,"w"))==NULL){
      fprintf(stderr,"could not open file %s\n",a_string);
      return;
      }
  */
    
  //for (j=1; j<=ncol;j++) {
  //  boundaryflux[j-1]=(-diffusionmatrix[SOUTH][2578][j]*auxin[2578][j])/spacestep; //flux exiting the boundary cell at the membrane grid point (influx into the system)
  //   fprintf(fp,"boundaryflux at column %d is %g\n",j, boundaryflux[j-1]);
  //}
    
  //  fclose(fp);
    
}


void UpdatePIN()
{
    
  CPLANE(state,
	 EXTRAS[state[i][j]].dynamicpermeability[PINDYN137]=pow(auxin[(int)(cells.shape[state[i][j]].meany)][(int)(cells.shape[state[i][j]].meanx)],powpindyn)/(pow(halfmaxpindyn,powpindyn)+pow(auxin[(int)(cells.shape[state[i][j]].meany)][(int)(cells.shape[state[i][j]].meanx)],powpindyn));
	 );
    
  UpdateDiffusionMatrix();
}


void FillPins()
{
  int k,m,type;

  for(k=0;k<TOTALPINS;k++)
    Fill(pin[k],0);
  
  CPLANE(state,
         if(corner[i][j])
	   {
	     k=cells.celltype[state[i][j]];
            
	     if(corner[i][j]==UPPER || corner[i][j]==UO || corner[i][j]==UI) {
               for(m=0;m<TOTALPINS;m++){
		 if(pinlocalization[k][m][UPPER])
		   pin[m][i][j]=1;
	       }
	     }
	     if(corner[i][j]==LOWER || corner[i][j]==LI || corner[i][j]==LO) {
	       for(m=0;m<TOTALPINS;m++){
		 if(pinlocalization[k][m][LOWER])
		   pin[m][i][j]=1;
	       }
	     }
	     if(corner[i][j]==OUTER || corner[i][j]==UO || corner[i][j]==LO){
	       for(m=0;m<TOTALPINS;m++){
		 if(pinlocalization[k][m][OUTER])
		   pin[m][i][j]=1;
	       }
	     }
	     if(corner[i][j]==INNER || corner[i][j]==UI || corner[i][j]==LI){
	       for(m=0;m<TOTALPINS;m++){
		 if(pinlocalization[k][m][INNER])
		   pin[m][i][j]=1;
	       }
	     }
         
	   }
         if(state[i][j]>CELLWALL && state[i][j]<SPECIALCELLS) {
           type=cells.celltype[state[i][j]];
           if(type == VASCULAR || type == VASCULAR_D || type == PERICYCLE ||  type == PERICYCLE_D || type == ENDODERMIS || type == ENDODERMIS_D) {
	     auxin[i][j]=boundaryconcentration;
	     //printf("(%d,%d) %f\n",i,j,auxin[i][j]);
	   }
         }
        
         );
        
}

/*void UpdateExpression()
  {
  int type;
  CPLANE(state,
  type=cells.celltype[state[i][j]];
  if(type>=VASCULAR_D && type<=EPIDERMIS_D) //in the DZ
  auxbexpression[i][j]=EXTRAS[state[i][j]].dynamicpermeability[AUXB];
  if(type==VASCULAR_D || type==PERICYCLE_D) //in DZ vasculature
  pinexpression[i][j]=EXTRAS[state[i][j]].dynamicpermeability[PINC];
  );
  }
*/


void UpdateHeatmapAuxin()
{
  int found;
  FLOAT maxinzoneflux,mininzoneflux;  
  FLOAT maxflux=0.,minflux=0.;
  FLOAT length;

  noreallyhighvalues=0;

  if(gradlog) {
    //this can be extended to make specific for zone, see shoeflux_02.c
    maxinzoneflux=log(fixedmax+1e-6);
    mininzoneflux=log(fixedmin+1e-6);
    PLANE(
          logauxin2[i][j]=(FLOAT)log(auxin[i][j]+1e-6);
          );
  }
  else {
    //this can be extended to make specific for zone, see shoeflux_02.c
    maxinzoneflux=fixedmax;
    mininzoneflux=fixedmin;
    FCopy(logauxin2,auxin);
  }

  found=0;
  
  CPLANE(state,
	 if(state[i][j]==CELLWALL || state[i][j]>=SPECIALCELLS) {
	   if(!found) {
	     found=1;
	     maxflux=logauxin2[i][j];
	     minflux=logauxin2[i][j];
	   }
	   else {
	     if((length=logauxin2[i][j])>maxflux)
	       maxflux=length;
	     else if(length<minflux)
	       minflux=length;
	   }
	 }
         );

  //check if any value will end up in the highest zone;
  if(maxflux-maxinzoneflux>EPSILON) {
    //first scale the high values 
    CPLANE(state,
	   length=max(minflux,logauxin2[i][j]);
	   if(length-maxinzoneflux>EPSILON) {
	     logauxin2[i][j]=maxinzoneflux+upperboundary*(maxinzoneflux-mininzoneflux)*(length-maxinzoneflux)/(maxflux-maxinzoneflux);
	   }
	   );
  }
  else {
    noreallyhighvalues=1;
    forcedmax=maxinzoneflux-mininzoneflux+lowerboundary*(maxinzoneflux-mininzoneflux);
    //printf("Warning: highest value less than fixedmaxauxin: %lf < %lf\n",exp(maxflux),exp(maxinzoneflux));
  }



  
  //then bring down the low values 

  CPLANE(state,
	 length=max(minflux,logauxin2[i][j]);
         if(length>mininzoneflux) {
           logauxin2[i][j]=length-mininzoneflux+lowerboundary*(maxinzoneflux-mininzoneflux);
         }
         else {
	   logauxin2[i][j]=lowerboundary*(maxinzoneflux-mininzoneflux)*(length-minflux)/(mininzoneflux-minflux);
         }
         );
}

void Biosynt()
{
  /* PLANE(
     if(state[i][j]>CELLWALL)

     influx[i][j]=bioauxin;
     else
     influx[i][j]=0.;
     );
  
  */
    
  int type;
    
    
  PLANE(
	if(state[i][j]>CELLWALL){
          
	  influx[i][j]=bioauxin;
	  	  
	  type=cells.celltype[state[i][j]];
           
	  if (type==QCROOT || type==COL_I)
      
	    influx[i][j]=bioauxinonecell;
	}
	else
	  influx[i][j]=0.;
	)
  
    }

void BiosyntOneCell()
{
  PLANE(
        if(state[i][j]==biosyntonecell)
  
          influx[i][j]=bioauxinonecell;
  
        else
          influx[i][j]=0.;
	//fprintf(stdout,"biosyntonecell = %d", biosyntonecell);
        );
}

void IC()
{
  int size=0;
  FLOAT adddose;

  if(readauxin)
    fprintf(stdout,"warning: initial dose will be applied on top of imported auxin pattern\n");
    

  PLANE(
        if(state[i][j]>=SPECIALCELLS)
          size++;
        );
  adddose=initialdose/(FLOAT)size;

  PLANE(
        if(state[i][j]>=SPECIALCELLS)
          auxin[i][j]+=adddose;
        );
}

External FindExtLayer()// to automatically detect which is the external LRC/COL layer of cells
{
    // Rationale: I first calculate the width of the cell wall (in case it is not always set to 1), then I scan columns (left and right halves of the root) starting from medium and checking if the first j-th grid point neighbouring the cell wall - here I need cell wall width - belongs to LRC or COLUMELLA (which I know are the most external tissues of the root together with EPIDERMIS_DFZ). Given that I have multiple LRC and COLUMELLA layers in my layout and I only want to detect the most external one, I set the j-th column that I found as the most external one and I obtain the corresponding cell numbers.
    
    //    static int step=0;
    //    FILE *fp=NULL;
    //    char a_string[STRING];
    //
    //    snprintf(a_string,STRING,"%s/%.5d_ext_cells.dat",dirname,step++);
    //    if((fp=fopen(a_string,"w"))==NULL){
    //        fprintf(stderr,"could not open file %s\n",a_string);
    //        return 0;
    //    }
    //
    int type;
    int ext_col=-1;
    int ext_cell[MAXCELLS];
    int cell_counter=0;
    int total_row_cells=0;
    int k;
    int wall_width=1;
    static int outer_cell[MAXCELLS];
    
    CELLS(cells,
          CPLANE(state,
                 type=cells.celltype[state[i][j]];
                 if (i==nrow-2 && (state[i][j]!=state[i][j+1])&& (type !=CELLWALL)) {
                     total_row_cells++;
                 }
                 )
          )
    // printf("cell in a row is %d\n", cell_number);
    double cell_length=(ncol/total_row_cells);
    // printf("cell_length is %g\n",cell_length);
    
    
    NEIGHBOURS(
               int i=0;
               int j;
               for(j=1; j<=cell_length; j++) { // only need to scan over the columns of a single cell
                   if (i==nrow && state[i][j]==CELLWALL){
                       if(cells.celltype[state[i][j-x]]==MEDIUM && state[i][j]==state[i][j+x]) {
                           wall_width+=1;
                       }
                   }
               }
               )
    //        fprintf(fp,"wall grid point %d\n",wall_width);
    
    
    PLANE(
          type=cells.celltype[state[i][j]];
          
          if (j<=ncol/2) {
              if ((type==MEDIUM && cells.celltype[state[i][j+1]]==CELLWALL) && (cells.celltype[state[i][j+1+wall_width]]==LRC || cells.celltype[state[i][j+1+wall_width]]==COLUMELLAROOT)) {
                  ext_col=j+1+wall_width;
              }
          }
          if (j>ncol/2) {
              if ((type==MEDIUM && cells.celltype[state[i][j-1]]==CELLWALL) && (cells.celltype[state[i][j-1-wall_width]]==LRC || cells.celltype[state[i][j-1-wall_width]]==COLUMELLAROOT))
                  ext_col=j-1-wall_width;
          }
          
          if (cells.celltype[state[i][ext_col]]!=CELLWALL && cells.celltype[state[i][ext_col]]!=MEDIUM) {
              // fprintf(fp,"cells %d\n",cells.celltype[state[i][ext_col]]);
              int counted = 0;
              for(k=0; k<cell_counter; k++) {
                  if(state[i][ext_col]==ext_cell[k])
                      counted=1;
              }
              if(counted)
                  continue;
              ext_cell[cell_counter]=state[i][ext_col];
              cell_counter++;
          }
          )
    
    for(k=0; k<cell_counter; k++) {
        outer_cell[k]=ext_cell[k];
        //fprintf(fp,"outer cell is %d\n",outer_cell[k]);
    }
    //  fprintf(fp,"number of elements is %d\n",cell_counter);
    //  fclose(fp);
    
    External external;
    external.cellnumber=outer_cell;
    external.numberofcells=cell_counter;
    
    return external;
    // my function returns both the array containing the number of external cells and the number of cells in the array, which I need in the Decay function
}

void Decay()
{
    //decay is everywhere
    //FFill(breakdown,dauxin);
    
    int type;
    int k;
    External outer=FindExtLayer();
    
    CPLANE(state,
	   if(state[i][j]>CELLWALL)
	     breakdown[i][j]=dauxin;
           
	   type=cells.celltype[state[i][j]];

	   //wild type, deg only in the external layer of LRC and EPI_DFZ (GH3 expression domain)
	   for(k=0;k<outer.numberofcells;k++)
	     if (state[i][j]==outer.cellnumber[k])
	       breakdown[i][j]=dauxinTZ;

	   if(type==EPIDERMIS_TZ || type==EPIDERMIS_EZ || type==EPIDERMIS_D)
	     breakdown[i][j]=dauxinTZ;
	     
	   if(!deg_only_ext_lrc)
	     if (type==LRC)
	       breakdown[i][j]=dauxinTZ;

	   //set back if null_decay
	   if(null_decay && (type==EPIDERMIS_TZ || type==EPIDERMIS_EZ || type==EPIDERMIS_D))
	     breakdown[i][j]=dauxin;
           );
   
    /*
     int counter=0;
     double sumdecay=0.;
     CPLANE(state,
     if(state[i][j]>CELLWALL) {
     counter++;
     sumdecay+=breakdown[i][j];
     }
     );
     printf("average breakdown rate: %g\n",sumdecay/(double)counter);
    */
}

void Immerse()
{
  if(!(bioauxin>EPSILON)) //influx has not yet been assigned values
    FFill(influx,0.); //make it empty
  Boundaries(state);
  CPLANE(state,
         NEIGHBOURS(
		    if(!state[i+y][j+x]) {
		      influx[i][j]+=liquid;
		      break;
		    }
		    );
         );
}

void InitCells()
{
  FILE *fp;
  int c,k;
  int i,j,location;
  FLOAT total=0.;
  char a_string[STRING];
	
  snprintf(a_string,STRING,"%s.state",readpattern);
  ReadPat(state,1,1,'i',a_string);
  snprintf(a_string,STRING,"%s.corners",readpattern);
  ReadPat(corner,1,1,'i',a_string);
  snprintf(a_string,STRING,"%s.cells",readpattern);
  CReadPat(&cells,a_string);
  PLANE(
	if(cells.celltype[state[i][j]]==FAKECELLS || cells.celltype[state[i][j]]==FAKECELLWALL)
          state[i][j]=0;
	)
    if(readauxin && !(bendingroot && bendingauxin)) {
      snprintf(a_string,STRING,"%s",readauxinpattern);
      FReadPat(auxin,1,1,'f',a_string);
      if(savegraphics) {
	snprintf(a_string,STRING,"cp %s %s/initialauxinpattern.dat",readauxinpattern,dirname);
	system(a_string);
      }
    }
  specialcelltype=MEDIUM;
  cells.celltype[CELLWALL]=CELLWALL;
	
  if(bendingroot) {
    snprintf(a_string,STRING,"%s_scaling.dat",readpattern);
    if((fp=fopen(a_string,"r"))!=NULL) {
      while(fscanf(fp,"%d",&c)!=EOF)
	for(k=1;k<9;k++)
	  fscanf(fp,"%lf",&EXTRAS[c].correction[k]);
      fclose(fp);
    }
    else {
      fprintf(stdout,"InitCells: error, file does not exist: %s\n",a_string);
      exit(EXIT_FAILURE);
    }
  }
	
  left=ncol;
  right=1;
  top=nrow;
  lower=1;
	
  CPLANE(state,
	 if(j<left)
           left=j;
	 if(j>right)
           right=j;
	 if(i<top)
           top=i;
	 if(i>lower)
           lower=i;
	 );
  right+=1;
  lower+=1;
	
  FillPins();
	
  UpdateCFill(state,&cells);
  InitCellPosition(&cells);
  UpdateCellPosition(state,&cells);
  UpdateCellShape(&cells);
	
  FindExtLayer();

  CELLS(cells,
	EXTRAS[c].auxinbefore=0.;
	EXTRAS[c].auxinafter=0.;
	EXTRAS[c].auxincorrection=0.;
	EXTRAS[c].auxinconcentration=0.;
	for(k=0;k<TOTALPINS;k++)
          EXTRAS[c].dynamicpermeability[k]=1.;
	// EXTRAS[c].dynamicpermeability[PINDYN137]=0.;
          
	);
	
  if(readauxin && !(bendingroot && bendingauxin)) {
    PLANE(
	  if(state[i][j])
	    EXTRAS[state[i][j]].auxinbefore+=auxin[i][j];
	  else
	    auxin[i][j]=0.;
	  );
		
    CELLS(cells,
	  if(cells.area[c]) {
	    EXTRAS[c].auxinconcentration=EXTRAS[c].auxinbefore/(FLOAT)cells.area[c];
	    //already plug in these values as the mean value up to now
	  }
	  );
  }
	
  else if(bendingroot && bendingauxin && readauxin) {
    for(i=1;i<=oldnrow;i++)
      for(j=1;j<=oldncol;j++) {
	EXTRAS[forwardcells[i][j]].auxinbefore+=forwardauxin[i][j];
	total+=forwardauxin[i][j];
      }
		
    PLANE(
	  location=backwardstate[i][j];
	  auxin[i][j]=forwardauxin[location/SHRT_MAX][location%SHRT_MAX];
	  if(state[i][j])
	    EXTRAS[state[i][j]].auxinafter+=forwardauxin[location/SHRT_MAX][location%SHRT_MAX];
	  else
	    auxin[i][j]=0.;
	  );
		
    CELLS(cells,
	  if(cells.area[c]) {
	    if(EXTRAS[c].auxinafter>EPSILON) {
	      EXTRAS[c].auxincorrection=EXTRAS[c].auxinbefore/EXTRAS[c].auxinafter;
	      //printf("the correction for cell %d is %lf\n",c,EXTRAS[c].auxincorrection);
	      //first calculate the old concentration
	      EXTRAS[c].auxinconcentration=EXTRAS[c].auxinbefore/(FLOAT)oldcells.area[c];
	      EXTRAS[c].meanauxbauxinconcentration=EXTRAS[c].auxinconcentration;
	      EXTRAS[c].meanpinauxinconcentration=EXTRAS[c].auxinconcentration;
	      //now calculate the new concentration
	      EXTRAS[c].auxinconcentration=EXTRAS[c].auxinbefore/(FLOAT)cells.area[c];
	      EXTRAS[c].auxinafter=0.;
	    }
	  }
	  );
		
    //printf("before: %lf after: %lf\n",total,CFTotal(auxin,state));
		
    CPLANE(state,
	   auxin[i][j]*=EXTRAS[state[i][j]].auxincorrection;
	   );
    //printf("before: %lf after: %lf\n",total,CFTotal(auxin,state));
  }
}

void AuxinRatio()// I want to check which is the ratio between rows where I observe the dip
{
  static int step=0;
  FILE *fp=NULL;
  char a_string[STRING];
    
  snprintf(a_string,STRING,"%s/%.5d_ratio.dat",dirname,step++);
  if((fp=fopen(a_string,"w"))==NULL){
    fprintf(stderr,"could not open file %s\n",a_string);
    return;
  }
    
    
  const int tissue_number=TOTALROOTTYPES;
  const char *tissueid[tissue_number];
    
  tissueid[MEDIUM]="MEDIUM";
  tissueid[CELLWALL]="CELLWALL";
  tissueid[VASCULAR]="VASCULAR";
  tissueid[PERICYCLE]="PERICYCLE";
  tissueid[ENDODERMIS]="ENDODERMIS";
  tissueid[CORTEX]="CORTEX";
  tissueid[EPIDERMIS]="EPIDERMIS";
  tissueid[PERICYCLE]="PERICYCLE";
  tissueid[LRC]="LRC";
    
  int type;
  int lowestrow=-1;
  int i,j;
  int cellsinarow[TOTALROOTTYPES];
    
    
  for(j=1; j<ncol+1; j++){
    for(i=1; i<nrow+1; i++)
      {
	type=cells.celltype[state[i][j]];
            
	//    if ((type == VASCULAR_TZ || type == PERICYCLE_TZ || type == ENDODERMIS_TZ || type == CORTEX_TZ || type == EPIDERMIS_TZ) && i>uppestrow)
	if ((type == VASCULAR || type == PERICYCLE || type == ENDODERMIS || type == CORTEX || type == EPIDERMIS) && i>lowestrow)
                
	  lowestrow=i;
      }
  }
    
  int cell_counter = 0;
  for (j=1;j<ncol+1;j++){
    int k;
    int counted = 0;
    for(k=0; k<cell_counter; k++){
      if(state[lowestrow][j] == cellsinarow[k]){
	counted = 1;
      }
    }
    if(counted)
      continue;
    cellsinarow[cell_counter] = state[lowestrow][j];
    cell_counter++;
  }
    
  int k;
  for(k=0; k<cell_counter; k++){
    if(cells.celltype[cellsinarow[k]]!=LRC && cells.celltype[cellsinarow[k]]>CELLWALL && cells.celltype[cellsinarow[k]]>specialcelltype){
      int lower_cell = cellsinarow[k];
      int upper_cell = cellsinarow[k]+(TOTALROOTTYPES-1)/2;
      double ratio = auxin[(int)(cells.shape[upper_cell].meany)][(int)(cells.shape[upper_cell].meanx)]/auxin[(int)(cells.shape[lower_cell].meany)][(int)(cells.shape[lower_cell].meanx)];
        
      //  printf("at time %g auxin ratio between cell %d and cell %d of tissue %s is:  %f\n",realtime,upper_cell,lower_cell,tissueid[cells.celltype[lower_cell]],ratio);
      fprintf(fp,"at time %g auxin ratio between cell %d and cell %d of tissue %s is:  %f\n",realtime,upper_cell,lower_cell,tissueid[cells.celltype[lower_cell]],ratio);
    }
  }
  fclose(fp);
}


void MyInDat(char filename[],char *format,char *text,void *value)
{
  if(!InDat(filename,format,text,value)) {
    fprintf(stdout,"cannot find \"%s\" in parameter file; exitting now!\n",text);
    exit(EXIT_FAILURE);
  }
}

void SetPars(char name[])
{
  char conversion[10];
  FLOAT tmpupperboundary,tmplowerboundary;

#ifdef _SMALL
  snprintf(conversion,10,"%%f");
#else
  snprintf(conversion,10,"%%lf");
#endif
  ReadOptions(name);
  MyInDat(name,"%d","showinterval",&showinterval);
  MyInDat(name,"%d","savegraphics",&savegraphics);
  MyInDat(name,"%d","saveinterval",&saveinterval);
  MyInDat(name,"%d","grace",&grace);
  MyInDat(name,"%d","graceinterval",&graceinterval);
  MyInDat(name,"%d","savegrace",&savegrace);
  MyInDat(name,"%d","savegraceinterval",&savegraceinterval);
  MyInDat(name,"%d","flux",&flux);
  MyInDat(name,"%d","gracefinterval",&gracefinterval);
  MyInDat(name,"%d","saveflux",&saveflux);
  MyInDat(name,"%d","savegracefinterval",&savegracefinterval);
  MyInDat(name,"%d","auxindatainterval",&auxindatainterval);
  MyInDat(name,"%d","includeequilibrium",&includeequilibrium);
  MyInDat(name,"%d","equilibriumstart",&equilibriumstart);
  MyInDat(name,"%d","equilibriumstop",&equilibriumstop);
  MyInDat(name,"%d","equilibriuminterval",&equilibriuminterval);
  MyInDat(name,"%s","readpattern",readpattern);
  MyInDat(name,"%d","readauxin",&readauxin);

  if(readauxin)
    MyInDat(name,"%s","readauxinpattern",readauxinpattern);
  MyInDat(name,conversion,"fixedfinalauxinvalue",&fixedfinalauxinvalue);
  MyInDat(name,conversion,"boundaryconcentration",&boundaryconcentration);
  MyInDat(name,conversion,"Dauxin",&Dauxin);
  MyInDat(name,conversion,"reducedincellwall",&reducedincellwall);
  MyInDat(name,conversion,"paux",&permeability[AUX]);
  MyInDat(name,conversion,"paux1",&permeability[AUX1]);
  MyInDat(name,conversion,"paux1half",&permeability[AUX1half]);
  MyInDat(name,conversion,"ppinmax",&permeability[PIN1max]);
  MyInDat(name,conversion,"ppinhalf",&permeability[PIN1half]);
  MyInDat(name,conversion,"ppinthird",&permeability[PIN1third]); 
  MyInDat(name,conversion,"ppinquarter",&permeability[PIN1quarter]); 
  MyInDat(name,conversion,"ppinfifth",&permeability[PIN1fifth]); 
  MyInDat(name,conversion,"ppinsixth",&permeability[PIN1sixth]); 
  MyInDat(name,conversion,"ppinmin",&permeability[PIN1min]);
  MyInDat(name,conversion,"ppinmax",&permeability[PIN3max]);//to check
  MyInDat(name,conversion,"ppinhalf",&permeability[PIN3half]);
  MyInDat(name,conversion,"ppinthird",&permeability[PIN3third]); 
  MyInDat(name,conversion,"ppinquarter",&permeability[PIN3quarter]); 
  MyInDat(name,conversion,"ppinfifth",&permeability[PIN3fifth]); 
  MyInDat(name,conversion,"ppinsixth",&permeability[PIN3sixth]); 
  MyInDat(name,conversion,"ppinmin",&permeability[PIN3min]);
  MyInDat(name,conversion,"ppinmax",&permeability[PIN7max]);//to check
  MyInDat(name,conversion,"ppinhalf",&permeability[PIN7half]);
  MyInDat(name,conversion,"ppinthird",&permeability[PIN7third]); 
  MyInDat(name,conversion,"ppinquarter",&permeability[PIN7quarter]); 
  MyInDat(name,conversion,"ppinfifth",&permeability[PIN7fifth]); 
  MyInDat(name,conversion,"ppinsixth",&permeability[PIN7sixth]); 
  MyInDat(name,conversion,"ppinmin",&permeability[PIN7min]);
  MyInDat(name,conversion,"ppin2max",&permeability[PIN2max]);//to check
  MyInDat(name,conversion,"ppin2half",&permeability[PIN2half]);
  MyInDat(name,conversion,"ppin2third",&permeability[PIN2third]);
  MyInDat(name,conversion,"ppin2quarter",&permeability[PIN2quarter]);
  MyInDat(name,conversion,"ppin2fifth",&permeability[PIN2fifth]);
  MyInDat(name,conversion,"ppin2sixth",&permeability[PIN2sixth]);
  MyInDat(name,conversion,"ppinmin",&permeability[PIN2min]);
  //  MyInDat(name,conversion,"halfmaxpindyn",&halfmaxpindyn);
  // MyInDat(name,"%d","pincregulation",&pincregulation);
  //  MyInDat(name,"%d","pindregulation",&pindregulation);
  // MyInDat(name,"%d","powpindyn",&powpindyn);

  MyInDat(name,conversion,"leakage",&leakage);
  MyInDat(name,conversion,"dauxin",&dauxin);
  MyInDat(name,conversion,"dauxinTZ",&dauxinTZ);// added by me
  MyInDat(name,conversion,"bioauxin",&bioauxin);
  MyInDat(name,conversion,"liquid",&liquid);
  MyInDat(name,conversion,"initialdose",&initialdose);
  MyInDat(name,"%d","biosyntonecell",&biosyntonecell);
  MyInDat(name,conversion,"bioauxinonecell",&bioauxinonecell);
  MyInDat(name,"%d","whattoshow",&whattoshow);
  MyInDat(name,"%d","alsoshowing",&alsoshowing);
  MyInDat(name,conversion,"fixedmin",&fixedmin);
  MyInDat(name,conversion,"fixedmax",&fixedmax);
  MyInDat(name,conversion,"upperboundary",&tmpupperboundary);
  MyInDat(name,conversion,"lowerboundary",&tmplowerboundary);
  if(!(1.-tmpupperboundary-tmplowerboundary>EPSILON))
    {
      fprintf(stderr,"error: sum of upperboundary and lowerboundary should be smaller than 1 (right now it's %lf)\n",tmpupperboundary+tmplowerboundary);
      exit(EXIT_FAILURE);
    }
  upperboundary=tmpupperboundary/(1.-tmpupperboundary-tmplowerboundary);
  lowerboundary=tmplowerboundary/(1.-tmpupperboundary-tmplowerboundary);
  MyInDat(name,"%d","upmutant",&upmutant);
  MyInDat(name,"%d","downmutant",&downmutant);
  MyInDat(name,"%d","lateralmutant",&lateralmutant);
  MyInDat(name,"%d","version2",&version2);
  MyInDat(name,"%d","equalstrengthPERIVASC",&equalstrengthPERIVASC);
  MyInDat(name,"%d","PINunifstrength",&PINunifstrength);
  MyInDat(name,"%d","PINunifpolandstrength",&PINunifpolandstrength);
  MyInDat(name,"%d","PINMZTZ",&PINMZTZ);
  MyInDat(name,"%d","aux_only_cor_epi_lrc",&aux_only_cor_epi_lrc);
  MyInDat(name,"%d","aux_only_vasc_peri",&aux_only_vasc_peri);
  MyInDat(name,"%d","no_aux_at_all",&no_aux_at_all);
  MyInDat(name,"%d","no_pin2_flip",&no_pin2_flip);
  MyInDat(name,"%d","deg_only_ext_lrc",&deg_only_ext_lrc);

  //when set, do not change this property when crossing TZ
  MyInDat(name,"%d","null_aux1",&null_aux1);
  MyInDat(name,"%d","null_pin_intensity",&null_pin_intensity);
  MyInDat(name,"%d","null_pin_localization",&null_pin_localization);
  MyInDat(name,"%d","null_decay",&null_decay);
}

void SetPinLocalization()
{
   
  int l,m,k;
	
  for(l=0;l<totaltypes;l++)
    for(m=0;m<TOTALPINS;m++)
      for(k=0;k<5;k++)
	pinlocalization[l][m][k]=0;
	
  if(scanroot)
    {
      //don't want the deadcells or fakecells/cellwall to have influx or efflux      
      for(l=2;l<DEADCELLS;l++) { 
	//currently not being used
	if(l==VASCULAR_D || l==PERICYCLE_D || l==ENDODERMIS_D || l==CORTEX_D || l==EPIDERMIS_D || l==SPECIALVASCULAR)
	  for(k=1;k<5;k++)
	    pinlocalization[l][AUXB][k]=1;
	//this is used
	for(k=1;k<5;k++)
	  pinlocalization[l][AUX][k]=1;
      }
        
        

      // AUX1
      if(!no_aux_at_all) {

	if(!aux_only_cor_epi_lrc) {

	  pinlocalization[VASCULAR][AUX1][UPPER]=1;
	  pinlocalization[VASCULAR][AUX1][LOWER]=1;
	  pinlocalization[VASCULAR][AUX1][OUTER]=1;
	  pinlocalization[VASCULAR][AUX1][INNER]=1;
        
        
	  pinlocalization[VASCULAR_TZ][AUX1][UPPER]=1;
	  pinlocalization[VASCULAR_TZ][AUX1][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][AUX1][OUTER]=1;
	  pinlocalization[VASCULAR_TZ][AUX1][INNER]=1;
        
        
	  pinlocalization[VASCULAR_EZ][AUX1][UPPER]=1;
	  pinlocalization[VASCULAR_EZ][AUX1][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][AUX1][OUTER]=1;
	  pinlocalization[VASCULAR_EZ][AUX1][INNER]=1;
        
        
	  pinlocalization[VASCULAR_D][AUX1][UPPER]=1;
	  pinlocalization[VASCULAR_D][AUX1][LOWER]=1;
	  pinlocalization[VASCULAR_D][AUX1][OUTER]=1;
	  pinlocalization[VASCULAR_D][AUX1][INNER]=1;
        
        
        
	  pinlocalization[PERICYCLE][AUX1][UPPER]=1;
	  pinlocalization[PERICYCLE][AUX1][LOWER]=1;
	  pinlocalization[PERICYCLE][AUX1][OUTER]=1;
	  pinlocalization[PERICYCLE][AUX1][INNER]=1;
        
	  pinlocalization[PERICYCLE_TZ][AUX1][UPPER]=1;
	  pinlocalization[PERICYCLE_TZ][AUX1][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][AUX1][OUTER]=1;
	  pinlocalization[PERICYCLE_TZ][AUX1][INNER]=1;
        
        
	  pinlocalization[PERICYCLE_EZ][AUX1][UPPER]=1;
	  pinlocalization[PERICYCLE_EZ][AUX1][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][AUX1][OUTER]=1;
	  pinlocalization[PERICYCLE_EZ][AUX1][INNER]=1;
        
	  pinlocalization[PERICYCLE_D][AUX1][UPPER]=1;
	  pinlocalization[PERICYCLE_D][AUX1][LOWER]=1;
	  pinlocalization[PERICYCLE_D][AUX1][OUTER]=1;
	  pinlocalization[PERICYCLE_D][AUX1][INNER]=1;
	}      

	if(!aux_only_vasc_peri && !null_aux1) {
	  pinlocalization[CORTEX_TZ][AUX1][LOWER]=1;
	  pinlocalization[CORTEX_TZ][AUX1][OUTER]=1;
	  pinlocalization[CORTEX_TZ][AUX1][INNER]=1;
	  pinlocalization[CORTEX_TZ][AUX1][UPPER]=1;
        
	  pinlocalization[CORTEX_EZ][AUX1][LOWER]=1;
	  pinlocalization[CORTEX_EZ][AUX1][OUTER]=1;
	  pinlocalization[CORTEX_EZ][AUX1][INNER]=1;
	  pinlocalization[CORTEX_EZ][AUX1][UPPER]=1;
        
	  pinlocalization[CORTEX_D][AUX1][LOWER]=1;
	  pinlocalization[CORTEX_D][AUX1][OUTER]=1;
	  pinlocalization[CORTEX_D][AUX1][INNER]=1;
	  pinlocalization[CORTEX_D][AUX1][UPPER]=1;
        
        
	  pinlocalization[EPIDERMIS_TZ][AUX1][LOWER]=1;
	  pinlocalization[EPIDERMIS_TZ][AUX1][UPPER]=1;
	  pinlocalization[EPIDERMIS_TZ][AUX1][INNER]=1;
	  pinlocalization[EPIDERMIS_TZ][AUX1][OUTER]=1;
     
     
        
	  pinlocalization[EPIDERMIS_EZ][AUX1][LOWER]=1;
	  pinlocalization[EPIDERMIS_EZ][AUX1][UPPER]=1;
	  pinlocalization[EPIDERMIS_EZ][AUX1][INNER]=1;
	  pinlocalization[EPIDERMIS_EZ][AUX1][OUTER]=1;
       
        
	  pinlocalization[EPIDERMIS_D][AUX1][LOWER]=1;
	  pinlocalization[EPIDERMIS_D][AUX1][UPPER]=1;
	  pinlocalization[EPIDERMIS_D][AUX1][INNER]=1;
	  pinlocalization[EPIDERMIS_D][AUX1][OUTER]=1;
	}
    
	if(!aux_only_vasc_peri) {
	  pinlocalization[LRC][AUX1][LOWER]=1;
	  pinlocalization[LRC][AUX1][INNER]=1;
	  pinlocalization[LRC][AUX1][UPPER]=1;
	  pinlocalization[LRC][AUX1][OUTER]=1;
        }
      
	if(!aux_only_cor_epi_lrc && !aux_only_vasc_peri) {
        
	  pinlocalization[QCROOT][AUX1][UPPER]=1;
	  pinlocalization[QCROOT][AUX1][INNER]=1;
	  pinlocalization[QCROOT][AUX1][LOWER]=1;
	  pinlocalization[QCROOT][AUX1][OUTER]=1;
         
      
	  pinlocalization[COL_I][AUX1][UPPER]=1;
	  pinlocalization[COL_I][AUX1][LOWER]=1;
	  pinlocalization[COL_I][AUX1][INNER]=1;
	  pinlocalization[COL_I][AUX1][OUTER]=1;
        
       
	  pinlocalization[COLUMELLAROOT][AUX1][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][AUX1][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][AUX1][INNER]=1;
	  pinlocalization[COLUMELLAROOT][AUX1][OUTER]=1;

	  pinlocalization[LRCEPI_I][AUX1][UPPER]=1;
	  pinlocalization[LRCEPI_I][AUX1][LOWER]=1;
	  pinlocalization[LRCEPI_I][AUX1][INNER]=1;
	  pinlocalization[LRCEPI_I][AUX1][OUTER]=1;

	  pinlocalization[CORTEXENDO_I][AUX1][UPPER]=1;
	  pinlocalization[CORTEXENDO_I][AUX1][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][AUX1][INNER]=1;
	  pinlocalization[CORTEXENDO_I][AUX1][OUTER]=1;
	}   
      }
    
      if (PINunifstrength) { //`old' null model, not used anymore
	pinlocalization[VASCULAR][PIN1max][LOWER]=1;
	pinlocalization[VASCULAR_TZ][PIN1max][LOWER]=1;
	pinlocalization[VASCULAR_EZ][PIN1max][LOWER]=1;
	pinlocalization[VASCULAR_D][PIN1max][LOWER]=1;

	pinlocalization[PERICYCLE][PIN1max][LOWER]=1;
	pinlocalization[PERICYCLE][PIN1max][OUTER]=1;

	pinlocalization[PERICYCLE_TZ][PIN1max][LOWER]=1;
	pinlocalization[PERICYCLE_TZ][PIN1max][OUTER]=1;

	pinlocalization[PERICYCLE_EZ][PIN1max][LOWER]=1;
	pinlocalization[PERICYCLE_EZ][PIN1max][OUTER]=1;

	pinlocalization[PERICYCLE_D][PIN1max][LOWER]=1;
	pinlocalization[PERICYCLE_D][PIN1max][OUTER]=1;

	pinlocalization[ENDODERMIS][PIN1max][LOWER]=1;
	pinlocalization[ENDODERMIS_TZ][PIN1max][LOWER]=1;
	pinlocalization[ENDODERMIS_EZ][PIN1max][LOWER]=1;
	pinlocalization[ENDODERMIS_D][PIN1max][LOWER]=1;

	pinlocalization[CORTEXENDO_I][PIN1max][LOWER]=1;
	pinlocalization[CORTEXENDO_I][PIN1max][OUTER]=1;
	

	//PIN3


	pinlocalization[VASCULAR][PIN3max][LOWER]=1;
	pinlocalization[VASCULAR_TZ][PIN3max][LOWER]=1;
	pinlocalization[VASCULAR_EZ][PIN3max][LOWER]=1;
	pinlocalization[VASCULAR_D][PIN3max][LOWER]=1;

	pinlocalization[PERICYCLE][PIN3max][LOWER]=1;
	pinlocalization[PERICYCLE][PIN3max][OUTER]=1;

	pinlocalization[PERICYCLE_TZ][PIN3max][LOWER]=1;
	pinlocalization[PERICYCLE_TZ][PIN3max][OUTER]=1;

	pinlocalization[PERICYCLE_EZ][PIN3max][LOWER]=1;
	pinlocalization[PERICYCLE_EZ][PIN3max][OUTER]=1;

	pinlocalization[PERICYCLE_D][PIN3max][LOWER]=1;
	pinlocalization[PERICYCLE_D][PIN3max][OUTER]=1;

	pinlocalization[ENDODERMIS][PIN3max][LOWER]=1;
	pinlocalization[ENDODERMIS_TZ][PIN3max][LOWER]=1;
	pinlocalization[ENDODERMIS_EZ][PIN3max][LOWER]=1;
	pinlocalization[ENDODERMIS_D][PIN3max][LOWER]=1;

	pinlocalization[QCROOT][PIN3max][LOWER]=1;

	pinlocalization[COLUMELLAROOT][PIN3max][UPPER]=1;
	pinlocalization[COLUMELLAROOT][PIN3max][LOWER]=1;
	pinlocalization[COLUMELLAROOT][PIN3max][INNER]=1;
	pinlocalization[COLUMELLAROOT][PIN3max][OUTER]=1;

	//only PIN3 is expressed in COL_I

	pinlocalization[COL_I][PIN3max][UPPER]=1;
	pinlocalization[COL_I][PIN3max][LOWER]=1;
	pinlocalization[COL_I][PIN3max][INNER]=1;
	pinlocalization[COL_I][PIN3max][OUTER]=1;

	pinlocalization[LRCEPI_I][PIN3max][LOWER]=1;
	pinlocalization[LRCEPI_I][PIN3max][OUTER]=1;
	pinlocalization[LRCEPI_I][PIN3max][INNER]=1;//added later
	pinlocalization[LRCEPI_I][PIN3max][UPPER]=1;//added later

	pinlocalization[CORTEXENDO_I][PIN3max][LOWER]=1;
	pinlocalization[CORTEXENDO_I][PIN3max][OUTER]=1;
	pinlocalization[CORTEXENDO_I][PIN3max][INNER]=1;//added later
	pinlocalization[CORTEXENDO_I][PIN3max][UPPER]=1;//added later




	//PIN7


	pinlocalization[VASCULAR][PIN7max][LOWER]=1;
	pinlocalization[VASCULAR_TZ][PIN7max][LOWER]=1;
	pinlocalization[VASCULAR_EZ][PIN7max][LOWER]=1;
	pinlocalization[VASCULAR_D][PIN7max][LOWER]=1;

	pinlocalization[PERICYCLE][PIN7max][LOWER]=1;
	pinlocalization[PERICYCLE][PIN7max][OUTER]=1;

	pinlocalization[PERICYCLE_TZ][PIN7max][LOWER]=1;
	pinlocalization[PERICYCLE_TZ][PIN7max][OUTER]=1;

	pinlocalization[PERICYCLE_EZ][PIN7max][LOWER]=1;
	pinlocalization[PERICYCLE_EZ][PIN7max][OUTER]=1;

	pinlocalization[PERICYCLE_D][PIN7max][LOWER]=1;
	pinlocalization[PERICYCLE_D][PIN7max][OUTER]=1;

	pinlocalization[ENDODERMIS][PIN7max][LOWER]=1;
	pinlocalization[ENDODERMIS_TZ][PIN7max][LOWER]=1;
	pinlocalization[ENDODERMIS_EZ][PIN7max][LOWER]=1;
	pinlocalization[ENDODERMIS_D][PIN7max][LOWER]=1;

	pinlocalization[COLUMELLAROOT][PIN7max][UPPER]=1;
	pinlocalization[COLUMELLAROOT][PIN7max][LOWER]=1;
	pinlocalization[COLUMELLAROOT][PIN7max][INNER]=1;
	pinlocalization[COLUMELLAROOT][PIN7max][OUTER]=1;


	pinlocalization[LRCEPI_I][PIN7max][LOWER]=1;
	pinlocalization[LRCEPI_I][PIN7max][OUTER]=1;

	pinlocalization[CORTEXENDO_I][PIN7max][LOWER]=1;
	pinlocalization[CORTEXENDO_I][PIN7max][OUTER]=1;



	//PIN2

	pinlocalization[CORTEX][PIN2max][INNER]=1;
	pinlocalization[CORTEX][PIN2max][LOWER]=1;
	pinlocalization[CORTEX][PIN2max][OUTER]=1;

	pinlocalization[CORTEX_TZ][PIN2max][INNER]=1;
	if(no_pin2_flip)
	  pinlocalization[CORTEX_TZ][PIN2max][LOWER]=1;
	else
	  pinlocalization[CORTEX_TZ][PIN2max][UPPER]=1;
	pinlocalization[CORTEX_TZ][PIN2max][OUTER]=1;

	pinlocalization[CORTEX_EZ][PIN2max][INNER]=1;
	if(no_pin2_flip)
	  pinlocalization[CORTEX_EZ][PIN2max][LOWER]=1;
	else
	  pinlocalization[CORTEX_EZ][PIN2max][UPPER]=1;
	pinlocalization[CORTEX_EZ][PIN2max][OUTER]=1;

	pinlocalization[CORTEX_D][PIN2max][INNER]=1;
	if(no_pin2_flip)
	  pinlocalization[CORTEX_D][PIN2max][LOWER]=1;
	else	
	  pinlocalization[CORTEX_D][PIN2max][UPPER]=1;
	pinlocalization[CORTEX_D][PIN2max][OUTER]=1;

	pinlocalization[EPIDERMIS][PIN2max][UPPER]=1;
	pinlocalization[EPIDERMIS][PIN2max][INNER]=1;

	pinlocalization[EPIDERMIS_TZ][PIN2max][UPPER]=1;
	pinlocalization[EPIDERMIS_TZ][PIN2max][INNER]=1;

	pinlocalization[EPIDERMIS_EZ][PIN2max][UPPER]=1;
	pinlocalization[EPIDERMIS_D][PIN2max][UPPER]=1;

	pinlocalization[LRC][PIN2max][UPPER]=1;
	pinlocalization[LRC][PIN2max][INNER]=1;
      }

        
      if (PINunifpolandstrength) {// another `old' null model, not used anymore
	pinlocalization[VASCULAR][PIN1max][LOWER]=1;
	pinlocalization[VASCULAR_TZ][PIN1max][LOWER]=1;
	pinlocalization[VASCULAR_EZ][PIN1max][LOWER]=1;
	pinlocalization[VASCULAR_D][PIN1max][LOWER]=1;

	pinlocalization[PERICYCLE][PIN1max][LOWER]=1;
	//pinlocalization[PERICYCLE][PIN1max][OUTER]=1;

	pinlocalization[PERICYCLE_TZ][PIN1max][LOWER]=1;
	//pinlocalization[PERICYCLE_TZ][PIN1max][OUTER]=1;

	pinlocalization[PERICYCLE_EZ][PIN1max][LOWER]=1;
	//pinlocalization[PERICYCLE_EZ][PIN1max][OUTER]=1;

	pinlocalization[PERICYCLE_D][PIN1max][LOWER]=1;
	//pinlocalization[PERICYCLE_D][PIN1max][OUTER]=1;

	pinlocalization[ENDODERMIS][PIN1max][LOWER]=1;
	pinlocalization[ENDODERMIS_TZ][PIN1max][LOWER]=1;
	pinlocalization[ENDODERMIS_EZ][PIN1max][LOWER]=1;
	pinlocalization[ENDODERMIS_D][PIN1max][LOWER]=1;

	pinlocalization[CORTEXENDO_I][PIN1max][LOWER]=1;
	//pinlocalization[CORTEXENDO_I][PIN1max][OUTER]=1;


	//PIN3


	pinlocalization[VASCULAR][PIN3max][LOWER]=1;
	pinlocalization[VASCULAR_TZ][PIN3max][LOWER]=1;
	pinlocalization[VASCULAR_EZ][PIN3max][LOWER]=1;
	pinlocalization[VASCULAR_D][PIN3max][LOWER]=1;

	pinlocalization[PERICYCLE][PIN3max][LOWER]=1;
	//pinlocalization[PERICYCLE][PIN3max][OUTER]=1;

	pinlocalization[PERICYCLE_TZ][PIN3max][LOWER]=1;
	//pinlocalization[PERICYCLE_TZ][PIN3max][OUTER]=1;

	pinlocalization[PERICYCLE_EZ][PIN3max][LOWER]=1;
	//pinlocalization[PERICYCLE_EZ][PIN3max][OUTER]=1;

	pinlocalization[PERICYCLE_D][PIN3max][LOWER]=1;
	//pinlocalization[PERICYCLE_D][PIN3max][OUTER]=1;

	pinlocalization[ENDODERMIS][PIN3max][LOWER]=1;
	pinlocalization[ENDODERMIS_TZ][PIN3max][LOWER]=1;
	pinlocalization[ENDODERMIS_EZ][PIN3max][LOWER]=1;
	pinlocalization[ENDODERMIS_D][PIN3max][LOWER]=1;

	pinlocalization[QCROOT][PIN3max][LOWER]=1;

	//pinlocalization[COLUMELLAROOT][PIN3max][UPPER]=1;
	pinlocalization[COLUMELLAROOT][PIN3max][LOWER]=1;
	//pinlocalization[COLUMELLAROOT][PIN3max][INNER]=1;
	//pinlocalization[COLUMELLAROOT][PIN3max][OUTER]=1;

	//only PIN3 is expressed in COL_I

	//pinlocalization[COL_I][PIN3max][UPPER]=1;
	pinlocalization[COL_I][PIN3max][LOWER]=1;
	//pinlocalization[COL_I][PIN3max][INNER]=1;
	//pinlocalization[COL_I][PIN3max][OUTER]=1;

	pinlocalization[LRCEPI_I][PIN3max][LOWER]=1;
	//pinlocalization[LRCEPI_I][PIN3max][OUTER]=1;
	//pinlocalization[LRCEPI_I][PIN3max][INNER]=1;//added later
	//pinlocalization[LRCEPI_I][PIN3max][UPPER]=1;//added later

	pinlocalization[CORTEXENDO_I][PIN3max][LOWER]=1;
	//pinlocalization[CORTEXENDO_I][PIN3max][OUTER]=1;
	//pinlocalization[CORTEXENDO_I][PIN3max][INNER]=1;//added later
	//pinlocalization[CORTEXENDO_I][PIN3max][UPPER]=1;//added later




	//PIN7


	pinlocalization[VASCULAR][PIN7max][LOWER]=1;
	pinlocalization[VASCULAR_TZ][PIN7max][LOWER]=1;
	pinlocalization[VASCULAR_EZ][PIN7max][LOWER]=1;
	pinlocalization[VASCULAR_D][PIN7max][LOWER]=1;

	pinlocalization[PERICYCLE][PIN7max][LOWER]=1;
	//pinlocalization[PERICYCLE][PIN7max][OUTER]=1;

	pinlocalization[PERICYCLE_TZ][PIN7max][LOWER]=1;
	//pinlocalization[PERICYCLE_TZ][PIN7max][OUTER]=1;

	pinlocalization[PERICYCLE_EZ][PIN7max][LOWER]=1;
	//pinlocalization[PERICYCLE_EZ][PIN7max][OUTER]=1;

	pinlocalization[PERICYCLE_D][PIN7max][LOWER]=1;
	//pinlocalization[PERICYCLE_D][PIN7max][OUTER]=1;

	pinlocalization[ENDODERMIS][PIN7max][LOWER]=1;
	pinlocalization[ENDODERMIS_TZ][PIN7max][LOWER]=1;
	pinlocalization[ENDODERMIS_EZ][PIN7max][LOWER]=1;
	pinlocalization[ENDODERMIS_D][PIN7max][LOWER]=1;

	//pinlocalization[COLUMELLAROOT][PIN7max][UPPER]=1;
	pinlocalization[COLUMELLAROOT][PIN7max][LOWER]=1;
	//pinlocalization[COLUMELLAROOT][PIN7max][INNER]=1;
	//pinlocalization[COLUMELLAROOT][PIN7max][OUTER]=1;

	//pinlocalization[LRCEPI_I][PIN7max][LOWER]=1;
	pinlocalization[LRCEPI_I][PIN7max][OUTER]=1;

	pinlocalization[CORTEXENDO_I][PIN7max][LOWER]=1;
	//pinlocalization[CORTEXENDO_I][PIN7max][OUTER]=1;



	//PIN2

	//pinlocalization[CORTEX][PIN2max][INNER]=1;
	pinlocalization[CORTEX][PIN2max][LOWER]=1;
	//pinlocalization[CORTEX][PIN2max][OUTER]=1;

	//pinlocalization[CORTEX_TZ][PIN2max][INNER]=1;
	//pinlocalization[CORTEX_TZ][PIN2max][UPPER]=1;
	//pinlocalization[CORTEX_TZ][PIN2max][OUTER]=1;

	//pinlocalization[CORTEX_EZ][PIN2max][INNER]=1;
	//pinlocalization[CORTEX_EZ][PIN2max][UPPER]=1;
	//pinlocalization[CORTEX_EZ][PIN2max][OUTER]=1;

	//pinlocalization[CORTEX_D][PIN2max][INNER]=1;
	//pinlocalization[CORTEX_D][PIN2max][UPPER]=1;
	//pinlocalization[CORTEX_D][PIN2max][OUTER]=1;

	//pinlocalization[EPIDERMIS][PIN2max][UPPER]=1;
	//pinlocalization[EPIDERMIS][PIN2max][INNER]=1;

	//pinlocalization[EPIDERMIS_TZ][PIN2max][UPPER]=1;
	//pinlocalization[EPIDERMIS_TZ][PIN2max][INNER]=1;

	//pinlocalization[EPIDERMIS_EZ][PIN2max][UPPER]=1;
	//pinlocalization[EPIDERMIS_D][PIN2max][UPPER]=1;

	//pinlocalization[LRC][PIN2max][UPPER]=1;
	//pinlocalization[LRC][PIN2max][INNER]=1;

	//the following ones are specifically added in PINunifpolandstrength
	pinlocalization[CORTEX_TZ][PIN2max][LOWER]=1;

	pinlocalization[CORTEX_EZ][PIN2max][LOWER]=1;

	pinlocalization[CORTEX_D][PIN2max][LOWER]=1;

	pinlocalization[EPIDERMIS][PIN2max][LOWER]=1;//changed

	pinlocalization[EPIDERMIS_TZ][PIN2max][LOWER]=1;

	pinlocalization[EPIDERMIS_EZ][PIN2max][LOWER]=1;
	pinlocalization[EPIDERMIS_D][PIN2max][LOWER]=1;

	pinlocalization[LRC][PIN2max][LOWER]=1;
      }
      if (PINMZTZ) {// another `old' null model, not used anymore
	pinlocalization[VASCULAR][PIN1max][LOWER]=1;
	pinlocalization[VASCULAR_TZ][PIN1max][LOWER]=1;
	pinlocalization[VASCULAR_EZ][PIN1quarter][LOWER]=1;
	pinlocalization[VASCULAR_D][PIN1quarter][LOWER]=1;

	pinlocalization[PERICYCLE][PIN1half][LOWER]=1;
	pinlocalization[PERICYCLE][PIN1half][OUTER]=1;

	pinlocalization[PERICYCLE_TZ][PIN1half][LOWER]=1;
	pinlocalization[PERICYCLE_TZ][PIN1half][OUTER]=1;

	pinlocalization[PERICYCLE_EZ][PIN1sixth][LOWER]=1;
	pinlocalization[PERICYCLE_EZ][PIN1sixth][OUTER]=1;

	pinlocalization[PERICYCLE_D][PIN1sixth][LOWER]=1;
	pinlocalization[PERICYCLE_D][PIN1sixth][OUTER]=1;

	pinlocalization[ENDODERMIS][PIN1fifth][LOWER]=1;
	pinlocalization[ENDODERMIS_TZ][PIN1fifth][LOWER]=1;
	pinlocalization[ENDODERMIS_EZ][PIN1min][LOWER]=1;
	pinlocalization[ENDODERMIS_D][PIN1min][LOWER]=1;

	pinlocalization[CORTEXENDO_I][PIN1quarter][LOWER]=1;
	pinlocalization[CORTEXENDO_I][PIN1quarter][OUTER]=1;
	

	//PIN3


	pinlocalization[VASCULAR][PIN3half][LOWER]=1;
	pinlocalization[VASCULAR_TZ][PIN3half][LOWER]=1;
	pinlocalization[VASCULAR_EZ][PIN3half][LOWER]=1;
	pinlocalization[VASCULAR_D][PIN3half][LOWER]=1;

	pinlocalization[PERICYCLE][PIN3half][LOWER]=1;
	pinlocalization[PERICYCLE][PIN3half][OUTER]=1;

	pinlocalization[PERICYCLE_TZ][PIN3half][LOWER]=1;
	pinlocalization[PERICYCLE_TZ][PIN3half][OUTER]=1;

	pinlocalization[PERICYCLE_EZ][PIN3half][LOWER]=1;
	pinlocalization[PERICYCLE_EZ][PIN3half][OUTER]=1;

	pinlocalization[PERICYCLE_D][PIN3half][LOWER]=1;
	pinlocalization[PERICYCLE_D][PIN3half][OUTER]=1;


	//the lateral flux in MZ ENDODERMIS we keep the same,to have comparable capacitor

	pinlocalization[ENDODERMIS][PIN3sixth][LOWER]=1;
	pinlocalization[ENDODERMIS_TZ][PIN3sixth][LOWER]=1;
	pinlocalization[ENDODERMIS_EZ][PIN3sixth][LOWER]=1;
	pinlocalization[ENDODERMIS_D][PIN3sixth][LOWER]=1;


	pinlocalization[QCROOT][PIN3half][LOWER]=1;

	pinlocalization[COLUMELLAROOT][PIN3max][UPPER]=1;
	pinlocalization[COLUMELLAROOT][PIN3max][LOWER]=1;
	pinlocalization[COLUMELLAROOT][PIN3max][INNER]=1;
	pinlocalization[COLUMELLAROOT][PIN3max][OUTER]=1;

	//only PIN3 is expressed in COL_I

	pinlocalization[COL_I][PIN3max][UPPER]=1;
	pinlocalization[COL_I][PIN3max][LOWER]=1;
	pinlocalization[COL_I][PIN3max][INNER]=1;
	pinlocalization[COL_I][PIN3max][OUTER]=1;

	pinlocalization[LRCEPI_I][PIN3third][LOWER]=1;
	pinlocalization[LRCEPI_I][PIN3third][OUTER]=1;
	pinlocalization[LRCEPI_I][PIN3third][INNER]=1;//added later
	pinlocalization[LRCEPI_I][PIN3third][UPPER]=1;//added later

	pinlocalization[CORTEXENDO_I][PIN3third][LOWER]=1;
	pinlocalization[CORTEXENDO_I][PIN3third][OUTER]=1;
	pinlocalization[CORTEXENDO_I][PIN3third][INNER]=1;//added later
	pinlocalization[CORTEXENDO_I][PIN3third][UPPER]=1;//added later




	//PIN7


	pinlocalization[VASCULAR][PIN7max][LOWER]=1;
	pinlocalization[VASCULAR_TZ][PIN7max][LOWER]=1;
	pinlocalization[VASCULAR_EZ][PIN7half][LOWER]=1;
	pinlocalization[VASCULAR_D][PIN7half][LOWER]=1;

	pinlocalization[PERICYCLE][PIN7half][LOWER]=1;
	pinlocalization[PERICYCLE][PIN7half][OUTER]=1;

	pinlocalization[PERICYCLE_TZ][PIN7half][LOWER]=1;
	pinlocalization[PERICYCLE_TZ][PIN7half][OUTER]=1;

	pinlocalization[PERICYCLE_EZ][PIN7quarter][LOWER]=1;
	pinlocalization[PERICYCLE_EZ][PIN7quarter][OUTER]=1;

	pinlocalization[PERICYCLE_D][PIN7quarter][LOWER]=1;
	pinlocalization[PERICYCLE_D][PIN7quarter][OUTER]=1;

	pinlocalization[ENDODERMIS][PIN7sixth][LOWER]=1;
	pinlocalization[ENDODERMIS_TZ][PIN7sixth][LOWER]=1;
	pinlocalization[ENDODERMIS_EZ][PIN7min][LOWER]=1;
	pinlocalization[ENDODERMIS_D][PIN7min][LOWER]=1;

	pinlocalization[COLUMELLAROOT][PIN7quarter][UPPER]=1;
	pinlocalization[COLUMELLAROOT][PIN7quarter][LOWER]=1;
	pinlocalization[COLUMELLAROOT][PIN7quarter][INNER]=1;
	pinlocalization[COLUMELLAROOT][PIN7quarter][OUTER]=1;


	pinlocalization[LRCEPI_I][PIN7fifth][LOWER]=1;
	pinlocalization[LRCEPI_I][PIN7fifth][OUTER]=1;

	pinlocalization[CORTEXENDO_I][PIN7fifth][LOWER]=1;
	pinlocalization[CORTEXENDO_I][PIN7fifth][OUTER]=1;



	//PIN2 strong as PIN137

	pinlocalization[CORTEX][PIN2half][INNER]=1;
	pinlocalization[CORTEX][PIN2max][LOWER]=1;
	pinlocalization[CORTEX][PIN2half][OUTER]=1;

	pinlocalization[CORTEX_TZ][PIN2half][INNER]=1;
	if(no_pin2_flip)
	  pinlocalization[CORTEX_TZ][PIN2max][LOWER]=1;
	else
	  pinlocalization[CORTEX_TZ][PIN2max][UPPER]=1;
	pinlocalization[CORTEX_TZ][PIN2half][OUTER]=1;


	pinlocalization[CORTEX_EZ][PIN2quarter][INNER]=1;
	if(no_pin2_flip)
	  pinlocalization[CORTEX_EZ][PIN2third][LOWER]=1;
	else
	  pinlocalization[CORTEX_EZ][PIN2third][UPPER]=1;
	pinlocalization[CORTEX_EZ][PIN2quarter][OUTER]=1;

	pinlocalization[CORTEX_D][PIN2quarter][INNER]=1;
	if(no_pin2_flip)
	  pinlocalization[CORTEX_D][PIN2third][LOWER]=1;
	else	
	  pinlocalization[CORTEX_D][PIN2third][UPPER]=1;
	pinlocalization[CORTEX_D][PIN2quarter][OUTER]=1;

	pinlocalization[EPIDERMIS][PIN2max][UPPER]=1;
	pinlocalization[EPIDERMIS][PIN2max][INNER]=1;
	pinlocalization[EPIDERMIS_TZ][PIN2max][UPPER]=1;
	pinlocalization[EPIDERMIS_TZ][PIN2max][INNER]=1;

	pinlocalization[EPIDERMIS_EZ][PIN2quarter][UPPER]=1;
	pinlocalization[EPIDERMIS_D][PIN2quarter][UPPER]=1;

	pinlocalization[LRC][PIN2half][UPPER]=1;
	pinlocalization[LRC][PIN2half][INNER]=1;
      }
    
      if (version2==1) {//standard model of the first main series of simulations

	if(null_pin_intensity && !null_pin_localization) {
	  pinlocalization[VASCULAR][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN1max][LOWER]=1;

	  if(equalstrengthPERIVASC) {
	    pinlocalization[PERICYCLE][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN1max][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN1max][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN1max][OUTER]=1;

	    pinlocalization[PERICYCLE_D][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN1max][OUTER]=1;
	  }
	  else {
	    pinlocalization[PERICYCLE][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN1half][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN1half][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN1half][OUTER]=1;

	    pinlocalization[PERICYCLE_D][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN1half][OUTER]=1;
	  }

	  pinlocalization[ENDODERMIS][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1fifth][LOWER]=1;

	  pinlocalization[CORTEXENDO_I][PIN1quarter][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN1quarter][OUTER]=1;
	

	  //PIN3


	  pinlocalization[VASCULAR][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN3half][LOWER]=1;

	  pinlocalization[PERICYCLE][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE][PIN3half][OUTER]=1;

	  pinlocalization[PERICYCLE_TZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][PIN3half][OUTER]=1;

	  pinlocalization[PERICYCLE_EZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][PIN3half][OUTER]=1;

	  pinlocalization[PERICYCLE_D][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_D][PIN3half][OUTER]=1;

	  pinlocalization[ENDODERMIS][PIN3sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3sixth][LOWER]=1;


	  pinlocalization[QCROOT][PIN3half][LOWER]=1;

	  pinlocalization[COLUMELLAROOT][PIN3max][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][OUTER]=1;

	  //only PIN3 is expressed in COL_I

	  pinlocalization[COL_I][PIN3max][UPPER]=1;
	  pinlocalization[COL_I][PIN3max][LOWER]=1;
	  pinlocalization[COL_I][PIN3max][INNER]=1;
	  pinlocalization[COL_I][PIN3max][OUTER]=1;

	  pinlocalization[LRCEPI_I][PIN3third][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN3third][OUTER]=1;
	  pinlocalization[LRCEPI_I][PIN3third][INNER]=1;//added later
	  pinlocalization[LRCEPI_I][PIN3third][UPPER]=1;//added later

	  pinlocalization[CORTEXENDO_I][PIN3third][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3third][OUTER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3third][INNER]=1;//added later
	  pinlocalization[CORTEXENDO_I][PIN3third][UPPER]=1;//added later




	  //PIN7


	  pinlocalization[VASCULAR][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN7max][LOWER]=1;

	  if(equalstrengthPERIVASC) {
	    pinlocalization[PERICYCLE][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN7max][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN7max][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN7max][OUTER]=1;
	    
	    pinlocalization[PERICYCLE_D][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN7max][OUTER]=1;
	  }
	  else {
	    pinlocalization[PERICYCLE][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN7half][OUTER]=1;
	    
	    pinlocalization[PERICYCLE_D][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN7half][OUTER]=1;
	  }

	  pinlocalization[ENDODERMIS][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7sixth][LOWER]=1;

	  pinlocalization[COLUMELLAROOT][PIN7quarter][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][OUTER]=1;


	  pinlocalization[LRCEPI_I][PIN7fifth][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN7fifth][OUTER]=1;

	  pinlocalization[CORTEXENDO_I][PIN7fifth][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN7fifth][OUTER]=1;



	  //PIN2

	  pinlocalization[CORTEX][PIN2half][INNER]=1;
	  pinlocalization[CORTEX][PIN2max][LOWER]=1;
	  pinlocalization[CORTEX][PIN2half][OUTER]=1;

	  pinlocalization[CORTEX_TZ][PIN2half][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_TZ][PIN2max][LOWER]=1;
	  else
	    pinlocalization[CORTEX_TZ][PIN2max][UPPER]=1;
	  pinlocalization[CORTEX_TZ][PIN2half][OUTER]=1;

	  pinlocalization[CORTEX_EZ][PIN2half][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_EZ][PIN2max][LOWER]=1;
	  else
	    pinlocalization[CORTEX_EZ][PIN2max][UPPER]=1;
	  pinlocalization[CORTEX_EZ][PIN2half][OUTER]=1;

	  pinlocalization[CORTEX_D][PIN2half][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_D][PIN2max][LOWER]=1;
	  else	
	    pinlocalization[CORTEX_D][PIN2max][UPPER]=1;
	  pinlocalization[CORTEX_D][PIN2half][OUTER]=1;

	  pinlocalization[EPIDERMIS][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_TZ][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS_TZ][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_EZ][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS_D][PIN2max][UPPER]=1;

	  pinlocalization[LRC][PIN2half][UPPER]=1;
	  pinlocalization[LRC][PIN2half][INNER]=1;
	}

	else if(null_pin_localization && !null_pin_intensity) {
	  pinlocalization[VASCULAR][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN1half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN1quarter][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN1quarter][LOWER]=1;

	  if(equalstrengthPERIVASC) {
	    pinlocalization[PERICYCLE][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN1max][OUTER]=1;
	    
	    pinlocalization[PERICYCLE_TZ][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN1half][OUTER]=1;
	    
	    pinlocalization[PERICYCLE_EZ][PIN1quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN1quarter][OUTER]=1;
	    
	    pinlocalization[PERICYCLE_D][PIN1quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN1quarter][OUTER]=1;
	  }
	  else {
	    pinlocalization[PERICYCLE][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN1half][OUTER]=1;
	    
	    pinlocalization[PERICYCLE_TZ][PIN1quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN1quarter][OUTER]=1;
	    
	    pinlocalization[PERICYCLE_EZ][PIN1sixth][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN1sixth][OUTER]=1;
	    
	    pinlocalization[PERICYCLE_D][PIN1sixth][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN1sixth][OUTER]=1;
	  }

	  pinlocalization[ENDODERMIS][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1min][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1min][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1min][LOWER]=1;

	  pinlocalization[CORTEXENDO_I][PIN1quarter][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN1quarter][OUTER]=1;
	

	  //PIN3


	  pinlocalization[VASCULAR][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN3half][LOWER]=1;

	  pinlocalization[PERICYCLE][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE][PIN3half][OUTER]=1;

	  pinlocalization[PERICYCLE_TZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][PIN3half][OUTER]=1;

	  pinlocalization[PERICYCLE_EZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][PIN3half][OUTER]=1;

	  pinlocalization[PERICYCLE_D][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_D][PIN3half][OUTER]=1;

	  pinlocalization[ENDODERMIS][PIN3sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3sixth][LOWER]=1;


	  pinlocalization[QCROOT][PIN3half][LOWER]=1;

	  pinlocalization[COLUMELLAROOT][PIN3max][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][OUTER]=1;

	  //only PIN3 is expressed in COL_I

	  pinlocalization[COL_I][PIN3max][UPPER]=1;
	  pinlocalization[COL_I][PIN3max][LOWER]=1;
	  pinlocalization[COL_I][PIN3max][INNER]=1;
	  pinlocalization[COL_I][PIN3max][OUTER]=1;

	  pinlocalization[LRCEPI_I][PIN3third][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN3third][OUTER]=1;
	  pinlocalization[LRCEPI_I][PIN3third][INNER]=1;//added later
	  pinlocalization[LRCEPI_I][PIN3third][UPPER]=1;//added later

	  pinlocalization[CORTEXENDO_I][PIN3third][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3third][OUTER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3third][INNER]=1;//added later
	  pinlocalization[CORTEXENDO_I][PIN3third][UPPER]=1;//added later




	  //PIN7


	  pinlocalization[VASCULAR][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN7half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN7half][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN7half][LOWER]=1;

	  if(equalstrengthPERIVASC) {
	    pinlocalization[PERICYCLE][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN7max][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_D][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN7half][OUTER]=1;
	  }
	  else {
	    pinlocalization[PERICYCLE][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN7quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN7quarter][OUTER]=1;

	    pinlocalization[PERICYCLE_D][PIN7quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN7quarter][OUTER]=1;
	  }

	  pinlocalization[ENDODERMIS][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7min][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7min][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7min][LOWER]=1;

	  pinlocalization[COLUMELLAROOT][PIN7quarter][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][OUTER]=1;


	  pinlocalization[LRCEPI_I][PIN7fifth][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN7fifth][OUTER]=1;

	  pinlocalization[CORTEXENDO_I][PIN7fifth][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN7fifth][OUTER]=1;



	  //PIN2

	  pinlocalization[CORTEX][PIN2half][INNER]=1;
	  pinlocalization[CORTEX][PIN2max][LOWER]=1;
	  pinlocalization[CORTEX][PIN2half][OUTER]=1;

	  pinlocalization[CORTEX_TZ][PIN2third][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_TZ][PIN2half][LOWER]=1;
	  else
	    pinlocalization[CORTEX_TZ][PIN2half][UPPER]=1;
	  pinlocalization[CORTEX_TZ][PIN2third][OUTER]=1;

	  pinlocalization[CORTEX_EZ][PIN2quarter][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_EZ][PIN2third][LOWER]=1;
	  else
	    pinlocalization[CORTEX_EZ][PIN2third][UPPER]=1;
	  pinlocalization[CORTEX_EZ][PIN2quarter][OUTER]=1;

	  pinlocalization[CORTEX_D][PIN2quarter][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_D][PIN2third][LOWER]=1;
	  else	
	    pinlocalization[CORTEX_D][PIN2third][UPPER]=1;
	  pinlocalization[CORTEX_D][PIN2quarter][OUTER]=1;

	  pinlocalization[EPIDERMIS][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_TZ][PIN2half][UPPER]=1;
	  pinlocalization[EPIDERMIS_TZ][PIN2half][INNER]=1;

	  pinlocalization[EPIDERMIS_EZ][PIN2quarter][UPPER]=1;
	  //only two changes
	  pinlocalization[EPIDERMIS_EZ][PIN2quarter][INNER]=1;

	  pinlocalization[EPIDERMIS_D][PIN2quarter][UPPER]=1;
	  //only two changes
	  pinlocalization[EPIDERMIS_D][PIN2quarter][INNER]=1;

	  pinlocalization[LRC][PIN2half][UPPER]=1;
	  pinlocalization[LRC][PIN2half][INNER]=1;

	}
	else if (null_pin_localization && null_pin_intensity) {
	  pinlocalization[VASCULAR][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN1max][LOWER]=1;

	  if(equalstrengthPERIVASC) {
	    pinlocalization[PERICYCLE][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN1max][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN1max][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN1max][OUTER]=1;

	    pinlocalization[PERICYCLE_D][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN1max][OUTER]=1;
	  }
	  else {
	    pinlocalization[PERICYCLE][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN1half][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN1half][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN1half][OUTER]=1;

	    pinlocalization[PERICYCLE_D][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN1half][OUTER]=1;
	  }

	  pinlocalization[ENDODERMIS][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1fifth][LOWER]=1;

	  pinlocalization[CORTEXENDO_I][PIN1quarter][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN1quarter][OUTER]=1;
	

	  //PIN3


	  pinlocalization[VASCULAR][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN3half][LOWER]=1;

	  pinlocalization[PERICYCLE][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE][PIN3half][OUTER]=1;

	  pinlocalization[PERICYCLE_TZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][PIN3half][OUTER]=1;

	  pinlocalization[PERICYCLE_EZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][PIN3half][OUTER]=1;

	  pinlocalization[PERICYCLE_D][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_D][PIN3half][OUTER]=1;

	  pinlocalization[ENDODERMIS][PIN3sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3sixth][LOWER]=1;


	  pinlocalization[QCROOT][PIN3half][LOWER]=1;

	  pinlocalization[COLUMELLAROOT][PIN3max][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][OUTER]=1;

	  //only PIN3 is expressed in COL_I

	  pinlocalization[COL_I][PIN3max][UPPER]=1;
	  pinlocalization[COL_I][PIN3max][LOWER]=1;
	  pinlocalization[COL_I][PIN3max][INNER]=1;
	  pinlocalization[COL_I][PIN3max][OUTER]=1;

	  pinlocalization[LRCEPI_I][PIN3third][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN3third][OUTER]=1;
	  pinlocalization[LRCEPI_I][PIN3third][INNER]=1;//added later
	  pinlocalization[LRCEPI_I][PIN3third][UPPER]=1;//added later

	  pinlocalization[CORTEXENDO_I][PIN3third][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3third][OUTER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3third][INNER]=1;//added later
	  pinlocalization[CORTEXENDO_I][PIN3third][UPPER]=1;//added later




	  //PIN7


	  pinlocalization[VASCULAR][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN7max][LOWER]=1;

	  if(equalstrengthPERIVASC) {
	    pinlocalization[PERICYCLE][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN7max][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN7max][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN7max][OUTER]=1;

	    pinlocalization[PERICYCLE_D][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN7max][OUTER]=1;
	  }
	  else {
	    pinlocalization[PERICYCLE][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_D][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN7half][OUTER]=1;
	  }

	  pinlocalization[ENDODERMIS][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7sixth][LOWER]=1;

	  pinlocalization[COLUMELLAROOT][PIN7quarter][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][OUTER]=1;


	  pinlocalization[LRCEPI_I][PIN7fifth][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN7fifth][OUTER]=1;

	  pinlocalization[CORTEXENDO_I][PIN7fifth][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN7fifth][OUTER]=1;



	  //PIN2

	  pinlocalization[CORTEX][PIN2half][INNER]=1;
	  pinlocalization[CORTEX][PIN2max][LOWER]=1;
	  pinlocalization[CORTEX][PIN2half][OUTER]=1;

	  pinlocalization[CORTEX_TZ][PIN2half][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_TZ][PIN2max][LOWER]=1;
	  else
	    pinlocalization[CORTEX_TZ][PIN2max][UPPER]=1;
	  pinlocalization[CORTEX_TZ][PIN2half][OUTER]=1;

	  pinlocalization[CORTEX_EZ][PIN2half][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_EZ][PIN2max][LOWER]=1;
	  else
	    pinlocalization[CORTEX_EZ][PIN2max][UPPER]=1;
	  pinlocalization[CORTEX_EZ][PIN2half][OUTER]=1;

	  pinlocalization[CORTEX_D][PIN2half][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_D][PIN2max][LOWER]=1;
	  else	
	    pinlocalization[CORTEX_D][PIN2max][UPPER]=1;
	  pinlocalization[CORTEX_D][PIN2half][OUTER]=1;

	  pinlocalization[EPIDERMIS][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_TZ][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS_TZ][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_EZ][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS_EZ][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_D][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS_D][PIN2max][INNER]=1;

	  pinlocalization[LRC][PIN2half][UPPER]=1;
	  pinlocalization[LRC][PIN2half][INNER]=1;
	}
 
	else { //default case for version2=1
	  pinlocalization[VASCULAR][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN1half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN1quarter][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN1quarter][LOWER]=1;

	  if(equalstrengthPERIVASC) {
	    pinlocalization[PERICYCLE][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN1max][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN1half][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN1quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN1quarter][OUTER]=1;

	    pinlocalization[PERICYCLE_D][PIN1quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN1quarter][OUTER]=1;
	  }
	  else {
	    pinlocalization[PERICYCLE][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN1half][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN1quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN1quarter][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN1sixth][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN1sixth][OUTER]=1;

	    pinlocalization[PERICYCLE_D][PIN1sixth][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN1sixth][OUTER]=1;
	  }

	  pinlocalization[ENDODERMIS][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1min][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1min][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1min][LOWER]=1;

	  pinlocalization[CORTEXENDO_I][PIN1quarter][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN1quarter][OUTER]=1;
	

	  //PIN3


	  pinlocalization[VASCULAR][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN3half][LOWER]=1;

	  pinlocalization[PERICYCLE][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE][PIN3half][OUTER]=1;

	  pinlocalization[PERICYCLE_TZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][PIN3half][OUTER]=1;

	  pinlocalization[PERICYCLE_EZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][PIN3half][OUTER]=1;

	  pinlocalization[PERICYCLE_D][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_D][PIN3half][OUTER]=1;

	  pinlocalization[ENDODERMIS][PIN3sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3sixth][LOWER]=1;


	  pinlocalization[QCROOT][PIN3half][LOWER]=1;

	  pinlocalization[COLUMELLAROOT][PIN3max][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][OUTER]=1;

	  //only PIN3 is expressed in COL_I

	  pinlocalization[COL_I][PIN3max][UPPER]=1;
	  pinlocalization[COL_I][PIN3max][LOWER]=1;
	  pinlocalization[COL_I][PIN3max][INNER]=1;
	  pinlocalization[COL_I][PIN3max][OUTER]=1;

	  pinlocalization[LRCEPI_I][PIN3third][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN3third][OUTER]=1;
	  pinlocalization[LRCEPI_I][PIN3third][INNER]=1;//added later
	  pinlocalization[LRCEPI_I][PIN3third][UPPER]=1;//added later

	  pinlocalization[CORTEXENDO_I][PIN3third][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3third][OUTER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3third][INNER]=1;//added later
	  pinlocalization[CORTEXENDO_I][PIN3third][UPPER]=1;//added later




	  //PIN7


	  pinlocalization[VASCULAR][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN7half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN7half][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN7half][LOWER]=1;


	  if(equalstrengthPERIVASC) {
	    pinlocalization[PERICYCLE][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN7max][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_D][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN7half][OUTER]=1;
	  }
	  else {
	    pinlocalization[PERICYCLE][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_TZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN7half][OUTER]=1;

	    pinlocalization[PERICYCLE_EZ][PIN7quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN7quarter][OUTER]=1;

	    pinlocalization[PERICYCLE_D][PIN7quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN7quarter][OUTER]=1;
	  }

	  pinlocalization[ENDODERMIS][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7min][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7min][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7min][LOWER]=1;

	  pinlocalization[COLUMELLAROOT][PIN7quarter][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][OUTER]=1;


	  pinlocalization[LRCEPI_I][PIN7fifth][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN7fifth][OUTER]=1;

	  pinlocalization[CORTEXENDO_I][PIN7fifth][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN7fifth][OUTER]=1;



	  //PIN2

	  pinlocalization[CORTEX][PIN2half][INNER]=1;
	  pinlocalization[CORTEX][PIN2max][LOWER]=1;
	  pinlocalization[CORTEX][PIN2half][OUTER]=1;

	  pinlocalization[CORTEX_TZ][PIN2third][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_TZ][PIN2half][LOWER]=1;
	  else
	    pinlocalization[CORTEX_TZ][PIN2half][UPPER]=1;
	  pinlocalization[CORTEX_TZ][PIN2third][OUTER]=1;

	  pinlocalization[CORTEX_EZ][PIN2quarter][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_EZ][PIN2third][LOWER]=1;
	  else
	    pinlocalization[CORTEX_EZ][PIN2third][UPPER]=1;
	  pinlocalization[CORTEX_EZ][PIN2quarter][OUTER]=1;

	  pinlocalization[CORTEX_D][PIN2quarter][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_D][PIN2third][LOWER]=1;
	  else	
	    pinlocalization[CORTEX_D][PIN2third][UPPER]=1;
	  pinlocalization[CORTEX_D][PIN2quarter][OUTER]=1;

	  pinlocalization[EPIDERMIS][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_TZ][PIN2half][UPPER]=1;
	  pinlocalization[EPIDERMIS_TZ][PIN2half][INNER]=1;

	  pinlocalization[EPIDERMIS_EZ][PIN2quarter][UPPER]=1;
	  pinlocalization[EPIDERMIS_D][PIN2quarter][UPPER]=1;

	  pinlocalization[LRC][PIN2half][UPPER]=1;
	  pinlocalization[LRC][PIN2half][INNER]=1;
	}
      }
      /*
	version2==2 contains the following modifications:
	1) no outwards directed PINs in pericycle
	2) endodermal pins on all sides with same strength in TZ EZ DZ
	the differences can be appreciated by comparing pinold and pinnew
       */
      else if (version2==2) {
	if(null_pin_intensity && !null_pin_localization) {
	

	  //PIN1


	  pinlocalization[VASCULAR][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN1max][LOWER]=1;

	  pinlocalization[PERICYCLE][PIN1max][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][PIN1max][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][PIN1max][LOWER]=1;
	  pinlocalization[PERICYCLE_D][PIN1max][LOWER]=1;
	  
	  pinlocalization[ENDODERMIS][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1fifth][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN1fifth][INNER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1fifth][INNER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1fifth][INNER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1fifth][INNER]=1;

	  pinlocalization[ENDODERMIS_TZ][PIN1min][OUTER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1min][OUTER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1min][OUTER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1min][UPPER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1min][UPPER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1min][UPPER]=1;

	  pinlocalization[LRCEPI_I][PIN1quarter][INNER]=1;
	  pinlocalization[CORTEXENDO_I][PIN1quarter][LOWER]=1;
	

	  //PIN3


	  pinlocalization[VASCULAR][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN3half][LOWER]=1;

	  pinlocalization[PERICYCLE][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_D][PIN3half][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN3half][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3half][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3half][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3half][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN3half][INNER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3half][INNER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3half][INNER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3half][INNER]=1;

	  pinlocalization[ENDODERMIS_TZ][PIN3min][OUTER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3min][OUTER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3min][OUTER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3min][UPPER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3min][UPPER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3min][UPPER]=1;

	  pinlocalization[QCROOT][PIN3half][LOWER]=1;

	  pinlocalization[COLUMELLAROOT][PIN3max][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][OUTER]=1;

	  //only PIN3 is expressed in COL_I

	  pinlocalization[COL_I][PIN3max][UPPER]=1;
	  pinlocalization[COL_I][PIN3max][LOWER]=1;
	  pinlocalization[COL_I][PIN3max][INNER]=1;
	  pinlocalization[COL_I][PIN3max][OUTER]=1;

	  pinlocalization[LRCEPI_I][PIN3sixth][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN3sixth][OUTER]=1;
	  pinlocalization[LRCEPI_I][PIN3sixth][INNER]=1;//added later
	  pinlocalization[LRCEPI_I][PIN3sixth][UPPER]=1;//added later

	  pinlocalization[CORTEXENDO_I][PIN3sixth][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3sixth][OUTER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3sixth][INNER]=1;//added later
	  pinlocalization[CORTEXENDO_I][PIN3sixth][UPPER]=1;//added later


	  //PIN7


	  pinlocalization[VASCULAR][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN7max][LOWER]=1;

	  pinlocalization[PERICYCLE][PIN7max][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][PIN7max][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][PIN7max][LOWER]=1;
	  pinlocalization[PERICYCLE_D][PIN7max][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7sixth][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN7sixth][INNER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7sixth][INNER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7sixth][INNER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7sixth][INNER]=1;

	  pinlocalization[ENDODERMIS_TZ][PIN7min][OUTER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7min][OUTER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7min][OUTER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7min][UPPER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7min][UPPER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7min][UPPER]=1;

	  pinlocalization[COLUMELLAROOT][PIN7quarter][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][OUTER]=1;

	  pinlocalization[LRCEPI_I][PIN7fifth][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN7fifth][INNER]=1;

	  pinlocalization[CORTEXENDO_I][PIN7fifth][LOWER]=1;


	  //PIN2


	  pinlocalization[CORTEX][PIN2half][INNER]=1;
	  pinlocalization[CORTEX][PIN2max][LOWER]=1;

	  pinlocalization[CORTEX_TZ][PIN2half][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_TZ][PIN2max][LOWER]=1;
	  else
	    pinlocalization[CORTEX_TZ][PIN2max][UPPER]=1;
	  pinlocalization[CORTEX_TZ][PIN2third][OUTER]=1;

	  pinlocalization[CORTEX_EZ][PIN2half][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_EZ][PIN2max][LOWER]=1;
	  else
	    pinlocalization[CORTEX_EZ][PIN2max][UPPER]=1;
	  pinlocalization[CORTEX_EZ][PIN2quarter][OUTER]=1;

	  pinlocalization[CORTEX_D][PIN2half][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_D][PIN2max][LOWER]=1;
	  else	
	    pinlocalization[CORTEX_D][PIN2max][UPPER]=1;
	  pinlocalization[CORTEX_D][PIN2quarter][OUTER]=1;

	  pinlocalization[EPIDERMIS][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_TZ][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS_TZ][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_EZ][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS_D][PIN2max][UPPER]=1;

	  pinlocalization[LRC][PIN2half][UPPER]=1;
	  pinlocalization[LRC][PIN2half][INNER]=1;
	}

	else if(null_pin_localization && !null_pin_intensity) {
	  	

	  //PIN1


	  pinlocalization[VASCULAR][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN1half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN1quarter][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN1quarter][LOWER]=1;

	  if(equalstrengthPERIVASC) {
	    pinlocalization[PERICYCLE][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN1quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN1quarter][LOWER]=1;
	  }
	  else {
	    pinlocalization[PERICYCLE][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN1quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN1sixth][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN1sixth][LOWER]=1;
	  }

	  pinlocalization[ENDODERMIS][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1min][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1min][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1min][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN1fifth][INNER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1min][INNER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1min][INNER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1min][INNER]=1;

	  pinlocalization[LRCEPI_I][PIN1quarter][INNER]=1;
	  pinlocalization[CORTEXENDO_I][PIN1quarter][LOWER]=1;
	

	  //PIN3


	  pinlocalization[VASCULAR][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN3half][LOWER]=1;

	  pinlocalization[PERICYCLE][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_D][PIN3half][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN3half][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3min][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3min][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3min][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN3half][INNER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3min][INNER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3min][INNER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3min][INNER]=1;

	  pinlocalization[QCROOT][PIN3half][LOWER]=1;

	  pinlocalization[COLUMELLAROOT][PIN3max][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][OUTER]=1;

	  //only PIN3 is expressed in COL_I

	  pinlocalization[COL_I][PIN3max][UPPER]=1;
	  pinlocalization[COL_I][PIN3max][LOWER]=1;
	  pinlocalization[COL_I][PIN3max][INNER]=1;
	  pinlocalization[COL_I][PIN3max][OUTER]=1;

	  pinlocalization[LRCEPI_I][PIN3sixth][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN3sixth][OUTER]=1;
	  pinlocalization[LRCEPI_I][PIN3sixth][INNER]=1;//added later
	  pinlocalization[LRCEPI_I][PIN3sixth][UPPER]=1;//added later

	  pinlocalization[CORTEXENDO_I][PIN3sixth][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3sixth][OUTER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3sixth][INNER]=1;//added later
	  pinlocalization[CORTEXENDO_I][PIN3sixth][UPPER]=1;//added later


	  //PIN7


	  pinlocalization[VASCULAR][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN7half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN7half][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN7half][LOWER]=1;

	  if(equalstrengthPERIVASC) {
	    pinlocalization[PERICYCLE][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN7half][LOWER]=1;
	  }
	  else {
	    pinlocalization[PERICYCLE][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN7quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN7quarter][LOWER]=1;
	  }

	  pinlocalization[ENDODERMIS][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7min][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7min][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7min][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN7sixth][INNER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7min][INNER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7min][INNER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7min][INNER]=1;

	  pinlocalization[COLUMELLAROOT][PIN7quarter][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][OUTER]=1;

	  pinlocalization[LRCEPI_I][PIN7fifth][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN7fifth][INNER]=1;

	  pinlocalization[CORTEXENDO_I][PIN7fifth][LOWER]=1;


	  //PIN2


	  pinlocalization[CORTEX][PIN2half][INNER]=1;
	  pinlocalization[CORTEX][PIN2max][LOWER]=1;

	  pinlocalization[CORTEX_TZ][PIN2third][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_TZ][PIN2half][LOWER]=1;
	  else
	    pinlocalization[CORTEX_TZ][PIN2half][UPPER]=1;

	  pinlocalization[CORTEX_EZ][PIN2quarter][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_EZ][PIN2third][LOWER]=1;
	  else
	    pinlocalization[CORTEX_EZ][PIN2third][UPPER]=1;

	  pinlocalization[CORTEX_D][PIN2quarter][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_D][PIN2third][LOWER]=1;
	  else	
	    pinlocalization[CORTEX_D][PIN2third][UPPER]=1;

	  pinlocalization[EPIDERMIS][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_TZ][PIN2half][UPPER]=1;
	  pinlocalization[EPIDERMIS_TZ][PIN2half][INNER]=1;

	  pinlocalization[EPIDERMIS_EZ][PIN2quarter][UPPER]=1;
	  pinlocalization[EPIDERMIS_EZ][PIN2quarter][INNER]=1;

	  pinlocalization[EPIDERMIS_D][PIN2quarter][UPPER]=1;
	  pinlocalization[EPIDERMIS_D][PIN2quarter][INNER]=1;

	  pinlocalization[LRC][PIN2half][UPPER]=1;
	  pinlocalization[LRC][PIN2half][INNER]=1;
	}

	else if (null_pin_localization && null_pin_intensity) {
	

	  //PIN1


	  pinlocalization[VASCULAR][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN1max][LOWER]=1;

	  pinlocalization[PERICYCLE][PIN1max][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][PIN1max][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][PIN1max][LOWER]=1;
	  pinlocalization[PERICYCLE_D][PIN1max][LOWER]=1;
	  
	  pinlocalization[ENDODERMIS][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1fifth][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN1fifth][INNER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1fifth][INNER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1fifth][INNER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1fifth][INNER]=1;

	  pinlocalization[LRCEPI_I][PIN1quarter][INNER]=1;
	  pinlocalization[CORTEXENDO_I][PIN1quarter][LOWER]=1;
	

	  //PIN3


	  pinlocalization[VASCULAR][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN3half][LOWER]=1;

	  pinlocalization[PERICYCLE][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_D][PIN3half][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN3half][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3half][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3half][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3half][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN3half][INNER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3half][INNER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3half][INNER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3half][INNER]=1;

	  pinlocalization[QCROOT][PIN3half][LOWER]=1;

	  pinlocalization[COLUMELLAROOT][PIN3max][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][OUTER]=1;

	  //only PIN3 is expressed in COL_I

	  pinlocalization[COL_I][PIN3max][UPPER]=1;
	  pinlocalization[COL_I][PIN3max][LOWER]=1;
	  pinlocalization[COL_I][PIN3max][INNER]=1;
	  pinlocalization[COL_I][PIN3max][OUTER]=1;

	  pinlocalization[LRCEPI_I][PIN3sixth][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN3sixth][OUTER]=1;
	  pinlocalization[LRCEPI_I][PIN3sixth][INNER]=1;//added later
	  pinlocalization[LRCEPI_I][PIN3sixth][UPPER]=1;//added later

	  pinlocalization[CORTEXENDO_I][PIN3sixth][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3sixth][OUTER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3sixth][INNER]=1;//added later
	  pinlocalization[CORTEXENDO_I][PIN3sixth][UPPER]=1;//added later


	  //PIN7


	  pinlocalization[VASCULAR][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN7max][LOWER]=1;

	  pinlocalization[PERICYCLE][PIN7max][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][PIN7max][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][PIN7max][LOWER]=1;
	  pinlocalization[PERICYCLE_D][PIN7max][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7sixth][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN7sixth][INNER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7sixth][INNER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7sixth][INNER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7sixth][INNER]=1;

	  pinlocalization[COLUMELLAROOT][PIN7quarter][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][OUTER]=1;

	  pinlocalization[LRCEPI_I][PIN7fifth][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN7fifth][INNER]=1;

	  pinlocalization[CORTEXENDO_I][PIN7fifth][LOWER]=1;


	  //PIN2


	  pinlocalization[CORTEX][PIN2half][INNER]=1;
	  pinlocalization[CORTEX][PIN2max][LOWER]=1;

	  pinlocalization[CORTEX_TZ][PIN2half][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_TZ][PIN2max][LOWER]=1;
	  else
	    pinlocalization[CORTEX_TZ][PIN2max][UPPER]=1;

	  pinlocalization[CORTEX_EZ][PIN2half][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_EZ][PIN2max][LOWER]=1;
	  else
	    pinlocalization[CORTEX_EZ][PIN2max][UPPER]=1;

	  pinlocalization[CORTEX_D][PIN2half][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_D][PIN2max][LOWER]=1;
	  else	
	    pinlocalization[CORTEX_D][PIN2max][UPPER]=1;

	  pinlocalization[EPIDERMIS][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_TZ][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS_TZ][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_EZ][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS_EZ][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_D][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS_D][PIN2max][INNER]=1;

	  pinlocalization[LRC][PIN2half][UPPER]=1;
	  pinlocalization[LRC][PIN2half][INNER]=1;
	}
 
	else { //default case for version2=2
	

	  //PIN1


	  pinlocalization[VASCULAR][PIN1max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN1half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN1quarter][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN1quarter][LOWER]=1;

	  if(equalstrengthPERIVASC) {
	    pinlocalization[PERICYCLE][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN1half][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN1quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN1quarter][LOWER]=1;
	  }
	  else {
	    pinlocalization[PERICYCLE][PIN1max][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN1quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN1sixth][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN1sixth][LOWER]=1;
	  }

	  pinlocalization[ENDODERMIS][PIN1fifth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1min][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1min][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1min][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN1fifth][INNER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1min][INNER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1min][INNER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1min][INNER]=1;

	  pinlocalization[ENDODERMIS_TZ][PIN1min][OUTER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1min][OUTER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1min][OUTER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN1min][UPPER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN1min][UPPER]=1;
	  pinlocalization[ENDODERMIS_D][PIN1min][UPPER]=1;

	  pinlocalization[LRCEPI_I][PIN1quarter][INNER]=1;
	  pinlocalization[CORTEXENDO_I][PIN1quarter][LOWER]=1;
	

	  //PIN3


	  pinlocalization[VASCULAR][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN3half][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN3half][LOWER]=1;

	  pinlocalization[PERICYCLE][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_TZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_EZ][PIN3half][LOWER]=1;
	  pinlocalization[PERICYCLE_D][PIN3half][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN3half][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3min][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3min][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3min][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN3half][INNER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3min][INNER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3min][INNER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3min][INNER]=1;

	  pinlocalization[ENDODERMIS_TZ][PIN3min][OUTER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3min][OUTER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3min][OUTER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN3min][UPPER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN3min][UPPER]=1;
	  pinlocalization[ENDODERMIS_D][PIN3min][UPPER]=1;

	  pinlocalization[QCROOT][PIN3half][LOWER]=1;

	  pinlocalization[COLUMELLAROOT][PIN3max][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN3max][OUTER]=1;

	  //only PIN3 is expressed in COL_I

	  pinlocalization[COL_I][PIN3max][UPPER]=1;
	  pinlocalization[COL_I][PIN3max][LOWER]=1;
	  pinlocalization[COL_I][PIN3max][INNER]=1;
	  pinlocalization[COL_I][PIN3max][OUTER]=1;

	  pinlocalization[LRCEPI_I][PIN3sixth][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN3sixth][OUTER]=1;
	  pinlocalization[LRCEPI_I][PIN3sixth][INNER]=1;//added later
	  pinlocalization[LRCEPI_I][PIN3sixth][UPPER]=1;//added later

	  pinlocalization[CORTEXENDO_I][PIN3sixth][LOWER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3sixth][OUTER]=1;
	  pinlocalization[CORTEXENDO_I][PIN3sixth][INNER]=1;//added later
	  pinlocalization[CORTEXENDO_I][PIN3sixth][UPPER]=1;//added later


	  //PIN7


	  pinlocalization[VASCULAR][PIN7max][LOWER]=1;
	  pinlocalization[VASCULAR_TZ][PIN7half][LOWER]=1;
	  pinlocalization[VASCULAR_EZ][PIN7half][LOWER]=1;
	  pinlocalization[VASCULAR_D][PIN7half][LOWER]=1;

	  if(equalstrengthPERIVASC) {
	    pinlocalization[PERICYCLE][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN7half][LOWER]=1;
	  }
	  else {
	    pinlocalization[PERICYCLE][PIN7max][LOWER]=1;
	    pinlocalization[PERICYCLE_TZ][PIN7half][LOWER]=1;
	    pinlocalization[PERICYCLE_EZ][PIN7quarter][LOWER]=1;
	    pinlocalization[PERICYCLE_D][PIN7quarter][LOWER]=1;
	  }

	  pinlocalization[ENDODERMIS][PIN7sixth][LOWER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7min][LOWER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7min][LOWER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7min][LOWER]=1;

	  pinlocalization[ENDODERMIS][PIN7sixth][INNER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7min][INNER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7min][INNER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7min][INNER]=1;

	  pinlocalization[ENDODERMIS_TZ][PIN7min][OUTER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7min][OUTER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7min][OUTER]=1;
	  pinlocalization[ENDODERMIS_TZ][PIN7min][UPPER]=1;
	  pinlocalization[ENDODERMIS_EZ][PIN7min][UPPER]=1;
	  pinlocalization[ENDODERMIS_D][PIN7min][UPPER]=1;

	  pinlocalization[COLUMELLAROOT][PIN7quarter][UPPER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][LOWER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][INNER]=1;
	  pinlocalization[COLUMELLAROOT][PIN7quarter][OUTER]=1;

	  pinlocalization[LRCEPI_I][PIN7fifth][LOWER]=1;
	  pinlocalization[LRCEPI_I][PIN7fifth][INNER]=1;

	  pinlocalization[CORTEXENDO_I][PIN7fifth][LOWER]=1;


	  //PIN2


	  pinlocalization[CORTEX][PIN2half][INNER]=1;
	  pinlocalization[CORTEX][PIN2max][LOWER]=1;

	  pinlocalization[CORTEX_TZ][PIN2third][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_TZ][PIN2half][LOWER]=1;
	  else
	    pinlocalization[CORTEX_TZ][PIN2half][UPPER]=1;
	  pinlocalization[CORTEX_TZ][PIN2third][OUTER]=1;

	  pinlocalization[CORTEX_EZ][PIN2quarter][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_EZ][PIN2third][LOWER]=1;
	  else
	    pinlocalization[CORTEX_EZ][PIN2third][UPPER]=1;
	  pinlocalization[CORTEX_EZ][PIN2quarter][OUTER]=1;

	  pinlocalization[CORTEX_D][PIN2quarter][INNER]=1;
	  if(no_pin2_flip)
	    pinlocalization[CORTEX_D][PIN2third][LOWER]=1;
	  else	
	    pinlocalization[CORTEX_D][PIN2third][UPPER]=1;
	  pinlocalization[CORTEX_D][PIN2quarter][OUTER]=1;

	  pinlocalization[EPIDERMIS][PIN2max][UPPER]=1;
	  pinlocalization[EPIDERMIS][PIN2max][INNER]=1;

	  pinlocalization[EPIDERMIS_TZ][PIN2half][UPPER]=1;
	  pinlocalization[EPIDERMIS_TZ][PIN2half][INNER]=1;

	  pinlocalization[EPIDERMIS_EZ][PIN2quarter][UPPER]=1;
	  pinlocalization[EPIDERMIS_D][PIN2quarter][UPPER]=1;

	  pinlocalization[LRC][PIN2half][UPPER]=1;
	  pinlocalization[LRC][PIN2half][INNER]=1;
	}
      }
    }   
}

/*void StringReplace()
  {
  char *s = dirname;
  char *p = s;
    
  while (*p) {
  if (isspace(*p))
  putchar('-');
  else
  putchar(*p);
  p++;
  }

  }*/

void Append(char* s, char c) // this function is useful for Apple users: if we simply set the upper case for the .par directory, we would have segmentation fault
{
  int len = strlen(s); // original length of the string
  s[len] = c;
  s[len+1] = '\0';
}


void SetUp(int ac,char *av[])// ac is argument count and av is argument value (array of string pointers containing command line arguments)
{
  int i,k;
  FILE *fp;
  int tmpnrow,tmpncol;
  int type;
  char a_string[STRING];

  SetPars(av[1]);
  //create a dir if defined
  if(ac>2) {
    if(snprintf(dirname,STRING,"%s",av[2])>=600) {
      dirname[0]=toupper(dirname[0]);
      fprintf(stderr,"warning: dirname too long: %s\n",dirname);
    }
    dirname[0]=toupper(dirname[0]);
     
    if(savegrace || savegraphics || auxindatainterval) {
      snprintf(a_string,STRING,"mkdir -p %s",dirname);
      system(a_string);

      if(snprintf(a_string,STRING,"%s/%s.par",dirname,av[2])>=STRING)
	fprintf(stderr,"warning: parfile name too long: %s\n",a_string);
      else
	SaveOptions(av[1],a_string);
    }  
  }
  else {
    savegrace=0;
    savegraphics=0;
    auxindatainterval=0;
  }
 
  snprintf(a_string,STRING,"%s.state",readpattern);
  ReadSize(&nrow,&ncol,a_string);

  AllocateFlux();

  if(grace) {
    OpenGrace(2,2,NULL);
    
    for(i=0;i<2;i++) {
      Grace(GGRAPH,i);
      Grace(GAXIS,'y');
      Grace(GLABEL,"Auxin concentration");
    }
    
    if (i==2){
      Grace(GGRAPH,i);
      Grace(GAXIS,'y');
      Grace(GLABEL,"Flux");
    }

    if (i==3){
      Grace(GGRAPH,i);
      Grace(GAXIS,'y');
      Grace(GLABEL,"Delta auxin");
    }
      
     
    Grace(GGRAPH,0);
    Grace(GAXIS,'x');
    Grace(GLABEL,"Distance from the root tip [um]");
    Grace(GTITLE,"Longitudinal");
      
    Grace(GGRAPH,1);
    Grace(GAXIS,'x');
    Grace(GLABEL,"Root width");
    Grace(GTITLE,"Transversal");
      
    Grace(GGRAPH,2);
    Grace(GAXIS,'x');
    Grace(GLABEL,"Distance from the root tip [um]");
      
      
    Grace(GGRAPH,3);
    Grace(GAXIS,'x');
    Grace(GLABEL,"Time");
      
   
    if(flux)
      fprintf(stderr,"warning: flux=1 is ignored when grace=1\n");
  }

      
  else if(flux) {
    grace=1; //trick to prevent grace to open in the background
    OpenGrace(1,1,NULL);
    Grace(GGRAPH,0);
    Grace(GAXIS,'x');
    Grace(GLABEL,"Distance from the root tip [um]");
    Grace(GAXIS,'y');
    Grace(GLABEL,"Total flux");
    grace=0;
  }

  if(grace||savegrace||flux||saveflux||(bendingroot && readauxin && bendingauxin)) {
    snprintf(a_string,STRING,"%s%s",readpattern,".centers");
    if((fp=fopen(a_string,"r"))==NULL) {
      fprintf(stdout,"error: could not open the file %s\n",a_string);
      exit(EXIT_FAILURE);
    }
    fscanf(fp,"%d %d",&totalcellrownumber,&totalcellcolnumber);
    if(((cellrowtofirstpixel=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL) ||
       ((cellrowtocenterpixel=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL) ||
       ((cellrowtolastpixel=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL) ||
       ((cellcoltofirstpixel=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL) ||
       ((cellcoltocenterpixel=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL) ||
       ((cellcoltolastpixel=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL)) {
      fprintf(stderr,"error in memory allocation\n");
      exit(EXIT_FAILURE);
    }
    for(k=0;k<totalcellrownumber;k++)
      fscanf(fp,"%*d %d %d %d",&cellrowtofirstpixel[k],&cellrowtocenterpixel[k],&cellrowtolastpixel[k]);
    
    for(k=0;k<totalcellcolnumber;k++)
      fscanf(fp,"%*d %d %d %d",&cellcoltofirstpixel[k],&cellcoltocenterpixel[k],&cellcoltolastpixel[k]);
    if(bendingroot) {
      backwardstate=New();
      snprintf(a_string,STRING,"%s%s",readpattern,".backwards");
      ReadPat(backwardstate,1,1,'i',a_string);
      snprintf(a_string,STRING,"%s%s",readpattern,".forwards");
      if(ReadSize(&oldnrow,&oldncol,a_string)) {
        fprintf(stdout,"the file %s is not a valid state file\n exitting now!\n",a_string);
        exit(EXIT_FAILURE);
      }  
      forwardstate=NewPlane(oldnrow,oldncol);
      if(readauxin && bendingauxin) {
        forwardauxin=FNewPlane(oldnrow,oldncol);
        forwardcells=NewPlane(oldnrow,oldncol);
      }
      tmpnrow=nrow;
      tmpncol=ncol;
      nrow=oldnrow;
      ncol=oldncol;
      ReadPat(forwardstate,1,1,'i',a_string);
      if(readauxin && bendingauxin) {
        snprintf(a_string,STRING,"%s%s",readpattern,".oldstate");
        ReadPat(forwardcells,1,1,'i',a_string);
        snprintf(a_string,STRING,"%s",readauxinpattern);
        FReadPat(forwardauxin,1,1,'f',a_string);
        //needed to know how big the cells used to be
        CNew(&oldcells,MAXCELLS,totaltypes);
        UpdateCFill(forwardcells,&oldcells);
      }
      nrow=tmpnrow;
      ncol=tmpncol;
    }
  }
  
  if(scanroot)
    totaltypes=TOTALROOTTYPES;
  else
    totaltypes=TOTALEMBRYOTYPES;
  if((pinlocalization=(TYPE***)calloc((size_t)totaltypes,sizeof(TYPE**)))==NULL) {
    fprintf(stderr,"error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  for(k=0;k<totaltypes;k++)
    pinlocalization[k]=NewPlane(TOTALPINS,5);
      
  state=New();
  corner=New();
  white=New();
  black=New();
  gh=New();
  for(k=0;k<TOTALPINS;k++)
    pin[k] = New();

  auxin=FNew();
  logauxin=FNew();
  logauxin2=FNew();
  fluxx=FNew();
  fluxy=FNew();
  fluxnorm=FNew();
  h=FNew();
  s=FNew();
  v=FNew();
  influx=FNew();
  breakdown=FNew();
  for(i=0;i<4;i++)
    displayauxin[i]=FNew();
  for(i=0;i<9;i++)
    if(i==EAST || i==WEST || i==NORTH || i==SOUTH)
      diffusionmatrix[i]=FNew();

  FFill(logauxin,log(0.1*boundaryconcentration+1e-6));
  FFill(fluxnorm,log(1e-6));

  SetPinLocalization();

	
  ColorTable(MEDIUM,MEDIUM,WHITE);
  ColorTable(CELLWALL,CELLWALL,BLACK);
  if(scanroot) {
    ColorTable(MEDIUM,MEDIUM,WHITE);
    ColorTable(CELLWALL,CELLWALL,BLACK);
    ColorTable(VASCULAR,VASCULAR,RED);
    ColorName(PERICYCLE,PERICYCLE,"dark orange");// 255,140,0
    ColorTable(ENDODERMIS,ENDODERMIS,YELLOW);
    ColorName(CORTEX,CORTEX,"forest green");
    ColorTable(EPIDERMIS,EPIDERMIS,BLUE);
    ColorRGB(LRC,190,190,190);//grey
    ColorRGB(COLUMELLAROOT,0,255,255);//light  blue
    ColorName(CORTEXENDO_I,CORTEXENDO_I,"dark green");
    ColorRGB(LRCEPI_I,127,0,177);// purple
    ColorRGB(COL_I,173,216,230);//light blue
    ColorRGB(QCROOT,100,100,100);//dove gray
    ColorRGB(SPECIALVASCULAR,255,0,183);//shocking pink
    ColorRGB(VASCULAR_D,238,64,0);// orange red 2
    ColorRGB(PERICYCLE_D,255,153,18);//cadmium yellow
    ColorRGB(ENDODERMIS_D,238,201,0);//gold 2	
    ColorRGB(CORTEX_D,60,179,113);//medium sea green 1
    ColorRGB(EPIDERMIS_D,42,82,190);//cerulean blue
    ColorRGB(VASCULAR_TZ,255,105,97);//pastel red
    ColorRGB(PERICYCLE_TZ,253,198,137);//pastel yellow orange
    ColorRGB(ENDODERMIS_TZ,253,253,150);//pastel yellow
    ColorRGB(CORTEX_TZ,144,238,144);//pale green 2
    ColorRGB(EPIDERMIS_TZ,127,127,255);//blue/purple
    ColorRGB(LRC_TZ,219,215,210);//timberwolf
    ColorRGB(VASCULAR_EZ,255,69,0);//orangered 1
    ColorRGB(PERICYCLE_EZ,251,175,93);//light yellow orange
    ColorRGB(ENDODERMIS_EZ, 255,215,0);//gold 1
    ColorRGB(CORTEX_EZ,124,205,124);//palegreen3 
    ColorRGB(EPIDERMIS_EZ,65,105,225);//royal blue
    ColorRGB(DEADCELLS,158,110,72);//light brown
    //ColorRGB(FAKECELLWALL,20,20,21);
    ColorRGB(FAKECELLWALL,200,200,210);//light grey
    ColorRGB(FAKECELLS,30,30,31);
	  
  }
  else {
    ColorTable(VASCULARROOT,VASCULARROOT,RED);
    ColorRGB(VASCULARSHOOT,255,0,192);
    ColorTable(ENDODERMISROOT,ENDODERMISROOT,YELLOW);
    ColorName(ENDODERMISSHOOT,ENDODERMISSHOOT,"orange");
    ColorRGB(CORTEXROOT,34,139,34);
    ColorTable(CORTEXSHOOT,CORTEXSHOOT,GREEN);
    ColorTable(EPIDERMISROOT,EPIDERMISROOT,BLUE);
    ColorName(EPIDERMISSHOOT,EPIDERMISSHOOT,"light steel blue");
    ColorTable(QCEMBRYO,QCEMBRYO,GREY);
    ColorName(PRIMORDIUM,PRIMORDIUM,"gray");
    ColorTable(COLUMELLAEMBRYO,COLUMELLAEMBRYO,CYAN);
    ColorRGB(CENTRALCELL,127,0,177);
  }
	
  ColorTable(40,40,WHITE);
  ColorTable(41,41,RED);
	
  ColorName(50,50,"gold");
  ColorName(51,51,"lime green");
  ColorName(52,52,"pale turquoise");
  ColorName(53,53,"dark salmon");
	
  ColorRamp(59,250,RED);
  ColorRamp(300,320,GREEN);
  ColorRamp(321,340,BLUE);
	
  //for absolute values
  ColorRGB(400,255,255,255);
  ColorGrad(ColorTable(401,401,GREEN),ColorTable(500,500,RED));
	
  //for flux norms
  ColorRamp(550,950,BLUE);
	
  //This is the new DR5, it has white if zero auxin
  ColorGrad(ColorRGB(1000,255,255,255),ColorRGB(1500,201,199,212));
  ColorGrad(1500,ColorRGB(5500,17,116,157));
  ColorGrad(5500,ColorRGB(10000,23,33,90));
  ColorGrad(10000,ColorRGB(11000,22,22,72));
	
  int first=12000,second=12125,third=12250,fourth=12375,fifth=12500,sixth=12625,seventh=12750,eighth=12875,nineth=13000;
  ColorGrad(ColorTable(first,first,BLACK),ColorRGB(second,79,0,95));
  ColorGrad(second,ColorTable(third,third,BLUE));
  ColorGrad(third,ColorTable(fourth,fourth,CYAN));
  ColorGrad(fourth,ColorTable(fifth,fifth,GREEN));
  ColorGrad(fifth,ColorTable(sixth,sixth,YELLOW));
  ColorGrad(sixth,ColorName(seventh,seventh,"orange"));
  ColorGrad(seventh,ColorTable(eighth,eighth,RED));
  ColorGrad(eighth,ColorRGB(nineth,139,0,0));
	
  north=NewP();
  south=NewP();
  west=NewP();
  east=NewP();
	
  CNew(&cells,MAXCELLS,totaltypes);
  if((cells.extras=(void *)calloc((size_t)cells.maxcells,sizeof(Extras)))==NULL) {
    fprintf(stderr,"InitCells: error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
	
  InitCells();
	
  //create a black and a white plane
  Fill(white,0);
  Fill(black,1);
	
  if(initialdose>EPSILON)
    IC();
  if(bioauxinonecell>EPSILON)
    BiosyntOneCell();
  if(bioauxin>EPSILON || bioauxinonecell>EPSILON)
    Biosynt();
  if(dauxin>EPSILON) //&& dauxinTZ>EPSILON)
    Decay();
  if(liquid>EPSILON)
    Immerse(); 
	
  UpdateDiffusionMatrix();

  if(graphics) {
    snprintf(a_string,STRING,"%s - %s",winfirst,winsecond); //this is to show two images together: "The Root" and "Logarithmic" 
    OpenDisplay(a_string,nrow,2*ncol);
  }

  if(savegraphics) {
    // OpenPNG(&windowpng,dirname);
    // printf("OpenPNG with directory %s\n", dirname);
		
    snprintf(a_string,STRING,"%s_log",av[2]);
    // toupper converts lowercase letter to uppercase: this is because I want the name of the folder to start with uppercase, and the name of the .par file to start with a lowercase
    a_string[0]=toupper(a_string[0]);
    OpenPNG(&png1,a_string);

    /*	snprintf(a_string,STRING,"%s_DR5",av[2]);
	a_string[0]=toupper(a_string[0]);
	OpenPNG(&png2,a_string);
    */

    //to save initial layout
    snprintf(a_string,STRING,"%s",av[2]);
    a_string[0]=toupper(a_string[0]);
    OpenPNG(&png2,a_string);

    //to save flux pngs
    snprintf(a_string,STRING,"%s_flux",av[2]);
    a_string[0]=toupper(a_string[0]);
    OpenPNG(&png3,a_string);
    /*
      snprintf(a_string,STRING,"%s_PIN3",av[2]);
      a_string[0]=toupper(a_string[0]);
      OpenPNG(&png4,a_string);

      snprintf(a_string,STRING,"%s_PIN7",av[2]);
      a_string[0]=toupper(a_string[0]);
      OpenPNG(&png6,a_string);
          
      snprintf(a_string,STRING,"%s_PIN2",av[2]);
      a_string[0]=toupper(a_string[0]);
      OpenPNG(&png7,a_string);
    */


    /*  snprintf(a_string,STRING,"%s_absvalues",av[2]);
	a_string[0]=toupper(a_string[0]);
	OpenPNG(&png5,a_string);*/
		
    //here we save once the state before it begins

    /*snprintf(a_string,STRING,"%s/state.dat",dirname);
      fp=fopen(a_string,"w");
      SavePat(fp,"%d\t",state);
      fclose(fp);*/
		
    /*	snprintf(a_string,STRING,"%s/pin1max.dat",dirname);
	fp=fopen(a_string,"w");
	SavePat(fp,"%d\t",pin[PIN1max]);
	fclose(fp);*/


    /*
      snprintf(a_string,STRING,"%s/pinA.dat",dirname);
      fp=fopen(a_string,"w");
      SavePat(fp,"%d\t",pin[PINA]);
      fclose(fp);
		
      snprintf(a_string,STRING,"%s/pinB.dat",dirname);
      fp=fopen(a_string,"w");
      SavePat(fp,"%d\t",pin[PINB]);
      fclose(fp);
		
      snprintf(a_string,STRING,"%s/pinC.dat",dirname);
      fp=fopen(a_string,"w");
      SavePat(fp,"%d\t",pin[PINC]);
      fclose(fp);
		
      snprintf(a_string,STRING,"%s/pinD.dat",dirname);
      fp=fopen(a_string,"w");
      SavePat(fp,"%d\t",pin[PIND]);
      fclose(fp);*/
		
    /*snprintf(a_string,STRING,"%s/aux.dat",dirname);
      fp=fopen(a_string,"w");
      SavePat(fp,"%d\t",pin[AUX]);
      fclose(fp);*/
		
    /*	snprintf(a_string,STRING,"%s/auxA.dat",dirname);
	fp=fopen(a_string,"w");
	SavePat(fp,"%d\t",pin[AUXA]);
	fclose(fp); 
		
	snprintf(a_string,STRING,"%s/auxB.dat",dirname);
	fp=fopen(a_string,"w");
	SavePat(fp,"%d\t",pin[AUXB]);
	fclose(fp); */
		
    //void CPlaneDisplay(TYPE **a,TYPE **b,int y,int x, TYPE c)
        
    /*	CPlaneDisplay(pin[PIN1max],state,0,0,40);
	WindowPNG(&windowpng);

	CPlaneDisplay(pin[PIN1half],state,0,0,40);
	//CPlaneDisplay(pin[AUX],state,0,0,40);
	WindowPNG(&windowpng);

	CPlaneDisplay(pin[PIN1third],state,0,0,40);
	WindowPNG(&windowpng);

	CPlaneDisplay(pin[PIN1quarter],state,0,0,40);
	WindowPNG(&windowpng);
                
	CPlaneDisplay(pin[PIN1fifth],state,0,0,40);
	WindowPNG(&windowpng);
                
	CPlaneDisplay(pin[PIN1sixth],state,0,0,40);
	WindowPNG(&windowpng);

	CPlaneDisplay(pin[PIN1min],state,0,0,40);
	WindowPNG(&windowpng);	

	CPlaneDisplay(pin[PIN3max],state,0,0,40);
	WindowPNG(&png4);

	CPlaneDisplay(pin[PIN3half],state,0,0,40);
	//CPlaneDisplay(pin[AUX],state,0,0,40);
	WindowPNG(&png4);

	CPlaneDisplay(pin[PIN3third],state,0,0,40);
	WindowPNG(&png4);

	CPlaneDisplay(pin[PIN3quarter],state,0,0,40);
	WindowPNG(&png4);
                
	CPlaneDisplay(pin[PIN3fifth],state,0,0,40);
	WindowPNG(&png4);
                
	CPlaneDisplay(pin[PIN3sixth],state,0,0,40);
	WindowPNG(&png4);

	CPlaneDisplay(pin[PIN3min],state,0,0,40);
	WindowPNG(&png4);

	CPlaneDisplay(pin[PIN7max],state,0,0,40);
	WindowPNG(&png6);

	CPlaneDisplay(pin[PIN7half],state,0,0,40);
	//CPlaneDisplay(pin[AUX],state,0,0,40);
	WindowPNG(&png6);

	CPlaneDisplay(pin[PIN7third],state,0,0,40);
	WindowPNG(&png6);

	CPlaneDisplay(pin[PIN7quarter],state,0,0,40);
	WindowPNG(&png6);
                
	CPlaneDisplay(pin[PIN7fifth],state,0,0,40);
	WindowPNG(&png6);
                
	CPlaneDisplay(pin[PIN7sixth],state,0,0,40);
	WindowPNG(&png6);

	CPlaneDisplay(pin[PIN7min],state,0,0,40);
	WindowPNG(&png6);

	CPlaneDisplay(pin[PIN2max],state,0,0,40);
	WindowPNG(&png7);

	CPlaneDisplay(pin[PIN2half],state,0,0,40);
	//CPlaneDisplay(pin[AUX],state,0,0,40);
	WindowPNG(&png7);

	CPlaneDisplay(pin[PIN2third],state,0,0,40);
	WindowPNG(&png7);

	CPlaneDisplay(pin[PIN2quarter],state,0,0,40);
	WindowPNG(&png7);
                
	CPlaneDisplay(pin[PIN2fifth],state,0,0,40);
	WindowPNG(&png7);
                
	CPlaneDisplay(pin[PIN2sixth],state,0,0,40);
	WindowPNG(&png7);

	CPlaneDisplay(pin[PIN2min],state,0,0,40);
	WindowPNG(&png7);

    */
    //WindowPNG(&png4);
			
    //	CPlaneDisplay(pin[AUXA],state,0,ncol,40);
    //	CPlaneDisplay(pin[AUXB],state,0,2*ncol,40);*/
	
    /*	CPlaneDisplay(pin[PINA],state,0,4*ncol,40);
	CPlaneDisplay(pin[PINB],state,0,5*ncol,40);
	CPlaneDisplay(pin[PINC],state,0,6*ncol,40);
	CPlaneDisplay(pin[PIND],state,0,7*ncol,40);
    */
    CellPNG(state,&cells,&png2,0);
    if(fabs(dauxinTZ-dauxin)>EPSILON) {
      PLANE(
	    if(fabs(breakdown[i][j]-dauxinTZ)<EPSILON)
	      gh[i][j]=1;
	    );
    }
    else { //dauxin==dauxinTZ, mutants
      PLANE(
	    type=cells.celltype[state[i][j]];
	    if (dauxin<bioauxin) // if gh3 mutant, no deg, empty layout -- comparison is ugly hack, should find a better way for the condition dauxin>bioauxin
                    gh[i][j]=0;
                else if (dauxin>bioauxin){ // if GH3 OE, deg everywhere -- comparison is ugly hack, should find a better way for the condition dauxin>bioauxin
                    if(type==MEDIUM)
                        gh[i][j]=0;
                    else
                        gh[i][j]=1;
                }
	    );
    }
    
    CPlanePNG(gh,state,&png2,40);

    snprintf(a_string,STRING,"%s/state.dat",dirname);
    fp=fopen(a_string,"w");
    SavePat(fp,"%d\t",state);
    fclose(fp);
    //WindowPNG(&windowpng);
		
  }  
	
  if(readauxin) {
    //for the moment, such that there is immediately a flux pattern available
    UpdateFlux();
    snprintf(a_string,STRING,"%s/final_fluxx.dat",dirname);
    fp=fopen(a_string,"w");
    FSavePat(fp,"%g\t",fluxx);
    fclose(fp); 
    snprintf(a_string,STRING,"%s/final_fluxy.dat",dirname);
    fp=fopen(a_string,"w");
    FSavePat(fp,"%g\t",fluxy);
    fclose(fp);
  }
}


void ShowGrace()
{
  int i,j,k;
  int location;
  float totalauxin;
    
  int index= (int)(tijd/graceinterval);
  int pass = (int)(index/LENGTHAUXINRECORD);
  index = index - pass*LENGTHAUXINRECORD;
    
  totalauxin=CFTotal(auxin,state)*spacestep*spacestep;
  totalauxinrecord[index] = totalauxin;
  //printf("totalauxin at index %d is %f\n",index,totalauxin);

    
  Grace(GGRAPH,0);
  for(k=1;k<totalcellcolnumber;k++){
    Grace(GSET,k-1);
    Grace(GKILL);
    Grace(GLINE,GCOLOR,400+k*100/totalcellcolnumber);
  }
  Grace(GGRAPH,1);
  for(k=1;k<totalcellrownumber;k++){
    Grace(GSET,k-1);
    Grace(GKILL);
    Grace(GLINE,GCOLOR,400+k*100/totalcellrownumber);
  }
  
  Grace(GGRAPH,2);
  for(k=1;k<totalcellcolnumber;k++){
    Grace(GSET,k-1);
    Grace(GKILL);
    Grace(GLINE,GCOLOR,400+k*100/totalcellcolnumber);
  }
    
  Grace(GGRAPH,3);
  Grace(GSET,0);
  Grace(GKILL);
  Grace(GLINE,GTYPE,0);
  Grace(GSYMBOL,GTYPE,1);
  Grace(GSYMBOL,GSTYLE,2);
  Grace(GSYMBOL,GCOLOR,500);
    
   
    
  if(bendingroot) {
    Grace(GGRAPH,0);//go
    //for(i=oldnrow;i>0;i--)
    for(i=1;i<=oldnrow;i++)
      for(k=1;k<totalcellcolnumber;k++) {
        Grace(GSET,k-1);
        if((location=forwardstate[i][cellcoltocenterpixel[k]])) {
          //Grace(GPOINT,2,(double)(oldnrow-i),auxin[location/SHRT_MAX][location%SHRT_MAX]>EPSILON?(double)auxin[location/SHRT_MAX][location%SHRT_MAX]:1e-6);
	  if(state[location/SHRT_MAX][location%SHRT_MAX]>=SPECIALCELLS || state[location/SHRT_MAX][location%SHRT_MAX]==CELLWALL)
	    //we multiply i*(double)spacestep to scale the length of the domain on grace plot
	    Grace(GPOINT,2,(double)i*(double)spacestep,auxin[location/SHRT_MAX][location%SHRT_MAX]>EPSILON?(double)auxin[location/SHRT_MAX][location%SHRT_MAX]:1e-6);
        }
      }
    Grace(GGRAPH,1);//g1
    for(j=1;j<=oldncol;j++)
      for(k=2;k<totalcellrownumber;k++) {
        Grace(GSET,k-1);
        if((location=forwardstate[cellrowtocenterpixel[k]][j])) {
	  if(state[location/SHRT_MAX][location%SHRT_MAX]>=SPECIALCELLS || state[location/SHRT_MAX][location%SHRT_MAX]==CELLWALL)
	    Grace(GPOINT,2,(double)j,auxin[location/SHRT_MAX][location%SHRT_MAX]>EPSILON?(double)auxin[location/SHRT_MAX][location%SHRT_MAX]:1e-6);
        }
      }
  }

  else {
    Grace(GGRAPH,0);//go - plot a line traced along the centre of the cell files
    for(i=1;i<=cellrowtocenterpixel[2];i++)//i=cellrowtocenterpixel[2] is the row-center-pixel of the second row of cells from the top (2476). This because I don't want to plot the intracellular concentration of the last row of cells, which is fixed to 0 (external cell files) or 0.1 (vasculature)
      //cellrowtocenterpixels are located at the center of each column (on the x direction) and span all rows
      for(k=1;k<totalcellcolnumber;k++) {
        Grace(GSET,k-1);
        if(state[i][cellcoltocenterpixel[k]]>=SPECIALCELLS || state[i][cellcoltocenterpixel[k]]==CELLWALL) {
          //Grace(GPOINT,2,(double)(nrow-i),auxin[i][cellcoltocenterpixel[k]]>EPSILON?(double)auxin[i][cellcoltocenterpixel[k]]:1e-6);
          Grace(GPOINT,2,(double)i*(double)spacestep,auxin[i][cellcoltocenterpixel[k]]>EPSILON?(double)auxin[i][cellcoltocenterpixel[k]]:1e-6);
        }
      }
    Grace(GGRAPH,1);//g1- plot a line traced along the centre of the cell rows
    for(j=1;j<=ncol;j++)
      for(k=2;k<totalcellrownumber;k++) {
        Grace(GSET,k-1);
        if(state[cellrowtocenterpixel[k]][j]>=SPECIALCELLS || state[cellrowtocenterpixel[k]][j]==CELLWALL) {
          Grace(GPOINT,2,(double)j,auxin[cellrowtocenterpixel[k]][j]>EPSILON?(double)auxin[cellrowtocenterpixel[k]][j]:1e-6);
        }
      }
      
      
    UpdateFlux();
      
    Grace(GGRAPH,2);
    for(i=1;i<=cellrowtocenterpixel[2];i++)
      for(k=1;k<totalcellcolnumber;k++){
	Grace(GSET,k-1);
	if(state[i][cellcoltocenterpixel[k]]){
	  // Grace(GPOINT,2,(double)i*(double)spacestep,fluxy[i][cellcoltocenterpixel[k]]);
	  Grace(GPOINT,2,(double)i*(double)spacestep,hypot(fluxx[i][cellcoltocenterpixel[k]],fluxy[i][cellcoltocenterpixel[k]]));
	}
      }
    Grace(GAUTOSCALE);
      
      
    Grace(GGRAPH,3);
    Grace(GSET,0);
    for (index=0;index<LENGTHAUXINRECORD;index++)
      // Grace(GPOINT,2,(double)index,totalauxinrecord[index]>EPSILON?totalauxinrecord[index]:1e-6);
      Grace(GPOINT,2,(double)index,totalauxinrecord[index]-totalauxinrecord[index-1]>EPSILON?totalauxinrecord[index]-totalauxinrecord[index-1]:1e-6);
  }
    
  for(i=0;i<4;i++){
    if (i!=2){
      Grace(GGRAPH,i);
      Grace(GAXIS,'y');
      Grace(GSCALE,"Logarithmic");
      Grace(GAUTOSCALE);
    }
  }
}

void CompareFiles(char *oldfilename,char *newfilename)
{
  FILE *fp1, *fp2;
  FILE *fp;
  int ch1, ch2;
  char a_string[STRING];

  fp1 = fopen(oldfilename, "r");
  fp2 = fopen(newfilename, "r");

  if (fp1 == NULL) {
    fprintf(stderr,"Cannot open %s for reading ", oldfilename);
    exit(1);
  } 
  else if (fp2 == NULL) {
    fprintf(stderr,"Cannot open %s for reading ", newfilename);
    exit(1);
  }
  
  else {
    ch1 = getc(fp1);
    ch2 = getc(fp2);
 
    while ((ch1 != EOF) && (ch2 != EOF) && (ch1 == ch2)) {
      ch1 = getc(fp1);
      ch2 = getc(fp2);
    }
 
    //if (ch1 == ch2) {
    //always save, such as to track development of code 
      snprintf(a_string,STRING,"cp %s %s/final_conc.dat",newfilename,dirname);
      system(a_string);
      snprintf(a_string,STRING,"%s/final_auxin.dat",dirname);
      fp=fopen(a_string,"w");
      FSavePat(fp,"%g\t",auxin);
      fclose(fp);
      UpdateFlux();
      snprintf(a_string,STRING,"%s/final_fluxx.dat",dirname);
      fp=fopen(a_string,"w");
      FSavePat(fp,"%g\t",fluxx);
      fclose(fp); 
      snprintf(a_string,STRING,"%s/final_fluxy.dat",dirname);
      fp=fopen(a_string,"w");
      FSavePat(fp,"%g\t",fluxy);
      fclose(fp);
      //}
    if (ch1 == ch2) {
      //printf("Files are identical n");
      timetoexit=1;
    }

    fclose(fp1);
    fclose(fp2);
  }
  return;
}

void SaveGrace()
{
  static int step=0;
  FILE *fp=NULL;
  int i,j,k,location;
  char a_string[STRING];
  static char old_string[STRING];

  if(step)
    snprintf(old_string,STRING,"%s/%.5d_conc.dat",dirname,step-1);
  snprintf(a_string,STRING,"%s/%.5d_conc.dat",dirname,step++);
  if((fp=fopen(a_string,"w"))==NULL) {
    fprintf(stderr,"could not open file %s\n",a_string);
    return;
  }

  if(bendingroot) {
    for(i=oldnrow;i>0;i--)
      for(k=1;k<totalcellcolnumber;k++)
        if((location=forwardstate[i][cellcoltocenterpixel[k]])) {
          //fprintf(fp,"0 %d %d %g\n",k,oldnrow-i,auxin[location/SHRT_MAX][location%SHRT_MAX]>EPSILON?(double)auxin[location/SHRT_MAX][location%SHRT_MAX]:1e-6);
          fprintf(fp,"0 %d %d %g\n",k,i,auxin[location/SHRT_MAX][location%SHRT_MAX]>EPSILON?(double)auxin[location/SHRT_MAX][location%SHRT_MAX]:1e-6);
        }
    for(j=1;j<=oldncol;j++)
      for(k=1;k<totalcellrownumber;k++)
        if((location=forwardstate[cellrowtocenterpixel[k]][j])) {
          fprintf(fp,"1 %d %d %g\n",k,j,auxin[location/SHRT_MAX][location%SHRT_MAX]>EPSILON?(double)auxin[location/SHRT_MAX][location%SHRT_MAX]:1e-6);
        }
  }
  else {
    for(i=nrow;i>0;i--)
      for(k=1;k<totalcellcolnumber;k++)
        if(state[i][cellcoltocenterpixel[k]]) {
          //fprintf(fp,"0 %d %d %g\n",k,nrow-i,auxin[i][cellcoltocenterpixel[k]]>EPSILON?(double)auxin[i][cellcoltocenterpixel[k]]:1e-6);
          fprintf(fp,"0 %d %d %g\n",k,i,auxin[i][cellcoltocenterpixel[k]]>EPSILON?(double)auxin[i][cellcoltocenterpixel[k]]:1e-6);
        }
    for(j=1;j<=ncol;j++)
      for(k=1;k<totalcellrownumber;k++)
        if(state[cellrowtocenterpixel[k]][j]) {
          fprintf(fp,"1 %d %d %g\n",k,j,auxin[cellrowtocenterpixel[k]][j]>EPSILON?(double)auxin[cellrowtocenterpixel[k]][j]:1e-6);
        }
  }
  
  //Grace(GPRINT,"PNG","trial.png"); // how can I assign the dir name and place it in the same dir of the current simulation?
  fclose(fp);

  if(step>1)
    CompareFiles(old_string,a_string);
}  

void ShowFGrace()
{
  int i;//,j,k;
  //int location;

  UpdateFlux();
  grace=1;//trick to let grace function properly

  /*  
      Grace(GGRAPH,0);
      for(k=1;k<totalcellcolnumber;k++) {
      Grace(GSET,k-1);
      Grace(GKILL);
      Grace(GLINE,GCOLOR,400+k*100/totalcellcolnumber);
      }
      Grace(GGRAPH,1);
      for(k=1;k<totalcellrownumber;k++) {
      Grace(GSET,k-1);
      Grace(GKILL);
      Grace(GLINE,GCOLOR,400+k*100/totalcellrownumber);
      }
  
      if(bendingroot) {
      Grace(GGRAPH,0);//go
      for(i=oldnrow;i>0;i--)
      for(k=1;k<totalcellcolnumber;k++) {
      Grace(GSET,k-1);
      if((location=forwardstate[i][cellcoltocenterpixel[k]])) {
      //Grace(GPOINT,2,(double)(oldnrow-i),hypot(fluxx[location/SHRT_MAX][location%SHRT_MAX],fluxy[location/SHRT_MAX][location%SHRT_MAX]));
      Grace(GPOINT,2,(double)i,hypot(fluxx[location/SHRT_MAX][location%SHRT_MAX],fluxy[location/SHRT_MAX][location%SHRT_MAX]));
      }
      }
      Grace(GGRAPH,1);//g1
      for(j=1;j<=oldncol;j++)
      for(k=1;k<totalcellrownumber;k++) {
      Grace(GSET,k-1);
      if((location=forwardstate[cellrowtocenterpixel[k]][j])) {
      Grace(GPOINT,2,(double)j,hypot(fluxx[location/SHRT_MAX][location%SHRT_MAX],fluxy[location/SHRT_MAX][location%SHRT_MAX]));
      }
      }
      }
  
      else {
      Grace(GGRAPH,0);//go
      for(i=nrow;i>0;i--)
      for(k=1;k<totalcellcolnumber;k++) {
      Grace(GSET,k-1);
      if(state[i][cellcoltocenterpixel[k]]) {
      //Grace(GPOINT,2,(double)(nrow-i),hypot(fluxx[i][cellcoltocenterpixel[k]],fluxy[i][cellcoltocenterpixel[k]]));
      Grace(GPOINT,2,(double)i,hypot(fluxx[i][cellcoltocenterpixel[k]],fluxy[i][cellcoltocenterpixel[k]]));
      }
      }
      Grace(GGRAPH,1);//g1
      for(j=1;j<=ncol;j++)
      for(k=1;k<totalcellrownumber;k++) {
      Grace(GSET,k-1);
      if(state[cellrowtocenterpixel[k]][j]) {
      Grace(GPOINT,2,(double)j,hypot(fluxx[cellrowtocenterpixel[k]][j],fluxy[cellrowtocenterpixel[k]][j]));
      }
      }
      }

      for(i=0;i<2;i++) {
      Grace(GGRAPH,i);
      Grace(GAUTOSCALE);
      }
  */

  Grace(GGRAPH,0);
  for (i=0;i<3;i++) {
    Grace(GSET,i);
    Grace(GKILL);
    Grace(GLINE,GCOLOR,50+i);
  }

  Grace(GTITLE,"Auxin Flux (Longitudinal)");
  Grace(GLEGEND,GVIEWPORT,0.7,.85);
  Grace(GSET,0);
  Grace(GLEGEND,GLABEL,"net flux");
  Grace(GSET,1);
  Grace(GLEGEND,GLABEL,"flux down");
  Grace(GSET,2);
  Grace(GLEGEND,GLABEL,"flux up");
  
  for(i=nrow;i>0;i--) {
    Grace(GSET,0);
    Grace(GPOINT,2,(double)i*spacestep,netflux[i]);
    Grace(GSET,1);
    Grace(GPOINT,2,(double)i*spacestep,fluxdown[i]);      
    Grace(GSET,2);
    Grace(GPOINT,2,(double)i*spacestep,fluxup[i]);        
  }
  Grace(GAXIS,'y');  
  Grace(GAUTOSCALE);
  grace=0;
}

void SaveFGrace()
{
  static int step=0;
  FILE *fp=NULL;
  int i;//,j,k,location;
  char a_string[STRING];

  snprintf(a_string,STRING,"%s/%.5d_flux.dat",dirname,step++);
  if((fp=fopen(a_string,"w"))==NULL) {
    fprintf(stderr,"could not open file %s\n",a_string);
    return;
  }

  UpdateFlux();
  for(i=1;i<=nrow;i++) {
    fprintf(fp,"%g %g %g %g\n",(double)i*spacestep,netflux[i],fluxdown[i],fluxup[i]);
  }

  /*
    if(bendingroot) {
    for(i=oldnrow;i>0;i--)
    for(k=1;k<totalcellcolnumber;k++)
    if((location=forwardstate[i][cellcoltocenterpixel[k]])) {
    //fprintf(fp,"0 %d %d %g\n",k,oldnrow-i,hypot(fluxx[location/SHRT_MAX][location%SHRT_MAX],fluxy[location/SHRT_MAX][location%SHRT_MAX]));
    fprintf(fp,"0 %d %d %g\n",k,i,hypot(fluxx[location/SHRT_MAX][location%SHRT_MAX],fluxy[location/SHRT_MAX][location%SHRT_MAX]));
    }
    for(j=1;j<=oldncol;j++)
    for(k=1;k<totalcellrownumber;k++)
    if((location=forwardstate[cellrowtocenterpixel[k]][j])) {
    fprintf(fp,"1 %d %d %g\n",k,j,hypot(fluxx[location/SHRT_MAX][location%SHRT_MAX],fluxy[location/SHRT_MAX][location%SHRT_MAX]));
    }
    }
    else {
    for(i=nrow;i>0;i--)
    for(k=1;k<totalcellcolnumber;k++)
    if(state[i][cellcoltocenterpixel[k]]) {
    //fprintf(fp,"0 %d %d %g\n",k,nrow-i,hypot(fluxx[i][cellcoltocenterpixel[k]],fluxy[i][cellcoltocenterpixel[k]]));
    fprintf(fp,"0 %d %d %g\n",k,i,hypot(fluxx[i][cellcoltocenterpixel[k]],fluxy[i][cellcoltocenterpixel[k]]));
    }
    for(j=1;j<=ncol;j++)
    for(k=1;k<totalcellrownumber;k++)
    if(state[cellrowtocenterpixel[k]][j]) {
    fprintf(fp,"1 %d %d %g\n",k,j,hypot(fluxx[cellrowtocenterpixel[k]][j],fluxy[cellrowtocenterpixel[k]][j]));
    }
    }
  */

  fclose(fp);
}  

void UpdateCellFlux() // I want to display only one arrow flux per cell: to do this I want to sum netfluxes in each cell
{
      
  static int step=0;
  FILE *fp=NULL;
  char a_string[STRING];

  snprintf(a_string,STRING,"%s/%.5d_netfluxc.dat",dirname,step++);
  if((fp=fopen(a_string,"w"))==NULL){
    fprintf(stderr,"could not open file %s\n",a_string);
    return;
  }
      
  static int oldtime=-1;
        
  if(oldtime==tijd)
    return;
        
        
  oldtime=tijd;
        
  CELLS(cells,
	if(c>1){ // I need to match cell number with state[i][j]- cell=0 or 1 would correspond to cell wall
	  netfluxcelly[c]=0.0;
	  netfluxcellx[c]=0.0;
	  CPLANE(state,
		 if(c==state[i][j])
		   NEIGHBOURS(
			      if(neigh==SOUTH && state[i+y][j]==CELLWALL)
				netfluxcelly[c]+=(diffusionmatrix[NORTH][i-1][j]*auxin[i-1][j]-diffusionmatrix[SOUTH][i][j]*auxin[i][j])/spacestep;
			      if(neigh==NORTH && state[i+y][j]==CELLWALL)
				netfluxcelly[c]+=(diffusionmatrix[NORTH][i][j]*auxin[i][j]-diffusionmatrix[SOUTH][i+1][j]*auxin[i+1][j])/spacestep;
			      if(neigh==WEST && state[i][j+x]==CELLWALL)
				netfluxcellx[c]+=(diffusionmatrix[EAST][i][j-1]*auxin[i][j-1]-diffusionmatrix[WEST][i][j]*auxin[i][j])/spacestep;
			      if(neigh==EAST && state[i][j+x]==CELLWALL)
				netfluxcellx[c]+=(diffusionmatrix[EAST][i][j]*auxin[i][j]-diffusionmatrix[WEST][i][j+1]*auxin[i][j+1])/spacestep;
			      )
		 )
	    fprintf(fp,"at time %g netfluxy at cell %d is:  %f\n",realtime,c,netfluxcelly[c]);
	}
	)
     
    fclose(fp);
}




void Show()
{
  /*
    from graphics.c:
    void Arrow(int y0,int x0,double my,double mx,double dy,double dx,int color)
    extern double arrowlength=4.;
    extern double arrowangle=M_PI/9.;
     
    mx and my are the displacement from the given initial position x0,y0
    dx and dy : length of the line
    {
    double theta;
     
    if(!tensorrelative) {
    y0+=(int)my;
    x0+=(int)mx;
    mx=0.;
    my=0.;
    }
     
    theta=atan2(dy,dx); // to convert to polar coordinates
     
    length of the line is length=hypot(dy,dx);
     
    Line(scale*y0,scale*x0,scale*my,scale*mx,scale*dy,scale*dx,color); - straight line
    Line(scale*y0,scale*x0,scale*(my+dy),scale*(mx+dx),-scale*arrowlength*sin(theta+arrowangle),-scale*arrowlength*cos(theta+arrowangle),color);
    Line(scale*y0,scale*x0,scale*(my+dy),scale*(mx+dx),-scale*arrowlength*sin(theta-arrowangle),-scale*arrowlength*cos(theta-arrowangle),color);
    }
     
  */
  FLOAT auxinceiling=.1,auxinmaximum;
  int tmpgradlog;
  int showit,alsoshowit;

  showit=whattoshow;
  alsoshowit=alsoshowing;

  switch(showit) {
  case 0:
    CellDisplay(state,&cells,0,0,0);
    break;
  case 1:
    CPlaneDisplay(pin[PIN1max],state,0,0,40);
   
    break;
  case 2:
    CPlaneDisplay(pin[AUX],state,0,0,40);
    break;
    /* case 3:
       CPlaneDisplay(pin[PINA],state,0,0,40);
       break;*/	
  case 4:
    /*
      CPLANE(state,   
      logauxin[i][j]=(FLOAT)log(auxin[i][j]+0.1*boundaryconcentration+1e-6);
      );   
      CFPlaneDisplay(logauxin,state,0,0,CFMin(logauxin,state),CFMax(logauxin,state),59,250);
    */
    UpdateHeatmapAuxin();
    if(noreallyhighvalues)
      CFPlaneDisplay(logauxin2,state,0,0,0.,forcedmax,12000,12875);
    else
      CFPlaneDisplay(logauxin2,state,0,0,0.,CFMax(logauxin2,state),12000,13000);
    break;
  case 5:
    auxinmaximum=CFMax(auxin,state);
    //  printf("auxinmaximum is %g", auxinmaximum);
    while(auxinceiling<auxinmaximum)
      auxinceiling*=10.;
    CFPlaneDisplay(auxin,state,0,0,0.,auxinceiling,300,340);
    break;
  case 6:
    UpdateFlux();
    CPLANE(state,
           fluxnorm[i][j]=(FLOAT)log((hypot(fluxx[i][j],fluxy[i][j])+1e-6));
           );
    CFPlaneDisplay(fluxnorm,state,0,0,CFMin(fluxnorm,state),CFMax(fluxnorm,state),550,950);
    break;
  case 7:
    CFPlaneDisplay(auxin,state,0,0,CFMin(auxin,state),CFMax(auxin,state),1000,11000);
    break;
  case 8:	
    UpdateFlux();
    UpdateCellFlux();
    //printf("fluxdata: (%g,%g,%g), (%g,%g,%g)\n",CFMax(h,state),CFMax(s,state),CFMax(v,state),CFMin(h,state),CFMin(s,state),CFMin(v,state));
    tmpgradlog=gradlog;
    gradlog=1;
    /* from gradient.c:
       FLOAT HSVFill(FLOAT **h,FLOAT **s,FLOAT **v,int planes,...)
       HSV is a representation for a gradient of colours
    */
    HSVFill(h,s,v,2,fluxx,0.,1.,0.,fluxy,0.,1.,M_PI_2); // the two planes fluxx and fluxy are orthogonal
    gradlog=tmpgradlog;
    //put into h,s,v, use 2 planes, first plane, offset, relative scaling, angle, second plane, etc.
    CHSVDisplay(h,s,v,state,0,0);
     
    //To plot arrows in a sensible way, I need to rescale the length of each arrow with respect to the arrow that has the highest length (otherwise I would have very long arrows)
    double max_length = -1.0;
    CELLS(cells,
	  max_length = max(max_length, hypot(netfluxcelly[c],netfluxcellx[c])); // here I calculated the length of the longest vector
	  )
      CELLS(cells,
	    if(cells.area[c] && c>1){
	      // I multiply by 100 to observe a length also in arrows representing minimal fluxes - but arrows are too long in QC
	      Arrow(cells.shape[c].meany,cells.shape[c].meanx,0.0,0.0,netfluxcelly[c]*100./max_length,netfluxcellx[c]*100./max_length,0); //0 is white
	    }
            )
      break;
    /* case 9:
       CPlaneDisplay(pin[PIND],state,0,0,40);
       break;
       case 10:
       UpdateExpression();
       CFPlaneDisplay(auxbexpression,state,0,0,0.,1.,12000,13000);
       break;
       case 11:
       UpdateExpression();
       CFPlaneDisplay(pinexpression,state,0,0,0.,1.,12000,13000);
       break;
    */
  default:
    break;
  }
  switch(alsoshowit) { 
  case 0:
    CellDisplay(state,&cells,0,ncol,0);
    break;
  case 1:
    //void CPlaneDisplay(TYPE **a,TYPE **b,int y,int x, TYPE c)
    CPlaneDisplay(pin[PIN1max],state,0,ncol,40);
    break;
  case 2:
   
    CPlaneDisplay(pin[AUX],state,0,ncol,40);
    break;
    /*  case 3:
	CPlaneDisplay(pin[PINA],state,0,ncol,40);
	break;*/	
  case 4:
    /*
      CPLANE(state,   
      logauxin[i][j]=(FLOAT)log(auxin[i][j]+0.1*boundaryconcentration+1e-6);
      );   
      CFPlaneDisplay(logauxin,state,0,ncol,CFMin(logauxin,state),CFMax(logauxin,state),59,250);
    */
    UpdateHeatmapAuxin();
    //void CFPlaneDisplay(FLOAT **a,TYPE **b,int y,int x, FLOAT low, FLOAT high, int start, int end)
    if(noreallyhighvalues)
      CFPlaneDisplay(logauxin2,state,0,ncol,0.,forcedmax,12000,12875);
    else
      CFPlaneDisplay(logauxin2,state,0,ncol,0.,CFMax(logauxin2,state),12000,13000);
    break;
  case 5:
    auxinmaximum=CFMax(auxin,state);
    //printf("auxinmaximum is %g", auxinmaximum);
    while(auxinceiling<auxinmaximum)
      auxinceiling*=10.;
    CFPlaneDisplay(auxin,state,0,ncol,0.,auxinceiling,300,340);
    break;
  case 6:
    UpdateFlux();
    CPLANE(state,
           fluxnorm[i][j]=(FLOAT)log((hypot(fluxx[i][j],fluxy[i][j])+1e-6));
           );
    CFPlaneDisplay(fluxnorm,state,0,ncol,CFMin(fluxnorm,state),CFMax(fluxnorm,state),550,950);
    break;
  case 7:
    CFPlaneDisplay(auxin,state,0,ncol,CFMin(auxin,state),CFMax(auxin,state),1000,11000);
    break;
  case 8:	
    UpdateFlux();
    // printf("fluxdata: (%g,%g,%g), (%g,%g,%g)\n",CFMax(h,state),CFMax(s,state),CFMax(v,state),CFMin(h,state),CFMin(s,state),CFMin(v,state));
    tmpgradlog=gradlog;
    gradlog=1;
    HSVFill(h,s,v,2,fluxx,0.,1.,0.,fluxy,0.,1.,M_PI_2);
    gradlog=tmpgradlog;
    //put into h,s,v, use 2 planes, first plane, offset, relative scaling, angle, second plane, etc.
    CHSVDisplay(h,s,v,state,0,ncol);
    break;
  default:
    break;
  }
}

void Save()
{
  //  FLOAT auxinceiling=.1,auxinmaximum;
  //  FILE *fp;
  int tmpgradlog;

  /* snprintf(a_string,STRING,"%s/%.5d_auxin.dat",dirname,tijd/saveinterval);
     fp=fopen(a_string,"w");
     FSavePat(fp,"%g\t",auxin);
     fclose(fp);
     snprintf(a_string,STRING,"%s/fluxx_%.5d",dirname,tijd/saveinterval);
     fp=fopen(a_string,"w");
     FSavePat(fp,"%g\t",fluxx);
     fclose(fp); 
     snprintf(a_string,STRING,"%s/fluxy_%.5d",dirname,tijd/saveinterval);
     fp=fopen(a_string,"w");
     FSavePat(fp,"%g\t",fluxy);
     fclose(fp);
  */
  /*
    CPLANE(state,
    logauxin[i][j]=(FLOAT)log(auxin[i][j]+0.1*boundaryconcentration+1e-6);
    );        
    CFPlanePNG(logauxin,state,&png1,CFMin(logauxin,state),CFMax(logauxin,state),59,250);
  */
   
  /*   fp=fopen(a_string,"w");
       CELLS(cells,
       if(cells.area[c]) {
       fprintf(fp,"%d\t%g\n",c,EXTRAS[c].dynamicpermeability[PINDYN137]);
       }
       );
       fclose(fp);
    
  */
    
    
  UpdateHeatmapAuxin();

  //void CFPlanePNG(FLOAT **a,TYPE **b, Png *png, FLOAT low, FLOAT high, int start, int end)
  if(noreallyhighvalues)
    CFPlanePNG(logauxin2,state,&png1,0.,forcedmax,12000,12875);
  else
    CFPlanePNG(logauxin2,state,&png1,0.,CFMax(logauxin2,state),12000,13000);

  //void CPlanePNG(TYPE **a,TYPE **b, Png *png, int c)
  //  CPlanePNG(pin[PIN1max],state,&png4,0);
  //CFPlanePNG(auxin,state,&png2,CFMin(auxin,state),(auxinmaximum=CFMax(auxin,state)),1000,11000);
  //while(auxinceiling<auxinmaximum)
  //  auxinceiling*=10.;
  //CFPlanePNG(logauxin,state,&png5,0.,auxinceiling,300,340);

  
  UpdateFlux();
  tmpgradlog=gradlog;
  gradlog=1;
  HSVFill(h,s,v,2,fluxx,0.,1.,0.,fluxy,0.,1.,M_PI_2);     
  gradlog=tmpgradlog;
  CHSVPNG(h,s,v,state,&png3);
  
  /*
    CPLANE(state,
    fluxnorm[i][j]=(FLOAT)log((hypot(fluxx[i][j],fluxy[i][j])+1e-6));
    );      
    CFPlanePNG(fluxnorm,state,&png4,CFMin(fluxnorm,state),CFMax(fluxnorm,state),550,950); 
  */
}

void Key(int key,int mouse_i,int mouse_j)
{
  int type;
  char a_string[STRING];

  switch(key) {
  case 'h':
  case '?':
    printf("Keys:\n"
	   "\n"
	   "'Esc'\tQuit the programme\n"
	   "'s'\tStart/Stop simulation\n"
	   "'s'\tStep simulation\n"
	   "'R'\tReset simulation\n"
	   "'z'\tChange what to show in left window\n"
	   "'x'\tChange what to show in right window\n"
	   "'D'\tIncrease the diffusion rate with sqrt(2)\n"
	   "'d'\tDecrease the diffusion rate with sqrt(2)\n"
	   "'T'\tIncrease the timestep 10-fold\n"
	   "'t'\tDecrease the timestep 10-fold\n"
	   "'C'\tCut the root\n"
	   "'c'\tMend the root again\n"
	   "'P'\tIncrease the permeability rates with sqrt(2)\n"
	   "'p'\tDecrease the permeability rates with sqrt(2)\n"
	   "'q'\tQuit the program\n"
	   "\n"
	   "Mouse:\n"
	   "\n"
	   "`left'\tGive info on concentrations\n"
	   "`middle'Remove spot of auxin\n"
	   "`right'\tAdd spot of auxin\n"
	   "\n"
	   );
    break;
  case 27 :
      
    exit(0);
    break;
  case 's':
    run=!run;
    break;
  case 'S':
    run=2;
    break;
  case 'z':
    whattoshow=(whattoshow+1)%9;
    if(whattoshow==0) {
      snprintf(winfirst,STRING,"The Root");
      snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
      ChangeWindowName(a_string);
    }
    else if(whattoshow==1) {
      snprintf(winfirst,STRING,"PIN1max");
      snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
      ChangeWindowName(a_string);
    }
    else if(whattoshow==2) {
      snprintf(winfirst,STRING,"AUX/LAX");
      snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
      ChangeWindowName(a_string);
    }
    /*  else if(whattoshow==3) {
        snprintf(winfirst,STRING,"PINA");
        snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
        ChangeWindowName(a_string);
	}
    */
    else if(whattoshow==4) {
      snprintf(winfirst,STRING,"Logarithmic");
      snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
      ChangeWindowName(a_string);
    }
    else if(whattoshow==5) {
      snprintf(winfirst,STRING,"absolute auxin");
      snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
      ChangeWindowName(a_string);
    }
    else if(whattoshow==6) {
      snprintf(winfirst,STRING,"flux norm");
      snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
      ChangeWindowName(a_string);
    }
    else if(whattoshow==7) {
      snprintf(winfirst,STRING,"DR5");
      snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
      ChangeWindowName(a_string);
    }
    else if(whattoshow==8) {
      snprintf(winfirst,STRING,"FLUX");
      snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
      ChangeWindowName(a_string);
    }
    break;
  case 'x':
    alsoshowing=(alsoshowing+1)%9;
    if(alsoshowing==0) {
      snprintf(winsecond,STRING,"The Root");
      snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
      ChangeWindowName(a_string);
    }
    else if(alsoshowing==1) {
      snprintf(winsecond,STRING,"PIN1max");
      snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
      ChangeWindowName(a_string);
    }
    else if(alsoshowing==2) {
      snprintf(winsecond,STRING,"AUX/LAX");
      snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
      ChangeWindowName(a_string);
    }
    /*   else if(alsoshowing==3) {
	 snprintf(winsecond,STRING,"PINA");
	 snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
	 ChangeWindowName(a_string);
	 }
       
	 else if(alsoshowing==4) {
	 snprintf(winsecond,STRING,"Logarithmic");
	 snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
	 ChangeWindowName(a_string);
	 }
	 else if(alsoshowing==5) {
	 snprintf(winsecond,STRING,"absolute auxin");
	 snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
	 ChangeWindowName(a_string);
	 }
	 else if(alsoshowing==6) {
	 snprintf(winsecond,STRING,"flux norm");
	 snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
	 ChangeWindowName(a_string);
	 }
	 else if(alsoshowing==7) {
	 snprintf(winsecond,STRING,"DR5");
	 snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
	 ChangeWindowName(a_string);
	 }
	 else if(alsoshowing==8) {
	 snprintf(winsecond,STRING,"FLUX");
	 snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
	 ChangeWindowName(a_string);
	 }*/
    break;
  case 'D':
    resetdiffusionmatrix=1;
    Dauxin*=M_SQRT2;
    printf("new diffusion rate: %g\n",Dauxin);
    break;
  case 'd':
    resetdiffusionmatrix=1;
    Dauxin/=M_SQRT2;
    printf("new diffusion rate: %g\n",Dauxin);
    break;
  case 'T':
    resetdiffusionmatrix=1;
    timestep*=10.;
    printf("new timestep: %g\n",timestep);
    break;
  case 't':
    resetdiffusionmatrix=1;
    timestep/=10.;
    printf("new timestep: %g\n",timestep);
    break;
  case 'C':
    CPLANE(state,
	   if(state[i][j]>CELLWALL && state[i][j]<SPECIALCELLS) {
	     type=cells.celltype[state[i][j]];
	     if(type == VASCULAR || type == VASCULAR_D || type == PERICYCLE ||  type == PERICYCLE_D || type == ENDODERMIS || type == ENDODERMIS_D)
	       auxin[i][j]=0.;	 
	   }
	   );
    printf("Root has been cut\n");
    break;
  case 'c':
    CPLANE(state,
	   if(state[i][j]>CELLWALL && state[i][j]<SPECIALCELLS) {
	     type=cells.celltype[state[i][j]];
	     if(type == VASCULAR || type == VASCULAR_D || type == PERICYCLE ||  type == PERICYCLE_D || type == ENDODERMIS || type == ENDODERMIS_D)
	       auxin[i][j]=boundaryconcentration;	 
	   }
	   );
    printf("Root has been mended\n");
    break;
  case 'R':
    PLANE(
	  if(state[i][j]>=SPECIALCELLS || state[i][j]==CELLWALL)
	    auxin[i][j]=0.;
	  );
    break;
    /*  case 'P':
	resetdiffusionmatrix=1;
	permeability[PIN]*=M_SQRT2;
	permeability[PINA]*=M_SQRT2;
	permeability[AUX]*=M_SQRT2;
      
	printf("new permeabily rates: strong PIN: %g; weak PIN: %g; AUX/LAX: %g\n",permeability[PIN],permeability[PINA],permeability[AUX]);
	break;
	case 'p':
	resetdiffusionmatrix=1;
	permeability[PIN]/=M_SQRT2;
	permeability[PINA]/=M_SQRT2;
	permeability[AUX]/=M_SQRT2;
      
	printf("new permeability rates: strong PIN: %g; weak PIN: %g; AUX/LAX: %g\n",permeability[PIN],permeability[PINA],permeability[AUX]);
	break;*/
  default:
    break;
  }
}


void Ratinho(int mousestroke,int mouse_i,int mouse_j)
{
  int k,m;

  while(mouse_j>ncol)
    mouse_j-=ncol;
  while(mouse_i>nrow)
    mouse_i-=nrow;
  if(mousestroke==1) {
    k=state[mouse_i][mouse_j];
      
    double pin_first=0;
      
 
    for (m=0;m<TOTALPINS;m++){
      if (pin[m][mouse_i][mouse_j]){
	pin_first+=permeability[m];
	const int asize=TOTALPINS;
	const char *a[asize];
              
	a[AUX]="AUX";
	a[AUX1]="AUX1";
	a[AUXB]="AUXB";
	a[AUX1half]="AUXhalf";
	a[PIN1max]="PIN1max";
	a[PIN1half]="PIN1half";
	a[PIN1third]="PIN1third";
	a[PIN1quarter]="PIN1quarter";
	a[PIN1fifth]="PIN1fifth";
	a[PIN1sixth]="PIN1sixth";
	a[PIN1min]="PIN1min";
	a[PIN3max]="PIN3max";
	a[PIN3half]="PIN3half";
	a[PIN3third]="PIN3third";
	a[PIN3quarter]="PIN3quarter";
	a[PIN3fifth]="PIN3fifth";
	a[PIN3sixth]="PIN3sixth";
	a[PIN3min]="PIN3min";
	a[PIN7max]="PIN7max";
	a[PIN7half]="PIN7half";
	a[PIN7third]="PIN7third";
	a[PIN7quarter]="PIN7quarter";
	a[PIN7fifth]="PIN7fifth";
	a[PIN7sixth]="PIN7sixth";
	a[PIN7min]="PIN7min";
	a[PIN2max]="PIN2max";
	a[PIN2half]="PIN2half";
	a[PIN2third]="PIN2third";
	a[PIN2quarter]="PIN2quarter";
	a[PIN2fifth]="PIN2fifth";
	a[PIN2sixth]="PIN2sixth";
	a[PIN2min]="PIN2min";
	a[PINdyn_pol]="PINdyn_pol";
	a[PINDYN137]="PINDYN137";
	a[PINDYN2]="PINDYN2";
              

	printf("pin_first %f, permeability %f, m %d, %s\n", pin_first, permeability[m], m, a[m]);
              
      }
    }
     
    
    
      
      
      
    fprintf(stdout,"at time=%g; position=(%d,%d); cell=%d: ; decay=%g; influx=%g; auxin: %f; netflux: %f; flux: %f (%f,%f); Ppin= %g; tissue=%d; nrow%d\n"
	    ,realtime,mouse_j,mouse_i,k,breakdown[mouse_i][mouse_j],influx[mouse_i][mouse_j],auxin[mouse_i][mouse_j],netflux[mouse_j], hypot(fluxx[mouse_i][mouse_j],fluxy[mouse_i][mouse_j]),fluxx[mouse_i][mouse_j],fluxy[mouse_i][mouse_j], pin_first, cells.celltype[state[mouse_i][mouse_j]], nrow);
          

  }
  else if(mousestroke==2) {
    CPLANE(
	   state,
	   auxin[i][j]+=100.*exp(-pow((hypot(i-mouse_i,j-mouse_j)),2.)/(9.));
	   );
    fprintf(stdout,"added auxin around position (%d,%d); cell %d of celltype %d; auxin is: %f\n",mouse_j,mouse_i,state[mouse_i][mouse_j],cells.celltype[state[mouse_i][mouse_j]],auxin[mouse_i][mouse_j]);
  }
  else if(mousestroke==3) {
    CPLANE(
	   state,
	   auxin[i][j]=max(0.,auxin[i][j]-100.*exp(-pow((hypot(i-mouse_i,j-mouse_j)),2.)/(9.)));
	   );
    fprintf(stdout,"removed auxin around position (%d,%d); cell %d of celltype %d; auxin is: %f; total auxin is: %g\n",mouse_j,mouse_i,state[mouse_i][mouse_j],cells.celltype[state[mouse_i][mouse_j]],auxin[mouse_i][mouse_j],CFTotal(auxin,state));
  }      
}

void SaveAuxin()
{
  FILE *fp;
  char a_string[STRING];

  snprintf(a_string,STRING,"%s/realtotauxin.dat",dirname);   
  if(!tijd)
    fp=fopen(a_string,"w");
  else
    fp=fopen(a_string,"a");
  fprintf(fp,"%d %20g\n",tijd,CFTotal(auxin,state)*spacestep*spacestep);
  fclose(fp); 


}

void SpeedUp()
{
  //code for speeding-up equilibrium 
  FLOAT totalauxin,requiredauxinscaling,production,removal=0,fromoutside=0,tooutside=0;
  int type;

  CPLANE(state,
         if(state[i][j]>CELLWALL && state[i][j]<SPECIALCELLS)
           state[i][j]=-state[i][j];
         );

    
    
  /*FLOAT CFTotal(FLOAT **a,TYPE **s)
    {
    int i,j;
    FLOAT k=0;
    int nc, nr;
    nr = nrow;
    nc = ncol;
    for (i=1; i <= nr; i++)
    for (j=1; j <= nc; j++)
    if(s[i][j]>(TYPE)specialcelltype)
    k += a[i][j];
    return k;
    }
  */
    
  totalauxin=CFTotal(auxin,state)*spacestep*spacestep;
  
  if(!(totalauxin>EPSILON)) {
    fprintf(stdout,"warning: no auxin, cannot speedup to equilibrium\n");
    return;
  }

  production=CFTotal(influx,state)*spacestep*spacestep;
  
  //if breakdown[i][j] would be the same everywhere:
  //removal=dauxin;
  //but let's make it more general
  CPLANE(state,
         removal+=breakdown[i][j]*auxin[i][j];
         );
  removal*=spacestep*spacestep/totalauxin;

  PLANE(
        NEIGHBOURS(
		   if(neigh==WEST || neigh==EAST || neigh==NORTH || neigh==SOUTH) {
		     if(state[i][j]>specialcelltype && state[i+y][j+x]<=specialcelltype && diffusionmatrix[neigh][i][j]>EPSILON) //what's leaving the system
		       tooutside+=diffusionmatrix[neigh][i][j]*auxin[i][j];//diffusionmatrix=perm.*spacestep
		     else if(state[i+y][j+x]>specialcelltype && state[i][j]<=specialcelltype && diffusionmatrix[neigh][i][j]>EPSILON) //what's entering the system
		       fromoutside+=diffusionmatrix[neigh][i][j]*auxin[i][j];
		   }
		   );
        );

  tooutside/=totalauxin;//fraction of whats leaving 

  
  if(!((tooutside+removal)>EPSILON)){
    fprintf(stdout,"warning: no decay nor efflux, cannot speedup to equilibrium\n");
    fprintf(stdout,"removal is %g tooutside is %g tooutside+removal is %g\n",removal,tooutside, tooutside+removal);
  
  }

  else {
    if(fixedfinalauxinvalue>EPSILON && fromoutside>EPSILON) {
      //requiredtotalauxin=(production+fromoutside)/(removal+tooutside);
      //printf("%g\n",fabs((fixedfinalauxinvalue*(removal+tooutside)-production)/fromoutside-1.));
      if(fabs((fixedfinalauxinvalue*(removal+tooutside)-production)/fromoutside-1.)>1e-6) {
	if(DEBUG)
	  fprintf(stdout,"boundaryconcentration has been changed from %g to ",boundaryconcentration);
	boundaryconcentration*=(fixedfinalauxinvalue*(removal+tooutside)-production)/fromoutside;
	if(DEBUG)
	  fprintf(stdout,"%g\n",boundaryconcentration);
        fromoutside*=(fixedfinalauxinvalue*(removal+tooutside)-production)/fromoutside;
        PLANE(
	      if(state[i][j]<0) {
		type=cells.celltype[-state[i][j]];
		if(type == VASCULAR || type == VASCULAR_D || type == PERICYCLE ||  type == PERICYCLE_D || type == ENDODERMIS || type == ENDODERMIS_D)
		  auxin[i][j]=boundaryconcentration;	 
	      }
	      );
      }
    }
    //requiredtotalauxin=(production+fromoutside)/(removal+tooutside);
    requiredauxinscaling=(production+fromoutside)/(totalauxin*(removal+tooutside));
    
    CPLANE(state,
           auxin[i][j]*=requiredauxinscaling;
           );

    if(DEBUG)
      fprintf(stdout,"prod: %g influx:%g removal:%g efflux:%g eq. auxin:%g scaling: %g, totalauxin: %g\n",production,fromoutside,removal,tooutside,(production+fromoutside)/(removal+tooutside),requiredauxinscaling,totalauxin);
  }

  PLANE(
        if(state[i][j]<0)
          state[i][j]=-state[i][j];
        );
}


int main(int argc,char *argv[])
{
	
  
  //FILE *state_out = fopen("state.dat", "w");
	
	
  int gentijd;
  char a_string[STRING];

  mouse=&Ratinho;
  keyboard=&Key;

  SetUp(argc,argv);
    
  
    
    
	
  /*	{
	int i,j;
		
	FILE *d=fopen("decay.dat","w");
		
	for(i=nrow;i>0;i--)
	{
	for (j=1;j<=ncol;j++)
	fprintf(d,"%g\t%d\t%d\n",breakdown[i][j],cells.celltype[state[i][j]],state[i][j]);
	fprintf(d,"\n");
	}
		
	fclose(d);
		
	}
  */

  /*{
    int k;

    FILE *p=fopen("permeability.dat","w");
		
    for(k=1;k<=TOTALPINS;k++)
    {
    fprintf(p,"%f\t",permeability[k]);
    fprintf(p,"\n");
			
    }
		
    fclose(p);
		
		
    }
  */
    
  /*  {
      int k;
     
    
      FILE *p=fopen("pin.dat","w");
      
      PLANE(
      for(k=1;k<=TOTALPINS;k++){
          
      fprintf(p,"%f\t",pin[k][i][j]);
      fprintf(p,"\n");
            
            
      }
      )
      fclose(p);
     
     
      }
     
  */
	
  //here I want to define an "old total concentration of auxin", so that at the next time step I will define a current total concentration and then compute the delta between the two -- hence setting this delta as an identifier for the steady state
  //int ss_counter=0;
  //double old_totalauxin = CFTotal(auxin,state)*spacestep*spacestep;

  int n,m,k;
  FILE *fp;
  
  snprintf(a_string,STRING,"%s/total_transport_strength.dat",dirname);
  fp=fopen(a_string,"w");

  for(n=0;n<TOTALROOTTYPES;n++) {
    for(m=1;m<5;m++) {
      for(k=0;k<TOTALPINS;k++)
	if(pinlocalization[n][k][m]) {
	  if(pinoraux[k]==PIN)
	    pinstrength[n][m]+=permeability[k];
	  else
	    auxstrength[n][m]+=permeability[k];
	}
      //if(!(pinstrength[n][m]>EPSILON))
      pinstrength[n][m]+=leakage;
      fprintf(fp,"%s\t%s\t%g\t%g\n",roottypes[n],orientation[m],pinstrength[n][m],auxstrength[n][m]);
    }
    fprintf(fp,"\n");
  }
  fclose(fp);

  for(gentijd=0;;gentijd++) {
			
    if((run && showinterval && !(tijd%showinterval)) || !run)
      Show();

    
    if(1) {
      if(DEBUG)
	fprintf(stdout,"gentijd %d value %g total %g\n",gentijd,auxin[100][100],CFTotal(auxin,state));
      if(gentijd%saveinterval==0)
	AuxinRatio();
            
      if(savegraphics && saveinterval && !(tijd%saveinterval)) {
	Save();
	BoundaryFlux();
      }

      if(grace) {
	if(graceinterval && !(tijd%graceinterval))
	  ShowGrace();
      }
    
      else if(flux && gracefinterval && !(tijd%gracefinterval))
	ShowFGrace();
    
      if(savegrace && savegraceinterval && !(tijd%savegraceinterval))
	SaveGrace();
      if(saveflux && savegracefinterval && !(tijd%savegracefinterval))
	SaveFGrace();
    
      if(auxindatainterval && !(tijd%auxindatainterval))
	SaveAuxin();
    
      if(timetoexit)
	exit(0);

      if((tijd || readauxin) && includeequilibrium && tijd>=equilibriumstart && tijd<equilibriumstop && !(tijd%equilibriuminterval)) //start
	SpeedUp();

			
      if(resetdiffusionmatrix)
	UpdateDiffusionMatrix();
        
      //  if(auxindatainterval && !(tijd%auxindatainterval))
      //    UpdatePIN();
			
      CPLANE(state,
	     if(state[i][j]>CELLWALL && state[i][j]<SPECIALCELLS)
	       state[i][j]=-state[i][j];
	     );
			
      if(tijd==0 || resetdiffusionmatrix)
	ADICReactionDiffusionPar(auxin,auxin,state,diffusionmatrix,influx,breakdown);
      else
	ADICReactionDiffusionPar(auxin,auxin,state,NULL,NULL,NULL);
			
      //here I update total auxin and set the condition for the simulation to stop if delta total auxin value over 100 time steps is fixed at <=1e-7
      //double curr_totalauxin=CFTotal(auxin,state)*spacestep*spacestep;
      //double deltauxin=fabs(curr_totalauxin-old_totalauxin);

      //  if (tijd%graceinterval==0) // print when grace is updated
      //   printf("current total auxin amount is %.10f and delta concentration is %.10f\n",curr_totalauxin,deltauxin);
            
      //is 1e-7 a reasonable value? I thought it was ok since FLT_EPSILON is on the same order of magnitude
      /*
	if(gentijd%10000== 0){
	FILE *fp;
	snprintf(a_string,STRING,"%s/deltauxin.dat",dirname);
	fp=fopen(a_string,"w");
	fprintf(fp,"at time %g current auxin is %f and previous time step was %f\t\ndelta is %.10f\n", realtime,curr_totalauxin, old_totalauxin, deltauxin);
	fclose(fp);
                
	}
            
           
            
	if(deltauxin<=1e-7)
	ss_counter++;
	else
	ss_counter=0;
            
	if(ss_counter>=50){
                
	printf("occurences of unchanged states %d\ntime to reach steady state is %g\n", ss_counter,realtime);
	printf("current auxin is %.10f and delta is %.10f\n", curr_totalauxin, deltauxin);
	break;
	}
            
	old_totalauxin=curr_totalauxin;
      */
      PLANE(
	    if(state[i][j]<0)
	      state[i][j]=-state[i][j];
	    );
			
      realtime+=timestep;
      resetdiffusionmatrix=0;
      tijd++;
		
		
      if(run==2)
	run=0;
    }
    else
      usleep(1);
	
    if(graphics && CheckDisplay())
      {
	snprintf(a_string,STRING,"%s - %s",winfirst,winsecond);
	OpenDisplay(a_string,nrow,2*ncol);
      }
		
  }
	
  return 0;
}

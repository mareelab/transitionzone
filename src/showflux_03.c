#include <excal2.h>

//be careful when TOTALTYPES becomes larger than 20!
#define MEDIUM          0
#define CELLWALL        1

#define VASCULAR        2
#define PERICYCLE       3
#define ENDODERMIS      4
#define CORTEX          5
#define EPIDERMIS       6
//lateral root cap
#define LRC             7
#define COLUMELLAROOT   8
//initials
#define CORTEXENDO_I    9
#define LRCEPI_I        10
#define COL_I           11
#define QCROOT          12
//differentiation zone
//specialvascular should be last
#define SPECIALVASCULAR 13
#define VASCULAR_D      14
#define PERICYCLE_D     15
#define ENDODERMIS_D    16
#define CORTEX_D        17
#define EPIDERMIS_D     18
//transition zone - is it ok to set TZ tissues after DZ ones?
#define VASCULAR_TZ     19
#define PERICYCLE_TZ    20
#define ENDODERMIS_TZ   21
#define CORTEX_TZ       22
#define EPIDERMIS_TZ    23
#define LRC_TZ          24
//elongation zone
#define VASCULAR_EZ     25
#define PERICYCLE_EZ    26
#define ENDODERMIS_EZ   27
#define CORTEX_EZ       28
#define EPIDERMIS_EZ    29
#define DEADCELLS       30
#define FAKECELLWALL    31
#define FAKECELLS       32
#define TOTALROOTTYPES  33 //it shouldnt go over 40...
#define SPECIALCELLS   100

extern int nrow,ncol,graphics,gradlog;
extern int (*newindex)(int,int);
extern int specialcelltype;
extern void (*mouse)(int,int,int);
extern void (*keyboard)(int,int,int);

static Png png1,windowpng;
static TYPE **state;
static FLOAT **fluxx,**fluxy,**flux;
static FLOAT upperboundary,lowerboundary;
static char a_string[STRING],arrowcolor[STRING],readpattern[STRING],readunbentpattern[STRING];
static FLOAT **h,**s,**v;
static FLOAT **auxin,**logauxin,**expression;
static double dynamicpermeability;
static Cell cells,unbentcells; 
static double fluxlength;
static int inversey;
static int useregion,usefixedauxin,usefixedflux,showflux,showexpression,showauxin,bendingroot;
static int startcellrow,startpixel,endcellrow,endpixel;
static int *cellrowtofirstpixel=NULL;
static int *cellrowtocenterpixel=NULL;
static int *cellrowtolastpixel=NULL;
static int *cellcoltofirstpixel=NULL;
static int *cellcoltocenterpixel=NULL;
static int *cellcoltolastpixel=NULL;
static int totalcellrownumber;
static int totalcellcolnumber;
static int oldnrow,oldncol;
static TYPE **forwardstate,**backwardstate,**forwardcells;
static FLOAT maxinzoneflux,mininzoneflux;
static FLOAT maxflux,minflux;
static FLOAT fixedminauxin,fixedmaxauxin;
static FLOAT fixedminflux,fixedmaxflux;
static int timetoexit=0;
static int noreallyhighvalues=0;
static FLOAT forcedmax;

//flux
static FLOAT *fluxdown;
static FLOAT *fluxup;
static FLOAT *netfluxdown;
static FLOAT *fluxin;
static FLOAT *fluxout;
static FLOAT *netfluxin;
static FLOAT *netfluxcelly;
static FLOAT *netfluxcellx;
extern FLOAT spacestep;
extern int grace;

double nearbyint(double x);

void AllocateFlux() //For plotting vertical or horizontal flux using GRACE
{
  if(
     ((fluxdown=(FLOAT *)calloc((size_t)(nrow+2),sizeof(FLOAT)))==NULL) ||
     ((fluxup=(FLOAT *)calloc((size_t)(nrow+2),sizeof(FLOAT)))==NULL) ||
     ((netfluxdown=(FLOAT *)calloc((size_t)(nrow+2),sizeof(FLOAT)))==NULL) || 
     ((fluxin=(FLOAT *)calloc((size_t)(nrow+2),sizeof(FLOAT)))==NULL) ||
     ((fluxout=(FLOAT *)calloc((size_t)(nrow+2),sizeof(FLOAT)))==NULL) ||
     ((netfluxin=(FLOAT *)calloc((size_t)(nrow+2),sizeof(FLOAT)))==NULL) || 
     ((netfluxcelly=(FLOAT *)calloc((size_t)(Max(state)+1),sizeof(FLOAT)))==NULL) ||
     ((netfluxcellx=(FLOAT *)calloc((size_t)(Max(state)+1),sizeof(FLOAT)))==NULL)) {
    fprintf(stderr,"error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
}

void UpdateFluxPatterns(char *av[])
{
  int i,k;
  char a_string[STRING];

  AllocateFlux();

  CPLANE(state,
         netfluxdown[i]-=spacestep*fluxy[i][j];
         if(fluxy[i][j]<0)
           fluxdown[i]-=spacestep*fluxy[i][j];
         else
           fluxup[i]+=spacestep*fluxy[i][j];
	 //we look at flux in right half 
	 //in right half of field, flux is between pixel and its right neighbour
	 //so we need cell wall and left of cell wall
	 if(i>=432 && i<(nrow-203) && j>=288 && j<=289) {
	   for(k=-3;k<=3;k++) {
	   //printf("%d\t%d\t%d\n",i,j,state[i][j]);
	     netfluxin[i]-=spacestep*fluxx[i+k][j];
	     if(fluxx[i+k][j]<0)
	       fluxin[i]-=spacestep*fluxx[i+k][j];
	     else
	       fluxout[i]+=spacestep*fluxx[i+k][j];
	   }
	 }
	 );

  OpenGrace(2,1,NULL);
  Grace(GGRAPH,0);
  for(i=0;i<3;i++)
  {
    Grace(GSET,i);
    Grace(GKILL);
    Grace(GLINE,GCOLOR,50+i);
  }
  Grace(GLEGEND,GVIEWPORT,.82,.9);
  Grace(GSET,0);
  Grace(GLEGEND,GLABEL,"net downwards flux");
  Grace(GSET,1);
  Grace(GLEGEND,GLABEL,"downwards flux");
  Grace(GSET,2);
  Grace(GLEGEND,GLABEL,"upwards flux");
  Grace(GAXIS,'x');
  Grace(GLABEL,"Distance from the root tip [um]");
  Grace(GAXIS,'y');
  Grace(GLABEL,"Longitudinal flux [a.u./s]");
  Grace(GTITLE,"Longitudinal auxin flux");
  for(i=nrow;i>0;i--) {
      Grace(GSET,0);
      Grace(GPOINT,2,(double)i*spacestep,netfluxdown[i]);
      Grace(GSET,1);
      Grace(GPOINT,2,(double)i*spacestep,fluxdown[i]);      
      Grace(GSET,2);
      Grace(GPOINT,2,(double)i*spacestep,fluxup[i]);        
  }
  Grace(GSCALE,"Logarithmic");
  Grace(GWORLD,0.,10.,spacestep*(double)nrow,100000.);
  Grace(GREDRAW);
  //Grace(GAUTOSCALE);
  Grace(GGRAPH,1);
  for(i=0;i<3;i++)
  {
    Grace(GSET,i);
    Grace(GKILL);
    Grace(GLINE,GCOLOR,50+i);
  }
  Grace(GLEGEND,GVIEWPORT,.7,.45);
  Grace(GSET,0);
  Grace(GLEGEND,GLABEL,"inwards flux");
  Grace(GSET,1);
  Grace(GLEGEND,GLABEL,"outwards flux");
  Grace(GAXIS,'x');
  Grace(GLABEL,"Distance from the root tip [um]");
  Grace(GAXIS,'y');
  Grace(GLABEL,"Radial flux [a.u./s]");
  Grace(GTITLE,"Radial auxin flux over Epidermis-Cortex boundary");
  for(i=432;i<(nrow-203);i++) {
      Grace(GSET,0);
      //Grace(GLINE,GWIDTH,3.);
      if(netfluxin[i]>EPSILON)
	Grace(GPOINT,2,(double)i*spacestep,netfluxin[i]);
      else
	Grace(GPOINT,2,(double)i*spacestep,0.01);
      Grace(GSET,1);
      if(netfluxin[i]<-EPSILON)
	Grace(GPOINT,2,(double)i*spacestep,-netfluxin[i]);
      else
	Grace(GPOINT,2,(double)i*spacestep,0.01);
      //Grace(GLINE,GWIDTH,2.);
      /* 
	 Grace(GPOINT,2,(double)i*spacestep,fluxin[i]);      
	 Grace(GSET,2);
	 Grace(GLINE,GWIDTH,1.);
	 Grace(GPOINT,2,(double)i*spacestep,fluxout[i]);        
      */
  }
  Grace(GSCALE,"Logarithmic");
  Grace(GWORLD,0.,0.1,spacestep*(double)nrow,1000.);
  Grace(GREDRAW);
  //Grace(GAUTOSCALE);
  snprintf(a_string,STRING,"%s/%s_flux_grace.agr",av[2],av[3]);
  Grace(GSAVE,a_string);
  if(grace)
    CloseGrace(GDETACH);
  else
    CloseGrace(GCLOSE);
}

void Key(int key,int mouse_i,int mouse_j)
{
}

void Ratinho(int mousestroke,int mouse_i,int mouse_j)
{
  timetoexit=1;
}

void MyInDat(char filename[],char *format,char *text,void *value)
{
  if(!InDat(filename,format,text,value)) {
    fprintf(stdout,"cannot find \"%s\" in parameter file; exitting now!\n",text);
    exit(EXIT_FAILURE);
  }
}

void SetPars(char name[])
{
  char conversion[10];
  FLOAT tmpupperboundary,tmplowerboundary;

#ifdef _SMALL
  snprintf(conversion,10,"%%f");
#else
  snprintf(conversion,10,"%%lf");
#endif
  ReadOptions(name);
  MyInDat(name,"%lf","fluxlength",&fluxlength);
  MyInDat(name,"%s","arrowcolor",arrowcolor);
  MyInDat(name,"%d","inversey",&inversey);
  MyInDat(name,"%d","useregion",&useregion);
  MyInDat(name,"%d","usefixedauxin",&usefixedauxin);
  MyInDat(name,"%d","usefixedflux",&usefixedflux);
  MyInDat(name,"%d","startcellrow",&startcellrow);
  MyInDat(name,"%d","startpixel",&startpixel);
  MyInDat(name,"%d","endcellrow",&endcellrow);
  MyInDat(name,"%d","endpixel",&endpixel);
  MyInDat(name,"%d","bendingroot",&bendingroot);
  MyInDat(name,"%d","showflux",&showflux);
  MyInDat(name,"%d","showauxin",&showauxin);
  MyInDat(name,"%d","showexpression",&showexpression);
  MyInDat(name,"%s","readpattern",readpattern);
  MyInDat(name,"%s","readunbentpattern",readunbentpattern);
  MyInDat(name,conversion,"fixedminauxin",&fixedminauxin);
  MyInDat(name,conversion,"fixedmaxauxin",&fixedmaxauxin);
  MyInDat(name,conversion,"fixedminflux",&fixedminflux);
  MyInDat(name,conversion,"fixedmaxflux",&fixedmaxflux);
  MyInDat(name,conversion,"upperboundary",&tmpupperboundary);
  MyInDat(name,conversion,"lowerboundary",&tmplowerboundary);
  if(!(1.-tmpupperboundary-tmplowerboundary>EPSILON)) {
    fprintf(stderr,"error: sum of upperboundary and lowerboundary should be smaller than 1 (right now it's %lf)\n",tmpupperboundary+tmplowerboundary);
    exit(EXIT_FAILURE);
  }
  upperboundary=tmpupperboundary/(1.-tmpupperboundary-tmplowerboundary);
  lowerboundary=tmplowerboundary/(1.-tmpupperboundary-tmplowerboundary);
  if(useregion && (usefixedauxin || usefixedflux))
    fprintf(stdout,"warning: both useregion and usefixed are defined, usefixed will be ignored\n");
}

void FillExpression(int i,int j)
{
  if(gradlog)
    expression[i][j]=log(dynamicpermeability+1e-6);
  else
    expression[i][j]=dynamicpermeability;
}

int main(int argc,char *argv[])
{
  int i,j,k,m;
  FILE *fp;
  int tmpnrow,tmpncol,found,foundinzone,location;
  FLOAT length,scaling;

  if(argc!=4) {
    fprintf(stdout,"usage: %s parfile datadir outputfile\n",argv[0]);
    exit(EXIT_FAILURE);
  }
  
  mouse=&Ratinho;
  keyboard=&Key;

  SetPars(argv[1]);
  if(snprintf(a_string,STRING,"%s/%s.par",argv[2],argv[3])>=STRING)
    fprintf(stderr,"warning: parfile name too long: %s\n",a_string);
  else
    SaveOptions(argv[1],a_string);

  snprintf(a_string,STRING,"%s/state.dat",argv[2]);
  if(ReadSize(&nrow,&ncol,a_string)) {
    fprintf(stdout,"the file %s is not a vadid state file\n exitting now!\n",a_string);
    exit(EXIT_FAILURE);
  }  

  if(useregion) {
    snprintf(a_string,STRING,"%s%s",readpattern,".centers");
    if((fp=fopen(a_string,"r"))==NULL) {
      printf("error: could not open the file %s\n",a_string);
      exit(EXIT_FAILURE);
    }
    fscanf(fp,"%d %d",&totalcellrownumber,&totalcellcolnumber);
    if(((cellrowtofirstpixel=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL) ||
       ((cellrowtocenterpixel=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL) ||
       ((cellrowtolastpixel=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL) ||
       ((cellcoltofirstpixel=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL) ||
       ((cellcoltocenterpixel=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL) ||
       ((cellcoltolastpixel=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL)) {
      fprintf(stderr,"error in memory allocation\n");
      exit(EXIT_FAILURE);
    }
    
    if(!(startcellrow<totalcellrownumber) || !(endcellrow<totalcellrownumber)) {
      fprintf(stderr,"error: startcellrow or endcellrow larger than totalcellrownumber (%d or %d > %d)\n",startcellrow,endcellrow,totalcellrownumber);
      exit(EXIT_FAILURE);
    }

    for(k=0;k<totalcellrownumber;k++)
      fscanf(fp,"%*d %d %d %d",&cellrowtofirstpixel[k],&cellrowtocenterpixel[k],&cellrowtolastpixel[k]);
    
    for(k=0;k<totalcellcolnumber;k++)
      fscanf(fp,"%*d %d %d %d",&cellcoltofirstpixel[k],&cellcoltocenterpixel[k],&cellcoltolastpixel[k]);
  }

  if(bendingroot) {
    backwardstate=New();
    snprintf(a_string,STRING,"%s%s",readpattern,".backwards");
    ReadPat(backwardstate,1,1,'i',a_string);
    snprintf(a_string,STRING,"%s%s",readpattern,".forwards");
    if(ReadSize(&oldnrow,&oldncol,a_string)) {
      fprintf(stdout,"the file %s is not a vadid state file\n exitting now!\n",a_string);
      exit(EXIT_FAILURE);
    }  
    forwardstate=NewPlane(oldnrow,oldncol);
    forwardcells=NewPlane(oldnrow,oldncol);
    tmpnrow=nrow;
    tmpncol=ncol;
    nrow=oldnrow;
    ncol=oldncol;
    ReadPat(forwardstate,1,1,'i',a_string);
    snprintf(a_string,STRING,"%s%s",readunbentpattern,".state");
    ReadPat(forwardcells,1,1,'i',a_string);
    CNew(&unbentcells,2000,TOTALROOTTYPES);
    UpdateCFill(forwardcells,&unbentcells);
    InitCellPosition(&unbentcells);
    UpdateCellPosition(forwardcells,&unbentcells);
    nrow=tmpnrow;
    ncol=tmpncol;
  }

  state = New();
  fluxx =FNew();
  fluxy =FNew();
  flux=FNew();
  h=FNew();
  s=FNew();
  v=FNew();
  auxin=FNew();
  logauxin=FNew();
  expression=FNew();

  ColorName(1,1,arrowcolor);
    
  OpenDisplay("Fluxes",nrow,ncol);
  OpenPNG(&windowpng,".");

  snprintf(a_string,STRING,"%s/state.dat",argv[2]);
  ReadPat(state,1,1,'i',a_string);
  if(showflux) {
    snprintf(a_string,STRING,"%s/final_fluxx.dat",argv[2]);
    FReadPat(fluxx,1,1,'f',a_string);
    snprintf(a_string,STRING,"%s/final_fluxy.dat",argv[2]);
    FReadPat(fluxy,1,1,'f',a_string);
  }
  if(showauxin) {
    snprintf(a_string,STRING,"%s/final_auxin.dat",argv[2]);
    FReadPat(auxin,1,1,'f',a_string);
    if(gradlog) {
      PLANE(
	    logauxin[i][j]=(FLOAT)log(auxin[i][j]+1e-6);
	    );
    }
    else {
      PLANE(
	    logauxin[i][j]=auxin[i][j];
	    );
    }
  }

  CNew(&cells,2000,TOTALROOTTYPES);
  UpdateCFill(state,&cells);
  InitCellPosition(&cells);
  UpdateCellPosition(state,&cells);
  
  int first=1000,second=1125,third=1250,fourth=1375,fifth=1500,sixth=1625,seventh=1750,eighth=1875,nineth=2000;
  ColorGrad(ColorTable(first,first,BLACK),ColorRGB(second,79,0,95));
  ColorGrad(second,ColorTable(third,third,BLUE));
  ColorGrad(third,ColorTable(fourth,fourth,CYAN));
  ColorGrad(fourth,ColorTable(fifth,fifth,GREEN));
  ColorGrad(fifth,ColorTable(sixth,sixth,YELLOW));
  ColorGrad(sixth,ColorName(seventh,seventh,"orange"));
  ColorGrad(seventh,ColorTable(eighth,eighth,RED));
  ColorGrad(eighth,ColorRGB(nineth,139,0,0));

  ColorName(50,50,"red");
  ColorName(51,51,"blue");
  ColorName(52,52,"green");
  ColorName(53,53,"dark salmon");

  //ColorRamp(1000,2000,RED);

  /*
  if(showexpression) {
    snprintf(a_string,STRING,"%s/expression_%.5d",argv[2],atoi(argv[3]));
    fp=fopen(a_string,"r");
    while(fscanf(fp,"%d\t",&c)!=EOF) {
      fscanf(fp,"%lg\t%*g\t%*g\t%*g\t%*g\n",&dynamicpermeability);
      OneCell(state,&cells,c,&FillExpression);
    }
  }
  */

  if(showflux) {
    foundinzone=0;
    found=0;
    CPLANE(state,
	   flux[i][j]=hypot(fluxx[i][j],fluxy[i][j]);
	   );
    specialcelltype=SPECIALCELLS;
    fprintf(stdout,"the lowest and highest fluxes in the root are: %g\t%g\n",CFMin(flux,state),CFMax(flux,state));
    specialcelltype=0;
    //before the rest, which modifies fluxx and fluxy
    UpdateFluxPatterns(argv);
    if(useregion || usefixedflux) {
      if(useregion) {
	CPLANE(state,
	       if(bendingroot)
		 location=backwardstate[i][j]/SHRT_MAX; //the y-position
	       else
		 location=i;
	       //fluxx[i][j]=location;
	       //fluxy[i][j]=0;
	       if(location>=cellrowtofirstpixel[startcellrow]+startpixel && location<cellrowtofirstpixel[endcellrow]+endpixel) { //we're in the special zone
		 if(!foundinzone) {
		   foundinzone=1;
		   maxinzoneflux=hypot(fluxx[i][j],fluxy[i][j]);
		   mininzoneflux=hypot(fluxx[i][j],fluxy[i][j]);
		 }
		 else {
		   if((length=hypot(fluxx[i][j],fluxy[i][j]))>maxinzoneflux)
		     maxinzoneflux=length;
		   else if(length<mininzoneflux)
		     mininzoneflux=length;
		 }
	       }
	       );
      }
      else {
	maxinzoneflux=fixedmaxflux;
	mininzoneflux=fixedminflux;
      }
      if(useregion)
	fprintf(stdout,"the lowest and highest fluxes in the zone are: %g\t%g\n",mininzoneflux,maxinzoneflux);
      CPLANE(state,
	     if(!found) {
	       found=1;
	       maxflux=hypot(fluxx[i][j],fluxy[i][j]);
	       minflux=hypot(fluxx[i][j],fluxy[i][j]);
	     }
	     else {
	       if((length=hypot(fluxx[i][j],fluxy[i][j]))>maxflux)
		 maxflux=length;
	       else if(length<minflux)
		 minflux=length;
	     }
	     );
      
      //printf("%lf %lf %lf %lf\n",minflux,maxflux,mininzoneflux,maxinzoneflux);
      //first scale the high values 
      CPLANE(state,
	     if((length=hypot(fluxx[i][j],fluxy[i][j]))>maxinzoneflux) {
	       scaling=(maxinzoneflux+upperboundary*(maxinzoneflux-mininzoneflux)*(length-maxinzoneflux)/(maxflux-maxinzoneflux))/length;
	       fluxx[i][j]*=scaling;
	       fluxy[i][j]*=scaling;
	     }
	     );
      //printf("%lf %lf\n",CFMin(fluxx,state),CFMax(fluxx,state));
      //then bring down the low values 
      if(mininzoneflux-lowerboundary*(maxinzoneflux-mininzoneflux)>EPSILON) { //we want to scale it
	//printf("%lf\n",mininzoneflux-lowerboundary*(maxinzoneflux-mininzoneflux));
	//printf("%lf\n",lowerboundary*(maxinzoneflux-mininzoneflux)/mininzoneflux);
	CPLANE(state,
	       if((length=hypot(fluxx[i][j],fluxy[i][j]))>mininzoneflux) {
		 scaling=(length-(mininzoneflux-lowerboundary*(maxinzoneflux-mininzoneflux)))/length;
		 fluxx[i][j]*=scaling;
		 fluxy[i][j]*=scaling;
	       }
	       else {
		 scaling=lowerboundary*(maxinzoneflux-mininzoneflux)/mininzoneflux;
		 fluxx[i][j]*=scaling;
		 fluxy[i][j]*=scaling;
	       }
	       );
      }
    }
    
    //put into h,s,v, use 2 planes, first plane, offset, relative scaling, angle, second plane, etc.
    HSVFill(h,s,v,2,fluxx,0.,1.,0.,fluxy,0.,(inversey?1.:-1.),M_PI_2);
    //ColorRamp(1000,2000,GREEN);
    //FPlaneDisplay(v,0,0,0.,FMax(v),1000,2000);
    //HSVDisplay(h,s,v,0,0);
    CHSVDisplay(h,s,v,state,0,0);
    //meanx and meany doesn't work, because can be even outside of the cell
    if(bendingroot) {
      CELLS(unbentcells,
	    if(c>=SPECIALCELLS && unbentcells.area[c]>0) {
	      i=nearbyint(unbentcells.shape[c].meany);
	      j=nearbyint(unbentcells.shape[c].meanx);
	      m=0;
	      location=forwardstate[i][j];
	      if(!location) {	  
		do {
		  m++;
		  NeighbourNumber(m);
		  //if(m>1)
		  fprintf(stdout,"working on finding center-of-mass of cell %d, level %d\n",c,m);
		  if(m>100)
		    break;
		  WIDENEIGHBOURS(
				 if(forwardcells[newindex(i+y,oldnrow)][newindex(j+x,oldncol)]==c &&
				    (location=forwardstate[newindex(i+y,oldnrow)][newindex(j+x,oldncol)])!=0)
				   break;
				 );
		} while(location==0);
	      }
	      i=location/SHRT_MAX;
	      j=location%SHRT_MAX;
	      //printf("drawing arrow for cell %d\n",c);
	      Arrow(0,0,i,j,fluxlength*v[i][j]*(inversey?1.:-1.)*sin(2.*M_PI*h[i][j]),fluxlength*v[i][j]*cos(2.*M_PI*h[i][j]),1);
	      // Arrow(0,0,cells.shape[c].meany,cells.shape[c].meanx, 0.1*fluxy[i][j], 0.1*fluxx[i][j], 9);
	      //fluxlength*v[i][j]*-sin(2.*M_PI*h[i][j]),fluxlength*v[i][j]*cos(2.*M_PI*h[i][j]),9);
	    }
	    );
    }
    else {
      CELLS(cells,
	    if(c>=SPECIALCELLS && cells.area[c]>0) {
	      i=nearbyint(cells.shape[c].meany);
	      j=nearbyint(cells.shape[c].meanx);
	      //printf("drawing arrow for cell %d\n",c);
	      Arrow(0,0,cells.shape[c].meany,cells.shape[c].meanx,fluxlength*v[i][j]*(inversey?1.:-1.)*sin(2.*M_PI*h[i][j]),fluxlength*v[i][j]*cos(2.*M_PI*h[i][j]),1);
	    }
	    );
    }
    if(graphics) {
      printf("finished! Click mouse to save picture.\n");
      while(!timetoexit)
	usleep(1);
    }
    WindowPNG(&windowpng);
    snprintf(a_string,STRING,"mv %.5d.png %s/%s_flux_arrows.png",windowpng.nframes-1,argv[2],argv[3]);
    system(a_string);
    OpenPNG(&png1,".");
    CHSVPNG(h,s,v,state,&png1);
    snprintf(a_string,STRING,"mv %.5d.png %s/%s_flux.png",png1.nframes-1,argv[2],argv[3]);
    system(a_string);
    printf("finished! Saved %s/%s_flux.png and %s/%s_flux_arrows.png\n",argv[2],argv[3],argv[2],argv[3]);
  } //end of (showflux)

  if(showauxin) {
    foundinzone=0;
    found=0;
    if(useregion || usefixedauxin) {
      if(useregion) {
	CPLANE(state,
	       location=backwardstate[i][j]/SHRT_MAX; //the y-position
	       //fluxx[i][j]=location;
	       //fluxy[i][j]=0;
	       if(location>=cellrowtofirstpixel[startcellrow]+startpixel && location<cellrowtofirstpixel[endcellrow]+endpixel) { //we're in the special zone
		 if(!foundinzone) {
		   foundinzone=1;
		   maxinzoneflux=logauxin[i][j];
		   mininzoneflux=logauxin[i][j];
		 }
		 else {
		   if((length=logauxin[i][j])>maxinzoneflux)
		     maxinzoneflux=length;
		   else if(length<mininzoneflux)
		     mininzoneflux=length;
		 }
	       }
	       );
      }
      else {
	if(gradlog) {
	  maxinzoneflux=log(fixedmaxauxin+1e-6);
	  mininzoneflux=log(fixedminauxin+1e-6);
	}
	else {
	  maxinzoneflux=fixedmaxauxin;
	  mininzoneflux=fixedminauxin;
	}
      }
      if(useregion)
	fprintf(stdout,"the lowest and highest values in the zone are: %g\t%g\n",exp(mininzoneflux),exp(maxinzoneflux));
      specialcelltype=SPECIALCELLS;
      fprintf(stdout,"the lowest and highest values in the root are %g\t%g\n",CFMin(auxin,state),CFMax(auxin,state));
      /*
      CPLANE(state,
	     if(auxin[i][j]<fixedminauxin)
	       fprintf(stdout,"auxin below fixed min at (%d,%d) [state=%d]: %g\n",i,j,state[i][j],auxin[i][j]);
	     );
      */
      specialcelltype=0;
      CPLANE(state,
	     if(state[i][j]==CELLWALL || state[i][j]>=SPECIALCELLS) {
	       if(!found) {
		 found=1;
		 maxflux=logauxin[i][j];
		 minflux=logauxin[i][j];
	       }
	       else {
		 if((length=logauxin[i][j])>maxflux)
		   maxflux=length;
		 else if(length<minflux)
		   minflux=length;
	       }
	     }
	     );
      
      //printf("%lf %lf %lf %lf\n",exp(minflux),exp(maxflux),exp(mininzoneflux),exp(maxinzoneflux));

      //check if any value will end up in the highest zone;
      if(maxflux-maxinzoneflux>EPSILON) {
	//first scale the high values 
	CPLANE(state,
	       length=max(minflux,logauxin[i][j]);
	       if(length-maxinzoneflux>EPSILON) {
		 logauxin[i][j]=maxinzoneflux+upperboundary*(maxinzoneflux-mininzoneflux)*(length-maxinzoneflux)/(maxflux-maxinzoneflux);
	       }
	       );
      }
      else {
	noreallyhighvalues=1;
	forcedmax=maxinzoneflux-mininzoneflux+lowerboundary*(maxinzoneflux-mininzoneflux);
	printf("Warning: highest value less than fixedmaxauxin: %lf < %lf\n",exp(maxflux),exp(maxinzoneflux));
      }

      //then bring down the low values 
      //if(mininzoneflux-lowerboundary*(maxinzoneflux-mininzoneflux)>EPSILON) { //not with logaux
      //printf("%lf\n",mininzoneflux-lowerboundary*(maxinzoneflux-mininzoneflux));
      CPLANE(state,
	     length=max(minflux,logauxin[i][j]);
	     //if(state[i][j]==17) {
	     //   printf("%g %g %g %g\n",minflux,logauxin[i][j],max(minflux,logauxin[i][j]),mininzoneflux);
	     //}
	     if(length>mininzoneflux) {
	       logauxin[i][j]=length-mininzoneflux+lowerboundary*(maxinzoneflux-mininzoneflux);
	     }
	     else {
	       logauxin[i][j]=lowerboundary*(maxinzoneflux-mininzoneflux)*(length-minflux)/(mininzoneflux-minflux);
	     }
	     );
    }
    
    if(graphics) {
      if(noreallyhighvalues)
	CFPlaneDisplay(logauxin,state,0,0,0.,forcedmax,1000,1875);
      else
	CFPlaneDisplay(logauxin,state,0,0,0.,CFMax(logauxin,state),1000,2000);
      printf("finished! Click mouse to save picture.\n");

      while(!timetoexit)
	usleep(1);
    }
    OpenPNG(&png1,".");
    if(noreallyhighvalues)
      CFPlanePNG(logauxin,state,&png1,0.,forcedmax,1000,1875);
    else
      CFPlanePNG(logauxin,state,&png1,0.,CFMax(logauxin,state),1000,2000);

    snprintf(a_string,STRING,"mv %.5d.png %s/%s_auxin.png",png1.nframes-1,argv[2],argv[3]);
    system(a_string);
    printf("finished! Saved %s/%s_auxin.png\n",argv[2],argv[3]);
  }

 if(showexpression) {
    foundinzone=0;
    found=0;
    //todo: introduce a usefixedexpression as well
    if(useregion || usefixedauxin) {
      if(useregion) {
	CPLANE(state,
	       location=backwardstate[i][j]/SHRT_MAX; //the y-position
	       //fluxx[i][j]=location;
	       //fluxy[i][j]=0;
	       if(location>=cellrowtofirstpixel[startcellrow]+startpixel && location<cellrowtofirstpixel[endcellrow]+endpixel) { //we're in the special zone
		 if(!foundinzone) {
		   foundinzone=1;
		   maxinzoneflux=expression[i][j];
		   mininzoneflux=expression[i][j];
		 }
		 else {
		   if((length=expression[i][j])>maxinzoneflux)
		     maxinzoneflux=length;
		   else if(length<mininzoneflux)
		     mininzoneflux=length;
		 }
	       }
	       );
      }
      else {
	if(gradlog) {
	  maxinzoneflux=log(fixedmaxauxin+1e-6);
	  mininzoneflux=log(fixedminauxin+1e-6);
	}
	else {
	  maxinzoneflux=fixedmaxauxin;
	  mininzoneflux=fixedminauxin;
	}
      }
      fprintf(stdout,"the lowest and highest values in the zone are: %g\t%g\n",mininzoneflux,maxinzoneflux);

      CPLANE(state,
	     if(!found) {
	       found=1;
	       maxflux=expression[i][j];
	       minflux=expression[i][j];
	     }
	     else {
	       if((length=expression[i][j])>maxflux)
		 maxflux=length;
	       else if(length<minflux)
		 minflux=length;
	     }
	     );
      
      //printf("%lf %lf %lf %lf\n",minflux,maxflux,mininzoneflux,maxinzoneflux);
      //first scale the high values 
      CPLANE(state,
	     if((length=expression[i][j])>maxinzoneflux) {
	       expression[i][j]=maxinzoneflux+upperboundary*(maxinzoneflux-mininzoneflux)*(length-maxinzoneflux)/(maxflux-maxinzoneflux);
	     }
	     );
      //printf("%lf %lf\n",CFMin(expression,state),CFMax(expression,state));
      //then bring down the low values 
      //if(mininzoneflux-lowerboundary*(maxinzoneflux-mininzoneflux)>EPSILON) { //not with logaux
      //printf("%lf\n",mininzoneflux-lowerboundary*(maxinzoneflux-mininzoneflux));
      CPLANE(state,
	     if((length=expression[i][j])-mininzoneflux>-EPSILON) {
	       expression[i][j]-=mininzoneflux-lowerboundary*(maxinzoneflux-mininzoneflux);
	     }
	     else {
	       expression[i][j]=lowerboundary*(maxinzoneflux-mininzoneflux)*(length-minflux)/(mininzoneflux-minflux);
	     }
	     );
      printf("%lf %lf\n",CFMin(expression,state),CFMax(expression,state)); 
    }
    
    if(graphics) {
      CFPlaneDisplay(expression,state,0,0,0.,CFMax(expression,state),1000,2000);
      printf("finished! Click mouse to save picture.\n");
      while(!timetoexit)
	usleep(1);
    }
    OpenPNG(&png1,".");
    CFPlanePNG(expression,state,&png1,0.,CFMax(expression,state),1000,2000);
    snprintf(a_string,STRING,"mv %.5d.png %s/%s_expression.png",png1.nframes-1,argv[2],argv[3]);
    system(a_string);
    printf("finished! Saved %s/%s_expression.png\n",argv[2],argv[3]);
  }

  return 0;
}

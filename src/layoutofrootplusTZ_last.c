//I just want to make a 2D-rectangular root here, without the need of
//specifying PINs or bending.  Thus, this program will only generate a
//layout, which can then be read (or used as a .png) for localizing
//PINs, bending etc.

#include <excal2.h>

#define MEDIUM          0
#define CELLWALL        1

#define VASCULAR        2
#define PERICYCLE       3
#define ENDODERMIS      4
#define CORTEX          5
#define EPIDERMIS       6
//lateral root cap
#define LRC             7
#define COLUMELLAROOT   8
//initials
#define CORTEXENDO_I    9
#define LRCEPI_I        10
#define COL_I           11
#define QCROOT          12
//differentiation zone
//specialvascular should be last
#define VASCULAR_D      13
#define PERICYCLE_D     14
#define ENDODERMIS_D    15
#define CORTEX_D        16
#define EPIDERMIS_D     17
#define SPECIALVASCULAR 18
//transition zone - is it ok to set TZ tissues before DZ ones?
#define VASCULAR_TZ     19
#define PERICYCLE_TZ    20
#define ENDODERMIS_TZ   21
#define CORTEX_TZ       22
#define EPIDERMIS_TZ    23
#define LRC_TZ          24
//elongation zone
#define VASCULAR_EZ     25
#define PERICYCLE_EZ    26
#define ENDODERMIS_EZ   27
#define CORTEX_EZ       28
#define EPIDERMIS_EZ    29
#define DEADCELLS       30
#define FAKECELLWALL    31
#define FAKECELLS       32
#define TOTALROOTTYPES  33 //it shouldnt go over 100...



#define SPECIALCELLS   100

#define DZ               0
#define EZ               1
#define TZ               2
#define MZ               3 //remember to respect the numbering order
#define TOTALZONES       4

//planes
static TYPE **state,**newstate;

//general
static char a_string[STRING];
static char winfirst[STRING]="The root layout";

//output
static int savegraphics;

//parameters
static int left,top;
static int lowest,right;
static int rootwidth,epiwidth,epiwidthup,cortexwidth,endowidth,periwidth,vascwidth,lrcwidth,lrcwidthdown;
//introducing here TZcells
static int cellwall,vascnum,epinum,epinumup,cortexnum,endonum,perinum,lrcnum,lrcnumup,EZblocknum,TIPblocknum,EZcellh,MZheight,MZblocknum,MZcellh,TZblocknum,TZcellh,TZheight,DZheight,DZblocknum,DZcellh,borderwidth;
static int EZheight,colltiers,collheight,collnum,QCnum,specialvascular,specialDZcell2,specialDZcell1,vascular_d;
static int cutshoot,cutroot;
static int shootexclude,rootexclude;
static int casparianEZ,casparianDZ;
static int totalseparation,totalblock,centrestrip,fileseparation;
static int totalseparationstart,totalseparationend;
static int separationstart,separationend;
static Png png1;
static int vacuole[TOTALZONES]; //0: no vacuole,1:diameter; 2:distance to border; 3:percentage- adjusted because now I have 4 zones
static double vacuolewidth[TOTALZONES];
static int ablation;
static int end=1;
static int curvedbending,curvedheight;
static double curvedb0,curvedangle,curvedpower,curvedpowerstep;
static double QCdistance,QCthickness;
//static double curvedratio=0.5,curveda0=.1;
static double *curvedb,*curveda;
static double *curvedstartx;
static int curvedfiles;


extern int nrow,ncol,graphics;
extern void (*mouse)(int,int,int);
extern int (*newindex)(int,int);

//todo
//these are only used in InitCells, but should be properly set to zero
int left1[TOTALROOTTYPES];
int right1[TOTALROOTTYPES];
int left2[TOTALROOTTYPES];
int right2[TOTALROOTTYPES];
int lower1[TOTALROOTTYPES];
int top1[TOTALROOTTYPES];
int roottypecellwidth[TOTALROOTTYPES];
int zonestart[TOTALZONES];//tocheck - checked
int zonecellheight[TOTALZONES];//tocheck - checked
//until here

void InitCells()
{
  int i,k,m;
  int collumellaissue=0;
  double vacwidth;
  double reli,relj;
  int zoneij,zoneimin1j,zoneijmin1,zoneimin1jmin1,leftzone,rightzone;
  double lrcedge,epiedge,cortexedge,endoedge,periedge,localwidth,localwidthmin1;
  //  double vascedge;
  int thiszone=0,outzone,belowoutzone;

  DZheight=DZblocknum*(DZcellh+cellwall); //Differentiation Zone 
  EZheight=EZblocknum*(EZcellh+cellwall); //Elongation Zone
  TZheight=TZblocknum*(TZcellh+cellwall);//TZ Zone - set TZblocknum to 3/4; check if I need the extra cellwall
  MZheight=MZblocknum*(MZcellh+cellwall)+cellwall;//MZ Zone

	
	
  if(!MZblocknum) 
  {
    MZheight-=cellwall;//that means that I don't have MZ: MZheight=0+cellwall-cellwall=0
      if(EZblocknum && TZblocknum) // if I don't have MZ but EZblocknum is !=0, add cell wall
     // EZheight+=cellwall;
      TZheight+=cellwall;
      else
      DZheight+=cellwall;
  }
  top=borderwidth+1;//top of whole root!
  zonestart[DZ]=top;
  zonecellheight[DZ]=DZcellh+cellwall;// height of single cell in each zone
  zonestart[EZ]=zonestart[DZ]+DZheight;
  zonecellheight[EZ]=EZcellh+cellwall;
  zonestart[TZ]=zonestart[EZ]+EZheight;
  zonecellheight[TZ]=TZcellh+cellwall;
  zonestart[MZ]=zonestart[TZ]+TZheight;
  zonecellheight[MZ]=MZcellh+cellwall;
  left=borderwidth+1;
  lowest=zonestart[MZ]+MZheight;
  right=left+rootwidth;

	
	if(totalseparationstart<DZblocknum)
		separationstart=zonestart[DZ]+(totalseparationstart-1)*(DZcellh+cellwall); // why -1?
	else if(totalseparationstart<DZblocknum+EZblocknum)
		separationstart=zonestart[EZ]+(totalseparationstart-DZblocknum-1)*(EZcellh+cellwall);
	else if(totalseparationstart<DZblocknum+EZblocknum+TZblocknum)
		separationstart=zonestart[TZ]+(totalseparationstart-DZblocknum-EZblocknum-1)*(TZcellh+cellwall);
	else
		separationstart=zonestart[MZ]+(totalseparationstart-DZblocknum-EZblocknum-TZblocknum-1)*(MZcellh+cellwall);
	if(totalseparationend<DZblocknum)
		separationend=zonestart[DZ]+(totalseparationend)*(DZcellh+cellwall);
	else if(totalseparationend<DZblocknum+EZblocknum)
		separationend=zonestart[EZ]+(totalseparationend-DZblocknum)*(EZcellh+cellwall);
	else if(totalseparationend<DZblocknum+EZblocknum+TZblocknum)
		separationend=zonestart[TZ]+(totalseparationend-DZblocknum-EZblocknum)*(TZcellh+cellwall);
	else
		separationend=zonestart[MZ]+(totalseparationend-DZblocknum-EZblocknum-TZblocknum)*(MZcellh+cellwall);
  
  if(shootexclude+rootexclude>=MZblocknum+TZblocknum+EZblocknum+DZblocknum) { //marks most n shootward and rootward cellrows as 'dead' cells & 'dead' cell wall
    fprintf(stdout,"the whole root would be cut, this is stupid!\n");
    exit(EXIT_FAILURE);
  }
  cutshoot=zonestart[DZ]+min(DZblocknum,shootexclude)*zonecellheight[DZ];// since shootexclude is set to 0 or 1, it will always be lower than DZblocknum (cutshoot = zonestart[DZ])
  shootexclude-=DZblocknum;
  if(shootexclude>0) {
  cutshoot+=min(EZblocknum,shootexclude)*zonecellheight[EZ];
  shootexclude-=EZblocknum;
  }
  if(shootexclude>0)
    cutshoot+=shootexclude*zonecellheight[MZ];

  cutroot=lowest-min(MZblocknum,rootexclude)*zonecellheight[MZ];
  rootexclude-=MZblocknum;
  if(rootexclude>0) {
  cutroot-=min(EZblocknum,rootexclude)*zonecellheight[EZ];
  rootexclude-=EZblocknum;
  }
  if(rootexclude>0)
    cutroot-=rootexclude*zonecellheight[DZ];
  
  //warning: celltypes can only use information of celltypes which have lower number 
	if(PERICYCLE<=VASCULAR || ENDODERMIS<=PERICYCLE || CORTEX<=ENDODERMIS || EPIDERMIS<=CORTEX || LRC<=EPIDERMIS || QCROOT<=ENDODERMIS || QCROOT<=COLUMELLAROOT || SPECIALVASCULAR<=PERICYCLE || ENDODERMIS_D<=PERICYCLE ||  CORTEX_D<=ENDODERMIS || EPIDERMIS_D<=CORTEX ||PERICYCLE_TZ<=VASCULAR_TZ || ENDODERMIS_TZ<=PERICYCLE || CORTEX_TZ<=ENDODERMIS_TZ || EPIDERMIS_TZ<=CORTEX_TZ || /*LRC_TZ<=EPIDERMIS_TZ || VASCULAR_EZ<=LRC_TZ || */PERICYCLE_EZ<=VASCULAR_EZ || ENDODERMIS_EZ<=PERICYCLE_EZ || CORTEX_EZ<=ENDODERMIS_EZ || EPIDERMIS_EZ<=CORTEX_EZ) {

    fprintf(stdout,"error in function InitCells, please look at your code\n");
    exit(EXIT_FAILURE);
  }
  
  for(i=0;i<TOTALROOTTYPES;i++) {
    switch(i) {// check a variable for equality against a list of values: when the variable being switched on is equal to a case, the statements following that case will execute until a break statement is reached
    case MEDIUM:
      break;
    case CELLWALL:
      break;
    case VASCULAR:
      left1[VASCULAR]=left+(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall));
      right1[VASCULAR]=left+(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall))+vascnum*(vascwidth+cellwall);// left and right boundaries of the vasculature
      if(vascular_d)// that means that if vascular_d is  different from 0
	top1[VASCULAR]=zonestart[MZ];
      else
	top1[VASCULAR]=zonestart[DZ];//=top
      lower1[VASCULAR]=lowest-colltiers*(collheight+cellwall)-MZcellh-cellwall; //also including the QC of cellheight: the height of the vasculature is shorter than the other files because we also have the QC ( its height is on one MZcellh+cellwall)     //but longer if curvedbending
      if(curvedbending)
	lower1[VASCULAR]+=MZcellh+cellwall;// basically, it includes QC height
      roottypecellwidth[VASCULAR]=vascwidth+cellwall;
      break;
    case PERICYCLE:
      left1[PERICYCLE]=left+(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall));
      right1[PERICYCLE]=left1[PERICYCLE]+perinum*(periwidth+cellwall);
      left2[PERICYCLE]=right1[VASCULAR];// the left margin of the pericycle to the right of the vasculature start at the same location of right1
      right2[PERICYCLE]=left2[PERICYCLE]+perinum*(periwidth+cellwall);
      lower1[PERICYCLE]=lowest-colltiers*(collheight+cellwall);// this height is lower than lower1[VASCULAR]- do pericycle and QC lower1 overlap?
      top1[PERICYCLE]=zonestart[MZ];//upto the MZ
      roottypecellwidth[PERICYCLE]=periwidth+cellwall;
      break;
    case ENDODERMIS:
      left1[ENDODERMIS]=left+(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall));
      right1[ENDODERMIS]=left1[PERICYCLE];
      left2[ENDODERMIS]=right2[PERICYCLE];
      right2[ENDODERMIS]=right2[PERICYCLE]+endonum*(endowidth+cellwall);
      lower1[ENDODERMIS]=lowest-colltiers*(collheight+cellwall);
      top1[ENDODERMIS]=zonestart[MZ]; //upto the MZ
      roottypecellwidth[ENDODERMIS]=endowidth+cellwall;
      break;  
    case CORTEX:
      left1[CORTEX]=left+lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall);
      right1[CORTEX]=left1[ENDODERMIS];
      left2[CORTEX]=right2[ENDODERMIS];
      right2[CORTEX]=right2[ENDODERMIS]+cortexnum*(cortexwidth+cellwall);
      lower1[CORTEX]=lowest-colltiers*(collheight+cellwall);
      top1[CORTEX]=zonestart[MZ]; 
      roottypecellwidth[CORTEX]=cortexwidth+cellwall;
      break;  
    case EPIDERMIS:
     left1[EPIDERMIS]=left+(lrcnumup*(lrcwidth+cellwall));
	//     left1[EPIDERMIS]=left+(lrcnumup*(lrcwidth+cellwall)/2);

      right1[EPIDERMIS]=left1[CORTEX];
      left2[EPIDERMIS]=right2[CORTEX];
            right2[EPIDERMIS]=left2[EPIDERMIS]+epinumup*(epiwidthup+cellwall)+cellwall; //extra +cellwall because right border (partially)
      //right2[EPIDERMIS]=left2[EPIDERMIS]+2*epinum*(epiwidth+cellwall)+cellwall; //extra +cellwall because right border (partially)
	  
      lower1[EPIDERMIS]=lowest-colltiers*(collheight+cellwall);
      top1[EPIDERMIS]=zonestart[MZ];//upto the EZ- to check with the new TZ
      roottypecellwidth[EPIDERMIS]=epiwidthup+cellwall;
      break;  
      case LRC:
     // left1[LRC]=left+lrcnum/2*(lrcwidth+cellwall);
      left1[LRC]=left;
      right1[LRC]=left1[EPIDERMIS];
      left2[LRC]=right2[EPIDERMIS]-cellwall;
      right2[LRC]=left2[LRC]+lrcnumup*(lrcwidth+cellwall)+cellwall; //extra +cellwall because right border (partially)
      lower1[LRC]=lowest-colltiers*(collheight+cellwall);
      top1[LRC]=zonestart[MZ];
      roottypecellwidth[LRC]=lrcwidth+cellwall;
      break;
    
    case COLUMELLAROOT:
	  left1[COLUMELLAROOT]=left+lrcnumup*(lrcwidth+cellwall);//the number of columella files and files width are the same as lrc  
      right1[COLUMELLAROOT]=right-lrcnumup*(lrcwidth+cellwall);// only one right because it's continuous and it ends where lrc starts (on the right side of the root)
	  top1[COLUMELLAROOT]=lowest-colltiers*(collheight+cellwall)-cellwall;
      lower1[COLUMELLAROOT]=lowest; 
      roottypecellwidth[COLUMELLAROOT]=(right1[COLUMELLAROOT]-left1[COLUMELLAROOT]-cellwall)/collnum;//divide by collnum because columella cells comprise different files and I want to know the width of each type file
      if(!fabs((double)roottypecellwidth[COLUMELLAROOT]-(double)(right1[COLUMELLAROOT]-left1[COLUMELLAROOT]-cellwall)/(double)collnum)<EPSILON) {
	fprintf(stdout,"warning, not able to nicely fit in the columella cells (%d/%d)\n",rootwidth-cellwall,collnum);
	collumellaissue=1;
      }
      break;
    case CORTEXENDO_I: //we dont use in geometric case
      break;
    case LRCEPI_I:    //we dont use in geometric case
      break;
    case COL_I:
      break;
    case QCROOT:    
      left1[QCROOT]=right1[ENDODERMIS]; //they will lie in the middle of the endo
      right1[QCROOT]=left2[ENDODERMIS];
	  top1[QCROOT]=top1[COLUMELLAROOT]-MZcellh-cellwall;// MZcellh+cellwall is the height of QC cells
      lower1[QCROOT]=top1[COLUMELLAROOT];
      roottypecellwidth[QCROOT]=(right1[QCROOT]-left1[QCROOT])/QCnum;
      break;
    case SPECIALVASCULAR:
      if(specialvascular==1){//we want these to be the pericycle expressing AUX1 or something else
	left1[SPECIALVASCULAR]=left2[PERICYCLE]; //it will be the pericycle of the MZ on the right 
	right1[SPECIALVASCULAR]=right2[PERICYCLE];
	top1[SPECIALVASCULAR]=zonestart[DZ]+(specialDZcell1-1)*(DZcellh+cellwall);// specialDZcell1 is set to 1 in the par file, thus  top1[SPECIALVASCULAR]=zonestart[DZ]
	lower1[SPECIALVASCULAR]=zonestart[DZ]+specialDZcell2*(DZcellh+cellwall);// the lower margin is at 2*(DZcellh+cellwall)
	roottypecellwidth[SPECIALVASCULAR]=periwidth+cellwall;
      }
      else if(specialvascular==2){//we want these to be the  pin37 reduction
	left1[SPECIALVASCULAR]=left1[PERICYCLE_D]; //it will be the pericycle of the DZ on the right 
	right1[SPECIALVASCULAR]=right2[PERICYCLE_D];
	top1[SPECIALVASCULAR]=zonestart[DZ]+(specialDZcell1-1)*(DZcellh+cellwall);// specialDZcell1 is set to 1 in the par file, thus  top1[SPECIALVASCULAR]=zonestart[DZ]
	lower1[SPECIALVASCULAR]=zonestart[DZ]+specialDZcell2*(DZcellh+cellwall);
	roottypecellwidth[SPECIALVASCULAR]=vascwidth+cellwall;
      }
      break;
    case VASCULAR_D://make 1 if vascular in differentiation zone should be different
      if(!vascular_d)// since vascular_d is set to 0 in the .par file, !vascular_d is 1
	break;
      left1[VASCULAR_D]=left+lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall);
      right1[VASCULAR_D]=left+lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall)+vascnum*(vascwidth+cellwall);
      lower1[VASCULAR_D]=zonestart[EZ];
      top1[VASCULAR_D]=zonestart[DZ];//upto the DZ
      roottypecellwidth[VASCULAR_D]=vascwidth+cellwall;
      break;
    case PERICYCLE_D:
      left1[PERICYCLE_D]=left+lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall);
      right1[PERICYCLE_D]=left1[PERICYCLE_D]+perinum*(periwidth+cellwall);
      left2[PERICYCLE_D]=right1[VASCULAR_D];
      right2[PERICYCLE_D]=left2[PERICYCLE_D]+perinum*(periwidth+cellwall);
      lower1[PERICYCLE_D]=zonestart[EZ];
      top1[PERICYCLE_D]=zonestart[DZ];//upto the DZ
      roottypecellwidth[PERICYCLE_D]=periwidth+cellwall;
      break;
    case ENDODERMIS_D:
      left1[ENDODERMIS_D]=left+lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall);
      right1[ENDODERMIS_D]=left1[PERICYCLE_D];
      left2[ENDODERMIS_D]=right2[PERICYCLE_D];
      right2[ENDODERMIS_D]=right2[PERICYCLE_D]+endonum*(endowidth+cellwall);
      lower1[ENDODERMIS_D]=zonestart[EZ];//starts at the end of EZ
      top1[ENDODERMIS_D]=zonestart[DZ];
      roottypecellwidth[ENDODERMIS_D]=endowidth+cellwall;
      break;  
    case CORTEX_D:
      left1[CORTEX_D]=left+lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall); 
      right1[CORTEX_D]=left1[ENDODERMIS_D];
      left2[CORTEX_D]=right2[ENDODERMIS_D];
      right2[CORTEX_D]=right2[ENDODERMIS_D]+cortexnum*(cortexwidth+cellwall);
      // lower1[CORTEX_D]=zonestart[EZ];//starts at the end of EZ
      lower1[CORTEX_D]=zonestart[EZ];
      top1[CORTEX_D]=zonestart[DZ];
      roottypecellwidth[CORTEX_D]=cortexwidth+cellwall;
      break;      
    case EPIDERMIS_D:
     // left1[EPIDERMIS_D]=left+lrcnum/2*(lrcwidth+cellwall);
	  left1[EPIDERMIS_D]=left+lrcnumup*(lrcwidth+cellwall);
      right1[EPIDERMIS_D]=left1[CORTEX_D];
      left2[EPIDERMIS_D]=right2[CORTEX_D];
      //right2[EPIDERMIS_D]=right2[CORTEX]+2*epinum*(epiwidthup+cellwall)+cellwall;  
      right2[EPIDERMIS_D]=right2[CORTEX_D]+(epinumup-1)*(epiwidthup+cellwall)+lrcnumup*(lrcwidthdown+cellwall)+cellwall;    
      lower1[EPIDERMIS_D]=zonestart[EZ];//starts at the end of EZ
      top1[EPIDERMIS_D]=zonestart[DZ];
      roottypecellwidth[EPIDERMIS_D]=epiwidthup+cellwall;
      break;      
     			
	case VASCULAR_TZ:
			
	  left1[VASCULAR_TZ]=left+(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall)); 
	  right1[VASCULAR_TZ]=left+(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall))+vascnum*(vascwidth+cellwall);
	  lower1[VASCULAR_TZ]=zonestart[MZ];//starts at the end of MZ
	  top1[VASCULAR_TZ]=zonestart[TZ];//goes until the end of the TZ
	  roottypecellwidth[VASCULAR_TZ]=vascwidth+cellwall;
	  break;
			
			
	case PERICYCLE_TZ: 
	  left1[PERICYCLE_TZ]=left+(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)); 
	  right1[PERICYCLE_TZ]=left1[PERICYCLE_TZ]+perinum*(periwidth+cellwall);
	  left2[PERICYCLE_TZ]=right1[VASCULAR_TZ];
	  right2[PERICYCLE_TZ]=left2[PERICYCLE_TZ]+perinum*(periwidth+cellwall);
	  lower1[PERICYCLE_TZ]=zonestart[MZ];//starts at the end of MZ
	  top1[PERICYCLE_TZ]=zonestart[TZ];//goes until the end of the TZ
	  roottypecellwidth[PERICYCLE_TZ]=periwidth+cellwall;			
	  break;
			
			
	case ENDODERMIS_TZ:
	  left1[ENDODERMIS_TZ]=left+(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall));
	  right1[ENDODERMIS_TZ]=left1[PERICYCLE_TZ];
	  left2[ENDODERMIS_TZ]=right2[PERICYCLE_TZ];
	  right2[ENDODERMIS_TZ]=right2[PERICYCLE_TZ]+endonum*(endowidth+cellwall);
	  lower1[ENDODERMIS_TZ]=zonestart[MZ];
	  top1[ENDODERMIS_TZ]=zonestart[TZ];
	  roottypecellwidth[ENDODERMIS_TZ]=endowidth+cellwall;
	  break;
			
			
    case CORTEX_TZ://this is the "new" cortex type, which has PINs flipped in relation to the MZ. 
      left1[CORTEX_TZ]=left+lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall); 
      right1[CORTEX_TZ]=left1[ENDODERMIS_TZ];
      left2[CORTEX_TZ]=right2[ENDODERMIS_TZ];
      right2[CORTEX_TZ]=right2[ENDODERMIS_TZ]+cortexnum*(cortexwidth+cellwall);
      lower1[CORTEX_TZ]=zonestart[MZ];//starts at the end of MZ
      top1[CORTEX_TZ]=zonestart[TZ];//goes until the end of the TZ
      roottypecellwidth[CORTEX_TZ]=cortexwidth+cellwall;
      break;
			
			
    case EPIDERMIS_TZ:
	  left1[EPIDERMIS_TZ]=left+(lrcnumup*(lrcwidth+cellwall));
	  right1[EPIDERMIS_TZ]=left1[CORTEX_TZ];
	  left2[EPIDERMIS_TZ]=right2[CORTEX_TZ];
	  right2[EPIDERMIS_TZ]=left2[EPIDERMIS_TZ]+epinumup*(epiwidthup+cellwall)+cellwall;
	  lower1[EPIDERMIS_TZ]=zonestart[MZ];
	  top1[EPIDERMIS_TZ]=zonestart[TZ];//upto the EZ- to check with the new TZ 
	  roottypecellwidth[EPIDERMIS_TZ]=epiwidthup+cellwall;
	  break;
			
			
 /* case LRC_TZ:
	  left1[LRC_TZ]=left;
	  right1[LRC_TZ]=left1[EPIDERMIS_TZ];
	  left2[LRC_TZ]=right2[EPIDERMIS_TZ]-1;
	  right2[LRC_TZ]=left2[LRC_TZ]+lrcnumup*(lrcwidth+cellwall)+cellwall; //extra +cellwall because right border (partially)
	  lower1[LRC_TZ]=zonestart[MZ]; 
	  top1[LRC_TZ]=zonestart[TZ]+1*(TZcellh+cellwall);// extended till first cell in TZ
	  roottypecellwidth[LRC_TZ]=lrcwidth+cellwall;
	  break;
		*/

	case VASCULAR_EZ: 
			
	  left1[VASCULAR_EZ]=left+(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall)); 
	  right1[VASCULAR_EZ]=left+(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall))+vascnum*(vascwidth+cellwall);
	  lower1[VASCULAR_EZ]=zonestart[TZ];//starts at the end of MZ
	  top1[VASCULAR_EZ]=zonestart[EZ];//goes until the end of the TZ
	  roottypecellwidth[VASCULAR_EZ]=vascwidth+cellwall;
	  break;
			
			
	case PERICYCLE_EZ: 
	  left1[PERICYCLE_EZ]=left+(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)); 
	  right1[PERICYCLE_EZ]=left1[PERICYCLE_EZ]+perinum*(periwidth+cellwall);
	  left2[PERICYCLE_EZ]=right1[VASCULAR_EZ];
	  right2[PERICYCLE_EZ]=left2[PERICYCLE_EZ]+perinum*(periwidth+cellwall);
	  lower1[PERICYCLE_EZ]=zonestart[TZ];//starts at the end of MZ
	  top1[PERICYCLE_EZ]=zonestart[EZ];//goes until the end of the TZ
	  roottypecellwidth[PERICYCLE_EZ]=periwidth+cellwall;			
	  break;
			
			
	case ENDODERMIS_EZ:
	  left1[ENDODERMIS_EZ]=left+(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall));
	  right1[ENDODERMIS_EZ]=left1[PERICYCLE_EZ];
	  left2[ENDODERMIS_EZ]=right2[PERICYCLE_EZ];
	  right2[ENDODERMIS_EZ]=right2[PERICYCLE_EZ]+endonum*(endowidth+cellwall);
	  lower1[ENDODERMIS_EZ]=zonestart[TZ];
	  top1[ENDODERMIS_EZ]=zonestart[EZ];
	  roottypecellwidth[ENDODERMIS_EZ]=endowidth+cellwall;
	  break;
			
			
    case CORTEX_EZ:
      left1[CORTEX_EZ]=left+lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall); 
      right1[CORTEX_EZ]=left1[ENDODERMIS_EZ];
      left2[CORTEX_EZ]=right2[ENDODERMIS_EZ];
      right2[CORTEX_EZ]=right2[ENDODERMIS_EZ]+cortexnum*(cortexwidth+cellwall);
      lower1[CORTEX_EZ]=zonestart[TZ];
      top1[CORTEX_EZ]=zonestart[EZ];
      roottypecellwidth[CORTEX_EZ]=cortexwidth+cellwall;
      break;
			
			
    case EPIDERMIS_EZ:
	  left1[EPIDERMIS_EZ]=left+(lrcnumup*(lrcwidth+cellwall));
	  right1[EPIDERMIS_EZ]=left1[CORTEX_EZ];
	  left2[EPIDERMIS_EZ]=right2[CORTEX_EZ];
	  right2[EPIDERMIS_EZ]=left2[EPIDERMIS_EZ]+epinumup*(epiwidthup+cellwall)+cellwall;
	  lower1[EPIDERMIS_EZ]=zonestart[TZ];
	  top1[EPIDERMIS_EZ]=zonestart[EZ];
	  roottypecellwidth[EPIDERMIS_EZ]=epiwidthup+cellwall;
	  break;
			
    case DEADCELLS:
      break;
    }
  }
  PLANE(
	for(k=0;k<TOTALROOTTYPES;k++) 
        {
	  if(curvedbending && (k==COLUMELLAROOT || k==QCROOT))
	    continue; //if it's true, continue breaks the loop and ignores the other statements ?? 
	  if(i>=top1[k] && i<lower1[k]) 
            {
	       if((j>=left1[k] && j<right1[k]) || (j>=left2[k] && j<right2[k]))
                {
	          if(ablation && k==QCROOT) //specific for ablation
                   {
	             if(j<left1[k]+cellwall || i<top1[k]+cellwall)
		       state[i][j]=((i>=cutshoot && i<cutroot)?CELLWALL:FAKECELLWALL);//if i>=cutshoot && i<cutroot,state[i][j]=CELLWALL, otherwise FAKECELLWALL
	             else
	                state[i][j]=FAKECELLS;
	             continue;
               	   }
	      m=0;
	      while(m<TOTALZONES-1 && i>=zonestart[m+1])
		m++;
	      if(((k!=LRC /*&& k!=LRC_TZ*/) && zonecellheight[m] && ((i-zonestart[m])%zonecellheight[m]<cellwall)) || ((k==LRC/* || k==LRC_TZ*/)  && zonecellheight[m] && ((i-zonestart[m])%(3*zonecellheight[m])<cellwall))) //horizontal- I changed here the factor 3*cellwall into 2*cellwall
              {
        	state[i][j]=((i>=cutshoot && i<cutroot)?CELLWALL:FAKECELLWALL);
		if(casparianDZ && k==ENDODERMIS_D && ((j>=left1[k] && j<right1[k] && roottypecellwidth[k] && (j-left1[k])%roottypecellwidth[k]==cellwall+(roottypecellwidth[k]-1-cellwall)/2) || (j>=left2[k] && j<right2[k] && (j-left2[k])==cellwall+(roottypecellwidth[k]-cellwall)/2)))
		  state[i][j]=FAKECELLWALL;
		else if(casparianEZ && m==EZ && k==ENDODERMIS_EZ && ((j>=left1[k] && j<right1[k] && roottypecellwidth[k] && (j-left1[k])%roottypecellwidth[k]==cellwall+(roottypecellwidth[k]-1-cellwall)/2) || (j>=left2[k] && j<right2[k] && (j-left2[k])==cellwall+(roottypecellwidth[k]-cellwall)/2)))
		  state[i][j]=FAKECELLWALL;
		else if(centrestrip && i>=separationstart && i<separationend+cellwall && j>=left+lrcnum*(lrcwidth+cellwall)+epinum*(epiwidth+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall)+vascnum/2*(vascwidth+cellwall) && j<left+lrcnum*(lrcwidth+cellwall)+epinum*(epiwidth+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall)+cellwall+vascnum/2*(vascwidth+cellwall))
		  state[i][j]=FAKECELLWALL;
		else if(fileseparation && i>=separationstart && i<separationend+cellwall && ((j>=left1[k] && j<right1[k] && roottypecellwidth[k] && (j-left1[k])%roottypecellwidth[k]<cellwall) || (j>=left2[k] && j<right2[k] && roottypecellwidth[k] && (j-left2[k])%roottypecellwidth[k]<cellwall)))
		  state[i][j]=FAKECELLWALL;
		else if(totalseparation && (k==PERICYCLE_D || k==PERICYCLE) && i>=separationstart && i<separationend && j>=left1[k] && j<right1[k] && roottypecellwidth[k] && (j-left1[k])%roottypecellwidth[k]<cellwall)
		  state[i][j]=FAKECELLWALL;
		else if(totalseparation && (k==ENDODERMIS ||k==ENDODERMIS_EZ || k==ENDODERMIS_D) && i>=separationstart && i<separationend && j>=left2[k] && j<right2[k] && (j-left2[k])<cellwall)
		  state[i][j]=FAKECELLWALL;
		else if(totalseparation && i>=separationend && i<separationend+cellwall && ((j>=left2[ENDODERMIS_D] && j<left2[ENDODERMIS_D]+cellwall) || (j>=left1[PERICYCLE_D] && j<left1[PERICYCLE_D]+cellwall)))
		  state[i][j]=FAKECELLWALL;
		else if(totalblock && ((i>=separationstart && i<separationstart+cellwall) || (i>=separationend && i<separationend+cellwall)) && (j>=left2[ENDODERMIS_D] || j<left1[PERICYCLE_D]+cellwall))
		  state[i][j]=FAKECELLWALL;
	      }
       	      else if(((!collumellaissue || k!=COLUMELLAROOT) && j>=left1[k] && j<right1[k] && roottypecellwidth[k] && (j-left1[k])%roottypecellwidth[k]<cellwall) || (collumellaissue && k==COLUMELLAROOT && j>=left1[k] && j<right1[k] && fmod(j-left1[k],(double)(rootwidth-cellwall)/(double)collnum)<(double)cellwall)) { //vertical
			 if (((int)(j-left1[k])/roottypecellwidth[k]>=1 )) {
                 
                 
			   if(k==EPIDERMIS){// && j>=left+lrcnumup*(lrcwidth+cellwall)) || (k==EPIDERMIS && j==left+lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidth+cellwall))){
		                state[i][j]=EPIDERMIS;//cell wall becomes epidermis (cell wall=blue, to unify epidermal files
                                break;
                      //this is to make 2 epidermis cells
			   }
			    if(k==EPIDERMIS_TZ){
                      state[i][j]=EPIDERMIS_TZ;
                      break;
		      }
                      if(k==EPIDERMIS_EZ){
                      state[i][j]=EPIDERMIS_EZ;
                      break;
		      } 
		   if(k==EPIDERMIS_D){
                      state[i][j]=EPIDERMIS_D;
                      break;
		      }
			 }
		state[i][j]=((i>=cutshoot && i<cutroot)?CELLWALL:FAKECELLWALL);
              if(totalseparation && (k==PERICYCLE_D || k==PERICYCLE) && i>=separationstart && i<separationend){
            state[i][j]=FAKECELLWALL;
          }
		else if(centrestrip && j>=left+lrcnum*(lrcwidth+cellwall)+epinum*(epiwidth+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall)+vascnum/2*(vascwidth+cellwall) && j<left+lrcnum*(lrcwidth+cellwall)+epinum*(epiwidth+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall)+cellwall+vascnum/2*(vascwidth+cellwall) && i>=separationstart && i<separationend)
            state[i][j]=FAKECELLWALL;
		else if(fileseparation && i>=separationstart && i<separationend)
		  state[i][j]=FAKECELLWALL;
	      }

	      else if(j>=left2[k] && j<right2[k] && roottypecellwidth[k] && (j-left2[k])%roottypecellwidth[k]<cellwall){// &&(j-right2[k])%roottypecellwidth[k]<cellwall ) { //vertical
              
              
		//	if (((int)(j-left2[k])/roottypecellwidth[k]>=1)|| ((int)(j-left2[k])/roottypecellwidth[k]==1)) {
          	if (j>left2[k] && j<left2[k]+(epinumup)*(epiwidthup+cellwall)) {
                
                
			   if(k==EPIDERMIS) {
		    state[i][j]=EPIDERMIS;//cell wall becomes epidermis (cell wall=blue, to unify epidermal files)
                  break;
                      //this is to make 2 epidermis cells
		      }
		      if(k==EPIDERMIS_TZ){
                      state[i][j]=EPIDERMIS_TZ;
                     break;
		      }
			 if(k==EPIDERMIS_EZ){
                      state[i][j]=EPIDERMIS_EZ;
                      break;
		      } 
		      else  if(k==EPIDERMIS_D){
                      state[i][j]=EPIDERMIS_D;
                      break;
		   
		      }
	      }
	state[i][j]=((i>=cutshoot && i<cutroot)?CELLWALL:FAKECELLWALL);
               if(totalseparation && (k==ENDODERMIS || k==ENDODERMIS_D) && i>=separationstart && i<separationend && (j-left2[k])<cellwall){
		  state[i][j]=FAKECELLWALL;
          }
		else if(centrestrip && j>=left+lrcnum*(lrcwidth+cellwall)+epinum*(epiwidth+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall)+vascnum/2*(vascwidth+cellwall) && j<left+lrcnum*(lrcwidth+cellwall)+epinum*(epiwidth+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall)+cellwall+vascnum/2*(vascwidth+cellwall) && i>=separationstart && i<separationend)
		  state[i][j]=FAKECELLWALL;
		else if(fileseparation && i>=separationstart && i<separationend)
		  state[i][j]=FAKECELLWALL;
	      }
	      else {
	        if(vacuole[MZ] || vacuole[TZ] || vacuole[EZ] || vacuole[DZ]) 
                 {
                     
	          reli=((i-zonestart[m])%max(zonecellheight[m],1))-cellwall+0.5;
	          if(j>=left1[k] && j<right1[k])// if I am on the left or the right side of the root
		    relj=((j-left1[k])%max(roottypecellwidth[k],1))-cellwall+0.5;
	          else
		    relj=((j-left2[k])%max(roottypecellwidth[k],1))-cellwall+0.5;
	          switch(vacuole[m]) 
                  {
	          case 0:
		    state[i][j]=((i>=cutshoot && i<cutroot)?k:FAKECELLS);
		    break;
	          case 1:
		vacwidth=vacuolewidth[m];
		break;
	          case 2:
		vacwidth=(min(zonecellheight[m],roottypecellwidth[k])-cellwall)/2.-vacuolewidth[m];
		break;
	          case 3:
		vacwidth=(-2.*(fabs(zonecellheight[m]-roottypecellwidth[k]))+sqrt(4.*pow(fabs(zonecellheight[m]-roottypecellwidth[k]),2.)+4.*M_PI*vacuolewidth[m]*(zonecellheight[m]-cellwall)*(roottypecellwidth[k]-cellwall)))/(2.*M_PI);
		break;
	          default:
		    fprintf(stdout,"This should never happen!\n");
		exit(EXIT_FAILURE);
		break;
                  }
	          if(vacuole[m]) 
                {
		if(zonecellheight[m]>roottypecellwidth[k]) 
                 {
		  if((fabs(reli-(zonecellheight[m]-cellwall)/2.)<(zonecellheight[m]-roottypecellwidth[k])/2. && fabs(relj-(roottypecellwidth[k]-cellwall)/2.)<vacwidth) ||
		     (fabs(reli-(zonecellheight[m]-cellwall)/2)>=(zonecellheight[m]-roottypecellwidth[k])/2. && hypot(relj-(roottypecellwidth[k]-cellwall)/2.,fabs(reli-(zonecellheight[m]-cellwall)/2.)-(zonecellheight[m]-roottypecellwidth[k])/2.)<vacwidth))
		    state[i][j]=0;
		  else
		    state[i][j]=((i>=cutshoot && i<cutroot)?k:FAKECELLS);
	          }
		else 
                  {
		  if((fabs(relj-(roottypecellwidth[k]-cellwall)/2.)<(roottypecellwidth[k]-zonecellheight[m])/2. && fabs(reli-(zonecellheight[m]-cellwall)/2.)<vacwidth) ||
		     (fabs(relj-(roottypecellwidth[k]-cellwall)/2.)>=(roottypecellwidth[k]-zonecellheight[m])/2. && hypot(reli-(zonecellheight[m]-cellwall)/2.,fabs(relj-(roottypecellwidth[k]-cellwall)/2.)-(roottypecellwidth[k]-zonecellheight[m])/2.)<vacwidth))
		    state[i][j]=0;
		  else
		    state[i][j]=((i>=cutshoot && i<cutroot)?k:FAKECELLS);
	          }
	         }
                 }
	        else
                
            state[i][j]=((i>=cutshoot && i<cutroot)?k:FAKECELLS);
            
	      }
	    }
	  }
	 
	}
	);

  PLANE(
	newstate[i][j]=state[nrow+1-i][j];
	);
  Copy(state,newstate);
  //added later
  /*for (i=0;i<nrow+1;i++) {
        for(j=0;j<=ncol/2+1;j++){
            state[i][j]=newstate[i][ncol+1-j];
            newstate[i][j]=state[i][j];
        }    
	}*/
 if(curvedbending) {
    PLANE(
	  if(i<=curvedheight+borderwidth) {

	    zoneij=(int)((2.*M_PI+atan2((double)i-1.-curvedheight+QCdistance/2.-borderwidth,(double)j-(double)ncol/2.))/(curvedangle/180.*M_PI));
	    zoneimin1j=(int)((2.*M_PI+atan2((double)i-2.-curvedheight+QCdistance/2.-borderwidth,(double)j-(double)ncol/2.))/(curvedangle/180.*M_PI));
	    zoneijmin1=(int)((2.*M_PI+atan2((double)i-1.-curvedheight+QCdistance/2.-borderwidth,(double)j-1.-(double)ncol/2.))/(curvedangle/180.*M_PI));
	    zoneimin1jmin1=(int)((2.*M_PI+atan2((double)i-2.-curvedheight+QCdistance/2.-borderwidth,(double)j-1.-(double)ncol/2.))/(curvedangle/180.*M_PI));
	    //    if ((curvedheight+borderwidth+1-i)>=(TIPblocknum*(MZcellh+cellwall)))
	    //    lrcedge=curveda[lrcnum]*pow(fabs((double)j-0.5-(double)ncol/2.),curvedpower+(double)(lrcnum)*curvedpowerstep)-curvedb[lrcnum]+curvedheight+(double)borderwidth;
	    //   else
	    lrcedge=curveda[0]*pow(fabs((double)j-0.5-(double)ncol/2.),curvedpower)-curvedb[0]+curvedheight+(double)borderwidth;
	    // modified epiedge to have a more realistic layout
	  
	     if ((curvedheight+borderwidth+1-i)>=(TIPblocknum*(MZcellh+cellwall)/2) &&  !( (zoneij)<28 && (zoneij)>=20) )//centre
	       epiedge=curveda[lrcnum+1]*pow(fabs((double)j-0.5-(double)ncol/2.),curvedpower+(double)(lrcnum+1)*curvedpowerstep)-curvedb[lrcnum+1]+curvedheight+(double)borderwidth;
	    // in case of one less LRC, is lrcnum+1 - if I want epidermis to go one cell up, invert lrcnum+1 and lrcnum+2
	       else if  ((curvedheight+borderwidth+1-i)>=(TIPblocknum*(MZcellh+cellwall)/2) &&  ( (zoneij)<28 && (zoneij)>=20) )
	      epiedge=curveda[lrcnum+2]*pow(fabs((double)j-0.5-(double)ncol/2.),curvedpower+(double)(lrcnum+2)*curvedpowerstep)-curvedb[lrcnum+2]+curvedheight+2*(double)borderwidth;
	     else 
	     epiedge=curveda[lrcnum]*pow(fabs((double)j-0.5-(double)ncol/2.),curvedpower+(double)(lrcnum)*curvedpowerstep)-curvedb[lrcnum]+curvedheight+2*(double)borderwidth;
	    cortexedge=curveda[lrcnum+epinum]*pow(fabs((double)j-0.5-(double)ncol/2.),curvedpower+(double)(lrcnum+epinum)*curvedpowerstep)-curvedb[lrcnum+epinum]+curvedheight+(double)borderwidth;
	    endoedge=curveda[lrcnum+epinum+cortexnum]*pow(fabs((double)j-0.5-(double)ncol/2.),curvedpower+(double)(lrcnum+epinum+cortexnum)*curvedpowerstep)-curvedb[lrcnum+epinum+cortexnum]+curvedheight+(double)borderwidth;
	    periedge=curveda[lrcnum+epinum+cortexnum+endonum]*pow(fabs((double)j-0.5-(double)ncol/2.),curvedpower+(double)(lrcnum+epinum+cortexnum+endonum)*curvedpowerstep)-curvedb[lrcnum+epinum+cortexnum+endonum]+curvedheight+(double)borderwidth;
	    ////not being used
	    //	    vascedge=curveda[lrcnum+epinum+cortexnum+endonum+perinum]*pow(fabs((double)j-0.5-(double)ncol/2.),curvedpower+(double)(lrcnum+epinum+cortexnum+endonum+perinum)*curvedpowerstep)-curvedb[lrcnum+epinum+cortexnum+endonum+perinum]+curvedheight+(double)borderwidth;

	    //for(k=0;k<=curvedfiles+1;k++) {
	    //creating curved lines
	    for(k=0;k<=lrcnum+epinum+cortexnum+endonum;k++) {
	      if((double)i-curvedheight-(double)borderwidth>curveda[k]*pow(fabs((double)j-0.5-(double)ncol/2.),curvedpower+(double)k*curvedpowerstep)-curvedb[k] &&
		 //(double)i-curvedheight-(double)borderwidth<curveda[k]*pow(fabs((double)j-0.5-(double)ncol/2.),curvedpower+(double)k*curvedpowerstep)-curvedb[k+1])
		 ((double)i-1.-curvedheight-(double)borderwidth<curveda[k]*pow(fabs((double)j-0.5-(double)ncol/2.),curvedpower+(double)k*curvedpowerstep)-curvedb[k] ||
		  ((double)i-curvedheight-(double)borderwidth<curveda[k]*pow(fabs((double)j-0.5-(double)ncol/2.)+1.,curvedpower+(double)k*curvedpowerstep)-curvedb[k]) ||
		  ((double)i-1.-curvedheight-(double)borderwidth<curveda[k]*pow(fabs((double)j-0.5-(double)ncol/2.)+1.,curvedpower+(double)k*curvedpowerstep)-curvedb[k]))) {
		//no curved line for some part for the endo-cortex boundary 
	       	if(((k==lrcnum+1) && (curvedheight+borderwidth+1-i)<=(TIPblocknum*(MZcellh+cellwall)/2))
		   || ((k==lrcnum+2) && (curvedheight+borderwidth+1-i)<=(TIPblocknum*(MZcellh+cellwall)/2)) 
		   //  || ((k==lrcnum+2) && (curvedheight+borderwidth+1-i)<=(TIPblocknum*2*(MZcellh+cellwall)))
		   ) {// if I say k==lrcnum+1, it will remove cellwall at epidermis, but only above EPILRC_I(zone>19<28) -this works for thick LRC*/
		//	if(((k==lrcnum+1)&& (curvedheight+borderwidth+1-i)<=(TIPblocknum*(MZcellh+cellwall)/2))){//this works for thin LRC
		  //the following code does not seem to do anything...
		  // if (zoneij>19 && zoneij<28) {
                  //   state[i][j]=CELLWALL;
		  //}
	//up to here
             }
	       
		else if((k==epinum+lrcnum-1 && zoneij<=27 && zoneij >=20) || ((k!=epinum+lrcnum-1) && (k<=epinum+lrcnum || k==epinum+lrcnum+cortexnum+endonum || zoneij>27 || zoneij <20))) {
		  state[i][j]=CELLWALL;
             }
	             }
         
        }
				  //straight horizontal lines in the vasculature
				  if(!((curvedheight+borderwidth+1-i)%max((MZcellh+cellwall),1))) {
					  if((curvedheight+borderwidth+1-i)<(TIPblocknum*(MZcellh+cellwall)/2) &&
						 (double)i>epiedge) //upper layers
						  state[i][j]=CELLWALL;//2
					  else if( (((curvedheight+borderwidth+1-i)==(TIPblocknum*(MZcellh+cellwall)/2)) || ((curvedheight+borderwidth+1-i)==(TIPblocknum*(MZcellh+cellwall)/2-2*(MZcellh+cellwall))))//< means upper- modified here to draw straight horizontal lines till LRC
							  &&
						  ((double)i>lrcedge)) //upper layers
						  state[i][j]=CELLWALL;//2
					  // else if((curvedheight+borderwidth+1-i)<(TIPblocknum*(MZcellh+cellwall)) &&
					  // (double)i>lrcedge); //upper layers
					  else if((curvedheight+borderwidth+1-i)<=(TIPblocknum*(MZcellh+cellwall)) &&
							  (double)i>periedge)
						  state[i][j]=CELLWALL;//3
					  
				  }
				  //more-or-less straight vertical lines
				  else if((double)i>periedge) {
					  localwidth=pow(((double)i-borderwidth-curvedheight-1.+QCdistance)/curveda[lrcnum+epinum+cortexnum+endonum],1./(curvedpower+(double)(lrcnum+epinum+cortexnum+endonum)*curvedpowerstep));
					  localwidthmin1=pow(((double)i-borderwidth-curvedheight-2.+QCdistance)/curveda[lrcnum+epinum+cortexnum+endonum],1./(curvedpower+(double)(lrcnum+epinum+cortexnum+endonum)*curvedpowerstep));
					  //number of pixels on one side available is: floor(localwidth)
					  //number of cells is vascnum/2+perinum
					  //interval is floor(localwidth)/(double)(vascnum/2+perinum)
					  thiszone=(int)(fabs((double)j-(double)ncol/2.-0.5)/(localwidth/(double)(vascnum/2+perinum)));
					  outzone=(int)((fabs((double)j-(double)ncol/2.-0.5)+1.)/(localwidth/(double)(vascnum/2+perinum)));
					  belowoutzone=(int)((fabs((double)j-(double)ncol/2.-0.5)+1.)/(localwidthmin1/(double)(vascnum/2+perinum)));	
					  if(//(abs(j-ncol/2-1)<floor(localwidth)) && 
						 thiszone<vascnum/2 && (thiszone<outzone || thiszone<belowoutzone))		  
						  state[i][j]=CELLWALL;//4
					  else if(j==ncol/2+1)//vertical line exactly at the centre of the root
						  state[i][j]=CELLWALL;//5
					  //printf("%d\t%d\t%lf\n",j,(int)(fabs((double)j-(double)ncol/2.-0.5)),fabs((double)j-(double)ncol/2.-0.5)/(floor(localwidth)/(double)(vascnum/2+perinum)));
					  /*
					   if(j==ncol/2+1) {
					   printf("%d\t%lg\t%lg\t%lg\n",i,(double)i-borderwidth-curvedheight-1+QCdistance,curveda[lrcnum+epinum+cortexnum+endonum],localwidth);
					   state[i][(int)(ncol/2+1+floor(localwidth))]=9;
					   state[i][(int)(ncol/2+1-floor(localwidth))]=9;
					   }
					   */
				  }
				  //radial horizontal lines
				  if((zoneij!=zoneimin1j ||
					  zoneij!=zoneijmin1 ||
					  zoneij!=zoneimin1jmin1) && 
					 atan2((double)i-1.-curvedheight+QCdistance/2.-borderwidth,(double)j-(double)ncol/2.)<0.) {
					  //radial lines only outside vasculature
					  if((double)i>lrcedge &&
						 (double)i<periedge) {
						  //one more compartment in lrc
						  leftzone=(double)i<(epiedge)?21:22;//change the line between epilrc cell on the left
						  rightzone=(double)i<(epiedge)?27:26;
						  if((double)i<cortexedge && ((zoneij<rightzone && zoneij>=leftzone) || (zoneimin1j<rightzone && zoneimin1j>=leftzone) || (zoneijmin1<rightzone && zoneijmin1>=leftzone) || (zoneimin1jmin1<rightzone && zoneimin1jmin1>=leftzone)))
							  state[i][j]=CELLWALL;//7, the radial lines
						  //lower density of lines further on 
						  else if(zoneij/2!=zoneimin1j/2 ||
								  zoneij/2!=zoneijmin1/2 ||
								  zoneij/2!=zoneimin1jmin1/2)
							  state[i][j]=CELLWALL;//8
					  }
				  }
				  if(!state[i][j]) {
				    
				  
				       if( (double)i>lrcedge) { // if i>=lrcedge, I add a weird pixel outside LRC
		        
					 if( (double)i>=epiedge ) {
							  
							  if((double)i>cortexedge) {
								  if((double)i>endoedge) {
									  if((double)i>periedge) {
										  if(thiszone<vascnum/2)
											  state[i][j]=VASCULAR;
										  else
											  state[i][j]=PERICYCLE;
									  }
									  else {
										  if(zoneij<26 && zoneij>=22)
											  state[i][j]=QCROOT;
										  else if(zoneij<28 && zoneij>=20)
											  state[i][j]=CORTEXENDO_I;
										  else
											  state[i][j]=ENDODERMIS;
									  }
								  }
								  else {
									  if(zoneij<26 && zoneij>=22)
										  state[i][j]=QCROOT;
									  else if(zoneij<28 && zoneij>=20)
										  state[i][j]=CORTEXENDO_I;
									  else
										  state[i][j]=CORTEX;
								  }
							  }
							  else {
								  if(zoneij<26 && zoneij>=22)//<26 is right and >22 is left -modifying this I add tissues up or down
									  state[i][j]=COL_I;
								  else if((zoneij)<28 && (zoneij)>=20)
								    state[i][j]=LRCEPI_I;
								  else 
                                                            {
							      state[i][j]=EPIDERMIS;
                                                            }
							  }
						  }
						  else {
							  if(zoneij<26 && zoneij>=22)
								  state[i][j]=COLUMELLAROOT;
							  // else if((zoneij)<28 && (zoneij)>=20)
							  else if( (zoneij)<28 && (zoneij)>=20)// increasing (zoneij)< and descreasing (zoneij)>=) I add more LRC upper
								  state[i][j]=LRC;
							 
								  else
							   state[i][j]=LRC;
								  
						   	  
							  
						  }
					  }
				  }

			  }

        //  else if(i==curvedheight+1+borderwidth && j>=left && j<right)
          			//  state[i][j]=CELLWALL;
          
          
		// this is to remove the cell wall at the boundary between the tip and the straight part of root
	  //the k is non-sensical here, and incorrect, because its value at this position of the code is unpredictable
	  //else if(k!=LRC && i==curvedheight+1+borderwidth && i!=lower1[LRC] && j>=left1[EPIDERMIS] && j<right2[EPIDERMIS])
	  else if(i==curvedheight+1+borderwidth && i!=lower1[LRC] && j>=left1[EPIDERMIS] && j<right2[EPIDERMIS])
	    state[i][j]=CELLWALL;
          /*
          else if(k==LRC && i==curvedheight+1+borderwidth && j>left1[LRC] && j<right1[LRC])
          state[i][j]=LRC;
	  */
			  );
         
         
		//one pixel is missing at the bound between QC cells
		if(newindex(curvedheight+borderwidth-TIPblocknum*(MZcellh+cellwall),nrow)==curvedheight+borderwidth-TIPblocknum*(MZcellh+cellwall) && newindex(ncol/2+1,ncol)==ncol/2+1) {
			//printf("[%d,%d]\n",curvedheight+borderwidth-TIPblocknum*(MZcellh+cellwall),ncol/2+1);
			state[curvedheight+borderwidth-TIPblocknum*(MZcellh+cellwall)][ncol/2+1]=CELLWALL;//6
         
		}
	}
}

			  
void MyInDat(char filename[],char *format,char *text,void *value)
{
  if(!InDat(filename,format,text,value)) {
    fprintf(stdout,"cannot find \"%s\" in parameter file; exitting now!\n",text);
    exit(EXIT_FAILURE);
  }
}

void Vacuole(int m,char a_string[])
{
  double tmpvacuole;

  if(a_string[strlen(a_string)-1]=='%') {
    vacuole[m]=3;
    a_string[strlen(a_string)-1]='\0';
  }
  tmpvacuole=atof(a_string);
  if(fabs(tmpvacuole)<EPSILON)
    vacuole[m]=0;
  else if(tmpvacuole<=-EPSILON) {
    vacuole[m]=2;
    vacuolewidth[m]=-tmpvacuole;
  }
  else {
    if(!vacuole[m])
      vacuole[m]=1;
    else if(vacuole[m]==3)
      tmpvacuole/=100.;
    vacuolewidth[m]=tmpvacuole;
  }
  // printf("type: %d string: %s vacuole: %d width: %f\n",m,a_string,vacuole[m],vacuolewidth[m]);
}

void SetPars(char name[])
{
  char conversion[10];
  char a_string[STRING];

#ifdef _SMALL
  snprintf(conversion,10,"%%f");
#else
  snprintf(conversion,10,"%%lf");
#endif
  ReadOptions(name);
  MyInDat(name,"%d","savegraphics",&savegraphics);
  MyInDat(name,"%d","EZblocknum",&EZblocknum);
  MyInDat(name,"%d","MZblocknum",&MZblocknum);
  MyInDat(name,"%d","TZblocknum",&TZblocknum);
  MyInDat(name,"%d","DZblocknum",&DZblocknum);
  MyInDat(name,"%d","EZcellh",&EZcellh);
  MyInDat(name,"%d","TZcellh",&TZcellh);// to insert in the par file
  MyInDat(name,"%d","MZcellh",&MZcellh);  
  MyInDat(name,"%d","DZcellh",&DZcellh);
  MyInDat(name,"%d","cellwall",&cellwall);
  MyInDat(name,"%d","collheight",&collheight);
  MyInDat(name,"%d","colltiers",&colltiers);
  MyInDat(name,"%d","collnum",&collnum);
  MyInDat(name,"%d","QCnum",&QCnum);
  MyInDat(name,"%d","epinum",&epinum);
  MyInDat(name,"%d","epinumup",&epinumup);
  MyInDat(name,"%d","cortexnum",&cortexnum);
  MyInDat(name,"%d","endonum",&endonum);
  MyInDat(name,"%d","perinum",&perinum);
  MyInDat(name,"%d","vascnum",&vascnum);
  MyInDat(name,"%d","epiwidth",&epiwidth);
  MyInDat(name,"%d","epiwidthup",&epiwidthup);
  MyInDat(name,"%d","cortexwidth",&cortexwidth);
  MyInDat(name,"%d","endowidth",&endowidth);
  MyInDat(name,"%d","periwidth",&periwidth);
  MyInDat(name,"%d","vascwidth",&vascwidth);
  MyInDat(name,"%d","specialvascular",&specialvascular);
  MyInDat(name,"%d","specialDZcell1",&specialDZcell1);
  MyInDat(name,"%d","specialDZcell2",&specialDZcell2);
  MyInDat(name,"%d","borderwidth",&borderwidth);
  MyInDat(name,"%d","vascular_d",&vascular_d);
  MyInDat(name,"%d","shootexclude",&shootexclude);
  MyInDat(name,"%d","rootexclude",&rootexclude);
  MyInDat(name,"%d","casparianEZ",&casparianEZ);
  MyInDat(name,"%d","casparianDZ",&casparianDZ);
  MyInDat(name,"%d","totalseparation",&totalseparation);
  MyInDat(name,"%d","totalblock",&totalblock);
  MyInDat(name,"%d","totalseparationstart",&totalseparationstart);
  MyInDat(name,"%d","totalseparationend",&totalseparationend);
  MyInDat(name,"%d","centrestrip",&centrestrip);  
  MyInDat(name,"%d","fileseparation",&fileseparation);  
  MyInDat(name,"%d","ablation",&ablation);  
  MyInDat(name,"%s","MZvacuole",&a_string);
  Vacuole(MZ,a_string);
  MyInDat(name,"%s","TZvacuole",&a_string);
  Vacuole(TZ,a_string);
  MyInDat(name,"%s","EZvacuole",&a_string);
  Vacuole(EZ,a_string);
  MyInDat(name,"%s","DZvacuole",&a_string);
  Vacuole(DZ,a_string);
  MyInDat(name,"%d","TIPblocknum",&TIPblocknum);  
  MyInDat(name,"%d","curvedbending",&curvedbending);  
  MyInDat(name,"%d","lrcnum",&lrcnum); 
  MyInDat(name,"%d","lrcnumup",&lrcnumup);
  MyInDat(name,"%d","lrcwidth",&lrcwidth);  
  MyInDat(name,"%d","lrcwidthdown",&lrcwidthdown); 
  MyInDat(name,"%lf","curvedb0",&curvedb0);  
  MyInDat(name,"%lf","curvedangle",&curvedangle);  
  MyInDat(name,"%lf","curvedpower",&curvedpower);  
  MyInDat(name,"%lf","curvedpowerstep",&curvedpowerstep);  
  MZblocknum+=colltiers; //easier to keep MZblocknum not including colltiers
  if(curvedbending)
    MZblocknum-=TIPblocknum;
}

void SetUp(int ac,char *av[])
{
  int i;
  double x1,x2;
  //double a1,a2;

  if(ac!=3) {
    fprintf(stdout,"usage: %s parfile name\n",av[0]);
    exit(EXIT_FAILURE);
  }

  SetPars(av[1]);
  if(snprintf(a_string,STRING,"%s.par",av[2])>=STRING)
    fprintf(stderr,"warning: parfile name too long: %s\n",a_string);
  else
    SaveOptions(av[1],a_string);
 
    ncol=2*(borderwidth+lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall))+vascnum*(vascwidth+cellwall)+cellwall;// why do I add a cellwall?
  nrow=2*borderwidth+EZblocknum*(EZcellh+cellwall)+MZblocknum*(MZcellh+cellwall)+DZblocknum*(DZcellh+cellwall)+TZblocknum*(TZcellh+cellwall)+cellwall;
  rootwidth=2*(lrcnumup*(lrcwidth+cellwall)+epinumup*(epiwidthup+cellwall)+cortexnum*(cortexwidth+cellwall)+endonum*(endowidth+cellwall)+perinum*(periwidth+cellwall))+cellwall+vascnum*(vascwidth+cellwall);
  QCdistance=TIPblocknum*(MZcellh+cellwall);
  QCthickness=MZcellh+cellwall;
	if(curvedbending) {
		/*{if(i<=curvedheight+borderwidth && (curvedheight+borderwidth+1-i)<(TIPblocknum*(MZcellh+cellwall)/2))

		        curvedfiles=lrcnumup+epinum+cortexnum+endonum+perinum+vascnum/2;
				
		else */
			
			curvedfiles=lrcnum+epinum+cortexnum+endonum+perinum+vascnum/2;
	/*	}*/
		if((curvedb=(double *)calloc((size_t)(curvedfiles+1),sizeof(double)))==NULL ||
		   (curveda=(double *)calloc((size_t)(curvedfiles+1),sizeof(double)))==NULL ||
		   (curvedstartx=(double *)calloc((size_t)(curvedfiles+1),sizeof(double)))==NULL)
			fprintf(stderr,"error in memory allocation\n");
		for(i=0;i<=curvedfiles;i++) { 
			if(i<=lrcnum)
			curvedstartx[i]=-1.+(double)borderwidth+(double)cellwall/2.+i*(lrcwidthdown+cellwall);
		else if(i<=lrcnum+epinum)
			curvedstartx[i]=curvedstartx[lrcnum]+(i-lrcnum)*(epiwidth+cellwall);
		else if(i<=lrcnum+epinum+cortexnum)
			curvedstartx[i]=curvedstartx[lrcnum+epinum]+(i-lrcnum-epinum)*(cortexwidth+cellwall);
		else if(i<=lrcnum+epinum+cortexnum+endonum)
			curvedstartx[i]=curvedstartx[lrcnum+epinum+cortexnum]+(i-lrcnum-epinum-cortexnum)*(endowidth+cellwall);
		else if(i<=lrcnum+epinum+cortexnum+endonum+perinum)
			curvedstartx[i]=curvedstartx[lrcnum+epinum+cortexnum+endonum]+(i-lrcnum-epinum-cortexnum-endonum)*(periwidth+cellwall);
		else
			curvedstartx[i]=curvedstartx[lrcnum+epinum+cortexnum+endonum+perinum]+(i-lrcnum-epinum-cortexnum-endonum-perinum)*(vascwidth+cellwall);
		}
		//indicate the thickness and distance of QC 
		//x1 is the radius the root, almost equal to half the field 
		x1=(double)ncol/2.-curvedstartx[lrcnum+epinum+cortexnum+endonum];
		//x2 is the radius up to the cortex
		x2=(double)ncol/2.-curvedstartx[lrcnum+epinum];
		//the following is not a success
		//    for(i=0;i<=curvedfiles;i++) {
		for(i=0;i<=lrcnum+epinum+cortexnum+endonum;i++) {
			////fixed curveda0
			//curvedb[i]=(curveda0*pow(curvedstartx[i]-(double)ncol/2.,2.));
			////relative weight curveda0,curvedb0
			//curvedb[i]=(curvedratio*((double)ncol/2.-curvedstartx[i])*curveda0+(1.-curvedratio)*curvedb0)*((double)ncol/2.-curvedstartx[i]);
			//curveda[i]=(1.-curvedratio)*curvedb0/((double)ncol/2.-curvedstartx[i])+curvedratio*curveda0;
	  
			curvedb[i]=pow(((double)ncol/2.-curvedstartx[i]-x1)/(x2-x1),curvedb0)*QCthickness+QCdistance;
			curveda[i]=curvedb[i]/pow(fabs(curvedstartx[i]-(double)ncol/2.),curvedpower+(double)i*curvedpowerstep);
			fprintf(stdout,"%d\t%lg\t%lg\t%lg\t%lg\t%lg\n",i,curvedstartx[i],(double)ncol/2.,(double)ncol/2.-curvedstartx[i],curveda[i],curvedb[i]);

		}
		////when final curvature equals 2
		//curvedheight=(int)(curveda[0]*pow((double)ncol/2.-curvedstartx[0],2.))+1;
		////when final curvature of root tip is of higher power
		curvedheight=(int)(curveda[0]*pow((double)ncol/2.-curvedstartx[0],curvedpower))+1;
		//printf("curve: %d %d %d %d\n",nrow,curvedheight,colltiers*(collheight+cellwall),curvedheight-colltiers*(collheight+cellwall));
		nrow+=curvedheight-colltiers*(collheight+cellwall);
  }
  state=New();
  newstate=New();

	ColorTable(MEDIUM,MEDIUM,WHITE);
	ColorTable(CELLWALL,CELLWALL,BLACK);
	ColorTable(VASCULAR,VASCULAR,RED);
	ColorName(PERICYCLE,PERICYCLE,"dark orange");// 255,140,0
	ColorTable(ENDODERMIS,ENDODERMIS,YELLOW);
	ColorName(CORTEX,CORTEX,"forest green");
	ColorTable(EPIDERMIS,EPIDERMIS,BLUE);
	ColorRGB(LRC,190,190,190);//grey
	ColorRGB(COLUMELLAROOT,0,255,255);//light  blue
	ColorName(CORTEXENDO_I,CORTEXENDO_I,"dark green");
	ColorRGB(LRCEPI_I,127,0,177);// purple
	ColorRGB(COL_I,173,216,230);//light blue
	ColorRGB(QCROOT,100,100,100);//dove gray
	ColorRGB(SPECIALVASCULAR,255,0,183);//shocking pink
	ColorRGB(VASCULAR_D,238,64,0);// orange red 2
	ColorRGB(PERICYCLE_D,255,153,18);//cadmium yellow
	ColorRGB(ENDODERMIS_D,238,201,0);//gold 2	
	ColorRGB(CORTEX_D,60,179,113);//medium sea green 1
	ColorRGB(EPIDERMIS_D,42,82,190);//cerulean blue
    ColorRGB(VASCULAR_TZ,255,105,97);//pastel red
	ColorRGB(PERICYCLE_TZ,253,198,137);//pastel yellow orange
	ColorRGB(ENDODERMIS_TZ,253,253,150);//pastel yellow
	ColorRGB(CORTEX_TZ,144,238,144);//pale green 2
	ColorRGB(EPIDERMIS_TZ,127,127,255);//blue/purple
	ColorRGB(LRC_TZ,219,215,210);//timberwolf
	ColorRGB(VASCULAR_EZ,255,69,0);//orangered 1
	ColorRGB(PERICYCLE_EZ,251,175,93);//light yellow orange
	ColorRGB(ENDODERMIS_EZ, 255,215,0);//gold 1
	ColorRGB(CORTEX_EZ,124,205,124);//palegreen3 
	ColorRGB(EPIDERMIS_EZ,65,105,225);//royal blue
	ColorRGB(DEADCELLS,158,110,72);//light brown
	//ColorRGB(FAKECELLWALL,20,20,21);
	ColorRGB(FAKECELLWALL,200,200,210);//light grey
	ColorRGB(FAKECELLS,30,30,31);
	
  InitCells();
  
  if(graphics) {
    snprintf(a_string,STRING,"%s",winfirst);
    OpenDisplay(a_string,nrow,ncol);
  }

  if(savegraphics)
    OpenPNG(&png1,".");
}
  
void Mouse(int mousestroke,int mouse_i,int mouse_j) 
{
  fprintf(stdout,"clicked position is %d,%d\n",mouse_i,mouse_j);
  end=0;
}

int main(int argc,char *argv[])
{
  mouse=&Mouse;
  SetUp(argc,argv);
	
	
	
  if(graphics) {
    PlaneDisplay(state,0,0,0);
    do {
      CheckInterface();
      usleep(100000);
    } while(end);
  }
  if(savegraphics) {
    PlanePNG(state,&png1,0);
    if(snprintf(a_string,STRING,"mv 00000.png %s_state.png",argv[2])>=STRING)
      fprintf(stderr,"warning: command too long: %s\n",a_string);
    else
      system(a_string);
  }
  CloseDisplay();
  return 0;
}

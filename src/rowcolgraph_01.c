#include <excal2.h>

int totalcellcolnumber,totalcellrownumber;
int *cellcolnumbers,*cellrownumbers;
int movie=0;
extern int grace;

void Show(char name[])
{
  FILE *fp=NULL;
  gzFile gzfile=NULL;
  int compressed=0;
  char textline[STRING];
  int i,j,k,graph,line,plot;
  int datapoints[2];
  double x,y;
  double spacestep=0.25;
  static int addcollines=0;
  static int addrowlines=0;
  int currentgraphnumber=0;

  if(!strncmp(&name[strlen(name)-3],".gz",3)) {
    compressed=1;
    gzfile=gzopen(name,"rb");
    if (gzfile==NULL) {
      fprintf(stderr,"warning: could not open %s\n",name);
      return;
    }
  }
  else if((fp=fopen(name,"r"))==NULL) {
    fprintf(stderr,"warning, could not open %s\n",name);
    return;
  }

  fprintf(stdout,"file: %s\n",name);

  if(totalcellcolnumber) {
    Grace(GGRAPH,0);
    for(k=0+addcollines;k<totalcellcolnumber+addcollines;k++) {
      printf("plotting column data set k = %d\n", k);
      Grace(GSET,k);
      if(movie)
	Grace(GKILL);
      if(totalcellcolnumber==1) {
	Grace(GLINE,GCOLOR,100+k*100/(totalcellcolnumber));
	
	printf("column colour for k = %d is %d\n", k, 100+(k)*100/(totalcellcolnumber));
      }
      else{
	Grace(GLINE,GCOLOR,100+(k-addcollines)*100/(totalcellcolnumber-1));
	printf("column colour for k = %d is %d\n", k, 100+(k-addcollines)*100/(totalcellcolnumber-1));
      }
    }
    currentgraphnumber++;
  }
  if(totalcellrownumber) {
    Grace(GGRAPH,totalcellrownumber);
    for(k=0+addrowlines;k<totalcellrownumber+addrowlines;k++) {
      printf("plotting row data set k = %d\n", k);
      Grace(GSET,k);
      if(movie)
	Grace(GKILL);
      if(totalcellrownumber==1) {
	Grace(GLINE,GCOLOR,100+k*100/(totalcellrownumber));
	printf("row colour for k = %d is %d\n", k,100+k*100/(totalcellrownumber));
      }
      else{
	Grace(GLINE,GCOLOR,100+(k-addrowlines)*100/(totalcellrownumber-1));
	printf("row colour for k = %d is %d\n", k, 100+(k-addrowlines)*100/(totalcellrownumber-1));
      }
    }
  }
  for(i=0;i<2;i++)
    datapoints[i]=0;
  if(compressed)
    gzgets(gzfile,textline,STRING);

  while((compressed && sscanf(textline,"%d %d %lf %lf\n",&graph,&line,&x,&y)==4) || (!compressed && fscanf(fp,"%d %d %lf %lf\n",&graph,&line,&x,&y)==4)) {
    if(compressed)
      gzgets(gzfile,textline,STRING);
    if(graph==0) {
      plot=0;
      for(j=0;j<totalcellcolnumber;j++)
	if(line==cellcolnumbers[j]) {
	  plot=1;
	  line=j+addcollines;
	  break;
	}
    }
    else { //if(graph==1)
      plot=0;
      for(j=0;j<totalcellrownumber;j++)
	if(line==cellrownumbers[j]) {
	  plot=1;
	  line=j+addrowlines;
	  break;
	}
    }
    if(plot) {
      if(totalcellcolnumber)
	Grace(GGRAPH,graph);
      else
	Grace(GGRAPH,0);
      Grace(GSET,line);
      if(y>1e-6) {
	Grace(GPOINT,2,x*spacestep,y);
	datapoints[graph]=1;
      }
    }
  }

  for(i=0;i<2;i++)
    if(datapoints[i]) {
      Grace(GGRAPH,i);
      Grace(GAXIS,'y');
      if(i==0)
	Grace(GSCALE,"Logarithmic");
      Grace(GAUTOSCALE);
    }
  if(compressed)
    gzclose(gzfile);
  else
    fclose(fp);
  if(!movie) {
    addrowlines += totalcellrownumber;
    addcollines += totalcellcolnumber;
  }
}

int main(int argc, char *argv[])
{
  int i,argnumber;
  int graphnumber=0,currentgraphnumber=0;
  //  char answer[STRING];

  if(argc==1) {
    fprintf(stdout,"usage: %s [movie] #longitudinal [filenumbers] #transversal [rownumbers] files\n",argv[0]);
    exit(EXIT_FAILURE);
  }

  grace=0;
  ColorGrad(ColorTable(100,100,GREEN),ColorTable(200,200,RED));

  argnumber=1;
  if(!isdigit(argv[argnumber][0])) {
    if(argv[argnumber][0]=='m')
      movie=1;
    argnumber++;
  }
  printf("argnumber = %d\n", argnumber);
  totalcellcolnumber=atoi(argv[argnumber]);
  printf("number of columns is %d\n", totalcellcolnumber);
  if(totalcellcolnumber<1)
    totalcellcolnumber=0;
  else { //if(totalcellcolnumber>0)
    if((cellcolnumbers=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL) {
      fprintf(stderr,"error in memory allocation\n");
      exit(EXIT_FAILURE);
    }
    if(argc<2+totalcellcolnumber+movie) {
      fprintf(stdout,"error: not enough arguments, cannot scan %d filenumbers\n",totalcellcolnumber);
      exit(EXIT_FAILURE);
    }
    for(i=0;i<totalcellcolnumber;i++)
      cellcolnumbers[i]=atoi(argv[argnumber+1+i]);
    //  printf( "col numbers %d\n", cellcolnumbers[totalcellcolnumber]);
  }
  totalcellrownumber=atoi(argv[argnumber+1+totalcellcolnumber]);
  if(totalcellrownumber<1)
    totalcellrownumber=0;
  else { //if(totalcellrownumber>0)
    if((cellrownumbers=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL) {
      fprintf(stderr,"error in memory allocation\n");
      exit(EXIT_FAILURE);
    }
    if(argc<2+totalcellrownumber+totalcellcolnumber+movie) {
      fprintf(stdout,"error: not enough arguments, cannot scan %d rownumbers\n",totalcellrownumber); 
      exit(EXIT_FAILURE);
    }
    for(i=0;i<totalcellrownumber;i++)
      cellrownumbers[i]=atoi(argv[argnumber+2+totalcellcolnumber+i]);
  }
  printf("totalcol: %d totalrow: %d\n",totalcellcolnumber,totalcellrownumber);

  for(i=0;i<totalcellcolnumber;i++)
    printf("col: %d %d\n",i,cellcolnumbers[i]);
  for(i=0;i<totalcellrownumber;i++)
    printf("row: %d %d\n",i,cellrownumbers[i]);

  if(totalcellcolnumber)
    graphnumber++;
  if(totalcellrownumber)
    graphnumber++;

  //OpenGrace(1,2,"/home/veronica/ProgArabi/newgraceroot_c.par"); 
  OpenGrace(1,graphnumber,NULL);

  for(i=0;i<graphnumber;i++) {
    Grace(GGRAPH,i);
    Grace(GAXIS,'y');
    Grace(GLABEL,"Auxin concentration");
  }
  
  if(totalcellcolnumber) {
    Grace(GGRAPH,0);
    Grace(GAXIS,'x');
    Grace(GLABEL,"length");
    Grace(GTITLE,"Longitudal");
    currentgraphnumber++;
  }
  if(totalcellrownumber) {
    Grace(GGRAPH,currentgraphnumber);
    Grace(GAXIS,'x');
    Grace(GLABEL,"width");
    Grace(GTITLE,"Transversal");
  }
  for (i = 2+argnumber+totalcellrownumber+totalcellcolnumber; i<argc; i++) {//argc all items in command line
    Show(argv[i]);//shows argv1, that's the data file name
    if(movie)
      usleep(20000);
  }
  Grace(GSAVE,"graphs.agr");
  return 0;
}

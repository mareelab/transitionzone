#include <stdio.h>
#include <stdlib.h>
#include <png.h>

void mycpy(unsigned char *to,unsigned char *from,int n)
{
  int i;

  for(i=0;i<n;i++)
    *(to+i)=*(from+i);
}

int main(int argc, char *argv[])
{
  static FILE *fpin1, *fpin2, *fpout;  /* "static" prevents setjmp corruption */
  png_structp read_ptr1, read_ptr2, write_ptr;
  png_infop read_info_ptr1, read_info_ptr2, write_info_ptr, end_info_ptr1,end_info_ptr2;
  png_bytep row_pointer1, row_pointer2;
  png_uint_32 width1, height1,width2, height2;
  int interlace_type1, compression_type1, filter_type1, interlace_type2, compression_type2, filter_type2;
  int bit_depth1, color_type1, bit_depth2, color_type2;
  int y,i,j,k,number;
  unsigned char *row_buf1, *row_buf2, *colors;
  char *array;

  if ((argc<6) || (argc%2))
    {
      printf("usage: %s inrgb outrgb [inrgb outrgb] infile1.png infile2.png outfile.png\n",argv[0]);
      exit(1);
    }
  
  if ((fpin1 = fopen(argv[(argc-3)], "rb")) == NULL)
    {
      printf("Could not find input file %s\n", argv[(argc-3)]);
      return (1);
    }

  if ((fpin2 = fopen(argv[(argc-2)], "rb")) == NULL)
    {
      printf("Could not find input file %s\n", argv[(argc-2)]);
      fclose(fpin1);
      return (1);
    }

  if ((fpout = fopen(argv[(argc-1)], "wb")) == NULL)
    {
      printf("Could not open output file %s\n", argv[(argc-1)]);
      fclose(fpin1);
      fclose(fpin2);
      return (1);
    }

  number = (argc-4)/2;
  array = (char *) malloc (12*(argc-4)*sizeof(char));
  colors = (unsigned char *) malloc (3*(argc-4)*sizeof(unsigned char));
  
  for(i=0;i<(argc-4);i++)
    {
      for(j=0;j<3;j++)
	{
	  for(k=0;k<3;k++)
	    array[(12*i+4*j+k)]=argv[(i+1)][(3*j+k)];
	  array[(12*i+4*j+3)]='\0';
	}
    }
  
  for(i=0;i<3*(argc-4);i++)
    colors[i]=(unsigned char)atoi(&array[(4*i)]);

  read_ptr1 = png_create_read_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL,
				    (png_error_ptr)NULL, (png_error_ptr)NULL);

  read_ptr2 = png_create_read_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL,
				    (png_error_ptr)NULL, (png_error_ptr)NULL);

  write_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, (png_voidp)NULL,
				      (png_error_ptr)NULL, (png_error_ptr)NULL);
  read_info_ptr1 = png_create_info_struct(read_ptr1);
  read_info_ptr2 = png_create_info_struct(read_ptr2);
  write_info_ptr = png_create_info_struct(write_ptr);
  end_info_ptr1 = png_create_info_struct(read_ptr1);
  end_info_ptr2 = png_create_info_struct(read_ptr2);
  png_init_io(read_ptr1, fpin1);
  png_init_io(read_ptr2, fpin2);
  png_init_io(write_ptr, fpout);
  png_read_info(read_ptr1, read_info_ptr1);
  png_read_info(read_ptr2, read_info_ptr2);
  png_get_IHDR(read_ptr1, read_info_ptr1, &width1, &height1, &bit_depth1,
	     &color_type1, &interlace_type1, &compression_type1, &filter_type1);
  png_get_IHDR(read_ptr2, read_info_ptr2, &width2, &height2, &bit_depth2,
	     &color_type2, &interlace_type2, &compression_type2, &filter_type2);
  png_set_IHDR(write_ptr, write_info_ptr, width1, height1, 8,
	       PNG_COLOR_TYPE_RGB,PNG_INTERLACE_NONE, compression_type1, filter_type1);
  png_write_info(write_ptr, write_info_ptr);
  row_buf1 = (unsigned char *) malloc (3*width1*height1*sizeof(unsigned char));
  row_buf2 = (unsigned char *) malloc (3*width2*height2*sizeof(unsigned char));
  
  if (color_type1 == PNG_COLOR_TYPE_PALETTE &&
      bit_depth1 <= 8) png_set_expand(read_ptr1);
  if (color_type1 == PNG_COLOR_TYPE_GRAY &&
      bit_depth1 <= 8) 
    png_set_gray_to_rgb(read_ptr1);
  if(bit_depth1 == 16)
    png_set_strip_16(read_ptr1);
  
  if (color_type2 == PNG_COLOR_TYPE_PALETTE) 
    png_set_expand(read_ptr2);
  if (color_type2 == PNG_COLOR_TYPE_GRAY) 
    png_set_gray_to_rgb(read_ptr2);
  if(bit_depth2 == 16)
    png_set_strip_16(read_ptr2);
  if(color_type2 == PNG_COLOR_TYPE_RGBA) {
    //png_set_strip_alpha(read_ptr2);
    printf("convert picture into better pict!\n");
    exit(1);
  }

  for (y=0; y<height1; y++) {
    row_pointer1 = (row_buf1 + 3*y*width1); 
    png_read_rows(read_ptr1, (png_bytepp)&row_pointer1, (png_bytepp)NULL, 1);

    if(y<height2) {
      row_pointer2 = (row_buf2 + 3*y*width2); 
      png_read_rows(read_ptr2, (png_bytepp)&row_pointer2, (png_bytepp)NULL, 1);
      for(i=0;i<width2;i++)
	for(j=0;j<number;j++) {
	  if((row_buf2[(3*(y*width2+i))]==colors[(6*j)]) && (row_buf2[(3*(y*width2+i)+1)]==colors[(6*j+1)]) && (row_buf2[(3*(y*width2+i)+2)]==colors[(6*j+2)])) {
	    row_buf1[(3*(y*width1+i))]=colors[(6*j+3)];
	    row_buf1[(3*(y*width1+i)+1)]=colors[(6*j+4)];
	    row_buf1[(3*(y*width1+i)+2)]=colors[(6*j+5)];
	    break;
	  }
	}
    }
  }
  
  for (y=0; y<height1; y++) {
    row_pointer1 = (row_buf1 + 3*y*width1);     
    png_write_rows(write_ptr, (png_bytepp)&row_pointer1, 1);
  }
  
  png_read_end(read_ptr1, end_info_ptr1);
  png_read_end(read_ptr2, end_info_ptr2);
  png_write_end(write_ptr, write_info_ptr);
  png_free(read_ptr1, row_buf1);
  png_free(read_ptr2, row_buf2);
  png_destroy_read_struct(&read_ptr1, &read_info_ptr1, &end_info_ptr1);
  png_destroy_read_struct(&read_ptr2, &read_info_ptr2, &end_info_ptr2);
  png_destroy_write_struct(&write_ptr, &write_info_ptr);
  
  fclose(fpin1);
  fclose(fpin2);
  fclose(fpout);
  return 0;
}

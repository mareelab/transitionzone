# Src

This directory is intended to contain all source code (c-code etc), that has to be compiled before running.

## layout of root

* *layoutofrootplusTZ_last.c* : create the root layout that is predominantly used in this paper
* *layoutofrootplusTZ_mostLRC.c* : create layout with LRC extending all the way to the top when cell sizes do not change, for new Null model
* *layoutofrootplusTZ_mostLRCsize.c* : create layout with LRC extending all the way to the top when cell sizes do change, for new almost Null model

## polarity of each cell within the layout

* *corners_31_TZ_EDZ.c* : define polarity for each cell  

## simulation of model

* *root_allTZ_newPINs.c* : run the model itself

## plotting of concentrations and fluxes

* *showflux_03.c* : show the auxin fluxes within the tissue

## generating final figures

* *pngcolor8.c* : changing colours in one image depending on colours in another image
* *playcolor_07.c* : generate colourbars
* *rowcolgraph_01.c* : make cross-section plots of auxin levels and fluxes 
* *thickenborder_02.c* : show polarity of cells 

//todo: merge with Renske code

//todo: cell shape-based corners
//todo: why limit to celltypes?

//older todo's:
//todo: don't count on instanteneous rebending after middle mouse click
//todo: graphically modify first, center, last
//todo: combination bending & shrinking not OK
//todo: not trimming does not go OK anymore with option combi -g2 -T2 -C1 -n
//todo: ((*) bending & trimming cellwall has to be improved
//      (soms wordt pointer op nul gezet terwijl ook nog naar iets anders zou kunnen wijzen, 
//       dit zal meestal niet zo belangrijk zijn)

#include <excal2.h>
#include <getopt.h>
#include <limits.h>

void Show();
void NoQuarter(int,int,int,int,int,int);
double nearbyint(double x);

#define ROOT            0
#define EMBRYO          1
#define FRUIT           2

#define MEDIUM          0
#define CELLWALL        1

#define VASCULAR        2
#define PERICYCLE       3
#define ENDODERMIS      4
#define CORTEX          5
#define EPIDERMIS       6
//lateral root cap
#define LRC             7
#define COLUMELLAROOT   8
//initials
#define CORTEXENDO_I    9
#define LRCEPI_I        10
#define COL_I           11
#define QCROOT          12
//differentiation zone
#define SPECIALVASCULAR 13
#define VASCULAR_D      14
#define PERICYCLE_D     15
#define ENDODERMIS_D    16
#define CORTEX_D        17
#define EPIDERMIS_D     18

//transition zone - is it ok to set TZ tissues before DZ ones?
#define VASCULAR_TZ     19
#define PERICYCLE_TZ    20
#define ENDODERMIS_TZ   21
#define CORTEX_TZ       22
#define EPIDERMIS_TZ    23
#define LRC_TZ          24
//elongation zone
#define VASCULAR_EZ     25
#define PERICYCLE_EZ    26
#define ENDODERMIS_EZ   27
#define CORTEX_EZ       28
#define EPIDERMIS_EZ    29
#define FAKECELLWALL    30
#define FAKECELLS       31
#define DEADCELLS       32
#define TOTALROOTTYPES  33 //it shouldnt go over 100...

#define VASCULARROOT    2
#define VASCULARSHOOT   3
#define ENDODERMISROOT  4
#define ENDODERMISSHOOT 5
#define CORTEXROOT      6
#define CORTEXSHOOT     7
#define EPIDERMISROOT   8
#define EPIDERMISSHOOT  9
#define QCEMBRYO        10
#define PRIMORDIUM      11
#define COLUMELLAEMBRYO 12
#define CENTRALCELL     13
#define TOTALEMBRYOTYPES 14

#define REPLUM          2
#define SEPLAYER        3
#define LIGLAYER        4
#define VALVE           5
#define TOTALFRUITTYPES 6 //it shouldnt go over 40...

#define UPPER          1
#define INNER          2
#define LOWER          3
#define OUTER          4
#define UO             5
#define UI             6
#define LI             7
#define LO             8

#define UPPEROUTER     11
#define UPPERINNER     21
#define INNERUPPER     12
#define INNERLOWER     22
#define LOWERINNER     13
#define LOWEROUTER     23
#define OUTERLOWER     14
#define OUTERUPPER     24

#define CCW            1
#define INWARDS        2
#define CW             3
#define OUTWARDS       4
#define CCWO           5
#define CCWI           6
#define CWI            7
#define CWO            8

#define MAXCORNERS     25

#define USECELLPNG     1
#define USESTATEPNG    2
#define USESTATE       3

#define MAXFILENAMES   8
#define SPECIALCELLS   200

static char *short_options = "hs:w:r:1tb:m:g:oc:C::e:RB:2f:F:G:E:n:T:SH:d:pO8z:Z:D";
static struct option long_options[] =
  {
    //second: 0,1,2 +> no,required,optional argument
    {"help",             0, NULL, 'h'},
    {"scale",            1, NULL, 's'},
    {"whattoshow",	 1, NULL, 'w'},
    {"reduction",	 1, NULL, 'r'},
    {"noreduction",	 0, NULL, '1'},
    {"trim",	         0, NULL, 't'},
    {"border",	         1, NULL, 'b'},
    {"maxcells",	 1, NULL, 'm'},
    {"guess",    	 1, NULL, 'g'},
    {"overwrite",    	 1, NULL, 'o'},
    {"cellwall",    	 1, NULL, 'c'},
    {"cellrows",    	 2, NULL, 'C'},
    {"expand",    	 1, NULL, 'e'},
    {"regular",    	 0, NULL, 'R'},
    {"bending",    	 1, NULL, 'B'},
    {"nobending",	 0, NULL, '2'},
    {"bendingfile",    	 1, NULL, 'f'},
    {"filenumer",    	 1, NULL, 'F'},
    {"rownumber",    	 1, NULL, 'G'},
    {"tissuetype",    	 1, NULL, 'E'},
    {"newname",    	 1, NULL, 'n'},
    {"toprows",    	 1, NULL, 'T'},
    {"save",    	 0, NULL, 'S'},
    {"symmetryaxes",     1, NULL, 'H'},
    {"divisions",        1, NULL, 'd'},
    {"polar",            0, NULL, 'p'},
    {"oldexcalib",       0, NULL, 'O'},
    {"eight",            0, NULL, '8'},
    {"outerzone",        1, NULL, 'z'},
    {"upperzone",        1, NULL, 'Z'},
    {"directedoutwards", 1, NULL, 'D'},
    {0,                  0, NULL, 0}
  };

typedef struct extras {
  int x[MAXCORNERS]; //we waste some memory here
  int y[MAXCORNERS];
  int oldx[MAXCORNERS];
  int oldy[MAXCORNERS];
  int count[MAXCORNERS]; //we waste some memory here
  int length[MAXCORNERS];
  int newlength[MAXCORNERS];
  double angle[5];
  int rowranking;
  int firstrowpixel;
  int lastrowpixel;
  int cellrownumber;
  int colranking;
  int firstcolpixel;
  int lastcolpixel;
  int cellcolnumber;
} Extras;

typedef struct bendingroot {
  int cellrow;
  int pixel;
  double curvature;
  double referenceline;
  double beginx;     //reference point of each zone before bending
  double beginy;
  double centerx; //real beginpoint of each zone in bended root 
  double centery;
  double normx;   //norm at beginpoint of each zone in bended root, facing towards the tip
  double normy;
  double referencex;//real refence point of each zone in bended root 
  double referencey;
  double rotationpointx;//real point of rotation 
  double rotationpointy;
  double rocrefx; //radius of curvature, vector from rotationpoint to reference point
  double rocrefy;
  double roccenter; //length of radius of curvature, from rotationpoint to centerpoint
} Bendingroot;

//debug
int debug=0;

static int clockwise[]={0,6,7,8,5,1,2,3,4};
static int counterclockwise[]={0,5,6,7,8,4,1,2,3};

static Bendingroot *curved=NULL;

static int file_exists[MAXFILENAMES];
static int file_changed[MAXFILENAMES];
static char *filenames[]={"_cells.png","_corners.png","_regions.png","_state.png",".cells",".corners",".regions",".state"};
static int filetype=-1;
static int trim=0;
static int trimborder=0;
static int top=1,left=1,right,lower;
static int cutlower,cutright;
static int currentcell=0,lastk=0,lastcell=0;
static int guess=0;
static int eight=0;
static double vecx,vecy;
static int overwrite=0;
static int bestmatchx,bestmatchy;
static int bestmatchxy,bestmatchyx;
static double bestangle,targetangle;
static double multx,multy;
static int imultx,imulty;
static double multtheta,multrho;
static int cellwall=-1;
static int expand=1;
static int regular=0;
static int noreduction=0;
static int nobending=0;
static int savecellrows=0;
static int specialcellrows=-1;
static int firstempty=SPECIALCELLS;
static int requiredk;
static int tissuetype=0;
static int totaltypes;
static int usenewname=0;
static char newfilename[STRING];
static char outputfilename[STRING];
static int oldnrow,oldncol;//before bending
static int toprows=0;
static int symmetry=0;
static int polar=0;
static int cornerdetection=0;
static int x,y,direction,cornerk,outer;
static double tissuecentrey, tissuecentrex;
static int divisions=2;
static int oldexcalib=0;
static double outerzone=0.,upperzone=0.;
static int directedoutwards=0;

static int filenumber=0;
static int rownumber=0;
static int zonenumber=1;
static int *cellrowtofirstpixel=NULL;//to get the "perfect" middle...
static int *cellrowtocenterpixel=NULL;
static int *cellrowtolastpixel=NULL;
static int *cellcoltofirstpixel=NULL;
static int *cellcoltocenterpixel=NULL;
static int *cellcoltolastpixel=NULL;
static int totalcellrownumber; //the total number of zones
static int totalcellcolnumber;

static int bending=0;
static int interactive=1;

#define EXTRAS ((Extras*)(cells.extras))

static Cell cells; 
static int upper=1;
static int side,sizereduction=0;
static char *cornername[]={"No Corner","Upper","Inner","Lower","Outer","Upper Outer","Upper Inner","Lower Inner","Lower Outer"};
static char *cornernamepolar[]={"No Corner","Outwards","Counterclockwise","Inwards","Clockwise","Outwards Counterclockwise","Inwards Counterclockwise","Inwards Clockwise","Outwards Clockwise"};
static char *symmetryname[]={"No symmetry","Vertical symmetry","Horizontal symmetry","Both vert. & hor. symmetry","Multiple symmetry"};
#define MAXSYMMETRY 5
static char *tissuename[]={"Root","Embryo","Fruit"};

//planes
static TYPE **state,**border,**corner,**region,**white,**cellstate;
static TYPE **smallstate,**smallborder,**smallcorner,**smallcellstate;
static FLOAT **angle;
static TYPE **originalstate,**newstate,**oldstate;//"originalstate" points to original (i.e. from bent to unbent), and newstate points from unbent to bent.
static TYPE **cellrowpict;//to make the pic with reference lines

//general
static char a_string[STRING];
static char filename[STRING];
static char programname[STRING];
static char winfirst[STRING]="The Root";
static char winsecond[STRING]="Upper Corners";

static int whattoshow=0;
static int maxcells=3000;
static Png png1;
static double xx1,xx2,yy1,yy2,l1,l2;
static int readpng=0;

extern int nrow,ncol,scale;
extern int boundary,specialcelltype;
extern unsigned char **userCol;
extern int optind;
extern int (*newindex)(int,int);
extern int iposition,ineigh,jposition,jneigh;
extern int sigma,sigmaneigh;
extern void (*mouse)(int,int,int);
extern void (*keyboard)(int,int,int);
extern void (*mouserelease)(int,int,int);
extern void (*mousemotion)(int,int);

void show_help(char *name, int exit_status)
{
  int i;
  fprintf(stdout,"\nUsage: %s [options] file\n\n",name);
  
  printf(
	 "options:\n"
	 "    --help, -h             This help screen\n"
	 "    --newname, -n x        Attribute a new radical for filenames\n"
	 "    --tissuetype, -E       Indicate tissue type (0=root,1=embryo,2=fruit)\n"
	 "    --scale, -s x          Scaling of pixels to be used\n"
	 "    --whattoshow, -w [0-2] Set what to show (0=cells,1=corners,2=regions)\n"
	 "    --reduction, -r x      Reduce picture x-fold\n"
	 "    --trim, -t             Trim picture by removing whitespace\n"
	 "    --border, -b x         Keep border of x pixels around tissue (implies -t)\n"
	 "    --maxcells, -m x       Define maximum number of cells\n"
	 "    --guess, -g [1-3]      Make an educated guess, using cell shape (1), extremes (2) or root shape (3)\n"
	 "    --overwrite, -o        Overwrite existing corners when making guesses\n"
	 "    --cellwall, -c x       Set cellwall thickness\n"
	 "    --expand, -e [0-2]     Only expand or only trim the cellwall (0=only trim,1=trim and expand,2=only expand)\n"
	 "    --regular, -R          Use a regular method to expand/reduce the cellwalls\n"
	 "    --bendingfile, -f file Bend the root as defined in file, which should contain\n"
         "                              [cellrow pixel curvature referenceline]\n"
	 "    --bending, -B \"x\"    Bend the root as indicated between quotes, which should contain (can be used multiple times)\n"
         "                              [cellrow pixel curvature referenceline]\n"
	 "    --filenumber, -F x     Predefine the number of cellfiles in the root, instead of using automatic detection\n"
	 "    --rownumber, -G x      Predefine the number of cellrows in the root, instead of using automatic detection\n"
	 "    --toprows, -T x        Only use the topmost x number of rows of the root for automatic cellfile detection\n"
	 "    --cellrows[=x], -C[x]     Save data and .png showing the determined cellrows,\n"
	 "                              optionally indicate up to which cellrow is special (do not use space!)\n"
	 "    --save, -S             Save right away, don't open interactive screen\n"
	 "    --symmetryaxes, -H [0-4] Indicate symmetry axes (0=none,1=vertical,2=horizontal,3=both,4=multiple)\n"
	 "    --divisions, -d        Number of multiple horizonal symmetryaxes (assumes -H4)\n"
	 "    --polar, -p            Use polar coordinates\n"
	 "    --oldexcalib, -O       Read in layout made with excalib v1.x\n"
	 "    --eight, -8            Divide cells into eight zones\n"
	 "    --outerzone, -z        Define outer area as fraction distance outer to inner corners (assumes -8)\n"
	 "    --upperzone, -Z        Define upper area as fraction distance upper to lower corners (assumes -8)\n"
	 "    --directedoutwards, -D Make the central cell directed outwards only (assumes -p)\n"
	 "\n"
	 "keystrokes:\n"
	 "    'c'                    Toggle upper/lower corners for corner selection\n"
	 "    's'                    Change what to show (cells,corners,regions)\n"
	 "    'i'                    Invert outer/inner corners\n"
	 "    'F'                    Flip upper/lower corners\n"
	 "    'x'                    Rotate regions\n"
	 "    'g'                    Make educated guess for selected cell, based on cell shape\n"
	 "    'G'                    Make educated guess for selected cell, based on extremes\n"
	 "    't'                    Make educated guess for selected cell, based on root shape\n"
	 "    'o'                    Toggle overwrite mode for educated guesses\n"
	 "    'z'                    Undo/redo last change\n"
	 "    'w'                    Toggle cellwall/fake cellwall\n"
	 "    'k'                    Save files without quitting\n"
	 "    'Q'                    Save files and quit program\n"
	 "\n"
	 "mouse:\n"
	 "    Left                   Select inner corner\n"
	 "    Right                  Select outer corner\n"
	 "\n"
	 "possible file extensions:\n");
  for(i=0;i<MAXFILENAMES;i++)
    fprintf(stdout,"    %s\n",filenames[i]);   
  fprintf(stdout,"\n");
  exit(0);
}

void options_read(int argc, char **argv)
{
  int long_index;
  int c,cnt;
  FILE *fp;
  int oldzonenumber=1,i;

  strncpy(programname,argv[0],STRING);
  for(i=1;i<argc;i++) {
    snprintf(a_string,STRING," \"%s\"",argv[i]);  
    strncat(programname,a_string,STRING-(strchr(programname,'\0')-programname));
  }
  
  while ((c = getopt_long(argc, argv, short_options,
			  long_options, &long_index)) != -1) {
    switch(c) {
    case 'h': 
      show_help(argv[0], 0); 
      break;
    case 'H': 
      symmetry=atoi(optarg);
      break;
    case 'd': 
      divisions=atoi(optarg);
      symmetry=4;
      break;
    case 'p':
      polar=1;
      snprintf(winsecond,STRING,"Counterclockwise Corners");
      break;
    case 'D':
      directedoutwards=1;
      polar=1;
      break;
    case 'O':
      oldexcalib=1;
      break;
    case 'f':
      bending=1;
      if((fp=fopen(optarg,"r"))==NULL) {
	fprintf(stderr,"error: file does nor exist: %s\n",optarg);
	show_help(argv[0], 0);
      };
      while(fscanf(fp,"%*f %*f %*f %*f")!=EOF)
	zonenumber++;
      fclose(fp);
      if(!zonenumber==oldzonenumber) {
	fprintf(stderr,"error: could not find zoneinformation in file %s\n\n",optarg);
	show_help(argv[0], 0);
      };
      if((curved=(Bendingroot*)realloc(curved,(size_t)((zonenumber+1)*sizeof(Bendingroot))))==NULL) {
	fprintf(stderr,"error in memory allocation\n");
	exit(EXIT_FAILURE);
      }
      fp=fopen(optarg,"r");
      for(i=oldzonenumber;i<zonenumber;i++) {
	fscanf(fp,"%d %d %lf %lf",&curved[i].cellrow,&curved[i].pixel,&curved[i].curvature,&curved[i].referenceline);
      }
      fclose(fp);
      oldzonenumber=zonenumber;
      break;
    case '1':
      noreduction=1;
      break;       
    case '2':
      nobending=1;
      break;       
    case 'B':
      bending=1;
      zonenumber++;
      if((curved=(Bendingroot*)realloc(curved,(size_t)((zonenumber+1)*sizeof(Bendingroot))))==NULL) {
	fprintf(stderr,"error in memory allocation\n");
	exit(EXIT_FAILURE);
      }
      snprintf(a_string,STRING,"%s",optarg);
      if(sscanf(a_string,"%d %d %lf %lf",&curved[oldzonenumber].cellrow,&curved[oldzonenumber].pixel,&curved[oldzonenumber].curvature,&curved[oldzonenumber].referenceline)!=4) {
	fprintf(stderr,"error: could not find correct zoneinformation in argument %s\n\n",optarg);
	show_help(argv[0], 0);
      };
      oldzonenumber=zonenumber;
      break;
    case 'F':
      filenumber=atoi(optarg);
      if(filenumber<1)
	filenumber=1;
      break;
    case 'G':
      rownumber=atoi(optarg);
      if(rownumber<1)
	rownumber=1;
      break;
    case 's': 
      scale=atoi(optarg);
      if(scale<1)
	scale=1;
      break;
    case 'T': 
      toprows=atoi(optarg);
      if(toprows<1)
	toprows=1;
      break;
    case 't': 
      trim=1;
      break;
    case 'S': 
      interactive=0;
      break;
    case 'C': 
      savecellrows=1;
      if(optarg!=NULL)
	specialcellrows=atoi(optarg);
      break;
    case 'b': 
      trim=1;
      trimborder=atoi(optarg);
      break;
    case 'm': 
      maxcells=atoi(optarg);
      if(maxcells<3)
	maxcells=3;
      break;
    case 'w': 
      whattoshow=atoi(optarg);
      if(whattoshow<0)
	whattoshow=0;
      else if(whattoshow>2)
	whattoshow=2;
      break;
    case 'g': 
      guess=atoi(optarg);
      if(guess<0)
	guess=0;
      else if(guess>3)
	guess=3;
      break;
    case '8':
      eight=1;
      break;
    case 'z':
      outerzone=atof(optarg);
      break;
    case 'Z':
      upperzone=atof(optarg);
      break;
    case 'c': 
      cellwall=atoi(optarg);
      if(cellwall<0)
	cellwall=0;
      break;
    case 'n': 
      usenewname=1;
      strncpy(newfilename,optarg,STRING);
      break;
    case 'E': 
      tissuetype=atoi(optarg);
      snprintf(winfirst,STRING,"%s",tissuename[tissuetype]);
      break;
    case 'e': 
      expand=atoi(optarg);
      if(expand<0)
	expand=0;
      else if(expand>2)
	expand=2;
      break;
    case 'r': 
      sizereduction=atoi(optarg);
      if(sizereduction<1)
	sizereduction=1;
      break;
    case 'o': 
      overwrite=1;
      break;
    case 'R': 
      regular=1;
      break;
    case 0:
    case '?': 
    default:
      show_help(argv[0], 1);
      break;
    }
  }

  cnt = argc - optind;
  if(!cnt)
    show_help(argv[0], 1);
  else {
    snprintf(filename,STRING,"%s",argv[optind+cnt-1]);
    fprintf(stdout,"filename: %s\n",filename);
  }
  if(nobending)
    bending=0;
  else if(bending)
    trim=0;//because after bending one trims anyway, it causes trouble to use twice.
  if(noreduction) {
    sizereduction=0;
    savecellrows=0;
    specialcellrows=-1;
  }
}

double vectorangle(double y1, double x1,double y2, double x2)
{
  //aXb=a*b*sin(tau)
  //a.b=a*b*cos(tau)
  //aXb/a.b=tan(tau)
  return atan2(x1*y2-y1*x2,x1*x2+y1*y2);
}

void Fishy() {
  int k;

  CELLS(cells,
	if(c>specialcelltype && cells.celltype[c]!=FAKECELLWALL && cells.area[c])
	  for(k=5;k<9;k++)
	    if((EXTRAS[c].x[k]!=-1 || EXTRAS[c].y[k]!=-1) && (EXTRAS[c].y[k]!=newindex(EXTRAS[c].y[k],nrow) || EXTRAS[c].x[k]!=newindex(EXTRAS[c].x[k],ncol) || state[EXTRAS[c].y[k]][EXTRAS[c].x[k]]!=c)) {
	      printf("something fishy: %d %d %d %d\n",c,EXTRAS[c].y[k],EXTRAS[c].x[k],k);
	      //exit(1);
	    }
	);
}

void OutwardsCell(int i,int j)
{
  if(border[i][j])
    corner[i][j]=OUTWARDS;
  region[i][j]=OUTWARDS;
}

void MoveCell(int i,int j)
{
  state[i][j]=firstempty;
}

void rotatevector(double angle,double normy,double normx,double *newnormy,double *newnormx)
{
  *newnormx=cos(angle)*normx-sin(angle)*normy;  
  *newnormy=sin(angle)*normx+cos(angle)*normy;
}

void FinishForwardBackwardPlanesPartTwo()
{
  FILE *fp;
  int k;
  int tmpnrow,tmpncol;

  tmpnrow=nrow;
  tmpncol=ncol;

  snprintf(a_string,STRING,"%s%s",outputfilename,".backwards");
  fp=fopen(a_string,"w");
  SavePat(fp,"%d\t",originalstate);
  fclose(fp);

  nrow=cutlower;
  ncol=cutright;
  snprintf(a_string,STRING,"%s%s",outputfilename,".forwards");
  fp=fopen(a_string,"w");
  SavePat(fp,"%d\t",newstate);
  fclose(fp);
  nrow=tmpnrow;
  ncol=tmpncol;

  cellrowpict=New();
  snprintf(a_string,STRING,"%s%s",filename,".centers");
  if((fp=fopen(a_string,"r"))==NULL) {
    fprintf(stderr,"error: could not open the file %s\n",a_string);
    exit(EXIT_FAILURE);
  }
  fscanf(fp,"%d %d",&totalcellrownumber,&totalcellcolnumber);
  if(((cellrowtofirstpixel=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL) ||
     ((cellrowtocenterpixel=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL) ||
     ((cellrowtolastpixel=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL) ||
     ((cellcoltofirstpixel=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL) ||
     ((cellcoltocenterpixel=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL) ||
     ((cellcoltolastpixel=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL)) {
    fprintf(stderr,"error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  for(k=0;k<totalcellrownumber;k++)
    fscanf(fp,"%*d %d %d %d",&cellrowtofirstpixel[k],&cellrowtocenterpixel[k],&cellrowtolastpixel[k]);
  
  for(k=0;k<totalcellcolnumber;k++)
    fscanf(fp,"%*d %d %d %d",&cellcoltofirstpixel[k],&cellcoltocenterpixel[k],&cellcoltolastpixel[k]);
  
  PLANE(//now that we have the maps and the .centers, we can draw out the new lines in the bent case -> these are the first lines
	cellrowpict[i][j]=cells.celltype[state[i][j]];
	for(k=1;k<totalcellrownumber;k++)
	  if(originalstate[i][j] && originalstate[i][j]/SHRT_MAX==cellrowtofirstpixel[k])
	    cellrowpict[i][j]=200;//this is colour
	for(k=1;k<totalcellcolnumber;k++)
	  if(originalstate[i][j] && originalstate[i][j]%SHRT_MAX==cellcoltofirstpixel[k])
	    cellrowpict[i][j]=200;
	);

  OpenPNG(&png1,".");
  CPlanePNG(cellrowpict,state,&png1,0);

  PLANE(//again for the center lines, other colour
	cellrowpict[i][j]=cells.celltype[state[i][j]];
	for(k=1;k<totalcellrownumber;k++)
	  if(originalstate[i][j] && originalstate[i][j]/SHRT_MAX==cellrowtocenterpixel[k])
	    cellrowpict[i][j]=201;
	for(k=1;k<totalcellcolnumber;k++)
	  if(originalstate[i][j] && originalstate[i][j]%SHRT_MAX==cellcoltocenterpixel[k])
	    cellrowpict[i][j]=201;
	);

  CPlanePNG(cellrowpict,state,&png1,0);
    
  snprintf(a_string,STRING,"mv %.5d.png %s_cellrows.png",png1.nframes-2,outputfilename);
  system(a_string);
  snprintf(a_string,STRING,"mv %.5d.png %s_centers.png",png1.nframes-1,outputfilename);
  system(a_string);
  PlaneFree(cellrowpict);
}

void FinishForwardBackwardPlanesPartOne(int yshift,int xshift)
{
  int i,j;

  cutlower=0;
  cutright=0;

  //below, the fact that the planes are shifted due to trimming, will be corrected with a horizontal and vertical shift. The mapping itself is "coded" by multiplying the y values by 32000 (SHRT_MAX), and then getting from the division the value back, and from the mod, the x value. 

  for(i=1;i<=oldnrow;i++)
    for(j=1;j<=oldncol;j++)
      if(newstate[i][j]) {
	newstate[i][j]=((newstate[i][j]/SHRT_MAX)+yshift)*SHRT_MAX + (newstate[i][j]%SHRT_MAX)+xshift;//making the shift in the forward plane
	if(i*SHRT_MAX+j!=originalstate[newstate[i][j]/SHRT_MAX][newstate[i][j]%SHRT_MAX])//to make sure that the "bent" root also agrees where it came from. 
	  fprintf(stderr,"warning: forward-backward referencing mismatch (%d,%d) -> (%d,%d) -> (%d,%d)\n",i,j,newstate[i][j]/SHRT_MAX,newstate[i][j]%SHRT_MAX,originalstate[newstate[i][j]/SHRT_MAX][newstate[i][j]%SHRT_MAX]/SHRT_MAX,originalstate[newstate[i][j]/SHRT_MAX][newstate[i][j]%SHRT_MAX]%SHRT_MAX);
      }

  PLANE(//to really keep track to the region from where the bent pixels are coming from
	if(originalstate[i][j]) {
	  if(originalstate[i][j]/SHRT_MAX>cutlower)
	    cutlower=originalstate[i][j]/SHRT_MAX;
	  if(originalstate[i][j]%SHRT_MAX>cutright)
	    cutright=originalstate[i][j]%SHRT_MAX;
	}
	);
}

void CellRows()
{
  int cellcolnumber;
  int maxcellcolnumber;
  int firststop=0;
  int c,ranking=1;
  int lastranked=0;
  int i,j,k;
  FILE *fp=NULL;
  int cellrownumber;
  int maxcellrownumber;
  int tmpnrow=0,tmpncol=0;

  if(specialcellrows!=-1) { //make sure that no cellnumber is used below SPECIALCELLS
    for(c=CELLWALL+1;c<SPECIALCELLS;c++) {
      if(cells.celltype[c]!=FAKECELLWALL && cells.area[c]) {
	while(cells.area[firstempty]) {
	  firstempty++;
	  if(firstempty>=cells.maxcells) {
	    fprintf(stdout,"maximum number of cells is insufficient,\n");
	    fprintf(stdout,"please increase, using -m,--maxcells\n");
	    exit(EXIT_FAILURE);
	  }
	}
	OneCell(state,&cells,c,&MoveCell);
	cells.area[firstempty]=cells.area[c];
	cells.celltype[firstempty]=cells.celltype[c];
	EXTRAS[firstempty]=EXTRAS[c];
	for(k=0;k<9;k++)
	  EXTRAS[c].x[k]=-1;
      }
    }
    UpdateCFill(state,&cells);//because small ones will be wrong
    UpdateCellPosition(state,&cells);//because of mx. my
  }

  //first find all the rows
  snprintf(a_string,STRING,"%s.cellrows",outputfilename);
  fp=fopen(a_string,"w");
  fprintf(fp,"row\tpixel\tfiles\tfirst\tlast\n");

  CELLS(cells,
	EXTRAS[c].rowranking=0;
	EXTRAS[c].firstrowpixel=0;
	EXTRAS[c].lastrowpixel=0;
	);
  
  totalcellrownumber=1;
  maxcellcolnumber=0;
  if(filenumber) //we don't wait untill the first row of cells stops 
    firststop=1;
  
  //for(i=1;i<=nrow;i++) { //does this work? Stan 16/07/10
  for(i=nrow;i>=1;i--) {
    cellcolnumber=0;
    for(j=1;j<=ncol;j++) {
      c=state[i][j];
      if(c>CELLWALL && cells.celltype[c]!=FAKECELLWALL) {
	if(!EXTRAS[c].rowranking) { //we see this cell for the first time
	  EXTRAS[c].rowranking=ranking++;
	  EXTRAS[c].firstrowpixel=i;
	  EXTRAS[c].lastrowpixel=i;
	  cellcolnumber++;
	}
	else {
	  //if(EXTRAS[c].lastrowpixel<i) {
	  if(EXTRAS[c].lastrowpixel>i) {
	    EXTRAS[c].lastrowpixel=i;
	    cellcolnumber++;//only when we see this cell for the first time in this line we count it 
	  }
	}
      }
    }

    if(cellcolnumber>maxcellcolnumber)
      maxcellcolnumber=cellcolnumber; //keep track of max number of cells in one cross-section
    if(filenumber)
      maxcellcolnumber=filenumber; //except when pre-defined

    if(!firststop) {
      CELLS(cells,
	    if(EXTRAS[c].rowranking && EXTRAS[c].lastrowpixel>i) { //we detected the first cell that finishes
	      firststop=1;
	      break;
	    }
	    );
    }
    if(firststop) {
      if(ranking>(lastranked+maxcellcolnumber+1)) { //more than found in any cross-section
	fprintf(fp,"%d\t%d\t%d\t%d\t%d\n",totalcellrownumber,i+1,maxcellcolnumber,lastranked+1,lastranked+maxcellcolnumber);
	CELLS(cells,
	      if(EXTRAS[c].rowranking>lastranked && EXTRAS[c].rowranking<=lastranked+maxcellcolnumber)
		EXTRAS[c].cellrownumber=totalcellrownumber;
	      );
	totalcellrownumber++;
	lastranked+=maxcellcolnumber;
	maxcellcolnumber=0;
      }
    }
  }
  CELLS(cells,
	if(EXTRAS[c].rowranking>lastranked) //label the remaining ones that were not yet labelled
	  EXTRAS[c].cellrownumber=totalcellrownumber;
	);

  fprintf(fp,"%d\t%d\t%d\t%d\t%d\n\n",totalcellrownumber,i,ranking-lastranked-1,lastranked+1,ranking-1);

  totalcellrownumber++;

  if(((cellrowtofirstpixel=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL) ||
     ((cellrowtocenterpixel=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL) ||
     ((cellrowtolastpixel=(int *)calloc((size_t)totalcellrownumber,sizeof(int)))==NULL)) {
    fprintf(stderr,"error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  
  cellrowtofirstpixel[0]=nrow;
  cellrowtocenterpixel[0]=nrow;
  cellrowtolastpixel[0]=nrow;
  CELLS(cells,
	if(EXTRAS[c].cellrownumber && (!cellrowtofirstpixel[EXTRAS[c].cellrownumber] || cellrowtofirstpixel[EXTRAS[c].cellrownumber]<EXTRAS[c].firstrowpixel))
	  cellrowtofirstpixel[EXTRAS[c].cellrownumber]=EXTRAS[c].firstrowpixel;
	if(EXTRAS[c].cellrownumber && (!cellrowtolastpixel[EXTRAS[c].cellrownumber] || cellrowtolastpixel[EXTRAS[c].cellrownumber]>EXTRAS[c].lastrowpixel))
	  cellrowtolastpixel[EXTRAS[c].cellrownumber]=EXTRAS[c].lastrowpixel;
	);
  for(i=0;i<totalcellrownumber;i++)
    cellrowtocenterpixel[i]=(cellrowtofirstpixel[i]+cellrowtolastpixel[i])/2;
  
  //then find all the cols
  fprintf(fp,"file\tpixel\trows\tfirst\tlast\n");

  CELLS(cells,
	EXTRAS[c].colranking=0;
	EXTRAS[c].firstcolpixel=0;
	EXTRAS[c].lastcolpixel=0;
	);
  
  totalcellcolnumber=1;
  maxcellrownumber=0;
  if(rownumber) //we don't wait untill the first col of cells stops 
    firststop=1;
  else
    firststop=0;
  ranking=1;
  lastranked=0;
  
  for(j=1;j<=ncol;j++) {
    cellrownumber=0;
    //for(i=1;i<=nrow;i++) { //does this work? Stan 16/07/10
    for(i=nrow;i>=1;i--) {
      c=state[i][j];
      if(c>CELLWALL && cells.celltype[c]!=FAKECELLWALL && (!toprows || EXTRAS[c].cellrownumber<=toprows)) {
	if(!EXTRAS[c].colranking) { //we see this cell for the first time
	  EXTRAS[c].colranking=ranking++;
	  EXTRAS[c].firstcolpixel=j;
	  EXTRAS[c].lastcolpixel=j;
	  cellrownumber++;
	}
	else {
	  if(EXTRAS[c].lastcolpixel<j) {
	    EXTRAS[c].lastcolpixel=j;
	    cellrownumber++;//only when we see this cell for the first time in this line we count it 
	  }
	}
      }
    }

    if(cellrownumber>maxcellrownumber)
      maxcellrownumber=cellrownumber; //keep track of max number of cells in one cross-section
    if(rownumber)
      maxcellrownumber=rownumber; //except when pre-defined

    if(!firststop) {
      CELLS(cells,
	    if(EXTRAS[c].colranking && EXTRAS[c].lastcolpixel<j) { //we detected the first cell that finishes
	      firststop=1;
	      break;
	    }
	    );
    }
    if(firststop) {
      if(ranking>(lastranked+maxcellrownumber+1)) { //more than found in any cross-section
	fprintf(fp,"%d\t%d\t%d\t%d\t%d\n",totalcellcolnumber,j-1,maxcellrownumber,lastranked+1,lastranked+maxcellrownumber);
	CELLS(cells,
	      if(EXTRAS[c].colranking>lastranked && EXTRAS[c].colranking<=lastranked+maxcellrownumber)
		EXTRAS[c].cellcolnumber=totalcellcolnumber;
	      );
	totalcellcolnumber++;
	lastranked+=maxcellrownumber;
	maxcellrownumber=0;
      }
    }
  }

  CELLS(cells,
	if(EXTRAS[c].colranking>lastranked) //label the remaining ones that were not yet labelled
	  EXTRAS[c].cellcolnumber=totalcellcolnumber;
	);

  fprintf(fp,"%d\t%d\t%d\t%d\t%d\n\n",totalcellcolnumber,j,ranking-lastranked-1,lastranked+1,ranking-1);

  totalcellcolnumber++;

  if(((cellcoltofirstpixel=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL) ||
     ((cellcoltocenterpixel=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL) ||
     ((cellcoltolastpixel=(int *)calloc((size_t)totalcellcolnumber,sizeof(int)))==NULL)) {
    fprintf(stderr,"error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  
  cellcoltofirstpixel[0]=1;
  cellcoltocenterpixel[0]=1;
  cellcoltolastpixel[0]=1;
  CELLS(cells,
	if(EXTRAS[c].cellcolnumber && (!cellcoltofirstpixel[EXTRAS[c].cellcolnumber] || cellcoltofirstpixel[EXTRAS[c].cellcolnumber]>EXTRAS[c].firstcolpixel))
	  cellcoltofirstpixel[EXTRAS[c].cellcolnumber]=EXTRAS[c].firstcolpixel;
	if(EXTRAS[c].cellcolnumber && (!cellcoltolastpixel[EXTRAS[c].cellcolnumber] || cellcoltolastpixel[EXTRAS[c].cellcolnumber]<EXTRAS[c].lastcolpixel))
	  cellcoltolastpixel[EXTRAS[c].cellcolnumber]=EXTRAS[c].lastcolpixel;
	);
  for(i=0;i<totalcellcolnumber;i++)
    cellcoltocenterpixel[i]=(cellcoltofirstpixel[i]+cellcoltolastpixel[i])/2; //here finally we get the center of the zone

  //now change cellnumbers
  if(specialcellrows!=-1) {
    firstempty=CELLWALL+1;
    for(c=SPECIALCELLS;c<cells.maxcells;c++) {
      if(cells.area[c] && cells.celltype[c]!=FAKECELLWALL && EXTRAS[c].cellrownumber<=specialcellrows) {
	while(cells.area[firstempty]) {
	  firstempty++;
	  if(firstempty>=SPECIALCELLS) {
	    fprintf(stdout,"SPECIALCELLS is insufficient,\n");
	    fprintf(stdout,"please increase SPECIALCELLS in the programcode\n");
	    exit(EXIT_FAILURE);
	  }
	}
	OneCell(state,&cells,c,&MoveCell);
	cells.area[firstempty]=cells.area[c];
	cells.celltype[firstempty]=cells.celltype[c];
	EXTRAS[firstempty]=EXTRAS[c];
	for(k=0;k<9;k++)
	  EXTRAS[c].x[k]=-1;
      }
    }
    UpdateCFill(state,&cells);//because small ones will be wrong
    UpdateCellPosition(state,&cells);//because of mx. my
  }
  
  //create & save pict
  fprintf(fp,"row\tfirst\tcenter\tlast\n");
  for(i=0;i<totalcellrownumber;i++)
    fprintf(fp,"%d\t%d\t%d\t%d\n",i,cellrowtofirstpixel[i],cellrowtocenterpixel[i],cellrowtolastpixel[i]);
  fprintf(fp,"\nfile\tfirst\tcenter\tlast\n");
  for(i=0;i<totalcellcolnumber;i++)
    fprintf(fp,"%d\t%d\t%d\t%d\n",i,cellcoltofirstpixel[i],cellcoltocenterpixel[i],cellcoltolastpixel[i]);
  fclose(fp);
  if(bending)
    snprintf(a_string,STRING,"%s_bended.centers",outputfilename);
  else
    snprintf(a_string,STRING,"%s.centers",outputfilename);
  fp=fopen(a_string,"w");
  fprintf(fp,"%d\t%d\n",totalcellrownumber,totalcellcolnumber);
  for(i=0;i<totalcellrownumber;i++)
    fprintf(fp,"%d\t%d\t%d\t%d\n",i,cellrowtofirstpixel[i],cellrowtocenterpixel[i],cellrowtolastpixel[i]);
  for(i=0;i<totalcellcolnumber;i++)
    fprintf(fp,"%d\t%d\t%d\t%d\n",i,cellcoltofirstpixel[i],cellcoltocenterpixel[i],cellcoltolastpixel[i]);
  fclose(fp);
      
  if(bending) {
    tmpnrow=nrow;
    tmpncol=ncol;
    nrow=lower-top+1;//here, u make it smaller
    ncol=right-left+1;
  }

  cellrowpict=New();

  //  for(i=1;i<=nrow;i++) {
  for(i=nrow;i>=1;i--) {
    for(j=1;j<=ncol;j++)
      cellrowpict[i][j]=cells.celltype[state[i][j]];
    for(k=0;k<totalcellrownumber;k++)
      if(i==cellrowtofirstpixel[k])
	for(j=1;j<=ncol;j++)
	  cellrowpict[i][j]=200;
  }

  for(j=1;j<=ncol;j++) {
    for(k=0;k<totalcellcolnumber;k++)
      if(j==cellcoltofirstpixel[k])
	//for(i=1;i<=nrow;i++)
	for(i=nrow;i>=1;i--)
	  cellrowpict[i][j]=200;
  }

  OpenPNG(&png1,".");
  CPlanePNG(cellrowpict,state,&png1,0);

  //  for(i=1;i<=nrow;i++) {
  for(i=nrow;i>=1;i--) {
    for(j=1;j<=ncol;j++)
      cellrowpict[i][j]=cells.celltype[state[i][j]];
    for(k=1;k<totalcellrownumber;k++)
      if(i==cellrowtocenterpixel[k])
	for(j=1;j<=ncol;j++)
	  cellrowpict[i][j]=201;
  }

  for(j=1;j<=ncol;j++) {
    for(k=1;k<totalcellcolnumber;k++)
      if(j==cellcoltocenterpixel[k])
	//for(i=1;i<=nrow;i++)
	for(i=nrow;i>=1;i--)
	  cellrowpict[i][j]=201;
  }

  CPlanePNG(cellrowpict,state,&png1,0);
    
  snprintf(a_string,STRING,"mv %.5d.png %s_cellrows.png",png1.nframes-2,outputfilename);
  system(a_string);
  snprintf(a_string,STRING,"mv %.5d.png %s_centers.png",png1.nframes-1,outputfilename);
  system(a_string);
  PlaneFree(cellrowpict);
  if(bending) {
    nrow=tmpnrow;
    ncol=tmpncol;
  }
}
 
void BendRoot()
{
  TYPE **bendplane,**bendcorners;
  double tangentx,tangenty;
  int i,k;
  double effectivey,effectivex;
  double straightangle,straightvector;
  double halfwidth;
  int oldy,oldx;
  FILE *fp;

  if(SHRT_MAX == INT_MAX) {
    fprintf(stderr,"SHRT_MAX == INT_MAX, change program\n");
    exit(EXIT_FAILURE);
  }

  if(file_exists[5] || file_exists[1]) {
    CELLS(cells,
	  for(k=0;k<9;k++)
	    EXTRAS[c].length[k]=0;
	  for(k=5;k<9;k++)
	    if(EXTRAS[c].x[k]>-1 && (EXTRAS[c].x[((k-4)%4+5)]>-1))
	      NoQuarter(c,k,EXTRAS[c].x[k],EXTRAS[c].y[k],EXTRAS[c].x[((k-4)%4+5)],EXTRAS[c].y[((k-4)%4+5)]);
	  );
    CPLANE(state,
	   if(corner[i][j])
	     EXTRAS[state[i][j]].length[corner[i][j]]++;
	   );
    CELLS(cells,
	  for(k=1;k<5;k++)
	    EXTRAS[c].length[k]+=2; //add two for the two cornerpoints
	  EXTRAS[c].length[UO]=EXTRAS[c].length[UPPER]+EXTRAS[c].length[OUTER];//for the cornerpoints we use the sum of the two lengths
	  EXTRAS[c].length[UI]=EXTRAS[c].length[UPPER]+EXTRAS[c].length[INNER];
	  EXTRAS[c].length[LI]=EXTRAS[c].length[LOWER]+EXTRAS[c].length[INNER];
	  EXTRAS[c].length[LO]=EXTRAS[c].length[LOWER]+EXTRAS[c].length[OUTER];
	  );
    if(usenewname)
      snprintf(a_string,STRING,"%s_length.dat",outputfilename);
    else
      snprintf(a_string,STRING,"%s_bended_length.dat",outputfilename);
    fp=fopen(a_string,"w");
    CELLS(cells,
	  if(c>specialcelltype && cells.celltype[c]!=FAKECELLWALL && cells.area[c]) {
	    fprintf(fp,"%d",c);
	    for(k=1;k<9;k++)
	      fprintf(fp,"\t%d",EXTRAS[c].length[k]);
	    fprintf(fp,"\n");
	  }
	  );
    fclose(fp);
  }

  curved[0].curvature=0.;
  curved[0].beginy=1.;  //this means that we take the middle of the pixel as meaning `1'
  curved[0].beginx=0.;
  curved[0].centerx=tissuecentrey;
  curved[0].centery=tissuecentrex;
  curved[0].normx=0.;
  curved[0].normy=1.;
  curved[0].referencex=curved[0].centerx;
  curved[0].referencey=curved[0].centery;
  curved[0].rotationpointx=0.;
  curved[0].rotationpointy=0.;
  
  //first further organize the bending zones 
  for(i=1;i<zonenumber;i++) {
    curved[i].beginy=(double)newindex(cellrowtofirstpixel[max(0,min(curved[i].cellrow,totalcellrownumber-1))]+curved[i].pixel,nrow);
    curved[i].beginx=curved[i].referenceline*(double)(right-left)/2.;
  }
  //we created a fake zone beyond all other zones, for which we only need beginy, to get the end of the last zone (i.e. zonenumber-1)
  curved[zonenumber].beginy=(double)(lower-top+2); //one pixel further than the end of the root, to be sure

  //here we should introduce sorting of the list

  for(i=1;i<zonenumber;i++) {
    if(fabs(curved[i-1].curvature)<EPSILON) { //previous part went straight
      curved[i].normx=curved[i-1].normx;
      curved[i].normy=curved[i-1].normy;
      curved[i].centerx=curved[i-1].centerx+curved[i-1].normx*(curved[i].beginy-curved[i-1].beginy);
      curved[i].centery=curved[i-1].centery+curved[i-1].normy*(curved[i].beginy-curved[i-1].beginy);
      rotatevector(-M_PI_2,curved[i].normy,curved[i].normx,&tangenty,&tangentx);
    }
    else { //previous part curled
      rotatevector(-curved[i-1].curvature*(curved[i].beginy-curved[i-1].beginy),curved[i-1].normy,curved[i-1].normx,&curved[i].normy,&curved[i].normx);
      rotatevector(-M_PI_2,curved[i].normy,curved[i].normx,&tangenty,&tangentx);
      curved[i].centerx=curved[i-1].rotationpointx-tangentx*(1./curved[i-1].curvature+curved[i-1].beginx);
      curved[i].centery=curved[i-1].rotationpointy-tangenty*(1./curved[i-1].curvature+curved[i-1].beginx);
    }
     
    curved[i].referencex=curved[i].centerx+tangentx*curved[i].beginx;
    curved[i].referencey=curved[i].centery+tangenty*curved[i].beginx;
    if(fabs(curved[i].curvature)<EPSILON) {
      curved[i].rotationpointx=0.;
      curved[i].rotationpointy=0.;
    }
    else {
      curved[i].rotationpointx=curved[i].referencex+tangentx/curved[i].curvature;
      curved[i].rotationpointy=curved[i].referencey+tangenty/curved[i].curvature;
    }
    curved[i].rocrefx=curved[i].referencex-curved[i].rotationpointx;
    curved[i].rocrefy=curved[i].referencey-curved[i].rotationpointy;
    curved[i].roccenter=hypot(curved[i].rotationpointy-curved[i].centery,curved[i].rotationpointx-curved[i].centerx);
  }
 
  /*
    for(k=0;k<=zonenumber;k++) {
    printf("zone: %d\n",k);
    printf("curvature: %lf\n",curved[k].curvature);
    printf("cellrow: %d\n",curved[k].cellrow);
    printf("pixel: %d\n",curved[k].pixel);
    printf("referenceline: %lf\n",curved[k].referenceline);
    printf("beginx: %lf\n",curved[k].beginx);
    printf("beginy: %lf\n",curved[k].beginy);
    printf("centerx: %lf\n",curved[k].centerx);
    printf("centery: %lf\n",curved[k].centery);
    printf("normx: %lf\n",curved[k].normx);
    printf("normy: %lf\n",curved[k].normy);
    printf("referencex: %lf\n",curved[k].referencex);
    printf("referencey: %lf\n",curved[k].referencey);
    printf("rotationpointx: %lf\n",curved[k].rotationpointx);
    printf("rotationpointy: %lf\n",curved[k].rotationpointy);
    printf("rocrefx: %lf\n",curved[k].rocrefx);
    printf("rocrefy: %lf\n",curved[k].rocrefy);
    printf("roccenter: %lf\n",curved[k].roccenter);
    }
  */

  //then fill the bendplanes
  bendplane=New();
  bendcorners=New();
  halfwidth=(double)(right-left)/2.;
  for(k=0;k<zonenumber;k++) {
    PLANE(
	  if(!bendplane[i][j]) {
	    if(fabs(curved[k].curvature)<EPSILON) { //straight
	      straightangle=vectorangle(curved[k].normy,curved[k].normx,(double)i-curved[k].centery,(double)j-curved[k].centerx);
	      effectivey=(straightvector=hypot((double)i-curved[k].centery,(double)j-curved[k].centerx))*cos(straightangle); 
	      //effectivey is the relative y-position within this zone we want to distort
	      //y=0 means at top of zone to be distorted, with higher y-values meaning moving downwards the root
	      //effectivex is the relative x-position within this zone we want to distort 
	      //x=0 means at centre of zone, with higher x-values meaning moving to the right before bending
	      effectivex=-straightvector*sin(straightangle);
	    }
	    else { //curved
	      effectivey=-vectorangle(curved[k].rocrefy,curved[k].rocrefx,(double)i-curved[k].rotationpointy,(double)j-curved[k].rotationpointx)/curved[k].curvature;
	      effectivex=sgn(curved[k].curvature)*(curved[k].roccenter-hypot((double)i-curved[k].rotationpointy,(double)j-curved[k].rotationpointx));
	      if(effectivey<-EPSILON) //we make at most one whole circle
		effectivey+=sgn(curved[k].curvature)*2.*M_PI/curved[k].curvature;
	      //printf("(%d,%d) (%lf,%lf) %lf %lf %lf\n",i,j,effectivey,effectivex,vectorangle(curved[k].rocrefy,curved[k].rocrefx,(double)i-curved[k].rotationpointy,(double)j-curved[k].rotationpointx),curved[k].roccenter,hypot((double)i-curved[k].rotationpointy,(double)j-curved[k].rotationpointx));
	    }
	    if(effectivey>=0. && effectivey<=curved[k+1].beginy-curved[k].beginy) { //pixel must be within zone
	      oldy=newindex((int)nearbyint(effectivey+curved[k].beginy),nrow);
	      oldx=newindex((int)nearbyint(effectivex+halfwidth+1e-8),ncol); //1e-8 to make straight parts nicely straight when halfwidth=0.5
	      bendplane[i][j]=state[oldy][oldx];
	      if(state[oldy][oldx]) {
		originalstate[i][j]=oldy*SHRT_MAX+oldx;
		newstate[oldy][oldx]=i*SHRT_MAX+j;
	      }
	      bendcorners[i][j]=region[oldy][oldx]; //save the region, not the corners
	    }
	  }
	  );
  }

  snprintf(a_string,STRING,"%s_bended%s",outputfilename,filenames[5]);
  fp=fopen(a_string,"w");
  SavePat(fp,"%d\t",bendcorners);
  fclose(fp);
  snprintf(a_string,STRING,"%s_bended%s",outputfilename,filenames[7]);
  fp=fopen(a_string,"w");
  SavePat(fp,"%d\t",bendplane);
  fclose(fp);
  snprintf(a_string,STRING,"%s_bended%s",outputfilename,filenames[4]);
  fp=fopen(a_string,"w");
  CSavePat(fp,&cells);
  fclose(fp);
  snprintf(a_string,STRING,"%s_bended%s",outputfilename,".backwards");
  fp=fopen(a_string,"w");
  SavePat(fp,"%d\t",originalstate);
  fclose(fp);
  snprintf(a_string,STRING,"%s_bended%s",outputfilename,".forwards");
  fp=fopen(a_string,"w");
  SavePat(fp,"%d\t",newstate);
  fclose(fp);
  //this could be nicer
  if(file_exists[7]) {
    snprintf(a_string,STRING,"cp %s%s %s_bended%s",filename,filenames[7],outputfilename,".oldstate");
    system(a_string);
  }
  snprintf(a_string,STRING,"%s --nobending -t %s_bended%s",programname,outputfilename,filenames[7]);
  system(a_string);
  exit(1);
  //misschien ooit uitbreiding om direct door te gaan 
}

void WallFreeCellwall()
{
  TYPE **regularplane=NULL;
  int found,remove,round=0;
  int i,j;

  regularplane=New();

  if(ncol%2) {
    for(j=ncol/2+1;j<ncol;j++)
      for(i=1;i<=nrow;i++)
	state[i][j]=state[i][j+1];
    for(i=1;i<=nrow;i++)
      state[i][ncol]=MEDIUM;
  }
  do {
    found=0;
    remove=0;
    round++;
    PLANE(
	  if(state[i][j]==CELLWALL) {
	    found++;
	    regularplane[i][j]=0;
	  }
	  else
	    regularplane[i][j]=1;
	  );
    Boundaries(regularplane);
    Boundaries(state);

    PLANE(
	  if(state[i][j]==CELLWALL) {
	    if(regularplane[i-1][j] && state[i-1][j]!=CELLWALL) {
	      state[i][j]=state[i-1][j];
	      remove++;
	    }
	    else if(regularplane[i][j+(j<=ncol/2?-1:1)]) {
	      state[i][j]=state[i][j+(j<=ncol/2?-1:1)];
	      remove++;
	    }
	    else if(regularplane[i-1][j+(j<=ncol/2?-1:1)]) {
	      state[i][j]=state[i-1][j+(j<=ncol/2?-1:1)];
	      remove++;
	    }
	  }
	  );
    fprintf(stdout,"round %d: %d removed cellwall\n",round,remove);
  } while (found);
  PlaneFree(regularplane);
}

void ReduceCellwall() 
{
  int found;
  int add=0,remove=0,round=0;
  int laatste,k,buurk;
  TYPE **regularplane=NULL;
  int originali,originalj;
  int buury,buurx;
  Position buren[4]={{1,0},{-1,0},{0,-1},{0,1}};

  NeighbourNumber(cellwall);
  regularplane=New();

  PLANE(
	if(corner[i][j]<5 || corner[i][j]>8) //corner is not `real' cornerpoint
	  corner[i][j]=0; 
	);
  
  do {
    add=0;
    remove=0;
    round++;
    if(regular)
      laatste=4;
    else
      laatste=1;
    
    for(k=0;k<laatste;k++) {
      if(regular)
	TOTALPLANE(
		   regularplane[i][j]=1;
		   );
      ASYNPLANE(
		//we want to trim
		sigma=state[i][j];
		if(expand<2 && (sigma==CELLWALL || cells.celltype[sigma]==FAKECELLWALL)) {
		  found=0;
		  ASYNNEIGHBOURS(
				 sigmaneigh=state[i+y][j+x];
				 if(( ((!regular || k==0) && neigh==EAST) || 
				      ((!regular || k==1) && neigh==WEST) || 
				      ((!regular || k==2) && neigh==NORTH) || 
				      ((!regular || k==3) && neigh==SOUTH)) && 
				    ((!regular || regularplane[i+y][j+x]) && sigmaneigh!=CELLWALL && cells.celltype[sigmaneigh]!=FAKECELLWALL && j+x!=0)) {
				   found=1;
				   ineigh=i+y;
				   jneigh=j+x;
				   break;
				 }
				 );
		  if(found==1) { //we found a potential neighbour
				 //which is not cellwall. The cellwall
				 //could become this neighbour if...
		    found=0;
		    WIDENEIGHBOURS(
				   //...there is no other cell within the neighbourhood
				   if(state[newindex(i+y,nrow)][newindex(j+x,ncol)]!=CELLWALL && cells.celltype[state[newindex(i+y,nrow)][newindex(j+x,ncol)]]!=FAKECELLWALL && state[newindex(i+y,nrow)][newindex(j+x,ncol)]!=sigmaneigh) {
				     found=1;
				     break;
				   }
				   );
		    //we want to change the cellwall
		    if(!found) {
		      iposition=i;
		      jposition=j;
		      regularplane[i][j]=0;
		      //CellMembrane assumes that (iposition,jposition) gets empty, and moves the corner
		      if(!nobending) {
			if(CellMembrane(corner,state,border))
			  fprintf(stdout,"oeps, there goes a corner\n");
			//printf("%d %d %d %d %d %d\n",iposition,jposition,ineigh,jneigh,sigma,sigmaneigh);
		      }
		      else { //we have to put the forwardbackwardplane correct
			//calculate new borders
			AsynCopy(border,iposition,jposition,0);
			for(buurk=0;buurk<4;buurk++){
			  buury=buren[buurk].yy+iposition;buurx=buren[buurk].xx+jposition;
			  if(sigma==state[buury][buurx]){
			    AsynCopy(border,buury,buurx,border[buury][buurx]+1);
			    AsynCopy(border,iposition,jposition,border[iposition][jposition]+1);
			  }
			  else if(sigmaneigh==state[buury][buurx])
			    AsynCopy(border,buury,buurx,border[buury][buurx]-1);
			  else
			    AsynCopy(border,iposition,jposition,border[iposition][jposition]+1);
			}

			//only one state has to be modified, and one potentially reset
			//reset the newstate if pointing to this cellwall
			if(newstate[originalstate[i][j]/SHRT_MAX][originalstate[i][j]%SHRT_MAX]==i*SHRT_MAX+j)
			  newstate[originalstate[i][j]/SHRT_MAX][originalstate[i][j]%SHRT_MAX]=0; //see todo (*)
			originalstate[i][j]=originalstate[ineigh][jneigh]; //(from bent to unbent)
			corner[i][j]=corner[ineigh][jneigh];
		      }
 		      state[i][j]=sigmaneigh;
		      remove++;
		    }
		  }
		}
		//we want to expand
		else if(expand && sigma!=CELLWALL && cells.celltype[sigma]!=FAKECELLWALL) {
		  found=0;
		  sigmaneigh=CELLWALL;//we always want it to become cellwall
		  NEIGHBOURS( //always about CELLWALL, so no ASYN needed
			     if(( ((!regular || k==0) && neigh==EAST) || 
				  ((!regular || k==1) && neigh==WEST) || 
				  ((!regular || k==2) && neigh==NORTH) || 
				  ((!regular || k==3) && neigh==SOUTH)) && 
				(!regular || regularplane[i+y][j+x]) && state[i+y][j+x]!=sigma) {//also when not cellwall, but simply other cell
			       found=1;
			       ineigh=i+y;
			       jneigh=j+x;
			       break;
			     }
			     );
		  if(found==1) { //we are bordering cellwall
		    found=0;
		    WIDENEIGHBOURS(
				   if(state[newindex(i+y,nrow)][newindex(j+x,ncol)]!=CELLWALL && cells.celltype[state[newindex(i+y,nrow)][newindex(j+x,ncol)]]!=FAKECELLWALL && state[newindex(i+y,nrow)][newindex(j+x,ncol)]!=sigma) {
				     found=1;
				     break;
				   }
				   );
		    if(found) {
		      iposition=i;
		      jposition=j;
		      regularplane[i][j]=0;
		      if(!nobending) {
			if(CellMembrane(corner,state,border))
			  fprintf(stdout,"oeps, there goes a corner\n");
		      }
		      else {//we have to put the forwardbackwardplanes correct, but complicated
			//calculate new borders
			AsynCopy(border,iposition,jposition,0);
			for(buurk=0;buurk<4;buurk++){
			  buury=buren[buurk].yy+iposition;buurx=buren[buurk].xx+jposition;
			  if(sigma==state[buury][buurx]){
			    AsynCopy(border,buury,buurx,border[buury][buurx]+1);
			    AsynCopy(border,iposition,jposition,border[iposition][jposition]+1);
			    if(border[buury][buurx]==1)
			      AsynCopy(corner,buury,buurx,corner[i][j]);
			  }
			  else if(sigmaneigh==state[buury][buurx])
			    AsynCopy(border,buury,buurx,border[buury][buurx]-1);
			  else
			    AsynCopy(border,iposition,jposition,border[iposition][jposition]+1);
			}
			corner[i][j]=0;
			if(originalstate[i][j]) { //make sure this is not medium
			  originali=originalstate[i][j]/SHRT_MAX;
			  originalj=originalstate[i][j]%SHRT_MAX;
			  if(newstate[originalstate[i][j]/SHRT_MAX][originalstate[i][j]%SHRT_MAX]==i*SHRT_MAX+j)
			    newstate[originalstate[i][j]/SHRT_MAX][originalstate[i][j]%SHRT_MAX]=0;
			}
			else { //it must be medium, so use the cell next to it
			  originali=originalstate[ineigh][jneigh]/SHRT_MAX;
			  originalj=originalstate[ineigh][jneigh]%SHRT_MAX;
			}
			if(sigmaneigh==CELLWALL || cells.celltype[sigmaneigh]==FAKECELLWALL)
			  originalstate[i][j]=originalstate[ineigh][jneigh];
			else {
			  WIDENEIGHBOURS(
					 if(oldstate[newindex(originali+y,nrow)][newindex(originalj+x,ncol)]==CELLWALL || cells.celltype[oldstate[newindex(originali+y,nrow)][newindex(originalj+x,ncol)]]==FAKECELLWALL) {
					   originalstate[i][j]=newindex(originali+y,nrow)*SHRT_MAX+newindex(originalj+x,ncol);
					   break;
					 }
					 );
			}
		      }
		      if(cells.celltype[sigmaneigh]==FAKECELLWALL)
			state[i][j]=sigmaneigh;
		      else
			state[i][j]=CELLWALL;
		      add++;
		    }
		  }
		}
		);
    }
    fprintf(stdout,"round %d: %d added cellwall; %d removed cellwall\n",round,add,remove);
  } while (add || remove);
  if(nobending) { //we have make sure that in the case a change has been made, and the 
  }
  UpdateCFill(state,&cells);//because small ones will be wrong
  UpdateCellPosition(state,&cells);//because of mx. my
  PlaneFree(regularplane);

  //get the corners correct again
  CELLS(cells,
	for(k=0;k<9;k++) {
	  EXTRAS[c].x[k]=-1;
	  EXTRAS[c].y[k]=-1;
	}
	);
  
  PLANE(
	if(corner[i][j]<5 || corner[i][j]>8) //corner is not `real' cornerpoint
	  corner[i][j]=0; 
	else if(state[i][j]>specialcelltype && cells.celltype[state[i][j]]!=FAKECELLWALL && EXTRAS[state[i][j]].x[corner[i][j]]<0 && border[i][j]) {
	  EXTRAS[state[i][j]].x[corner[i][j]]=j;
	  EXTRAS[state[i][j]].y[corner[i][j]]=i;
	}
	else
	  corner[i][j]=0;
	);
  Fishy();
}

void NorthAngle(int i,int j)
{
  if(border[i][j])
    angle[i][j]=vectorangle(vecy,vecx,(double)i-cells.shape[state[i][j]].meany,(double)j-cells.shape[state[i][j]].meanx);
}

void BestAngle(int i,int j)
{
  if(border[i][j])
    if(fabs(angle[i][j]-targetangle)< bestangle) {
      bestangle=fabs(angle[i][j]-targetangle);
      bestmatchx=j;
      bestmatchy=i;
    }
}

void BestCorner(int i,int j)
{
  double arclength,radiallength,distance,distancetocentre;
  //double theta,thetatocentre;

  if (polar) {
    if (border[i][j]) {
      //we want to calculate the sum of the arc length plus the distance along the radius
      distancetocentre=hypot(cells.shape[state[i][j]].meany-tissuecentrey,cells.shape[state[i][j]].meanx-tissuecentrex);
      //goes wrong
      //thetatocentre=atan2(cells.shape[state[i][j]].meany-tissuecentrey,cells.shape[state[i][j]].meanx-tissuecentrex);

      distance=hypot((double)i-tissuecentrey,(double)j-tissuecentrex);
      //goes wrong
      //theta=atan2((double)i-tissuecentrey,(double)j-tissuecentrex);

      //the following goes wrong at -pi,pi transition 
      //arclength=distancetocentre*(theta-thetatocentre);
      arclength=distancetocentre*vectorangle(cells.shape[state[i][j]].meany-tissuecentrey,cells.shape[state[i][j]].meanx-tissuecentrex,(double)i-tissuecentrey,(double)j-tissuecentrex);
      radiallength=distance-distancetocentre;
      angle[i][j]=multtheta*arclength+multrho*radiallength;
      //test
      if(multtheta*arclength<0. || multrho*radiallength<0.)
	angle[i][j]=0.;
    }
  }
  else {
    if(border[i][j]) {
      angle[i][j]=multy*((double)i-cells.shape[state[i][j]].meany)+multx*((double)j-cells.shape[state[i][j]].meanx);
      //test
      if(multy*((double)i-cells.shape[state[i][j]].meany)<0. || multx*((double)j-cells.shape[state[i][j]].meanx)<0.)
	angle[i][j]=0.;
    }
  }
}

void BestCorner2(int i,int j)
{
  if(border[i][j])
    if(angle[i][j]>bestangle) {
      bestangle=angle[i][j];
      bestmatchx=j;
      bestmatchy=i;
    }
}

void BestCorner3(int i,int j)
{
  if(border[i][j]) {
    if(!bestmatchx || j*imultx>bestmatchx || (j*imultx==bestmatchx && i*imulty>bestmatchxy)) {
      bestmatchx=j*imultx;
      bestmatchxy=i*imulty;
    }
    if(!bestmatchy || i*imulty>bestmatchy || (i*imulty==bestmatchy && j*imultx>bestmatchyx)) {
      bestmatchy=i*imulty;
      bestmatchyx=j*imultx;
    }
  }
}

void EmptyCell(int i,int j)
{
  if(corner[i][j] && (corner[i][j]<5 || corner[i][j]>8))
    corner[i][j]=0;
  region[i][j]=0;
}

void EducatedGuessThree(int c)
{
  int k;

  for(k=5;k<9;k++) {
    if(overwrite || EXTRAS[c].x[k]==-1) {
      imultx=1;
      imulty=1;
      if(k==LI || k==LO) imulty*=-1;
      if(k==UI || k==LI) imultx*=-1;
      if(cells.shape[c].meany<=tissuecentrey && (symmetry==2 || symmetry==3)) imulty*=-1;
      if(cells.shape[c].meanx<=tissuecentrex && (symmetry==1 || symmetry==3)) imultx*=-1; 
      if(symmetry==4 || tissuetype==FRUIT)
	if(((ncol+(int)cells.shape[c].meanx-(int)tissuecentrex-1)%(ncol/divisions)+1)<=(ncol/(2*divisions)))
	  imultx*=-1;
      bestmatchx=0;
      bestmatchy=0;
      OneCell(state,&cells,c,&BestCorner3);
      if(bestmatchx) {
	if(EXTRAS[c].x[k]>-1 && corner[EXTRAS[c].y[k]][EXTRAS[c].x[k]]==k) {//this is not the first time we find this corner; check that this position really has the correct value!
	  corner[EXTRAS[c].y[k]][EXTRAS[c].x[k]]=0; //remove old one 
	  region[EXTRAS[c].y[k]][EXTRAS[c].x[k]]=0;
	}
	EXTRAS[c].oldx[k]=EXTRAS[c].x[k];
	EXTRAS[c].oldy[k]=EXTRAS[c].y[k];
	if(cells.celltype[c]==QCROOT || cells.celltype[c]==COLUMELLAROOT) {
	  if(k==UO || k==LI) {
	    EXTRAS[c].y[k]=imulty*bestmatchy;
	    EXTRAS[c].x[k]=imultx*bestmatchyx;
	    corner[imulty*bestmatchy][imultx*bestmatchyx]=k;
	  }
	  else {
	    EXTRAS[c].y[k]=imulty*bestmatchxy;
	    EXTRAS[c].x[k]=imultx*bestmatchx;
	    corner[imulty*bestmatchxy][imultx*bestmatchx]=k;
	  }
	}
	else {
	  if(k==UI || k==LO) {
	    EXTRAS[c].y[k]=imulty*bestmatchy;
	    EXTRAS[c].x[k]=imultx*bestmatchyx;
	    corner[imulty*bestmatchy][imultx*bestmatchyx]=k;
	  }
	  else {
	    EXTRAS[c].y[k]=imulty*bestmatchxy;
	    EXTRAS[c].x[k]=imultx*bestmatchx;
	    corner[imulty*bestmatchxy][imultx*bestmatchx]=k;
	  }
	}
      }
    }
    else {
      EXTRAS[c].oldx[k]=EXTRAS[c].x[k];
      EXTRAS[c].oldy[k]=EXTRAS[c].y[k];
    }
  }
  for(k=5;k<9;k++)
    if(EXTRAS[c].x[k]>-1 && (EXTRAS[c].x[((k-4)%4+5)]>-1))
      NoQuarter(c,k,EXTRAS[c].x[k],EXTRAS[c].y[k],EXTRAS[c].x[((k-4)%4+5)],EXTRAS[c].y[((k-4)%4+5)]);
  
  lastk=0;
  lastcell=c;
}

void EducatedGuessTwo(int c)
{
  int k;

  for(k=5;k<9;k++) {
    if(overwrite || EXTRAS[c].x[k]==-1) {
      if(polar) {
	multrho=1.;
	multtheta=1.;
	if(k==CCWI || k==CWI )
	  multrho*=-1.;
	if(k==CWI || k==CWO)
	  multtheta*=-1.;
	if(cells.shape[c].meany<=tissuecentrey && (symmetry==2 || symmetry==3)) multtheta*=-1.;
	if(cells.shape[c].meanx<=tissuecentrex && (symmetry==1 || symmetry==3)) multtheta*=-1.; 
      }
      else {
	multx=1.;
	multy=1.;
	if(k==LI || k==LO) multy*=-1.;
	if(k==UI || k==LI) multx*=-1.;
	if(cells.shape[c].meany<=tissuecentrey && (symmetry==2 || symmetry==3)) multy*=-1.;
	if(cells.shape[c].meanx<=tissuecentrex && (symmetry==1 || symmetry==3)) multx*=-1.; 
	if(symmetry==4 || tissuetype==FRUIT)
	  if(((ncol+(int)cells.shape[c].meanx-(int)tissuecentrex-1)%(ncol/divisions)+1)<=(ncol/(2*divisions)))
	    multx*=-1.;
      }
      OneCell(state,&cells,c,&BestCorner);
      bestmatchx=0;
      bestmatchy=0;
      bestangle=0.;
      OneCell(state,&cells,c,&BestCorner2);
      if(bestmatchx) {
	if(EXTRAS[c].x[k]>-1 && corner[EXTRAS[c].y[k]][EXTRAS[c].x[k]]==k) {//this is not the first time we find this corner; check that this position really has the correct value!
	  corner[EXTRAS[c].y[k]][EXTRAS[c].x[k]]=0; //remove old one 
	  region[EXTRAS[c].y[k]][EXTRAS[c].x[k]]=0;
	}
	EXTRAS[c].oldx[k]=EXTRAS[c].x[k];
	EXTRAS[c].oldy[k]=EXTRAS[c].y[k];
	EXTRAS[c].x[k]=bestmatchx;
	EXTRAS[c].y[k]=bestmatchy;
	corner[bestmatchy][bestmatchx]=k;
      }
    }
    else {
      EXTRAS[c].oldx[k]=EXTRAS[c].x[k];
      EXTRAS[c].oldy[k]=EXTRAS[c].y[k];
    }
  }
  for(k=5;k<9;k++)
    if(EXTRAS[c].x[k]>-1 && (EXTRAS[c].x[((k-4)%4+5)]>-1))
      NoQuarter(c,k,EXTRAS[c].x[k],EXTRAS[c].y[k],EXTRAS[c].x[((k-4)%4+5)],EXTRAS[c].y[((k-4)%4+5)]);
  
  lastk=0;
  lastcell=c;
}

void EducatedGuessOne(int c)
{
  double tmpvec,hoek;
  int k;

  if(c<=specialcelltype || cells.celltype[c]==FAKECELLWALL)
    return;
  OneCellShape(&cells,c);

  //first calculate the angle which, for a square, point towards the corner
  //for an ellipse, this angle divides the ellipse in 4 equal parts
  hoek=atan2(cells.shape[c].eigenval1,cells.shape[c].eigenval2);
  //let's make (vecx,vecy) point to the north (ie within +-45
  //degrees), and angle represent the angular distance to north
  //make sure it point to northern hemisphere
  if(cells.shape[c].eigenvec1y<0.) {
    vecx=-cells.shape[c].eigenvec1x;
    vecy=-cells.shape[c].eigenvec1y;
  }
  else {
    vecx=cells.shape[c].eigenvec1x;
    vecy=cells.shape[c].eigenvec1y;
  }
  //turn around if |x|>|y|
  if(vecx>M_SQRT1_2) {
    tmpvec=vecx;
    vecx=-vecy;
    vecy=tmpvec;
  }
  else if(vecx<-M_SQRT1_2) {
    tmpvec=vecx;
    vecx=vecy;
    vecy=-tmpvec;
  }
  else //now hoek should be relative to longest axis
    hoek=M_PI_2-hoek;

  //calculate for each point the angle not to the true north, 
  //but to the main axis that is pointing within 45 degrees from the true north
  OneCell(state,&cells,c,&NorthAngle);
  
  for(k=5;k<9;k++) {
    if(overwrite || EXTRAS[c].x[k]==-1) {
      bestmatchx=0;
      bestmatchy=0;
      bestangle=M_PI+1.;
      //hoe is between 0 & M_PI_2
      targetangle=hoek;//for UI, ccw first corner
      if(k==LI || k==LO) targetangle=M_PI-targetangle; //for LI, ccw second corner
      if(cells.shape[c].meany<=tissuecentrey && (symmetry==2 || symmetry==3)) targetangle=M_PI-targetangle;
      if(k==UO || k==LO) targetangle=-targetangle; //to move cw instead of ccw
      if(cells.shape[c].meanx<=tissuecentrex && (symmetry==1 || symmetry==3)) targetangle=-targetangle;
      if(symmetry==4 || tissuetype==FRUIT)
	if(((ncol+(int)cells.shape[c].meanx-(int)tissuecentrex-1)%(ncol/divisions)+1)<=(ncol/(2*divisions)))
	  targetangle=-targetangle;

      //targetangle=(cells.shape[c].meany<nrow/2 && symmetry>1)?M_PI-hoek:hoek;
      //if(cells.shape[c].meanx<ncol/2 && (symmetry==1 || symmetry==3)) targetangle=-targetangle; 
      OneCell(state,&cells,c,&BestAngle);
      if(bestmatchx) {
	if(EXTRAS[c].x[k]>-1 && corner[EXTRAS[c].y[k]][EXTRAS[c].x[k]]==k) { //this is not the first time we find this corner; check that this position really has the correct value!
	  corner[EXTRAS[c].y[k]][EXTRAS[c].x[k]]=0; //remove old one 
	  region[EXTRAS[c].y[k]][EXTRAS[c].x[k]]=0;
	}
	EXTRAS[c].oldx[k]=EXTRAS[c].x[k];
	EXTRAS[c].oldy[k]=EXTRAS[c].y[k];
	EXTRAS[c].x[k]=bestmatchx;
	EXTRAS[c].y[k]=bestmatchy;
	corner[bestmatchy][bestmatchx]=k;
      }
    }
    else {
      EXTRAS[c].oldx[k]=EXTRAS[c].x[k];
      EXTRAS[c].oldy[k]=EXTRAS[c].y[k];
    }
  }
  for(k=5;k<9;k++)
    if(EXTRAS[c].x[k]>-1 && (EXTRAS[c].x[((k-4)%4+5)]>-1))
      NoQuarter(c,k,EXTRAS[c].x[k],EXTRAS[c].y[k],EXTRAS[c].x[((k-4)%4+5)],EXTRAS[c].y[((k-4)%4+5)]);
  lastk=0;
  lastcell=c;
}

void SplitZone(int i,int j)
{
  int c;
  double xc,yc;

  if((region[i][j]%10)!=side)
    return;

  region[i][j]%=10;
  c=state[i][j];
  xc=(double)j-cells.shape[c].meanx;
  yc=(double)i-cells.shape[c].meany;
  if(((side==OUTER || side==INNER) && upperzone>EPSILON) || ((side==UPPER || side==LOWER) && outerzone>EPSILON)) {
    if(yc>0) { //just to get symmetry in end-result
      if(side==OUTER && upperzone>EPSILON) //multiply with vectorangle of cornerpoints to get right orientation
	region[i][j]+=((vectorangle((upperzone*EXTRAS[c].y[side+4]+(1.-upperzone)*EXTRAS[c].y[(side%4+5)])-cells.shape[c].meany,(upperzone*EXTRAS[c].x[side+4]+(1.-upperzone)*EXTRAS[c].x[(side%4+5)])-cells.shape[c].meanx,yc,xc)*vectorangle(EXTRAS[c].y[side+4]-cells.shape[c].meany,EXTRAS[c].x[side+4]-cells.shape[c].meanx,EXTRAS[c].y[(side%4+5)]-cells.shape[c].meany,EXTRAS[c].x[(side%4+5)]-cells.shape[c].meanx)<0.)?10:20);
      else if(side==INNER && upperzone>EPSILON)
	region[i][j]+=((vectorangle(((1.-upperzone)*EXTRAS[c].y[side+4]+upperzone*EXTRAS[c].y[(side%4+5)])-cells.shape[c].meany,((1.-upperzone)*EXTRAS[c].x[side+4]+upperzone*EXTRAS[c].x[(side%4+5)])-cells.shape[c].meanx,yc,xc)*vectorangle(EXTRAS[c].y[side+4]-cells.shape[c].meany,EXTRAS[c].x[side+4]-cells.shape[c].meanx,EXTRAS[c].y[(side%4+5)]-cells.shape[c].meany,EXTRAS[c].x[(side%4+5)]-cells.shape[c].meanx)<0.)?10:20);
      if(side==LOWER && outerzone>EPSILON)
	region[i][j]+=((vectorangle((outerzone*EXTRAS[c].y[side+4]+(1.-outerzone)*EXTRAS[c].y[(side%4+5)])-cells.shape[c].meany,(outerzone*EXTRAS[c].x[side+4]+(1.-outerzone)*EXTRAS[c].x[(side%4+5)])-cells.shape[c].meanx,yc,xc)*vectorangle(EXTRAS[c].y[side+4]-cells.shape[c].meany,EXTRAS[c].x[side+4]-cells.shape[c].meanx,EXTRAS[c].y[(side%4+5)]-cells.shape[c].meany,EXTRAS[c].x[(side%4+5)]-cells.shape[c].meanx)<0.)?10:20);
      else if(side==UPPER && outerzone>EPSILON)
	region[i][j]+=((vectorangle(((1.-outerzone)*EXTRAS[c].y[side+4]+outerzone*EXTRAS[c].y[(side%4+5)])-cells.shape[c].meany,((1.-outerzone)*EXTRAS[c].x[side+4]+outerzone*EXTRAS[c].x[(side%4+5)])-cells.shape[c].meanx,yc,xc)*vectorangle(EXTRAS[c].y[side+4]-cells.shape[c].meany,EXTRAS[c].x[side+4]-cells.shape[c].meanx,EXTRAS[c].y[(side%4+5)]-cells.shape[c].meany,EXTRAS[c].x[(side%4+5)]-cells.shape[c].meanx)<0.)?10:20);
    }
    else {
      if(side==OUTER && upperzone>EPSILON)
	region[i][j]+=((vectorangle((upperzone*EXTRAS[c].y[side+4]+(1.-upperzone)*EXTRAS[c].y[(side%4+5)])-cells.shape[c].meany,(upperzone*EXTRAS[c].x[side+4]+(1.-upperzone)*EXTRAS[c].x[(side%4+5)])-cells.shape[c].meanx,yc,xc)*vectorangle(EXTRAS[c].y[side+4]-cells.shape[c].meany,EXTRAS[c].x[side+4]-cells.shape[c].meanx,EXTRAS[c].y[(side%4+5)]-cells.shape[c].meany,EXTRAS[c].x[(side%4+5)]-cells.shape[c].meanx)>0.)?20:10);
      else if(side==INNER && upperzone>EPSILON)
	region[i][j]+=((vectorangle(((1.-upperzone)*EXTRAS[c].y[side+4]+upperzone*EXTRAS[c].y[(side%4+5)])-cells.shape[c].meany,((1.-upperzone)*EXTRAS[c].x[side+4]+upperzone*EXTRAS[c].x[(side%4+5)])-cells.shape[c].meanx,yc,xc)*vectorangle(EXTRAS[c].y[side+4]-cells.shape[c].meany,EXTRAS[c].x[side+4]-cells.shape[c].meanx,EXTRAS[c].y[(side%4+5)]-cells.shape[c].meany,EXTRAS[c].x[(side%4+5)]-cells.shape[c].meanx)>0.)?20:10);
      if(side==LOWER && outerzone>EPSILON)
	region[i][j]+=((vectorangle((outerzone*EXTRAS[c].y[side+4]+(1.-outerzone)*EXTRAS[c].y[(side%4+5)])-cells.shape[c].meany,(outerzone*EXTRAS[c].x[side+4]+(1.-outerzone)*EXTRAS[c].x[(side%4+5)])-cells.shape[c].meanx,yc,xc)*vectorangle(EXTRAS[c].y[side+4]-cells.shape[c].meany,EXTRAS[c].x[side+4]-cells.shape[c].meanx,EXTRAS[c].y[(side%4+5)]-cells.shape[c].meany,EXTRAS[c].x[(side%4+5)]-cells.shape[c].meanx)>0.)?20:10);
      else if(side==UPPER && outerzone>EPSILON)
	region[i][j]+=((vectorangle(((1.-outerzone)*EXTRAS[c].y[side+4]+outerzone*EXTRAS[c].y[(side%4+5)])-cells.shape[c].meany,((1.-outerzone)*EXTRAS[c].x[side+4]+outerzone*EXTRAS[c].x[(side%4+5)])-cells.shape[c].meanx,yc,xc)*vectorangle(EXTRAS[c].y[side+4]-cells.shape[c].meany,EXTRAS[c].x[side+4]-cells.shape[c].meanx,EXTRAS[c].y[(side%4+5)]-cells.shape[c].meany,EXTRAS[c].x[(side%4+5)]-cells.shape[c].meanx)>0.)?20:10);
    }
  }
  else {
    if(yc>0) //just to get symmetry in end-result
      region[i][j]+=((fabs(vectorangle(EXTRAS[c].y[side+4]-cells.shape[c].meany,EXTRAS[c].x[side+4]-cells.shape[c].meanx,yc,xc))<fabs(vectorangle(EXTRAS[c].y[(side%4+5)]-cells.shape[c].meany,EXTRAS[c].x[(side%4+5)]-cells.shape[c].meanx,yc,xc)))?10:20);
    else
      region[i][j]+=((fabs(vectorangle(EXTRAS[c].y[side+4]-cells.shape[c].meany,EXTRAS[c].x[side+4]-cells.shape[c].meanx,yc,xc))>fabs(vectorangle(EXTRAS[c].y[(side%4+5)]-cells.shape[c].meany,EXTRAS[c].x[(side%4+5)]-cells.shape[c].meanx,yc,xc)))?20:10);
  }

  if(corner[i][j])
    corner[i][j]=region[i][j];
}

void SplitZonePolar(int i,int j)
{
  int c;
  double xc,yc;

  if((region[i][j]%10)!=side)
    return;

  region[i][j]%=10;
  c=state[i][j];
  //just to get symmetry in end-result
  if(side==OUTWARDS || side==INWARDS) { 
    if(upperzone>EPSILON) {
      xc=(double)j-tissuecentrex;
      yc=(double)i-tissuecentrey;
    }
    else {
      xc=(double)j-cells.shape[c].meanx;
      yc=(double)i-cells.shape[c].meany;
    }
    if(side==OUTWARDS) {//just to get symmetry in end-result
      if(upperzone>EPSILON)
	region[i][j]+=(upperzone*fabs(vectorangle(EXTRAS[c].y[side+4]-tissuecentrey,EXTRAS[c].x[side+4]-tissuecentrex,yc,xc))<(1.-upperzone)*fabs(vectorangle(EXTRAS[c].y[(side%4+5)]-tissuecentrey,EXTRAS[c].x[(side%4+5)]-tissuecentrex,yc,xc))?10:20);
      else
	region[i][j]+=((fabs(vectorangle(EXTRAS[c].y[side+4]-cells.shape[c].meany,EXTRAS[c].x[side+4]-cells.shape[c].meanx,yc,xc))<fabs(vectorangle(EXTRAS[c].y[(side%4+5)]-cells.shape[c].meany,EXTRAS[c].x[(side%4+5)]-cells.shape[c].meanx,yc,xc)))?10:20);
    }
    else { //side==INWARDS
      if(upperzone>EPSILON)
	region[i][j]+=((1.-upperzone)*fabs(vectorangle(EXTRAS[c].y[side+4]-tissuecentrey,EXTRAS[c].x[side+4]-tissuecentrex,yc,xc))<upperzone*fabs(vectorangle(EXTRAS[c].y[(side%4+5)]-tissuecentrey,EXTRAS[c].x[(side%4+5)]-tissuecentrex,yc,xc))?10:20);
      else
	region[i][j]+=((fabs(vectorangle(EXTRAS[c].y[side+4]-cells.shape[c].meany,EXTRAS[c].x[side+4]-cells.shape[c].meanx,yc,xc))>fabs(vectorangle(EXTRAS[c].y[(side%4+5)]-cells.shape[c].meany,EXTRAS[c].x[(side%4+5)]-cells.shape[c].meanx,yc,xc)))?20:10);
    }
  }
  else { //side==CCW || side==CW
    if(side==CCW) {
      if(outerzone>EPSILON)
	//we need mean of radial component 
	region[i][j]+=((hypot((double)i-tissuecentrey,(double)j-tissuecentrex)>hypot((1.-outerzone)*EXTRAS[c].y[side+4]+outerzone*EXTRAS[c].y[(side%4+5)]-tissuecentrey,(1.-outerzone)*EXTRAS[c].x[side+4]+outerzone*EXTRAS[c].x[(side%4+5)]-tissuecentrex))?10:20);
      else
	region[i][j]+=((hypot((double)i-tissuecentrey,(double)j-tissuecentrex)>hypot(cells.shape[state[i][j]].meany-tissuecentrey,cells.shape[state[i][j]].meanx-tissuecentrex))?10:20);
    }
    else { //side==CW
      if(outerzone>EPSILON)
	region[i][j]+=((hypot((double)i-tissuecentrey,(double)j-tissuecentrex)>hypot(outerzone*EXTRAS[c].y[side+4]+(1.-outerzone)*EXTRAS[c].y[(side%4+5)]-tissuecentrey,outerzone*EXTRAS[c].x[side+4]+(1.-outerzone)*EXTRAS[c].x[(side%4+5)]-tissuecentrex))?20:10);
      else
	region[i][j]+=((hypot((double)i-tissuecentrey,(double)j-tissuecentrex)>hypot(cells.shape[state[i][j]].meany-tissuecentrey,cells.shape[state[i][j]].meanx-tissuecentrex))?20:10);
    }
  }
  if(corner[i][j])
    corner[i][j]=region[i][j];
}

void SplitZones(int c)
{
    if(polar) {
      for(side=1;side<5;side++) {
	OneCell(state,&cells,c,&SplitZonePolar);
      }
    }
    else {
      for(side=1;side<5;side++) {
	OneCell(state,&cells,c,&SplitZone);
      }
    }
}

void EducatedGuess()
{
  if(guess==1) {
    CELLS(cells,
	  if(c>specialcelltype && cells.celltype[c]!=FAKECELLWALL && cells.area[c])
	    EducatedGuessOne(c);
	  );
  }
  else if(guess==2) {
    CELLS(cells,
	  if(c>specialcelltype && cells.celltype[c]!=FAKECELLWALL && cells.area[c])
	    EducatedGuessTwo(c);
	  );
  }
  else {
    CELLS(cells,
	  if(c>specialcelltype && cells.celltype[c]!=FAKECELLWALL && cells.area[c])
	    EducatedGuessThree(c);
	  );
  }
  if(eight) {
    CELLS(cells,
	  if(c>specialcelltype && cells.celltype[c]!=FAKECELLWALL && cells.area[c])
	    SplitZones(c);
	  );
  }
  if(directedoutwards) {
    CELLS(cells,
	  if(hypot(cells.shape[c].meany-tissuecentrey,cells.shape[c].meanx-tissuecentrex)<sqrt(cells.area[c]/M_PI))
	    OneCell(state,&cells,c,&OutwardsCell);
	  );
  }
}

#define UNUSED(x) ((void)(x))

void SizeReduction()
{
  int tmpnrow,tmpncol;
  int newposx,newposy;
  int found,k;
  FILE *fp;

  tmpnrow=nrow;
  tmpncol=ncol;
  nrow/=sizereduction;
  ncol/=sizereduction;
  MemoryFree();
  smallstate=New();
  smallborder=New();
  smallcorner=New();
  smallcellstate=New();

  NeighbourNumber(10);

  PLANE(
	smallstate[i][j]=state[sizereduction*i][sizereduction*j];
	if(smallstate[i][j]) {
	  originalstate[i][j]=(sizereduction*i)*SHRT_MAX+sizereduction*j;
	  newstate[sizereduction*i][sizereduction*j]=i*SHRT_MAX+j;
	}
	);
  Boundaries(smallstate);
  /*
  ASYNPLANE(
	    if(smallstate[i][j]>specialcelltype && cells.celltype[smallstate[i][j]]!=FAKECELLWALL ) {
	      NEIGHBOURS(
			 if(smallstate[i][j]!=smallstate[i+y][j+x] && smallstate[i+y][j+x]!=CELLWALL && cells.celltype[smallstate[i+y][j+x]]!=FAKECELLWALL)
			   //TODO: this never makes it into FAKECELLWALL
			   AsynCopy(smallstate,i,j,CELLWALL);//smallstate[i][j]=CELLWALL; + keep boundary OK
			 );
	    }
	    );
  */
  CBoundaries(smallborder,smallstate);
  CELLS(cells,
        if(cells.area[c]) {
          for(k=5;k<9;k++)
	    if(EXTRAS[c].x[k]>-1) {
	      newposx=EXTRAS[c].x[k]/sizereduction;
	      newposy=EXTRAS[c].y[k]/sizereduction;
	      found=0;
	      if(smallstate[newposy][newposx]==c && smallborder[newposy][newposx] && !smallcorner[newposy][newposx]) {
		smallcorner[newposy][newposx]=k;
		found=1;
	      }
	      else
		ASYNNEIGHBOURS(
			       UNUSED(neigh);
			       if(smallstate[newindex(newposy+y,nrow)][newindex(newposx+x,ncol)]==c && smallborder[newindex(newposy+y,nrow)][newindex(newposx+x,ncol)] && !smallcorner[newindex(newposy+y,nrow)][newindex(newposx+x,ncol)]) {
				 smallcorner[newindex(newposy+y,nrow)][newindex(newposx+x,ncol)]=k;
				 found=1;
				 break;
			       }
			       );
	      if(!found) {
		WIDENEIGHBOURS(
			       if(smallstate[newindex(newposy+y,nrow)][newindex(newposx+x,ncol)]==c && smallborder[newindex(newposy+y,nrow)][newindex(newposx+x,ncol)] && !smallcorner[newindex(newposy+y,nrow)][newindex(newposx+x,ncol)]) {
				 smallcorner[newindex(newposy+y,nrow)][newindex(newposx+x,ncol)]=k;
				 found=1;
				 break;
			       }
			       );
	    
	      }
	      if(!found) 
		fprintf(stdout,"warning: no %s corner found for cell %d, correct afterwards by hand\n",polar?cornernamepolar[k]:cornername[k],c);
	    }
        }
        );
  
  snprintf(a_string,STRING,"%s_reduction%d%s",outputfilename,sizereduction,filenames[5]);
  fp=fopen(a_string,"w");
  SavePat(fp,"%d\t",smallcorner);
  fclose(fp);
  snprintf(a_string,STRING,"%s_reduction%d%s",outputfilename,sizereduction,filenames[7]);
  fp=fopen(a_string,"w");
  SavePat(fp,"%d\t",smallstate);
  fclose(fp);
  snprintf(a_string,STRING,"%s_reduction%d%s",outputfilename,sizereduction,filenames[4]);
  fp=fopen(a_string,"w");
  CSavePat(fp,&cells);
  fclose(fp);
  snprintf(a_string,STRING,"%s_reduction%d%s",outputfilename,sizereduction,".backwards");
  fp=fopen(a_string,"w");
  SavePat(fp,"%d\t",originalstate);
  fclose(fp);
  snprintf(a_string,STRING,"%s_reduction%d%s",outputfilename,sizereduction,".forwards");
  fp=fopen(a_string,"w");
  SavePat(fp,"%d\t",newstate);
  fclose(fp);
  snprintf(a_string,STRING,"cp -f %s%s %s_reduction%d%s",filename,".centers",outputfilename,sizereduction,".centers");
  system(a_string);

  OpenPNG(&png1,".");
  PLANE(
	smallcellstate[i][j]=cells.celltype[smallstate[i][j]];
	);
  PlanePNG(smallcellstate,&png1,0);
  snprintf(a_string,STRING,"mv %.5d.png %s_reduction%d%s",png1.nframes-1,outputfilename,sizereduction,filenames[3]);
  system(a_string);
  OpenPNG(&png1,".");
  CellPNG(smallstate,&cells,&png1,0);
  CPlanePNG(smallcorner,smallstate,&png1,120);
  snprintf(a_string,STRING,"mv %.5d.png %s_reduction%d%s",png1.nframes-2,outputfilename,sizereduction,filenames[0]);
  system(a_string);
  snprintf(a_string,STRING,"mv %.5d.png %s_reduction%d%s",png1.nframes-1,outputfilename,sizereduction,filenames[1]);
  system(a_string);
  
  fprintf(stdout,"Saved small version, reduced %d times\n",sizereduction);
  
  PlaneFree(smallstate);
  PlaneFree(smallborder);
  PlaneFree(smallcorner);
  
  nrow=tmpnrow;
  ncol=tmpncol;
  MemoryFree();
  
  snprintf(a_string,STRING,"%s --noreduction %s_reduction%d%s",programname,outputfilename,sizereduction,filenames[7]);
  system(a_string);
  exit(1);
  //misschien ooit uitbreiding om direct door te gaan 
}

void TheEnd()
{  
  FILE *fp;
  int i,c,k;

  snprintf(a_string,STRING,"%s_length.dat",outputfilename);
  if((fp=fopen(a_string,"r"))!=NULL) {
    while(fscanf(fp,"%d",&c)!=EOF)
      for(k=1;k<9;k++)
	fscanf(fp,"%d",&EXTRAS[c].length[k]);
    fclose(fp);
    CELLS(cells,
	  for(k=0;k<9;k++)
	    EXTRAS[c].newlength[k]=0;
	  );
    CPLANE(state,
	   if(corner[i][j])
	     EXTRAS[state[i][j]].newlength[corner[i][j]]++;
	   );
    CELLS(cells,
	  for(k=1;k<5;k++)
	    EXTRAS[c].newlength[k]+=2; //add two for the two cornerpoints
	  EXTRAS[c].newlength[UO]=EXTRAS[c].newlength[UPPER]+EXTRAS[c].newlength[OUTER];//for the cornerpoints we use the sum of the two newlengths
	  EXTRAS[c].newlength[UI]=EXTRAS[c].newlength[UPPER]+EXTRAS[c].newlength[INNER];
	  EXTRAS[c].newlength[LI]=EXTRAS[c].newlength[LOWER]+EXTRAS[c].newlength[INNER];
	  EXTRAS[c].newlength[LO]=EXTRAS[c].newlength[LOWER]+EXTRAS[c].newlength[OUTER];
	  );
    snprintf(a_string,STRING,"%s_scaling.dat",outputfilename);
    fp=fopen(a_string,"w");
    CELLS(cells,
	  if(c>specialcelltype && cells.celltype[c]!=FAKECELLWALL && cells.area[c]) {
	    fprintf(fp,"%d",c);
	    for(k=1;k<9;k++) {
	      if(EXTRAS[c].newlength[k])
		fprintf(fp,"\t%lf",(double)EXTRAS[c].length[k]/(double)EXTRAS[c].newlength[k]);
	      else
		fprintf(fp,"\t1.");
	    }
	    fprintf(fp,"\n");
	  }
	  );
    fclose(fp);
  }

  if(file_changed[0]) {
    file_changed[0]=0;
    OpenPNG(&png1,".");
    CellPNG(state,&cells,&png1,0);
    snprintf(a_string,STRING,"mv %.5d.png %s%s",png1.nframes-1,outputfilename,filenames[0]);
    system(a_string);
  }

  if(file_changed[1]) {
    //file_changed[1]=0;
    OpenPNG(&png1,".");
    CPlanePNG(corner,state,&png1,120);
    snprintf(a_string,STRING,"mv %.5d.png %s%s",png1.nframes-1,outputfilename,filenames[1]);
    system(a_string);
  }
  if(file_changed[2]) {
    //file_changed[2]=0;
    OpenPNG(&png1,".");
    CPlanePNG(region,state,&png1,120);
    snprintf(a_string,STRING,"mv %.5d.png %s%s",png1.nframes-1,outputfilename,filenames[2]);
    system(a_string);
  }
  if(file_changed[3]) {
    file_changed[3]=0;
    OpenPNG(&png1,".");
    PLANE(
	  cellstate[i][j]=cells.celltype[state[i][j]];
	  );
    PlanePNG(cellstate,&png1,0);
    snprintf(a_string,STRING,"mv %.5d.png %s%s",png1.nframes-1,outputfilename,filenames[3]);
    system(a_string);
  }

  if(file_changed[4]) {
    file_changed[4]=0;
    snprintf(a_string,STRING,"%s%s",outputfilename,filenames[4]);
    fp=fopen(a_string,"w");
    CSavePat(fp,&cells);
    fclose(fp);
  }
  if(file_changed[5]) {
    //file_changed[5]=0;
    snprintf(a_string,STRING,"%s%s",outputfilename,filenames[5]);
    fp=fopen(a_string,"w");
    SavePat(fp,"%d\t",corner);
    fclose(fp);
  }
  if(file_changed[6]) {
    //file_changed[6]=0;
    snprintf(a_string,STRING,"%s%s",outputfilename,filenames[6]);
    fp=fopen(a_string,"w");
    SavePat(fp,"%d\t",region);
    fclose(fp);
  }
  if(file_changed[7]) {
    file_changed[7]=0;
    snprintf(a_string,STRING,"%s%s",outputfilename,filenames[7]);
    fp=fopen(a_string,"w");
    SavePat(fp,"%d\t",state);
    fclose(fp);
  }

  if((noreduction || nobending) && usenewname) {
    if(noreduction) {
      snprintf(a_string,STRING,"mv %s%s %s%s",filename,".backwards",outputfilename,".backwards");
      system(a_string);
      snprintf(a_string,STRING,"mv %s%s %s%s",filename,".forwards",outputfilename,".forwards");
      system(a_string);
    }
    for(i=0;i<MAXFILENAMES;i++) {
      snprintf(a_string,STRING,"rm -f %s%s",filename,filenames[i]);
      system(a_string);
    }
    snprintf(a_string,STRING,"rm -f %s%s",filename,".forwards");
    system(a_string);
    snprintf(a_string,STRING,"rm -f %s%s",filename,".backwards");
    system(a_string);
    snprintf(a_string,STRING,"mv %s%s %s%s",filename,".centers",outputfilename,".centers");
    system(a_string);
    snprintf(a_string,STRING,"test -f %s%s",filename,".oldstate");
    if(!system(a_string)) {//does exist
      snprintf(a_string,STRING,"mv %s%s %s%s",filename,".oldstate",outputfilename,".oldstate");
      system(a_string);
    }
  }
}

void FillCornerAlternative(int i, int j)
{
  int k;
  int minccwk=0,mincwk=0;
  double angleccw[9],anglecw[9];

  for(k=5;k<9;k++) {
    if(EXTRAS[state[i][j]].x[k]!=-1) {
      angleccw[k]=vectorangle((double)i-cells.shape[state[i][j]].meany,(double)j-cells.shape[state[i][j]].meanx,EXTRAS[state[i][j]].y[k]-cells.shape[state[i][j]].meany,EXTRAS[state[i][j]].x[k]-cells.shape[state[i][j]].meanx);
      if(angleccw[k]<0)
	angleccw[k]+=2.*M_PI;
      anglecw[k]=angleccw[k]-2.*M_PI;
      if(!minccwk) {
	minccwk=k;
	mincwk=k;
      }
      else {
	if(angleccw[k]<angleccw[minccwk])
	  minccwk=k;
	if(anglecw[k]>anglecw[mincwk])
	  mincwk=k;
      }
    }
  }

  if(region[i][j]%10==side) //completely reset this region
    region[i][j]=0;
  if(corner[i][j]>4 && corner[i][j]<9)
    region[i][j]=corner[i][j];//keep the cornerpixels 
  
  if((corner[i][j]<5 || corner[i][j]>8) && ((minccwk==side+4 && mincwk==(side%4)+5) || (mincwk==side+4 && minccwk==(side%4)+5))) {
    region[i][j]=side;
    if(border[i][j] && (corner[i][j]<5 || corner[i][j]>8)) //could second requirement still happen?
      corner[i][j]=side;
  }
}

void FillCorner(int i,int j)
{
  int c;
  double xc,yc;
  //double lc;

  c=state[i][j];
  xc=(double)j-cells.shape[c].meanx;
  yc=(double)i-cells.shape[c].meany;
  //lc=max(1,hypot(xc,yc));
  if(region[i][j]%10==side) //completely reset this region
    region[i][j]=0;
  if(corner[i][j]>4 && corner[i][j]<9)
    region[i][j]=corner[i][j];//keep the cornerpixels 

  //start at one corner, and move either to the other corner or to the pixel under consideration
  //are both to the left or to the right?
  //then start at the other corner and do the same.
  //if both times the pixel under consideration and the other corner are in the same direction
  //then the pixel under consideration is in between the two corners
  if((corner[i][j]<5 || corner[i][j]>8) && (xx2*yy1-yy2*xx1)*(xx2*yc-yy2*xc)>=0. && //vector product to
					     //determine z-component,
					     //and dot prod. to see if
					     //in same direction.
     (xx1*yy2-yy1*xx2)*(xx1*yc-yy1*xc)>=0.) {
    region[i][j]=side;
    if(border[i][j] && (corner[i][j]<5 || corner[i][j]>8)) //second requirement could
				       //happen when all corner in one half
      corner[i][j]=side;
  }
}

void FillCornerAlternative3(int i,int j)
{
  int c;
  double xc,yc;
  //double lc;
  
  c=state[i][j];
  xc=(double)j-cells.shape[c].meanx;
  yc=(double)i-cells.shape[c].meany;
  //lc=max(1,hypot(xc,yc));
  if(region[i][j]%10==side) //completely reset this region
    region[i][j]=0;
  if(corner[i][j]>4 && corner[i][j]<9)
    region[i][j]=corner[i][j];//keep the cornerpixels 

  if(!border[i][j] && (corner[i][j]<5 || corner[i][j]>8) && (xx2*yy1-yy2*xx1)*(xx2*yc-yy2*xc)>0. && //vector product to
					     //determine z-component,
					     //and dot prod. to see if
					     //in same direction.
     (xx1*yy2-yy1*xx2)*(xx1*yc-yy1*xc)>0.)
    region[i][j]=side;

  //OUTER has preference over LOWER, if wanted differently, uncomment this
  //if(border[i][j] && corner[i][j]<5 && (side!=OUTER || corner[i][j]!=LOWER)) {
  if(border[i][j] && (corner[i][j]<5 || corner[i][j]>8)) {
    if((side==UPPER && state[i][j]!=state[i+1][j]) ||
       (side==LOWER && state[i][j]!=state[i-1][j]) ||
       (side==INNER && state[i][j]!=state[i][j+(j<ncol/2+1?1:-1)]) ||
       (side==OUTER && state[i][j]!=state[i][j-(j<ncol/2+1?1:-1)])) {
      region[i][j]=side;
      corner[i][j]=side;
    }
  }
  if(!border[i][j] && !region[i][j])
    region[i][j]=OUTER;
}

void NoQuarter(int c,int k,int x1,int y1,int x2,int y2) //find the zone indicated by side
{
  side=k-4;
  xx1=(double)x1-cells.shape[c].meanx;
  xx2=(double)x2-cells.shape[c].meanx;
  yy1=(double)y1-cells.shape[c].meany;
  yy2=(double)y2-cells.shape[c].meany;

  l1=max(1,hypot(xx1,yy1));
  l2=max(1,hypot(xx2,yy2));
  EXTRAS[c].angle[side]=acos((xx1*xx2+yy1*yy2)/(l1*l2));//angle actually not needed for calculating regions
                                                        //we use vector product only
  if(polar)
    OneCell(state,&cells,c,FillCornerAlternative);
  else if(guess==3 && (cells.celltype[c]==VASCULAR || cells.celltype[c]==PERICYCLE))
    OneCell(state,&cells,c,FillCornerAlternative3);
  else
    OneCell(state,&cells,c,FillCorner);
}

void Key(int key,int mouse_i,int mouse_j)
{
  int k,tmpx,tmpy;
  int eerste,laatste;
  int found;
  FILE *fp;

  switch(key) {
  case 'w':
    if(state[mouse_i][mouse_j]==CELLWALL || cells.celltype[state[mouse_i][mouse_j]]==FAKECELLWALL) {
      if(state[mouse_i][mouse_j]==CELLWALL) {
	found=0;
	CELLS(cells,
	      if(cells.celltype[c]==FAKECELLWALL) {
		found=c;
		break;
	      }
	      );
	if(!found) {
	  CELLS(cells,
		if(c>CELLWALL && !cells.area[c]) {
		  found=c;
		  break;
		}
		);
	}
	if(found) {
	  state[mouse_i][mouse_j]=found;
	  cells.celltype[found]=FAKECELLWALL;
	  fprintf(stdout,"(%d,%d) changed into FAKECELLWALL\n",mouse_i,mouse_j);
	  UpdateCFill(state,&cells);
	}
	else 
	  fprintf(stdout,"operation not permitted, increase maxcells\n");
      }
      else {
	state[mouse_i][mouse_j]=CELLWALL;
	fprintf(stdout,"(%d,%d) changed into CELLWALL\n",mouse_i,mouse_j);
	UpdateCFill(state,&cells);
      }
    }
    else
      fprintf(stdout,"(%d,%d) is not (FAKE) CELLWALL\n",mouse_i,mouse_j);
    break;
  case 'c':
    upper=!upper;
    if(polar) {
      if(upper)
	snprintf(winsecond,STRING,"Counterclockwise Corners");
      else
	snprintf(winsecond,STRING,"Clockwise Corners");
    }
    else {
      if(upper)
	snprintf(winsecond,STRING,"Upper Corners");
      else
	snprintf(winsecond,STRING,"Lower Corners");
    }
    snprintf(a_string,STRING,"%s - %s",winsecond,winfirst);
    ChangeWindowName(a_string);
    fprintf(stdout,"Now looking for %s\n",winsecond);
    break;
  case 's':
    whattoshow=(whattoshow+1)%3;
    if(whattoshow==0) {
      snprintf(winfirst,STRING,"%s",tissuename[tissuetype]);
      snprintf(a_string,STRING,"%s - %s",winsecond,winfirst);
      ChangeWindowName(a_string);
    }
    else if(whattoshow==1) {
      snprintf(winfirst,STRING,"Corners");
      snprintf(a_string,STRING,"%s - %s",winsecond,winfirst);
      ChangeWindowName(a_string);
    }
    else if(whattoshow==2) {
      snprintf(winfirst,STRING,"Regions");
      snprintf(a_string,STRING,"%s - %s",winsecond,winfirst);
      ChangeWindowName(a_string);
    }
    break;
  case 'i':
    if(currentcell>specialcelltype && cells.celltype[currentcell]!=FAKECELLWALL) {
      for(k=5;k<9;k++) {
	if(EXTRAS[currentcell].x[k]>-1 && corner[EXTRAS[currentcell].y[k]][EXTRAS[currentcell].x[k]]==k) { //this is not the first time we find this corner
	  corner[EXTRAS[currentcell].y[k]][EXTRAS[currentcell].x[k]]=0; //remove old one 
	  region[EXTRAS[currentcell].y[k]][EXTRAS[currentcell].x[k]]=0; //remove old one 
	}
	EXTRAS[currentcell].oldx[k]=EXTRAS[currentcell].x[k];
	EXTRAS[currentcell].oldy[k]=EXTRAS[currentcell].y[k];
      }
      for(k=5;k<9;k++) {
	EXTRAS[currentcell].x[k]=EXTRAS[currentcell].oldx[(2*((k+1)/2)-(k+1)%2)];
	EXTRAS[currentcell].y[k]=EXTRAS[currentcell].oldy[(2*((k+1)/2)-(k+1)%2)];
	corner[EXTRAS[currentcell].y[k]][EXTRAS[currentcell].x[k]]=k;
      }
      for(k=5;k<9;k++)
	if(EXTRAS[currentcell].x[k]>-1 && (EXTRAS[currentcell].x[((k-4)%4+5)]>-1))
	  NoQuarter(currentcell,k,EXTRAS[currentcell].x[k],EXTRAS[currentcell].y[k],EXTRAS[currentcell].x[((k-4)%4+5)],EXTRAS[currentcell].y[((k-4)%4+5)]);
    }
    break;
  case 'F':
    if(currentcell>specialcelltype && cells.celltype[currentcell]!=FAKECELLWALL) {
      for(k=5;k<9;k++) {
	if(EXTRAS[currentcell].x[k]>-1 && corner[EXTRAS[currentcell].y[k]][EXTRAS[currentcell].x[k]]==k) { //this is not the first time we find this corner
	  corner[EXTRAS[currentcell].y[k]][EXTRAS[currentcell].x[k]]=0; //remove old one 
	  region[EXTRAS[currentcell].y[k]][EXTRAS[currentcell].x[k]]=0; //remove old one 
	}
	EXTRAS[currentcell].oldx[k]=EXTRAS[currentcell].x[k];
	EXTRAS[currentcell].oldy[k]=EXTRAS[currentcell].y[k];
      }
      for(k=5;k<9;k++) {
	EXTRAS[currentcell].x[k]=EXTRAS[currentcell].oldx[(13-k)];
	EXTRAS[currentcell].y[k]=EXTRAS[currentcell].oldy[(13-k)];
	corner[EXTRAS[currentcell].y[k]][EXTRAS[currentcell].x[k]]=k;
      }
      for(k=5;k<9;k++)
	if(EXTRAS[currentcell].x[k]>-1 && (EXTRAS[currentcell].x[((k-4)%4+5)]>-1))
	  NoQuarter(currentcell,k,EXTRAS[currentcell].x[k],EXTRAS[currentcell].y[k],EXTRAS[currentcell].x[((k-4)%4+5)],EXTRAS[currentcell].y[((k-4)%4+5)]);
    }
    break;
  case 'x':
    if(currentcell>specialcelltype && cells.celltype[currentcell]!=FAKECELLWALL) {
      for(k=5;k<9;k++) {
	if(EXTRAS[currentcell].x[k]>-1 && corner[EXTRAS[currentcell].y[k]][EXTRAS[currentcell].x[k]]==k) { //this is not the first time we find this corner
	  corner[EXTRAS[currentcell].y[k]][EXTRAS[currentcell].x[k]]=0; //remove old one
	  region[EXTRAS[currentcell].y[k]][EXTRAS[currentcell].x[k]]=0; //remove old one
	}
	EXTRAS[currentcell].oldx[k]=EXTRAS[currentcell].x[k];
	EXTRAS[currentcell].oldy[k]=EXTRAS[currentcell].y[k];
      }
      for(k=5;k<9;k++) {
	EXTRAS[currentcell].x[k]=EXTRAS[currentcell].oldx[(5+(k)%4)];
	EXTRAS[currentcell].y[k]=EXTRAS[currentcell].oldy[(5+(k)%4)];
	corner[EXTRAS[currentcell].y[k]][EXTRAS[currentcell].x[k]]=k;
      }
      for(k=5;k<9;k++)
	if(EXTRAS[currentcell].x[k]>-1 && (EXTRAS[currentcell].x[((k-4)%4+5)]>-1))
	  NoQuarter(currentcell,k,EXTRAS[currentcell].x[k],EXTRAS[currentcell].y[k],EXTRAS[currentcell].x[((k-4)%4+5)],EXTRAS[currentcell].y[((k-4)%4+5)]);
    }
    break;
  case 'o':
    overwrite=!overwrite;
    fprintf(stdout,"Overwriting mode is now %s!\n",(overwrite?"on":"off"));
    break;
  case 'g':
    EducatedGuessOne(currentcell);
    if(eight) {
      CELLS(cells,
	    if(c>specialcelltype && cells.celltype[c]!=FAKECELLWALL && cells.area[c])
	      SplitZones(c);
	    );
    }
    break;
  case 'G':
    EducatedGuessTwo(currentcell);
    if(eight) {
      CELLS(cells,
	    if(c>specialcelltype && cells.celltype[c]!=FAKECELLWALL && cells.area[c])
	      SplitZones(c);
	    );
    }
    break;
  case 't':
    EducatedGuessThree(currentcell);
    if(eight) {
      CELLS(cells,
	    if(c>specialcelltype && cells.celltype[c]!=FAKECELLWALL && cells.area[c])
	      SplitZones(c);
	    );
    }
    break;
  case 'H':
    symmetry=(symmetry+1)%MAXSYMMETRY;
    fprintf(stdout,"%s\n",symmetryname[symmetry]);
    EducatedGuess();
    break;
  case 'h':
    printf(
	   "    c                      toggle upper/lower corners for corner selection\n"
	   "    s                      change what to show (cells,corners,regions)\n"
	   "    i                      invert outer/inner corners\n"
	   "    F                      flip upper/lower corners\n"
	   "    x                      rotate regions\n"
	   "    g                      make educated guess for selected cell, based on cell shape\n"
	   "    G                      make educated guess for selected cell, based on extremes\n"
	   "    t                      make educated guess for selected cell, based on root shape\n"
	   "    o                      toggle overwrite mode for educated guesses\n"
	   "    z                      undo/redo last change\n"
	   "    w                      toggle cellwall/fake cellwall\n"
	   "    k                      save files without quitting\n"
	   "    Q                      save files and quit program\n"
	   "\n"
	   "Mouse:\n"
	   "    left                   select inner corner\n"
	   "    right                  select outer corner\n"
	   "\n");
    break;
  case 'z':
    if(lastcell>specialcelltype && cells.celltype[lastcell]!=FAKECELLWALL) {
      if(!lastk) {
	eerste=5;
	laatste=8;
      }
      else {
	eerste=lastk;
	laatste=lastk;
      }
      for(k=eerste;k<=laatste;k++) {
	if(EXTRAS[lastcell].x[k]>-1 && corner[EXTRAS[lastcell].y[k]][EXTRAS[lastcell].x[k]]==k) { //don't do it if we just removed it
	  corner[EXTRAS[lastcell].y[k]][EXTRAS[lastcell].x[k]]=0; //remove old one 
	  region[EXTRAS[lastcell].y[k]][EXTRAS[lastcell].x[k]]=0;
	}
	tmpx=EXTRAS[lastcell].x[k];
	tmpy=EXTRAS[lastcell].y[k];
	EXTRAS[lastcell].x[k]=EXTRAS[lastcell].oldx[k];
	EXTRAS[lastcell].y[k]=EXTRAS[lastcell].oldy[k];
	EXTRAS[lastcell].oldx[k]=tmpx;
	EXTRAS[lastcell].oldy[k]=tmpy;
	if(EXTRAS[lastcell].x[k]>-1) //this was not the first time we had found this corner
	  corner[EXTRAS[lastcell].y[k]][EXTRAS[lastcell].x[k]]=k;
	else
	  OneCell(state,&cells,lastcell,&EmptyCell);
      }
      for(k=5;k<9;k++)
	if(EXTRAS[lastcell].x[k]>-1 && (EXTRAS[lastcell].x[((k-4)%4+5)]>-1))
	  NoQuarter(lastcell,k,EXTRAS[lastcell].x[k],EXTRAS[lastcell].y[k],EXTRAS[lastcell].x[((k-4)%4+5)],EXTRAS[lastcell].y[((k-4)%4+5)]);
    }
    break;
  case 'k':
    TheEnd();
    break;
  case 'Q':
    TheEnd();
   //quick hack for wallfreestate, wallfreecells
    if(cellwall==-1 && regular)
      WallFreeCellwall();
    else {
      cellwall=0;
      regular=1;
      ReduceCellwall();
    }
    snprintf(a_string,STRING,"%s%s",outputfilename,".wallfreestate");
    fp=fopen(a_string,"w");
    SavePat(fp,"%d\t",state);
    fclose(fp);
    snprintf(a_string,STRING,"%s%s",outputfilename,".wallfreecells");
    fp=fopen(a_string,"w");
    CSavePat(fp,&cells);
    fclose(fp);
    OpenPNG(&png1,".");
    CellPNG(state,&cells,&png1,0);
    snprintf(a_string,STRING,"mv %.5d.png %s%s",png1.nframes-1,outputfilename,"_wallfreecells.png");
    system(a_string);
    OpenPNG(&png1,".");
    PLANE(
	  cellstate[i][j]=cells.celltype[state[i][j]];
	  );
    ncol-=1;
    PlanePNG(cellstate,&png1,0);
    ncol+=1;
    snprintf(a_string,STRING,"mv %.5d.png %s%s",png1.nframes-1,outputfilename,"_wallfreestate.png");
    system(a_string);
    exit(0);
    break;
  default:
    break;
  }
}

void Show()
{
  int showit;

  showit=whattoshow;

  switch(showit) {
  case 0:
    CellDisplay(state,&cells,0,0,0);
    break;
  case 1:
    CPlaneDisplay(corner,state,0,0,120);
    break;
  case 2:
    CPlaneDisplay(region,state,0,0,120);
    break;
  default:
    break;
  }
}

void FindCornerAfterBend(int i,int j)
{
  int left=0,right=0,centre=0;
  int neighx,neighy;

  if(EXTRAS[state[i][j]].x[requiredk]<0 && border[i][j]) {
    WIDENEIGHBOURS(
		   neighy=newindex(i+y,nrow);
		   neighx=newindex(j+x,ncol);
		   if(state[neighy][neighx]==state[i][j] && border[neighy][neighx]) {
		     if(corner[neighy][neighx]==requiredk)
		       centre++;
		     else if(corner[neighy][neighx]==clockwise[requiredk])
		       right++;
		     else if(corner[neighy][neighx]==counterclockwise[requiredk])
		       left++;
		   }
		   );
    if(centre || (right && left)) {
      EXTRAS[state[i][j]].y[requiredk]=i;
      EXTRAS[state[i][j]].x[requiredk]=j;
      corner[i][j]=requiredk;
    }
  }
}

void FindCornersAfterBend()
{
  int casesleft,oldcasesleft;
  int neighy,neighx;
  int k,m;
  
  CELLS(cells,
	for(k=0;k<9;k++) {
	  EXTRAS[c].x[k]=-1;
	  EXTRAS[c].y[k]=-1;
	  EXTRAS[c].count[k]=0;
	}
	);
  PLANE(
	if(state[i][j]>specialcelltype && cells.celltype[state[i][j]]!=FAKECELLWALL) {
	  EXTRAS[state[i][j]].count[corner[i][j]]++;
	  if(EXTRAS[state[i][j]].x[corner[i][j]]<0 && border[i][j]) {
	    EXTRAS[state[i][j]].x[corner[i][j]]=j;
	    EXTRAS[state[i][j]].y[corner[i][j]]=i;
	  }
	}
	);
  
  for(k=5;k<9;k++) {
    requiredk=k;
    m=0;
    do {
      m++;
      NeighbourNumber(m);
      if(m>1)
	fprintf(stdout,"working on recovering corners; %s, level %d\n",polar?cornernamepolar[k]:cornername[k],m);
      if(m>100)
	break;
      casesleft=0;
      CELLS(cells,
	    if(c>specialcelltype && cells.celltype[c]!=FAKECELLWALL && cells.area[c] && EXTRAS[c].x[k]<0) {
	      if(EXTRAS[c].count[k] || (EXTRAS[c].count[clockwise[k]] && EXTRAS[c].count[counterclockwise[k]])) { //we have a resolvable case
		//printf("%d %d %d %d %d\n",c,k,EXTRAS[c].count[k],EXTRAS[c].count[clockwise[k]],EXTRAS[c].count[counterclockwise[k]]);
		OneCell(state,&cells,c,&FindCornerAfterBend);
		if(EXTRAS[c].x[k]<0)
		  casesleft++;
	      }
	      /*
		else 
		printf("%d %d %d %d %d\n",c,k,EXTRAS[c].count[k],EXTRAS[c].count[clockwise[k]],EXTRAS[c].count[counterclockwise[k]]);
	      */
	    }
	    );
    } while(casesleft);
  }

  casesleft=0;
  PLANE(
	if(border[i][j]) {
	  if(((corner[i][j]>4 && corner[i][j]<9) && !(EXTRAS[state[i][j]].x[corner[i][j]]==j && EXTRAS[state[i][j]].y[corner[i][j]]==i)) || (state[i][j]>CELLWALL && cells.celltype[state[i][j]]!=FAKECELLWALL && !corner[i][j])) { //not the `real' corner
	    casesleft++;
	    corner[i][j]=0;
	  }
	  region[i][j]=corner[i][j];
	}
	else {
	  region[i][j]=corner[i][j];
	  corner[i][j]=0;
	}
	);

  m=0;
  oldcasesleft=casesleft;
  while(m<=100 && casesleft) {
    if(oldcasesleft==casesleft)
      m++;
    oldcasesleft=casesleft;
    NeighbourNumber(m);
    PLANE(
	  if(border[i][j] && state[i][j]>CELLWALL && cells.celltype[state[i][j]]!=FAKECELLWALL && !corner[i][j]) {
	    WIDENEIGHBOURS(
			   neighy=newindex(i+y,nrow);
			   neighx=newindex(j+x,ncol);
			   if(state[i][j]==state[neighy][neighx] && border[neighy][neighx] && corner[neighy][neighx] && (corner[neighy][neighx]<5 || corner[neighy][neighx]>8)) {
			     corner[i][j]=corner[neighy][neighx];
			     region[i][j]=corner[i][j];
			     casesleft--;
			     break;
			   }
			   );
	  }
	  );
  }
  if(casesleft)
    fprintf(stderr,"warning: %d edge%s could not be resolved\n",casesleft,casesleft>1?"s":"");

  CELLS(cells,
	if(c>specialcelltype && cells.celltype[c]!=FAKECELLWALL && cells.area[c])
	  for(k=5;k<9;k++)
	    if(EXTRAS[c].x[k]<0)  
	      fprintf(stdout,"warning: lost corner %s of cell %d, correct afterwards by hand\n",polar?cornernamepolar[k]:cornername[k],c);
	);
}

void SetUp(int ac,char *av[])
{
  int k,stoppen=0;
  int i;
  int tmpnrow,tmpncol;
  TYPE **tmpplane;
  
  boundary=FIXED;//maybe in the future also different boundary?
  switch (tissuetype) {
  case ROOT:
    totaltypes=TOTALROOTTYPES;
    break;
  case EMBRYO:
    totaltypes=TOTALEMBRYOTYPES;
    break;
  case FRUIT:
    totaltypes=TOTALFRUITTYPES;
    break;
  default:
    break;
  }
  
  for(i=0;i<MAXFILENAMES;i++) {
    if(!strncmp(filenames[i],&filename[strlen(filename)-strlen(filenames[i])],strlen(filenames[i]))) {//because we only want to compare the ending.
      filetype=i;
      filename[strlen(filename)-strlen(filenames[i])]='\0';//keep the front piece
      fprintf(stdout,"file %s of filetype %d (%s)\n",filename,filetype,filenames[filetype]);
      if(usenewname) {
	strncpy(outputfilename,newfilename,STRING);
	fprintf(stdout,"output prefix will be %s\n",outputfilename);
      }
      else
	strncpy(outputfilename,filename,STRING);
      break;
    }
  }
  if(filetype<0)
    show_help(av[0],1);

  for(i=0;i<MAXFILENAMES;i++) {
    snprintf(a_string,STRING,"test -f %s%s",filename,filenames[i]);
    if(system(a_string)) {//does not exist
      file_exists[i]=0;
      file_changed[i]=1;
    }
    else {
      file_exists[i]=1;
      file_changed[i]=1;
    }
  }
  
  if(filetype==0) {
    readpng=USECELLPNG;
    file_changed[0]=0;
  }
  else if(filetype==1 || filetype==2) {
    if(file_exists[0]) {
      readpng=USECELLPNG;
      file_changed[0]=0;
    }
    else if(file_exists[3]) {
      readpng=USESTATEPNG;
      file_changed[3]=0;
    }
    else if(file_exists[4] && file_exists[7]) {
      readpng=USESTATE;
      file_changed[4]=0;
      file_changed[7]=0;
    }
  }
  else if(filetype==3) {
    readpng=USESTATEPNG;
    file_changed[3]=0;
  }
  else if(file_exists[4] && file_exists[7]) {
    readpng=USESTATE;
    file_changed[4]=0;
    file_changed[7]=0;
  }
  if(!readpng)
    show_help(av[0],1);

  if(readpng==USECELLPNG || readpng==USESTATEPNG) {
    if(readpng==USECELLPNG)
      snprintf(a_string,STRING,"%s%s",filename,filenames[0]);
    if(readpng==USESTATEPNG)
      snprintf(a_string,STRING,"%s%s",filename,filenames[3]);
    if(ReadSizePNG(&nrow,&ncol,a_string)) {
      fprintf(stdout,"the file %s is not a vadid png file\n exitting now!\n",a_string);
      exit(EXIT_FAILURE);
    }
  }
  else {
    snprintf(a_string,STRING,"%s%s",filename,filenames[7]);
    if(ReadSize(&nrow,&ncol,a_string)) {
      fprintf(stdout,"the file %s is not a vadid state file\n exitting now!\n",a_string);
      exit(EXIT_FAILURE);
    }
  }
  if(readpng==USECELLPNG) {
    nrow/=2;
    ncol/=2;
  }

  CNew(&cells,maxcells,totaltypes); //create cell structure
  if((cells.extras=(void *)calloc((size_t)cells.maxcells,sizeof(Extras)))==NULL) {
    fprintf(stderr,"error in memory allocation\n");
    exit(EXIT_FAILURE);
  }
  CELLS(cells,
	for(k=0;k<9;k++) {
	  EXTRAS[c].x[k]=-1;
	  EXTRAS[c].y[k]=-1;
	  EXTRAS[c].count[k]=0;
	}
	);
  
  specialcelltype=CELLWALL;
  cells.celltype[CELLWALL]=CELLWALL;

  ColorTable(MEDIUM,MEDIUM,WHITE);
  ColorTable(CELLWALL,CELLWALL,BLACK);

  if(tissuetype==ROOT) {
	  
	  ColorTable(MEDIUM,MEDIUM,WHITE);
	  ColorTable(CELLWALL,CELLWALL,BLACK);
	  ColorTable(VASCULAR,VASCULAR,RED);
	  ColorName(PERICYCLE,PERICYCLE,"dark orange");// 255,140,0
	  ColorTable(ENDODERMIS,ENDODERMIS,YELLOW);
	  ColorName(CORTEX,CORTEX,"forest green");
	  ColorTable(EPIDERMIS,EPIDERMIS,BLUE);
	  ColorRGB(LRC,190,190,190);//grey
	  ColorRGB(COLUMELLAROOT,0,255,255);//light  blue
	  ColorName(CORTEXENDO_I,CORTEXENDO_I,"dark green");
	  ColorRGB(LRCEPI_I,127,0,177);// purple
	  ColorRGB(COL_I,173,216,230);//light blue
	  ColorRGB(QCROOT,100,100,100);//dove gray
	  ColorRGB(SPECIALVASCULAR,255,0,183);//shocking pink
	  ColorRGB(VASCULAR_D,238,64,0);// orange red 2
	  ColorRGB(PERICYCLE_D,255,153,18);//cadmium yellow
	  ColorRGB(ENDODERMIS_D,238,201,0);//gold 2	
	  ColorRGB(CORTEX_D,60,179,113);//medium sea green 1
	  ColorRGB(EPIDERMIS_D,42,82,190);//cerulean blue
	  ColorRGB(VASCULAR_TZ,255,105,97);//pastel red
	  ColorRGB(PERICYCLE_TZ,253,198,137);//pastel yellow orange
	  ColorRGB(ENDODERMIS_TZ,253,253,150);//pastel yellow
	  ColorRGB(CORTEX_TZ,144,238,144);//pale green 2
	  ColorRGB(EPIDERMIS_TZ,127,127,255);//blue/purple
	  ColorRGB(LRC_TZ,219,215,210);//timberwolf
	  ColorRGB(VASCULAR_EZ,255,69,0);//orangered 1
	  ColorRGB(PERICYCLE_EZ,251,175,93);//light yellow orange
	  ColorRGB(ENDODERMIS_EZ, 255,215,0);//gold 1
	  ColorRGB(CORTEX_EZ,124,205,124);//palegreen3 
	  ColorRGB(EPIDERMIS_EZ,65,105,225);//royal blue
	  ColorRGB(DEADCELLS,158,110,72);//light brown
	  //ColorRGB(FAKECELLWALL,20,20,21);
	  ColorRGB(FAKECELLWALL,200,200,210);//light grey
	  ColorRGB(FAKECELLS,30,30,31);
	  
  }
  else if (tissuetype==EMBRYO) {
    ColorTable(VASCULARROOT,VASCULARROOT,RED);
    ColorRGB(VASCULARSHOOT,255,0,192);
    ColorTable(ENDODERMISROOT,ENDODERMISROOT,YELLOW);
    ColorName(ENDODERMISSHOOT,ENDODERMISSHOOT,"orange");
    ColorRGB(CORTEXROOT,34,139,34);
    ColorTable(CORTEXSHOOT,CORTEXSHOOT,GREEN);
    ColorTable(EPIDERMISROOT,EPIDERMISROOT,BLUE);
    ColorName(EPIDERMISSHOOT,EPIDERMISSHOOT,"light steel blue");
    ColorTable(QCEMBRYO,QCEMBRYO,GREY);
    ColorName(PRIMORDIUM,PRIMORDIUM,"gray");
    ColorTable(COLUMELLAEMBRYO,COLUMELLAEMBRYO,CYAN);
    ColorRGB(CENTRALCELL,127,0,177);
  }

  else { //if(tissuetype==FRUIT)
    ColorTable(VALVE,VALVE, GREEN);
    ColorTable(REPLUM,REPLUM,BLUE);
    ColorName(LIGLAYER,LIGLAYER,"dark orange");
    ColorTable(SEPLAYER,SEPLAYER,YELLOW);
  }
  ColorTable(120,120,WHITE);
  ColorHSV(121,0.25,1.,1.);
  ColorHSV(122,0.50,1.,1.);
  ColorHSV(123,0.75,1.,1.);
  ColorHSV(124,0.,1.,1.);
  ColorHSV(125,0.125,1.,1.);
  ColorHSV(126,0.375,1.,1.);
  ColorHSV(127,0.625,1.,1.);
  ColorHSV(128,0.875,1.,1.);
  ColorHSV(131,0.1875,1.,1.);
  ColorHSV(132,0.4375,1.,1.);
  ColorHSV(133,0.6875,1.,1.);
  ColorHSV(134,0.9375,1.,1.);
  ColorHSV(141,0.3125,1.,1.);
  ColorHSV(142,0.5625,1.,1.);
  ColorHSV(143,0.8125,1.,1.);
  ColorHSV(144,0.0625,1.,1.);
  ColorTable(130,130,BLACK);
  ColorName(200,201,"lime green","dark red"); //first,center

  //set up centre of mass
  tissuecentrey = (double)nrow/2.;
  tissuecentrex = (double)ncol/2.;
 
  if(trim || cellwall!=-1 || specialcellrows!=-1 || usenewname || noreduction || nobending)
    for(i=0;i<MAXFILENAMES;i++)
      file_changed[i]=1;
  
  if(trim || bending) {
    state = New();
    if(readpng==USECELLPNG || readpng==USESTATEPNG) {
      if(readpng==USECELLPNG) {
	snprintf(a_string,STRING,"%s%s",filename,filenames[0]);
	CReadPatPNG(state,&cells,1,1,SPECIALCELLS,a_string,0);
      }
      if(readpng==USESTATEPNG) {
	snprintf(a_string,STRING,"%s%s",filename,filenames[3]);
	ReadPatPNG(state,1,1,a_string,0);
	Type2Cell(state,state,&cells,SPECIALCELLS);
      }
    }
    else {
      snprintf(a_string,STRING,"%s%s",filename,filenames[7]);
      ReadPat(state,1,1,'i',a_string);
    }
    
    top=nrow+1;
    left=ncol+1;
    lower=0;
    right=0;
    PLANE(
	  if(state[i][j]) {
	    if(i<top)
	      top=i;
	    if(i>lower)
	      lower=i;
	    if(j<left)
	      left=j;
	    if(j>right)
	      right=j;
	  }
	  );
    PlaneFree(state);
    MemoryFree();
    if(trim) {
      nrow=lower-top+1+2*trimborder;
      if(tissuetype!=FRUIT)
	ncol=right-left+1+2*trimborder;
      top-=trimborder;
      if(tissuetype!=FRUIT)
	left-=trimborder;
    }
    else if(bending) {
      nrow=2*(lower-top+right-left);
      ncol=2*(lower-top+right-left);
    }
    if(nrow<=0 || ncol<=0) {
      fprintf(stdout,"impossible size for %s: (%d,%d)\n",tissuename[tissuetype],nrow,ncol);
      exit(EXIT_FAILURE);
    }
  }

  if(bending || nobending)
    originalstate=New();
  else if(sizereduction)
    originalstate=NewPlane(nrow/sizereduction,ncol/sizereduction);
  if(bending)
    newstate=New();
  else if(nobending) {
    snprintf(a_string,STRING,"%s%s",filename,".forwards");
    if(ReadSize(&oldnrow,&oldncol,a_string)) {
      fprintf(stdout,"the file %s is not a vadid state file\n exitting now!\n",a_string);
      exit(EXIT_FAILURE);
    }
    newstate=NewPlane(oldnrow,oldncol);
    oldstate=NewPlane(oldnrow,oldncol);
  }
  else if(sizereduction)
    newstate=New();
  
  state=New();
  cellstate=New();

  border=New();
  corner=New();
  region=New();
  white=New();
  angle=FNew();

  Fill(white,0);

  if(readpng==USECELLPNG || readpng==USESTATEPNG) {
    if(readpng==USECELLPNG) {
      snprintf(a_string,STRING,"%s%s",filename,filenames[0]);
      CReadPatPNG(state,&cells,2-top,2-left,SPECIALCELLS,a_string,0);
    }
    if(readpng==USESTATEPNG) {
      snprintf(a_string,STRING,"%s%s",filename,filenames[3]);
      ReadPatPNG(state,2-top,2-left,a_string,0);
      Type2Cell(state,state,&cells,SPECIALCELLS);
    }

    CELLS(cells,
	  if(cells.celltype[c]>=totaltypes) {
	    fprintf(stdout,"png picture \"%s\" contains unrecognized colours: (%d,%d,%d)\n",a_string,userCol[cells.celltype[c]][1],userCol[cells.celltype[c]][2],userCol[cells.celltype[c]][3]);
	    stoppen=1;
	  }
	  );
    if(stoppen)
      exit(EXIT_FAILURE);
  }

  else {
    snprintf(a_string,STRING,"%s%s",filename,filenames[7]);
    ReadPat(state,2-top,2-left,'i',a_string);
    snprintf(a_string,STRING,"%s%s",filename,filenames[4]);
    CReadPat(&cells,a_string);
    CELLS(cells,
	  if(cells.celltype[c]>=totaltypes) {
	    fprintf(stdout,"invalid celltype: %d\n",cells.celltype[c]);
	    exit(EXIT_FAILURE);
	  }
	  );
  }

  if(nobending) {
    snprintf(a_string,STRING,"%s%s",filename,".backwards");
    ReadPat(originalstate,2-top,2-left,'i',a_string);
    tmpnrow=nrow;
    tmpncol=ncol;
    nrow=oldnrow;
    ncol=oldncol;
    snprintf(a_string,STRING,"%s%s",filename,".forwards");
    ReadPat(newstate,1,1,'i',a_string);
    snprintf(a_string,STRING,"%s%s",filename,".oldstate");
    ReadPat(oldstate,1,1,'i',a_string);
    nrow=tmpnrow;
    ncol=tmpncol;
    FinishForwardBackwardPlanesPartOne(1-top,1-left);
  }

  UpdateCFill(state,&cells);//because small ones will be wrong
  InitCellPosition(&cells);//to initiate shape struct
  UpdateCellPosition(state,&cells);//because of mx. my
  CBoundaries(border,state);

  if(file_exists[5] || file_exists[1]) {
    if(filetype==1 || !file_exists[5]) { //we use _corners.png only when explicitly asked for, or when .corners does not exist
      tmpplane=NewPlane(2*nrow,2*ncol);
      snprintf(a_string,STRING,"%s%s",filename,filenames[1]);
      nrow*=2;ncol*=2;
      ReadPatPNG(tmpplane,2-top,2-left,a_string,120);
      nrow/=2;ncol/=2;
      PLANE(
	    ////note oldexcalib
	    corner[i][j]=tmpplane[2*i-1+oldexcalib][2*j-1];
	    );
      PlaneFree(tmpplane);
    }
    else {
      snprintf(a_string,STRING,"%s%s",filename,filenames[5]);
      ReadPat(corner,2-top,2-left,'i',a_string);
    }

    if(nobending) {
      FindCornersAfterBend();
      if(cellwall!=-1) {
	PLANE(
	      if(state[i][j]>specialcelltype && cells.celltype[state[i][j]]!=FAKECELLWALL) {
		if(!corner[i][j])
		  corner[i][j]=region[i][j];
	      }
	      );
	ReduceCellwall();
	FindCornersAfterBend();
      }
      FinishForwardBackwardPlanesPartTwo();
    }
    else {
      PLANE(
	    if(corner[i][j]<5 || corner[i][j]>8) //corner is not `real' cornerpoint
	      corner[i][j]=0; 
	    else if(state[i][j]>specialcelltype && cells.celltype[state[i][j]]!=FAKECELLWALL && EXTRAS[state[i][j]].x[corner[i][j]]<0 && border[i][j]) {
	      EXTRAS[state[i][j]].x[corner[i][j]]=j;
	      EXTRAS[state[i][j]].y[corner[i][j]]=i;
	    }
	    else
	      corner[i][j]=0;
	    );
      Fishy();
    }
  }

  if(bending || savecellrows)
    CellRows();
  if(bending)
    BendRoot();
  
  if(!nobending && cellwall!=-1)
    ReduceCellwall();
  Fishy();
 
  if((file_exists[5] || file_exists[1]) && !nobending) {
    CELLS(cells,
	  for(k=5;k<9;k++) {
	    if(EXTRAS[c].x[k]>-1 && (EXTRAS[c].x[((k-4)%4+5)]>-1))
	      NoQuarter(c,k,EXTRAS[c].x[k],EXTRAS[c].y[k],EXTRAS[c].x[((k-4)%4+5)],EXTRAS[c].y[((k-4)%4+5)]);
	    else if(cells.area[c])
	      printf("missing %d %d\n",c,k);
	  }
	  );
  }
  
  if(guess)
    EducatedGuess();

  if(sizereduction)
    SizeReduction();

  snprintf(a_string,STRING,"%s - %s",winsecond,winfirst);

  if(!interactive) {
    TheEnd();
    exit(0);
  }

  OpenDisplay(a_string,nrow,ncol);
  Show();
}

void RatinhoRelease(int mousestroke,int mouse_i,int mouse_j)
{
  int k;

  //printf("Release: %d at (%d,%d)\n",mousestroke,mouse_j,mouse_i);
  
  if(!cornerdetection) { //dont do anything if initially clicked inside cellwall or medium or with wrong button or outside window
    return;
  }
  cornerdetection=0;

  if(currentcell!=state[mouse_i][mouse_j] && x!=-1) {//we left the cell, this is the moment to do something & corner point has been found
    cornerk= 7-upper+outer-2*upper*outer; //determine corner direction
    lastk=cornerk;
    lastcell=currentcell;
    if(EXTRAS[currentcell].x[cornerk]>-1 && corner[EXTRAS[currentcell].y[cornerk]][EXTRAS[currentcell].x[cornerk]]==cornerk) { //this is not the first time we find this corner
      corner[EXTRAS[currentcell].y[cornerk]][EXTRAS[currentcell].x[cornerk]]=0; //remove old one 
      region[EXTRAS[currentcell].y[cornerk]][EXTRAS[currentcell].x[cornerk]]=0;
    }
    EXTRAS[currentcell].oldx[cornerk]=EXTRAS[currentcell].x[cornerk];
    EXTRAS[currentcell].oldy[cornerk]=EXTRAS[currentcell].y[cornerk];
    EXTRAS[currentcell].x[cornerk]=x;
    EXTRAS[currentcell].y[cornerk]=y;
    corner[y][x]=cornerk;
    for(k=5;k<9;k++)
      if(EXTRAS[currentcell].x[k]>-1 && (EXTRAS[currentcell].x[((k-4)%4+5)]>-1))
	NoQuarter(currentcell,k,EXTRAS[currentcell].x[k],EXTRAS[currentcell].y[k],EXTRAS[currentcell].x[((k-4)%4+5)],EXTRAS[currentcell].y[((k-4)%4+5)]);
  }
}

void RatinhoMotion(int mouse_i,int mouse_j)
{
  //printf("Motion: at (%d,%d)\n",mouse_j,mouse_i);
  //state[mouse_i][mouse_j]=0;

  if(!cornerdetection) //dont do anything if not in detection mode
    return;

  if(currentcell==state[mouse_i][mouse_j] && border[mouse_i][mouse_j]) {
    fprintf(stdout,"found border: (%d,%d) cell: %d corner: %s\n",mouse_j,mouse_i,currentcell,polar?cornernamepolar[direction]:cornername[direction]);
    x=mouse_j;
    y=mouse_i;
  }
}

void Ratinho(int mousestroke,int mouse_i,int mouse_j)
{
  currentcell=state[mouse_i][mouse_j];
  if(mousestroke==1) {//left mouse
    if(currentcell<=specialcelltype || cells.celltype[currentcell]==FAKECELLWALL )
      //dont do anything if clicked inside cellwall or medium
      return;
    x=-1;
    y=-1;
    cornerdetection=1;
    outer=0;
    if(upper)
      direction=UI;
    else
      direction=LI;
    fprintf(stdout,"indicate corner of cell: %d corner: %s\n",currentcell,polar?cornernamepolar[direction]:cornername[direction]);
    if(polar) {
    fprintf(stdout,"\t(press 'c' to toggle cw/ccw corners for corner selection)\n");
    fprintf(stdout,"\t(use right mouse for outwards corner selection)\n");
    }
    else {
    fprintf(stdout,"\t(press 'c' to toggle upper/lower corners for corner selection)\n");
    fprintf(stdout,"\t(use right mouse for outer corner selection)\n");
    }
  }
  else if(mousestroke==2) {//right mouse
    if(currentcell<=specialcelltype || cells.celltype[currentcell]==FAKECELLWALL )
      //dont do anything if clicked inside cellwall or medium
      return;
    x=-1;
    y=-1;
    cornerdetection=1;
    outer=1;
    if(upper)
      direction=UO;
    else
      direction=LO;
    fprintf(stdout,"indicate corner of cell: %d corner: %s\n",currentcell,polar?cornernamepolar[direction]:cornername[direction]);
    if(polar) {
    fprintf(stdout,"\t(press 'c' to toggle cw/ccw corners for corner selection)\n");
    fprintf(stdout,"\t(use left mouse for inwards corner selection)\n");
    }
    else {
    fprintf(stdout,"\t(press 'c' to toggle upper/lower corners for corner selection)\n");
    fprintf(stdout,"\t(use left mouse for inner corner selection)\n");
    }
  }
  else if (mousestroke==3) { //middle mouse
    tissuecentrey=(double)mouse_i;
    tissuecentrex=(double)mouse_j;
    EducatedGuess();
  }  
  else 
    return;
}

int main(int argc,char *argv[])
{
  mouse=&Ratinho;
  keyboard=&Key;
  mousemotion=&RatinhoMotion;
  mouserelease=&RatinhoRelease;
	
	
  options_read(argc,argv);
  SetUp(argc,argv);
	
	{
		int i;
		int j = ncol/2;
		fprintf(stdout,"j:%d\n",j);
		for (i=nrow;i>0;i--){
			//if(cells.celltype[state[i][j]]==VASCULAR_EZ)
					fprintf(stdout,"tissue type: %d\n", cells.celltype[state[i][j]]);
				//fprintf(stdout, "ciao\n");	

			//	fprintf(stdout,"state: %d\n", state[i][j]);	
				
				}
	}
	
	
	
	{
		int i;
		int j;
		int min_i=1;
		int min_j=1;
	
		for (j=1;j<=ncol;j++) 
			for (i=nrow;i>0;i--){
				if (cells.celltype[state[i][j]]==VASCULAR && i>=min_i && j>=min_j){
								min_i=i;
				                min_j=j;
					
				}
				
			}
		
		fprintf(stdout,"right V:%d upper V:%d\n", min_j, nrow - min_i);
	}
	
	
	
	{	
	int i;
	int j;
	int max_i=3000;
	int max_j=1000;

	for (j=1;j<=ncol;j++) 
		for (i=nrow;i>0;i--){
			
			if (cells.celltype[state[i][j]]==VASCULAR && i<=max_i && j<=max_j){
				
				max_i=i;
				max_j=j;
				
				
			}
			
		}
		fprintf(stdout,"left V:%d lower V:%d\n", max_j,nrow - max_i);
}
	
	{
		int i;
		int j;
		int min_i=1;
		int min_j=1;
		
		for (j=1;j<=ncol;j++) 
			for (i=nrow;i>0;i--){
				if (cells.celltype[state[i][j]]==EPIDERMIS && i>=min_i && j>=min_j){
					min_i=i;
					min_j=j;
					
				}
				
			}
		
		fprintf(stdout,"right Epi_D:%d upper Epi_D:%d\n", min_j, nrow - min_i);
	}
	
	
	
	{	
		int i;
		int j;
		int max_i=3000;
		int max_j=1000;
		
		for (j=1;j<=ncol;j++) 
			for (i=nrow;i>0;i--){
				
				if (cells.celltype[state[i][j]]==EPIDERMIS && i<=max_i && j<=max_j){
					
					max_i=i;
					max_j=j;
					
					
				}
				
			}
		fprintf(stdout,"left Epi_D:%d lower Epi_D:%d\n", max_j,nrow - max_i);
	}
	
	
	
	
	
  do {
    Show();
    usleep(1);
  } while(!CheckDisplay());
  return 0;
}

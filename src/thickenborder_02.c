#include <excal2.h>

#define CELLWALL 1

static TYPE **state; 
static TYPE **border; 
static TYPE **oldpattern;
static TYPE **newpattern;
static TYPE **additionalpattern;
static int additionalcell;

static Cell cells,fakecells;
static Png pictures;
static pid_t pid;

extern int nrow,ncol,scale,boundary;
extern unsigned char **userCol;
extern int lastcolor;
extern void (*mouse)(int,int,int);
extern int (*newindex)(int,int);

void Mouse(int mousestroke,int mouse_i,int mouse_j)
{
  printf("This is cell %d at (%d,%d) (%d,%d)\n",state[NewIndexWrap(mouse_i,nrow)][NewIndexWrap(mouse_j,ncol)],NewIndexWrap(mouse_j,ncol),NewIndexWrap(mouse_i,nrow),ncol,nrow);
}

void MyCBoundaries(TYPE **border,TYPE **state)
{
  Boundaries(state);
  Fill(border,0);
  NeighbourNumber(1);
  WIDENEIGHBOURS(
		 PLANE(
		       if(state[i][j])
			 border[i][j]+=(state[i][j]!=state[i+y][j+x]);
		       );
	     );
}

int main(int argc,char *argv[])
{
  char dirname[STRING],a_string[STRING];
  int k;
  //int m;

  scale=2;
  boundary=FIXED;
  mouse=&Mouse;
  if(argc<5 || argc==6) {
    fprintf(stdout,"usage: %s statepicture patternpicture boundarywidth outputfilename [additional_picture] [cell_for_which to_use_additional_picture]\n",argv[0]);
    exit(1);
  }

  k=atoi(argv[3]);

  ColorTable(0,0,WHITE); //this should be the colour of the medium
  ColorTable(CELLWALL,CELLWALL,BLACK); //this is the colour of the cell wall
  ReadSizePNG(&nrow,&ncol,argv[1]);
  nrow/=2;
  ncol/=2;
  state=New();
  border=New();
  oldpattern=New();
  newpattern=New();
  if(argc==7)
    additionalpattern=New();

  CNew(&cells,100000,1000);
  CNew(&fakecells,100000,1000);
  CReadPatPNG(state,&cells,1,1,1,argv[1],0); //destrow,destcol,destcell
  //because cell wall is black, CReadPatPNG thinks each pixel is a different cell
  PLANE(
	if(cells.celltype[state[i][j]]==CELLWALL)
	  state[i][j]=CELLWALL;
	);

  CReadPatPNG(oldpattern,&fakecells,1,1,1,argv[2],0); //destrow,destcol,destcell
  /*
  for(m=0;m<=lastcolor;m++)
    printf("%d: (%d,%d,%d,%d)\n",m,userCol[m][0],userCol[m][1],userCol[m][2],userCol[m][3]);
  */

  PLANE(
	oldpattern[i][j]=fakecells.celltype[oldpattern[i][j]];
	);

  if(argc==7) {
    CReadPatPNG(additionalpattern,&fakecells,1,1,1,argv[5],0);
    additionalcell=atoi(argv[6]);
    PLANE(
	  additionalpattern[i][j]=fakecells.celltype[additionalpattern[i][j]];
	  );
  }

  Copy(newpattern,oldpattern);
  MyCBoundaries(border,state);
  NeighbourNumber(k);
  
  CPLANE(state,
	 if(!newpattern[i][j]) {
	   WIDENEIGHBOURS(
			  if(border[i+y][j+x] && state[i][j]==state[i+y][j+x]) {
			    newpattern[i][j]=oldpattern[i+y][j+x];
			    break;
			  }
			  );
	 }
	 );

  if(argc==7) {
    CPLANE(state,
	   if(state[i][j]==additionalcell) {
	     newpattern[i][j]=additionalpattern[i][j];
	   }
	   );
  }

  OpenDisplay(argv[0],nrow,2*ncol);

  PLANE(
	if(state[i][j]==CELLWALL)
	  newpattern[i][j]=CELLWALL;
	);

  //CellDisplay(oldpattern,&fakecells,0,0,0);
  //CellDisplay(state,&cells,0,ncol,0);
  CPlaneDisplay(oldpattern,state,0,0,0);
  CPlaneDisplay(newpattern,state,0,ncol,0);

  pid = getpid();
  snprintf(dirname,STRING,"/tmp/thickenborder_%d",pid);
  OpenPNG(&pictures,dirname);
  CPlanePNG(newpattern,state,&pictures,0);
  //PlanePNG(newpattern,&pictures,0);
  snprintf(a_string,STRING,"mv %s/00000.png %s",dirname,argv[4]);
  system(a_string);
  snprintf(a_string,STRING,"rmdir %s",dirname);
  system(a_string);

  while(!CheckDisplay())
    usleep(10000);
  return 0;
}

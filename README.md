# TransitionZone

* An overview of where to find detailed documentation can be found [here](docs/README.md).
* An overview of all the c programmes used can be found [here](src/README.md).
* An overview of all the scripts to run the simulations and create the figures can be found [here](scripts/README.md).
* A description of the parameter files can be found [here](data/parameterfiles/README.md).
* A description of the experimental data analysis can be found [here](data/processed/README.md).
